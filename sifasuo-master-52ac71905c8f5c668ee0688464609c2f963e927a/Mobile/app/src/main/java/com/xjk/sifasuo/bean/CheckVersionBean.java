package com.xjk.sifasuo.bean;

/**
 * Created by zhouzhuo on 2017/7/14.
 */

public class CheckVersionBean extends BaseBean{
    public CheckVersionChildBean bean;
    public String Msg;
    public String code;

    public static class CheckVersionChildBean{
        public String id;
        public String type;
        public String url;
        public String remark;
        public String update_time;
        public String version;
    }


}
