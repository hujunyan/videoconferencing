package com.xjk.sifasuo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.activity.FileDetailActivity;
import com.xjk.sifasuo.activity.LoginActivity;
import com.xjk.sifasuo.adapter.FileTypeAdapter;
import com.xjk.sifasuo.bean.FileBean;
import com.xjk.sifasuo.model.BaseModel;
import com.xjk.sifasuo.model.FileListModel;
import com.xjk.sifasuo.utils.IntentUtils;
import com.xjk.sifasuo.utils.NewWorkUtils;
import com.xjk.sifasuo.utils.SharePreferenceUtils;
import com.xjk.sifasuo.utils.ToastUtils;
import com.xjk.sifasuo.utils.pulltorefresh.PullToRefreshBase;
import com.xjk.sifasuo.utils.pulltorefresh.PullToRefreshListView;
import com.xjk.sifasuo.view.SelectStatePop;
import com.xjk.sifasuo.view.SelectYearPop;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class MainFragment extends BaseFragment {
    private PullToRefreshListView mRefreshListView;
    private ListView mListView;

    private ImageView iv_type_arrow;
    private ImageView iv_type_time;
    private Button btn_confirm;
    private TextView tv_state,tv_time;
    private String year = Calendar.getInstance().get(Calendar.YEAR)+"";
    private int type ;

    private FileTypeAdapter adapter;
    private final String[] status = { "正在调解", "调解成功", "调解失败","全部"};
    private ArrayList<FileBean.FileItem> totalList ;
    //为0=有下一页，为1=最后一页
    private String isLastpage;
    private int currentPage = 1;


    @Override
    public View onCreateView(LayoutInflater inflater,ViewGroup container,Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_main, null);
        initView(view);
        requestData();
        return view;
    }



    private void initView(View view) {
        iv_type_arrow = (ImageView) view.findViewById(R.id.iv_type_arrow);
        iv_type_arrow.setOnClickListener(this);
        iv_type_time = (ImageView) view.findViewById(R.id.iv_type_time);
        iv_type_time.setOnClickListener(this);
        btn_confirm = (Button) view.findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(this);
        tv_time = (TextView) view.findViewById(R.id.tv_time);
        tv_time.setOnClickListener(this);
        tv_state = (TextView) view.findViewById(R.id.tv_state);
        tv_state.setOnClickListener(this);
        mRefreshListView = (PullToRefreshListView) view.findViewById(R.id.lv_list);
        mListView = mRefreshListView.getRefreshableView();
        mListView.setDivider(getResources().getDrawable(R.drawable.listview_dash));
        mListView.setDividerHeight(10);
        mListView.setVerticalScrollBarEnabled(false);
        mRefreshListView.setPullLoadEnabled(true);

        mRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                Log.d("zhouzhuo","下拉===");
                currentPage = 1;
                adapter = null;
                requestData();
                mRefreshListView.onPullDownRefreshComplete();

            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if(isLastpage.equals("0")){
                    currentPage++;
                    requestData();
                }else {
                    ToastUtils.showShort(getActivity(),"没有更多数据");
                }
                mRefreshListView.onPullUpRefreshComplete();
            }
        });
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.iv_type_arrow:
            case R.id.tv_state:
                final SelectStatePop selectStatePop = new SelectStatePop(getActivity(),type);
                selectStatePop.setListener(new SelectStatePop.SelectItemListener() {
                    @Override
                    public void select(int currentType) {
                        type = currentType;
                        if(selectStatePop !=null && selectStatePop.isShowing()){
                            selectStatePop.dismiss();
                        }
                        tv_state.setText(status[currentType]);
                    }
                });
                selectStatePop.showPopupWindow(iv_type_arrow);
                break;
            case R.id.iv_type_time:
            case R.id.tv_time:
                final SelectYearPop selectYearPop =new SelectYearPop(getActivity(),year);
                selectYearPop.setListener(new SelectYearPop.SelectItemListener() {
                    @Override
                    public void select(String currentYear) {
                        year = currentYear;
                        if(selectYearPop !=null && selectYearPop.isShowing()){
                            selectYearPop.dismiss();
                        }
                        tv_time.setText(year);
                    }
                });
                selectYearPop.showPopupWindow(iv_type_time);
                break;
            case R.id.btn_confirm:
                if(!NewWorkUtils.isConnected(getActivity())){
                    ToastUtils.showShort(getActivity(),"当前网络不好");
                    return;
                }
                //initFalseAdapter();
                adapter = null;
                currentPage = 1;
                requestData();
                //Intent intent = new Intent(getActivity(),FileDetailActivity.class);
                //IntentUtils.startFragmentToActivity(MainFragment.this,intent,false,0);
                break;
        }
    }



    private void requestData(){
        FileListModel model = new FileListModel();
        String number = (String) SharePreferenceUtils.get(getActivity(),"loginname","");
        Log.d("zhouzhuo","number:"+number);
        if(TextUtils.isEmpty(number)){
            Intent intent = new Intent(getActivity(),LoginActivity.class);
            IntentUtils.startFragmentToActivity(MainFragment.this,intent,false,0);
            return;
        }
        String idcard = (String) SharePreferenceUtils.get(getActivity(),"idcard","");
        model.getList(type,number,year,idcard,currentPage);
        model.setCallBackListener(new BaseModel.CallBackListener<FileBean>() {
            @Override
            public void success(FileBean message) {
                if(message!=null){
                    if(message.code == 1){
                        isLastpage = message.isLastpage;
                        currentPage = message.currentPage;
                        ArrayList<FileBean.FileItem>  list = message.list;
                        if(message.list!=null && message.list.size()>0){
                            if(adapter ==null){
                                totalList = list;
                                adapter =  new FileTypeAdapter(getActivity(),list);
                                adapter.setDetailsInterface(new FileTypeAdapter.DetailsInterface() {
                                    @Override
                                    public void details(String id,String status) {
                                        Intent intent = new Intent(getActivity(),FileDetailActivity.class);
                                        intent.putExtra("id",id);
                                        intent.putExtra("status",status);
                                        IntentUtils.startFragmentToActivity(MainFragment.this,intent,false,0);
                                    }
                                });
                                mListView.setAdapter(adapter);
                            }else {
                                totalList.addAll(list);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                }
            }
            @Override
            public void failed() {
                if(totalList!=null){
                    totalList.clear();
                    if(adapter == null){
                        adapter =  new FileTypeAdapter(getActivity(),totalList);
                        mListView.setAdapter(adapter);
                    }else {
                        adapter.notifyDataSetChanged();
                    }
                }
                Toast.makeText(getActivity(),"没有符合条件的结果",Toast.LENGTH_SHORT).show();
            }
        });

    }
}
