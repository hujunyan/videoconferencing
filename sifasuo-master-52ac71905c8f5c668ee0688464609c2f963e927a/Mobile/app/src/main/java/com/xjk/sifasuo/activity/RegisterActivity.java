package com.xjk.sifasuo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.model.BaseModel;
import com.xjk.sifasuo.model.RegisterModel;
import com.xjk.sifasuo.utils.IntentUtils;
import com.xjk.sifasuo.utils.StringUtils;
import com.xjk.sifasuo.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class RegisterActivity extends BaseActivity{
    private ImageView iv_back;
    private EditText et_name,et_identification,et_PhoneNumber,et_password,et_confirmPassWord;
    private TextView tv_confirm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initView();
    }

    private void initView() {

        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        et_name = (EditText) findViewById(R.id.et_name);
        et_identification = (EditText) findViewById(R.id.et_identification);
        et_PhoneNumber = (EditText) findViewById(R.id.et_PhoneNumber);
        et_password = (EditText) findViewById(R.id.et_password);
        et_confirmPassWord = (EditText) findViewById(R.id.et_confirm_password);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);
        tv_confirm.setOnClickListener(this);
        et_name.addTextChangedListener(new MyTextWatch());
        et_identification.addTextChangedListener(new MyTextWatch());
        et_PhoneNumber.addTextChangedListener(new MyTextWatch());
        et_password.addTextChangedListener(new MyTextWatch());
        et_confirmPassWord.addTextChangedListener(new MyTextWatch());
    }

    private class MyTextWatch implements TextWatcher{

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(!check()){
                tv_confirm.setBackgroundResource(R.drawable.btn_normal);
            }else {
                tv_confirm.setBackgroundResource(R.drawable.btn_login_finish);
            }

        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_confirm:
                if(checkMessage()){
                    RegisterModel model = new RegisterModel();
                    model.register(et_name.getText().toString(),et_identification.getText().toString().trim(),et_PhoneNumber.getText().toString(),et_password.getText().toString().trim());
                    model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                        @Override
                        public void success(String message) {
                            try {
                                JSONObject object = new JSONObject(message);
                                String data = object.getString("data");
                                int code = object.getInt("code");
                                Toast.makeText(RegisterActivity.this,data,Toast.LENGTH_SHORT).show();
                                if(code == 1){
                                    Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                                    IntentUtils.startActivity(RegisterActivity.this,intent);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        @Override
                        public void failed() {

                        }
                    });
                }
                break;
        }
    }

    private boolean check(){
        /*if(TextUtils.isEmpty(et_name.getText().toString().trim())){
            return  false;
        }
        if(!StringUtils.isIDCard(et_identification.getText().toString().trim())){
            return  false;
        }
        if(!StringUtils.isPhone(et_PhoneNumber.getText().toString().trim())){
            return  false;
        }
        if(TextUtils.isEmpty(et_password.getText().toString().trim())){
            return  false;
        }*/
        if(!et_password.getText().toString().trim().equals(et_confirmPassWord.getText().toString().trim())){
            return  false;
        }
        return true;
    }

    /***
     * check message
     * @return
     */
    private boolean checkMessage() {
        if(TextUtils.isEmpty(et_name.getText().toString().trim())){
            ToastUtils.showShort(RegisterActivity.this, "请输入您的姓名");
            return  false;
        }
        if(!StringUtils.isIDCard(et_identification.getText().toString().trim())){
            ToastUtils.showShort(RegisterActivity.this, "请输入正确的身份证号码");
            return  false;
        }
        if(!StringUtils.isPhone(et_PhoneNumber.getText().toString().trim())){
            ToastUtils.showShort(RegisterActivity.this, "请输入正确的手机号");
            return  false;
        }
        if(TextUtils.isEmpty(et_password.getText().toString().trim())){
            ToastUtils.showShort(RegisterActivity.this, "请输入密码");
            return  false;
        }
        if(!et_password.getText().toString().trim().equals(et_confirmPassWord.getText().toString().trim())){
            ToastUtils.showShort(RegisterActivity.this, "两次密码不一致");
            return  false;
        }
        return true;
    }
}
