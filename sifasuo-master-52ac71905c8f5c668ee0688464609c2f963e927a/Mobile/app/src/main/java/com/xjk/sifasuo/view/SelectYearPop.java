package com.xjk.sifasuo.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.adapter.SelectYearAdapter;
import com.xjk.sifasuo.utils.CommonUtils;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by zhouzhuo on 2017/7/11.
 */

public class SelectYearPop extends PopupWindow {
    //private TextView tv_state_doing,tv_state_success,tv_state_fail;
    private Activity activity;
    private ArrayList<String> list;


    public SelectYearPop(Activity activity, String selectPosition){
        this.activity = activity;
        list = new ArrayList<>();
        int year = Calendar.getInstance().get(Calendar.YEAR);
        for (int i=-10;i<=10;i++){
            list.add(year+i+"");
        }

        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.main_fragment_select_date, null);
        ListView lv_list = (ListView) view.findViewById(R.id.lv_list);
        this.setWidth(CommonUtils.dp2px(activity,123));
        // 设置弹出窗体的高
        this.setHeight(CommonUtils.dp2px(activity,130));
        this.setBackgroundDrawable(new BitmapDrawable());
        // 设置弹出窗体可点击
        //this.setTouchable(true);
        //this.setFocusable(true);
        // 设置点击是否消失
        this.setOutsideTouchable(true);
        lv_list.setAdapter(new SelectYearAdapter(activity,list,list.indexOf(selectPosition)));
        lv_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listener!=null){
                    listener.select(list.get(position));
                }
            }
        });

        this.setContentView(view);// 设置弹出窗体的宽

    }



    public void showPopupWindow(View parent) {
       if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
             // x y
            int[] location = new int[2];
            //获取在整个屏幕内的绝对坐标
            parent.getLocationOnScreen(location);
            this.showAtLocation(parent, 0, location[0]-CommonUtils.dp2px(activity,120), location[1] + parent.getHeight()+20);
        } else {
           this.dismiss();
        }
    }





    public SelectItemListener listener;
    public void setListener(SelectItemListener listener){
        this.listener = listener;
    }
    public interface  SelectItemListener {
        void select(String year);
    }

}
