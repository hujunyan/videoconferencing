package com.xjk.sifasuo.adapter;

import android.app.Activity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.bean.MessageBean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class MessageAdapter extends BaseAdapter {
    private ArrayList<MessageBean.MessageChildBean> list;
    private Activity activity;

    public MessageAdapter(Activity activity,ArrayList<MessageBean.MessageChildBean> list){
        this.activity = activity;
        this.list =list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder holder;
        if(convertView == null){
            holder = new MyViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.message_item,parent,false);
            holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            holder.tv_content = (TextView) convertView.findViewById(R.id.tv_content);
            convertView.setTag(holder);
        }else {
            holder = (MyViewHolder) convertView.getTag();
        }
        MessageBean.MessageChildBean bean = list.get(position);
        String content = bean.subject;
        String title = bean.message;
        if(!TextUtils.isEmpty(title)){
            holder.tv_title.setText(title);
        }

        if(!TextUtils.isEmpty(content)){
            holder.tv_content.setText(content);
        }
        return convertView;
    }

    class MyViewHolder{
        public ImageView iv_image;
        public TextView tv_content;
        public TextView tv_title;
    }
}
