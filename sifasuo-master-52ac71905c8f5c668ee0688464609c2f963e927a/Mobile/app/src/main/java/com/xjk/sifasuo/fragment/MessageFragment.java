package com.xjk.sifasuo.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.xjk.sifasuo.MainActivity;
import com.xjk.sifasuo.R;
import com.xjk.sifasuo.activity.LoginActivity;
import com.xjk.sifasuo.bean.CommonBean;
import com.xjk.sifasuo.bean.LoginBean;
import com.xjk.sifasuo.model.BaseModel;
import com.xjk.sifasuo.model.MessageResponseModel;
import com.xjk.sifasuo.utils.IntentUtils;
import com.xjk.sifasuo.utils.NewWorkUtils;
import com.xjk.sifasuo.utils.SharePreferenceUtils;
import com.xjk.sifasuo.utils.ToastUtils;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class MessageFragment extends BaseFragment {
    private TextView tv_confirm;
    private EditText et_content;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_message, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        tv_confirm = (TextView) view.findViewById(R.id.tv_confirm);
        tv_confirm.setOnClickListener(this);
        et_content = (EditText) view.findViewById(R.id.et_content);
        et_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
           public void afterTextChanged(Editable s) {
                if(TextUtils.isEmpty(et_content.getText().toString().trim())){
                    tv_confirm.setBackgroundResource(R.drawable.btn_normal);
                }else {
                    tv_confirm.setBackgroundResource(R.drawable.btn_login_finish);
                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.tv_confirm:
                if(!NewWorkUtils.isConnected(getActivity())){
                    ToastUtils.showShort(getActivity(),"当前网络不好");
                    return;
                }
                String content = et_content.getText().toString();
                if(TextUtils.isEmpty(content)&&content.length()>300){
                    ToastUtils.showShort(getActivity(),"长度过长,150字以内");
                }
                if(!TextUtils.isEmpty(content)){
                    MessageResponseModel messageResponseModel = new MessageResponseModel();
                    LoginBean.LoginContentBean bean = ((MainActivity)getActivity()).bean;
                    String number;
                    if(bean != null){
                        number = bean.loginname;
                    }else {
                        number = (String) SharePreferenceUtils.get(getActivity(),"loginname","");
                    }
                    Log.d("zhouzhuo","number==="+number);
                    if(TextUtils.isEmpty(number)){
                        Intent intent = new Intent(getActivity(), LoginActivity.class);
                        IntentUtils.startFragmentToActivity(MessageFragment.this,intent,false,0);
                        break;
                    }
                    messageResponseModel.setCallBackListener(new BaseModel.CallBackListener<CommonBean>() {
                        @Override
                        public void success(CommonBean message) {
                            if(message!=null){
                                Toast.makeText(getActivity(),message.data,Toast.LENGTH_SHORT).show();
                                if(message.code ==1){
                                    et_content.setText("");
                                    ToastUtils.showShort(getActivity(),message.data);
                                }
                            }
                        }
                        @Override
                        public void failed() {

                        }
                    });
                    messageResponseModel.response(number,content);
                }else {
                    Toast.makeText(getActivity(),"内容不能为空",Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }
}
