package com.xjk.sifasuo.activity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.bean.CommonBean;
import com.xjk.sifasuo.model.BaseModel;
import com.xjk.sifasuo.model.ResetPassWordModel;
import com.xjk.sifasuo.utils.NewWorkUtils;
import com.xjk.sifasuo.utils.SharePreferenceUtils;
import com.xjk.sifasuo.utils.ToastUtils;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class ResetPasswordActivity extends BaseActivity{
    private FrameLayout fl_back;
    private EditText et_password,et_confirmPassWord;
    private TextView tv_confirm;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initView();
    }

    private void initView() {
        fl_back = (FrameLayout) findViewById(R.id.fl_back);
        fl_back.setOnClickListener(this);
        et_password = (EditText) findViewById(R.id.et_password);
        et_confirmPassWord = (EditText) findViewById(R.id.et_confirm_password);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);
        tv_confirm.setOnClickListener(this);
        et_password.addTextChangedListener(new MyTextWatch());
        et_confirmPassWord.addTextChangedListener(new MyTextWatch());
    }

    private class MyTextWatch implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(!checkMessage()){
                tv_confirm.setBackgroundResource(R.drawable.btn_normal);
            }else {
                tv_confirm.setBackgroundResource(R.drawable.btn_login_finish);
            }

        }
    }



    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.fl_back:
                finish();
                break;
            case R.id.tv_confirm:
                if(!NewWorkUtils.isConnected(ResetPasswordActivity.this)){
                    ToastUtils.showShort(ResetPasswordActivity.this,"当前网络不好");
                    break;
                }
                if(check()){
                    String pwd = et_password.getText().toString().trim();
                    String newPwd = et_confirmPassWord.getText().toString().trim();
                    String id = (String) SharePreferenceUtils.get(ResetPasswordActivity.this,"id","");
                    ResetPassWordModel model = new ResetPassWordModel();
                    model.setCallBackListener(new BaseModel.CallBackListener<CommonBean>() {
                        @Override
                        public void success(CommonBean bean) {
                            ToastUtils.showShort(ResetPasswordActivity.this,bean.data);
                            if(bean.code == 1){
                                finish();
                            }
                        }

                        @Override
                        public void failed() {

                        }
                    });
                    model.restPassword(id,pwd,newPwd);

                }

                break;
        }
    }

    private boolean check(){
        if(TextUtils.isEmpty(et_password.getText().toString().trim())){
            ToastUtils.showShort(ResetPasswordActivity.this, "密码不能为空");
            return  false;
        }
        if(TextUtils.isEmpty(et_confirmPassWord.getText().toString().trim())){
            ToastUtils.showShort(ResetPasswordActivity.this, "密码不能为空");
            return  false;
        }
        if(et_password.getText().toString().trim().equals(et_confirmPassWord.getText().toString().trim())){
            ToastUtils.showShort(ResetPasswordActivity.this, "新旧密码不能一样");
            return  false;
        }
        return true;
    }

    /***
     * check message
     * @return
     */
    private boolean checkMessage() {
        if(TextUtils.isEmpty(et_password.getText().toString().trim())){
            return  false;
        }
        if(TextUtils.isEmpty(et_confirmPassWord.getText().toString().trim())){
            return  false;
        }
        return true;
    }
}
