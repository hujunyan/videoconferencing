package com.xjk.sifasuo.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.bean.FileDetailBean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/6/30.
 */

public class FileDetailAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<FileDetailBean> list;

    public FileDetailAdapter(Activity activity, ArrayList<FileDetailBean> list) {
        this.activity = activity;
        this.list = list;
    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public ArrayList<FileDetailBean> getList() {
        return list;
    }

    public void setList(ArrayList<FileDetailBean> list) {
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder holder;
        if(convertView == null){
            holder = new MyViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.file_detail_item,parent,false);
            convertView.setTag(holder);
        }else {
            holder = (MyViewHolder) convertView.getTag();
        }
        return convertView;
    }

    class MyViewHolder{
        TextView tv_name;
    }
}
