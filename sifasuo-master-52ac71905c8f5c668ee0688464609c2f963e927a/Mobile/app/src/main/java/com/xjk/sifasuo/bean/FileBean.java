package com.xjk.sifasuo.bean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class FileBean {
    public String Msg;
    public int  code;
    public ArrayList<FileItem> list;
    public String isLastpage;
    public int currentPage;
    public static class FileItem{
        public String id;
        public String filename;
        public String date;
        public String status;
    }

}
