package com.xjk.sifasuo.bean;

import java.io.Serializable;

/**
 * Created by zhouzhuo on 2017/7/14.
 */

public class LoginBean {
    public String Msg;
    public int code;
    public LoginContentBean bean;

    public static class LoginContentBean implements Serializable{
        public String id;
        public String name;
        public String idcard;
        public String loginname;
        public String password;
        public String photo;
        public String isdelete;
        public String errorMessage;
    }

}
