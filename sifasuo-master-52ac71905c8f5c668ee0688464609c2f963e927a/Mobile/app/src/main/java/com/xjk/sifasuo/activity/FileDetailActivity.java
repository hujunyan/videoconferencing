package com.xjk.sifasuo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.bean.FileDetailBean;
import com.xjk.sifasuo.model.BaseModel;
import com.xjk.sifasuo.model.FileCaseModel;
import com.xjk.sifasuo.utils.IntentUtils;
import com.xjk.sifasuo.utils.SharePreferenceUtils;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class FileDetailActivity extends BaseActivity{
    private ImageView iv_back;
    private TextView tv_title;
    private String currentId;
    private TextView tv_detail;
    private ImageView iv_file_state;
    private TextView tv_state;
    private String url;
    private String title;
    private TextView tv_userName;
    private RelativeLayout rl_detail;
    private String id;
    private String casetype;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_detail);
        initView();
    }
    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_detail = (TextView) findViewById(R.id.tv_detail);
        tv_detail.setOnClickListener(this);
        iv_file_state = (ImageView) findViewById(R.id.iv_file_state);
        tv_state = (TextView) findViewById(R.id.tv_state);
        rl_detail = (RelativeLayout) findViewById(R.id.rl_detail);
        tv_userName = (TextView) findViewById(R.id.tv_userName);


        id = getIntent().getStringExtra("id");
        String status = getIntent().getStringExtra("status");
        String userName = (String) SharePreferenceUtils.get(this,"name","");
        if(!TextUtils.isEmpty(userName)){
            tv_userName.setText(userName);
        }
        Log.d("zhouzhuo","status:"+status);
        switch (status){
            case "0":
                tv_state.setText("正在调解中");
                iv_file_state.setImageResource(R.drawable.file_state_success);
                break;
            case "1":
                tv_state.setText("调解成功");
                iv_file_state.setImageResource(R.drawable.file_state_success);
                break;
            case "2":
                tv_state.setText("调解未成功");
                iv_file_state.setImageResource(R.drawable.file_state_failed);
                break;
            default:
                break;


        }
        initData(id);
    }



    private void initData(final String id) {
        FileCaseModel model = new FileCaseModel();
        model.getList(id);
        model.setCallBackListener(new BaseModel.CallBackListener<FileDetailBean.ChildBean>() {
            @Override
            public void success(FileDetailBean.ChildBean message) {
                if(message!=null){
                    rl_detail.setVisibility(View.VISIBLE);
                    url = message.image_url;
                    title = message.filename;
                    Log.i("zhouzhuo","rrrrr"+title);
                    casetype = message.casetype;
                    if(!TextUtils.isEmpty(title)){
                        tv_title.setText(title);
                    }
                }else {

                    rl_detail.setVisibility(View.GONE);
                }
            }
            @Override
            public void failed() {

            }
        });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
            case R.id.tv_detail:
                Intent intent = new Intent(FileDetailActivity.this,MoreDetailActivity.class);
                intent.putExtra("title",title);
                intent.putExtra("url",url);
                intent.putExtra("id",id);
                intent.putExtra("casetype",casetype);
                IntentUtils.startActivity(FileDetailActivity.this,intent);
                break;
        }
    }
}
