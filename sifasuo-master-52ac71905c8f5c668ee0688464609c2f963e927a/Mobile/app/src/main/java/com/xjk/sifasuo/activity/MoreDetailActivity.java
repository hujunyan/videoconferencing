package com.xjk.sifasuo.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.xjk.sifasuo.R;

import uk.co.senab.photoview.PhotoView;

/**
 * Created by zhouzhuo on 2017/7/11.
 */

public class MoreDetailActivity extends BaseActivity {
    private PhotoView photoView;
    private ImageView iv_back;
    private TextView tv_title;
    private WebView web_view;
    private String caseId;
    public String casetype;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_more_detail_activity);
        initView();
        initData();
    }



    private void initData() {
        String title = getIntent().getStringExtra("title");
        if(!TextUtils.isEmpty(title)){
            tv_title.setText(title);
        }

        WebSettings settings = web_view.getSettings();
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setJavaScriptEnabled(true);// 开启js功能, 同时, 百度网页不再跳转到浏览器
        settings.setDefaultTextEncodingName("UTF-8") ;
        String url;
       // intent.putExtra("casetype",casetype);
        casetype = getIntent().getStringExtra("casetype");
        Log.d("zhouzhuo","caseType=="+casetype);

        if(!TextUtils.isEmpty(casetype)&&casetype.equals("0")){
            url ="http://211.154.167.34/xjk/index.php/Admin/Auditing/oralcase/case_id/"+caseId+".html";
        }else {
            url = "http://211.154.167.34/xjk/index.php/Admin/Auditing/casePart4/case_id/"+caseId+".html";
        }
        Log.d("zhouzhuo","url=="+url);
        web_view.loadUrl(url);

       /* String url = getIntent().getStringExtra("url");
        if(!TextUtils.isEmpty(url)){
            ImageLoaderModel.instance().displayImage(url,photoView);
        }*/
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);
        web_view = (WebView) findViewById(R.id.web_view);
        tv_title = (TextView) findViewById(R.id.tv_title);
        casetype = getIntent().getStringExtra("casetype");
        caseId = getIntent().getStringExtra("id");
        //photoView = (PhotoView) findViewById(R.id.test_iv);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.iv_back:
                finish();
                break;
        }
    }
}
