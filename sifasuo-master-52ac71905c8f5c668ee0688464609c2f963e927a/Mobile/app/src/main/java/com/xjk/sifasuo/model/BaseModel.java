package com.xjk.sifasuo.model;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public  class BaseModel  {
    public interface  CallBackListener<T>{
        void success(T message);
        void failed();
    }
    public CallBackListener callBackListener;
    public void setCallBackListener(CallBackListener callBackListener){
        this.callBackListener = callBackListener;
    }
}
