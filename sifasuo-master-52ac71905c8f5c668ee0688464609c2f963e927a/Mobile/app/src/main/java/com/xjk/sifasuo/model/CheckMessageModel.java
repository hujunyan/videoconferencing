package com.xjk.sifasuo.model;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xjk.sifasuo.MyApplication;
import com.xjk.sifasuo.bean.CommonBean;
import com.xjk.sifasuo.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/9/29.
 */

public class CheckMessageModel extends BaseModel{
    public void check(String id){
        String url2 = UrlDomainUtil.urlHeader+"/User/isHaveMessage/id/"+id;
        Log.d("zhouzhuo","url=="+url2);
        StringRequest stringRequest = new StringRequest(url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response!=null){
                            CommonBean bean = new Gson().fromJson(response,CommonBean.class);
                            callBackListener.success(bean);
                        }
                        Log.d("zhouzhuo","是否有消息==="+response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        MyApplication.getHttpQueue().add(stringRequest);
    }
}
