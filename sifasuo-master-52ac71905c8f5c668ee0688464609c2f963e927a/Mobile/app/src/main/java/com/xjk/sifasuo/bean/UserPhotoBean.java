package com.xjk.sifasuo.bean;

/**
 * Created by zhouzhuo on 2017/7/14.
 */

public class UserPhotoBean {
    public String Msg;
    public int code;
    public UserPhotoInnerBean bean;
    public static class UserPhotoInnerBean{
        public String photo;
        public String last_login_time;
    }
}
