package com.xjk.sifasuo.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xjk.sifasuo.R;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class SelectYearAdapter extends BaseAdapter {
    private Activity activity;
    private ArrayList<String> list;
    private int selectPosition;
    public SelectYearAdapter(Activity activity,ArrayList<String> list,int selectPosition){
        this.activity = activity;
        this.list = list;
        this.selectPosition = selectPosition;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder holder;
        if(convertView == null){
            holder = new MyViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.year_item,parent,false);
            holder.tv_year = (TextView) convertView.findViewById(R.id.tv_year);
            convertView.setTag(holder);
        }else {
            holder = (MyViewHolder) convertView.getTag();
        }
        holder.tv_year.setText(list.get(position));
        if(position == selectPosition){
            holder.tv_year.setTextColor(Color.parseColor("#4e9fef"));
        }else {
            holder.tv_year.setTextColor(Color.parseColor("#aaaaaa"));
        }
        return convertView;
    }
    class MyViewHolder{
        TextView tv_year;
    }
}
