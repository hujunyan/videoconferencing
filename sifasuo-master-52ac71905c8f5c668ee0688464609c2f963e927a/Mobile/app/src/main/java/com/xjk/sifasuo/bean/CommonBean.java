package com.xjk.sifasuo.bean;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class CommonBean{
    @Override
    public String toString() {
        return "RegisterBean{" +
                "code='" + code + '\'' +
                ", data='" + data + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }

    public int code;
    public String data;
    public String msg;
}
