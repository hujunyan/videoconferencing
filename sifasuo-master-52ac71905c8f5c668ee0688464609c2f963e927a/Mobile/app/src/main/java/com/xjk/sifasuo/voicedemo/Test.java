package com.xjk.sifasuo.voicedemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.baidu.voicerecognition.android.ui.BaiduASRDigitalDialog;
import com.baidu.voicerecognition.android.ui.DialogRecognitionListener;
import com.xjk.sifasuo.R;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/3.
 */

public class Test extends Activity {
    /**
     * <!-- 请填写真实的APP_ID API_KEY SECRET_KEY -->
     <meta-data android:name="com.baidu.speech.APP_ID" android:value="9835518"/>
     <meta-data android:name="com.baidu.speech.API_KEY" android:value="KyHQRI3hTesCilGySgGB53zq"/>
     <meta-data android:name="com.baidu.speech.SECRET_KEY" android:value="2a7f92635efbc77794703996d22d1e6f"/>

     */
    private Button BtnStart;
    private EditText InputBox;
    private BaiduASRDigitalDialog mDialog=null;
    private DialogRecognitionListener mDialogListener=null;
    private String API_KEY="KyHQRI3hTesCilGySgGB53zq";
    private String SECRET_KEY="2a7f92635efbc77794703996d22d1e6f";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        if (mDialog == null) {
            if (mDialog != null) {
                mDialog.dismiss();
            }
            Bundle params = new Bundle();
            params.putString(BaiduASRDigitalDialog.PARAM_API_KEY, API_KEY);
            params.putString(BaiduASRDigitalDialog.PARAM_SECRET_KEY, SECRET_KEY);
            params.putInt(BaiduASRDigitalDialog.PARAM_DIALOG_THEME, BaiduASRDigitalDialog.THEME_BLUE_LIGHTBG);
            mDialog = new BaiduASRDigitalDialog(this, params);
            mDialogListener=new DialogRecognitionListener()
            {

                @Override
                public void onResults(Bundle mResults)
                {
                    ArrayList<String> rs = mResults != null ? mResults.getStringArrayList(RESULTS_RECOGNITION) : null;
                    if (rs != null && rs.size() > 0) {
                        InputBox.setText(rs.get(0));
                    }

                }

                
            };
            mDialog.setDialogRecognitionListener(mDialogListener);
        }
        mDialog.setSpeechMode(BaiduASRDigitalDialog.SPEECH_MODE_INPUT);
        mDialog.getParams().putBoolean(BaiduASRDigitalDialog.PARAM_NLU_ENABLE, false);

        BtnStart=(Button)findViewById(R.id.BtnStart);
        InputBox=(EditText)findViewById(R.id.InputBox);
        BtnStart.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                mDialog.show();
            }
        });
    }

}
