package com.xjk.sifasuo.utils;

import com.google.gson.Gson;

/**
 * Created by JimGong on 2016/6/23.
 */
public class JsonGenericsSerializator  {
    Gson mGson = new Gson();

    public <T> T transform(String response, Class<T> classOfT) {
        return mGson.fromJson(response, classOfT);
    }
}
