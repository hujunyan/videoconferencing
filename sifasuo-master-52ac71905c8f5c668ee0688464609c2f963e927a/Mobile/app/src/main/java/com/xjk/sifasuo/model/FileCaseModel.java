package com.xjk.sifasuo.model;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xjk.sifasuo.MyApplication;
import com.xjk.sifasuo.bean.FileDetailBean;
import com.xjk.sifasuo.utils.UrlDomainUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/7/17.
 */

public class FileCaseModel extends BaseModel{
    public void getList(String id) {
        String url2 = UrlDomainUtil.urlHeader + "/User/case_agreement_list/case_id/"+id;
        Log.d("zhouzhuo", "url===" + url2);
        StringRequest stringRequest = new StringRequest(url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.getInt("code");
                            if(code == 1){
                                JSONObject object1 = object.getJSONObject("data");
                                FileDetailBean.ChildBean bean1 = new FileDetailBean.ChildBean();
                                bean1.filename = object1.getString("filename");
                                bean1.casetype = object1.getString("casetype");
                                try {
                                    bean1.case_id = object1.getString("case_id");
                                    bean1.id = object1.getString("id");
                                    bean1.issue = object1.getString("issue");
                                    bean1.image_url = object1.getString("image_url");
                                }catch (Exception e){
                                    e.toString();
                                }

                                callBackListener.success(bean1);
                            }else if(code == 4005){
                                callBackListener.success(null);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.d("zhouzhuo", "file case ===" + response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApplication.getHttpQueue().add(stringRequest);
    }
}
