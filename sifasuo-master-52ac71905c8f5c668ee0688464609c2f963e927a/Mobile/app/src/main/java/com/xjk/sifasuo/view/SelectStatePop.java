package com.xjk.sifasuo.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.utils.CommonUtils;

/**
 * Created by zhouzhuo on 2017/7/11.
 */

public class SelectStatePop extends PopupWindow implements View.OnClickListener{
    //private TextView tv_state_doing,tv_state_success,tv_state_fail;
    private Activity activity;


    public SelectStatePop(Activity activity,int selectPosition){
        this.activity = activity;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.main_fragment_select_state_pop, null);

        this.setWidth(CommonUtils.dp2px(activity,123));
        // 设置弹出窗体的高
        this.setHeight(CommonUtils.dp2px(activity,180));
        this.setBackgroundDrawable(new BitmapDrawable());
        // 设置弹出窗体可点击
        //this.setTouchable(true);
        //this.setFocusable(true);
        // 设置点击是否消失
        this.setOutsideTouchable(true);
        TextView tv_state_doing = (TextView) view.findViewById(R.id.tv_state_doing);
        tv_state_doing.setOnClickListener(this);
        TextView tv_state_success = (TextView) view.findViewById(R.id.tv_state_success);
        tv_state_success.setOnClickListener(this);
        TextView tv_state_fail = (TextView) view.findViewById(R.id.tv_state_fail);
        tv_state_fail.setOnClickListener(this);
        TextView tv_state_all = (TextView) view.findViewById(R.id.tv_state_all);
        tv_state_all.setOnClickListener(this);
        this.setContentView(view);// 设置弹出窗体的宽
        initTextColor(tv_state_doing,tv_state_success,tv_state_fail,tv_state_all,selectPosition);

    }

    private void initTextColor(TextView tv_state_doing, TextView tv_state_success,
                               TextView tv_state_fail,TextView tv_state_all, int selectPosition) {
        switch (selectPosition){
            case 0:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv_state_doing.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));
                    tv_state_success.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_state_fail.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_state_all.setTextColor(activity.getResources().getColor(R.color.popText,null));
                }else {
                    tv_state_doing.setTextColor(activity.getResources().getColor(R.color.colorCommon));
                    tv_state_success.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_state_fail.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_state_all.setTextColor(activity.getResources().getColor(R.color.popText));

                }
                break;
            case 1:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv_state_doing.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_state_success.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));
                    tv_state_fail.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_state_all.setTextColor(activity.getResources().getColor(R.color.popText,null));

                }else {
                    tv_state_doing.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_state_success.setTextColor(activity.getResources().getColor(R.color.colorCommon));
                    tv_state_fail.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_state_all.setTextColor(activity.getResources().getColor(R.color.popText));
                }
                break;
            case 2:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv_state_doing.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_state_success.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_state_fail.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));
                    tv_state_all.setTextColor(activity.getResources().getColor(R.color.popText,null));

                }else {
                    tv_state_doing.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_state_success.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_state_fail.setTextColor(activity.getResources().getColor(R.color.colorCommon));
                    tv_state_all.setTextColor(activity.getResources().getColor(R.color.popText));
                }
                break;
            case 3:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv_state_doing.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_state_success.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_state_fail.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_state_all.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));

                }else {
                    tv_state_doing.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_state_success.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_state_fail.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_state_all.setTextColor(activity.getResources().getColor(R.color.colorCommon));
                }
        }
    }

    public void showPopupWindow(View parent) {
       if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
             // x y
            int[] location = new int[2];
            //获取在整个屏幕内的绝对坐标
            parent.getLocationOnScreen(location);
            this.showAtLocation(parent, 0, location[0]-CommonUtils.dp2px(activity,120), location[1] + parent.getHeight()+20);
        } else {
           this.dismiss();
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.tv_state_doing:
                if(listener!= null){
                    listener.select(0);
                }
                dismiss();
                break;
            case R.id.tv_state_success:
                if(listener!= null){
                    listener.select(1);
                }
                dismiss();
                break;
            case R.id.tv_state_fail:
                if(listener!= null){
                    listener.select(2);
                }
                dismiss();
                break;
            case R.id.tv_state_all:
                if(listener!= null){
                    listener.select(3);
                }
                dismiss();
                break;


        }

    }



    public SelectItemListener listener;
    public void setListener(SelectItemListener listener){
        this.listener = listener;
    }
    public interface  SelectItemListener {
        void select(int type);
    }

}
