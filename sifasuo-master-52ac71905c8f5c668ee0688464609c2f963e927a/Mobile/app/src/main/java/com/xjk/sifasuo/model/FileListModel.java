package com.xjk.sifasuo.model;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xjk.sifasuo.MyApplication;
import com.xjk.sifasuo.bean.CommonBean;
import com.xjk.sifasuo.bean.FileBean;
import com.xjk.sifasuo.utils.UrlDomainUtil;
import com.zhy.http.okhttp.callback.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class FileListModel extends BaseModel{
    public void getList(int type, String phone, String year,String idcard,int page){
        String url2 = UrlDomainUtil.urlHeader+"/User/case_list/tel/"+
                phone+"/page/"+page+"/year/"+year+"/idcard/"+idcard;
        if(type!=3){
            url2+="/type/"+type;
        }
        Log.d("zhouzhuo","url==="+url2);
        StringRequest stringRequest = new StringRequest(url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("zhouzhuo","file list==="+response);
                        FileBean beanTwo = new FileBean();

                        try {
                            JSONObject object = new JSONObject(response);
                            beanTwo.code = object.getInt("code");
                            if(beanTwo.code ==1){
                                beanTwo.Msg = object.getString("Msg");
                                JSONObject object1 = object.getJSONObject("data");
                                JSONArray object2 = object1.getJSONArray("content");
                                beanTwo.isLastpage = object1.getString("isLastpage");
                                beanTwo.currentPage = object1.getInt("currentPage");
                                ArrayList<FileBean.FileItem> list = new ArrayList<>();
                                for (int i =0;i<object2.length();i++){
                                    Gson gson = new Gson();
                                    FileBean.FileItem jsonBean = gson.fromJson(object2.get(i).toString(),FileBean.FileItem.class);
                                    list.add(jsonBean);
                                }
                                beanTwo.list = list;
                                callBackListener.success(beanTwo);
                            }else {
                                callBackListener.failed();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //callBackListener.success(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApplication.getHttpQueue().add(stringRequest);
        //String url2 = UrlDomainUtil.urlHeader+"/User/search_case/type/1/date/2011/tel/13240449760";
        /*OkHttpUtils
                .get()
                .url(url)
                .addParams("tel", "13240449760")
                .addParams("type", "1")
                .addParams("date", "2011")
                .build()
                .execute(new Callback() {
                    @Override
                    public Object parseNetworkResponse(Response response, int id) throws Exception {
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("zhouzhuo","==请求失败="+e.getMessage());
                    }

                    @Override
                    public void onResponse(Object response, int id) {

                        Log.d("zhouzhuo","==请求成功="+response);
                    }
                });*/

/*
        HashMap<String,String> map = new HashMap<>();
        map.put("tel", "13240449760");
        map.put("type", "1");
        map.put("date", "2011");
        OkHttpUtils//
                .post()//
                .url(url)//
                .params(map)
                .build()//
                .execute(new Callback() {
                    @Override
                    public Object parseNetworkResponse(Response response, int id) throws Exception {
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("zhouzhuo","获取列表失败:"+e.getMessage());
                    }

                    @Override
                    public void onResponse(Object response, int id) {
                        Log.d("zhouzhuo","获取列表成功:"+response +"=====");
                    }
                });*/

    }

    private abstract class  RegisterCallBack extends Callback<CommonBean> {



    }

}
