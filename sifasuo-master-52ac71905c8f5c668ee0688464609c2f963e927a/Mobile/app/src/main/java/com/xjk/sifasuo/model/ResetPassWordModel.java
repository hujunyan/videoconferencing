package com.xjk.sifasuo.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xjk.sifasuo.MyApplication;
import com.xjk.sifasuo.bean.CommonBean;
import com.xjk.sifasuo.utils.UrlDomainUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class ResetPassWordModel extends BaseModel{
    public void restPassword(String id,String pwd, String newPwd){
        String url = UrlDomainUtil.urlHeader+"/User/resetPwd";
        final HashMap<String,String> map = new HashMap<>();
        map.put("id",id);
        map.put("pwd",pwd);
        map.put("newPwd",newPwd);
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                Log.d("zhouzhuo","重置密码==="+s);
                if(callBackListener!=null){
                    if(!TextUtils.isEmpty(s)){
                        CommonBean commonBean = new Gson().fromJson(s,CommonBean.class);
                        callBackListener.success(commonBean);
                    }

                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {

            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        MyApplication.getHttpQueue().add(request);

        /*OkHttpUtils
                .get()
                .url(url)
                .addParams("name",name)
                .addParams("IDcard",IDcard)
                .addParams("loginName",phone)
                .addParams("newPassword",password)
                .build()
                .execute(new RegisterCallBack() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        // callBackListener.success();
                        Log.d("zhouzhuo","请求失败");
                    }

                    @Override
                    public void onResponse(RegisterBean response, int id) {
                        callBackListener.success(response);
                        Log.d("zhouzhuo","请求成功:"+response.toString());
                    }
                });*/
       /* HashMap<String,String> map = new HashMap<>();
        map.put("tel",phone);
        map.put("pwd",password);
        OkHttpUtils//
                .post()//
                .url(url)//
                .params(map)
                .build()//
                .execute(new Callback() {
                    @Override
                    public Object parseNetworkResponse(Response response, int id) throws Exception {
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("zhouzhuo","登录失败:"+e.getMessage());
                    }

                    @Override
                    public void onResponse(Object response, int id) {
                        Log.d("zhouzhuo","登录成功:"+response +"=====");
                    }
                });*/
    }

   /* private abstract class  RegisterCallBack extends Callback<RegisterBean> {
        @Override
        public RegisterBean parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            RegisterBean registerBean = new Gson().fromJson(string,RegisterBean.class);
            return registerBean;
        }


    }*/

}
