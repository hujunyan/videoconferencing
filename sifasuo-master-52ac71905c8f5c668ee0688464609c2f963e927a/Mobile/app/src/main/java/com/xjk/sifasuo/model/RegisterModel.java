package com.xjk.sifasuo.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xjk.sifasuo.MyApplication;
import com.xjk.sifasuo.utils.UrlDomainUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class RegisterModel extends BaseModel{
    public void register(String name, String idCard, String loginName, String password){
        String url = UrlDomainUtil.urlHeader+"/User/register";
        Log.d("zhouzhuo","url:"+url);
        final HashMap<String,String> map = new HashMap<>();
        map.put("name",name);
        map.put("IDcard",idCard);
        map.put("loginName",loginName);
        map.put("password",password);
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if(callBackListener!=null){
                    Log.d("zhouzhuo","注册成功:"+s);
                    if(!TextUtils.isEmpty(s)){
                         callBackListener.success(s);
                        }//
                    }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("zhouzhuo","volleyError:"+volleyError.toString());
                Log.d("zhouzhuo","volleyError:"+volleyError.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        MyApplication.getHttpQueue().add(request);

        /*OkHttpUtils
                .get()
                .url(url)
                .addParams("name", name)
                .addParams("IDcard", idCard)
                .addParams("loginName", loginName)
                .addParams("password", password)
                .build()
                .execute(new RegisterCallBack() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                       // callBackListener.success();
                        Log.d("zhouzhuo","请求失败");
                    }

                    @Override
                    public void onResponse(RegisterBean response, int id) {
                        callBackListener.success(response);
                        Log.d("zhouzhuo","请求成功:"+response.toString());
                    }
                });*/
        /**
         *  map.put("IDcard","130627198809120655");
         // map.put("loginName","13240449760");
         // map.put("password","123456");
         */


        /*String url = UrlDomainUtil.urlHeader+"/User/register";
        HashMap<String,String> map = new HashMap<>();
        map.put("name","张玉龙");
        map.put("IDcard","130627198809120655");
        map.put("loginName","13716115730");
        map.put("password","123456");
        OkHttpUtils//
                .post()//
                .url(url)//
                .params(map)
                .build()//
                .execute(new Callback() {
                    @Override
                    public Object parseNetworkResponse(Response response, int id) throws Exception {
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("zhouzhuo","注册失败:"+e.getMessage());
                    }

                    @Override
                    public void onResponse(Object response, int id) {
                        Log.d("zhouzhuo","注册成功:"+response +"=====");
                    }
                });*/


    }


}
