package com.xjk.sifasuo.activity;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.xjk.sifasuo.MainActivity;
import com.xjk.sifasuo.R;
import com.xjk.sifasuo.model.BaseModel;
import com.xjk.sifasuo.model.WelcomeModel;
import com.xjk.sifasuo.utils.NewWorkUtils;
import com.xjk.sifasuo.utils.SharePreferenceUtils;
import com.xjk.sifasuo.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class WelcomeActivity extends BaseActivity{
    private ImageView rl_rootview;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_Launcher);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);// 设置全屏
        // 选择支持半透明模式，在有surfaceview的activity中使用
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        setContentView(R.layout.activity_welcome);
        rl_rootview = (ImageView) findViewById(R.id.rl_rootview);
        initData();
        //init();
    }


    private void initData() {
        if(!NewWorkUtils.isConnected(WelcomeActivity.this)){
            ToastUtils.showShort(WelcomeActivity.this,"当前网络不好");
            setLocalImage();
            return;
        }
        WelcomeModel model = new WelcomeModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                if(!TextUtils.isEmpty(message)){
                    Log.d("zhouzhuo","message=="+message);
                    try {
                        JSONObject object = new JSONObject(message);
                        int code = object.getInt("code");
                        if(code == 1){
                            String url = object.getJSONObject("data").getString("img_url");
                            Glide.with(WelcomeActivity.this).load(url).into(rl_rootview);
                            init();
                        }else {
                            setLocalImage();
                        }
                    } catch (JSONException e) {
                        setLocalImage();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failed() {
                setLocalImage();

            }
        });
        model.getImage();
    }

    //设置本地图片
    private void setLocalImage(){
        rl_rootview.setImageResource(R.drawable.welcome);
        init();
    }


    private void init() {
        new Handler().postDelayed(new Runnable() {//MediationRecordActivity
            @Override
            public void run() {
                String login = (String) SharePreferenceUtils.get(WelcomeActivity.this,"login","");
                if(!TextUtils.isEmpty(login)){
                    Intent intent = new Intent(WelcomeActivity.this,LoginActivity.class);
                    startActivity(intent);
                    finish();
                }else {
                    Intent intent = new Intent(WelcomeActivity.this,MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        },2000);

    }
}
