package com.xjk.sifasuo.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xjk.sifasuo.MyApplication;
import com.xjk.sifasuo.bean.LoginBean;
import com.xjk.sifasuo.utils.MD5;
import com.xjk.sifasuo.utils.UrlDomainUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class LoginModel extends BaseModel{
    public void login(String phone,String password){
        String url = UrlDomainUtil.urlHeader+"/User/login";
        final HashMap<String,String> map = new HashMap<>();
        map.put("tel",phone);

        map.put("pwd",MD5.MD5(password));
        Log.d("zhouzhuo","tel"+phone+"pwd"+MD5.MD5(password)+"==url=="+url);
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if(callBackListener!=null){
                    if(!TextUtils.isEmpty(s)){
                        Log.d("zhouzhuo","s==="+s);
                        LoginBean loginBean = new LoginBean();
                        try {
                            JSONObject object = new JSONObject(s);
                            loginBean.code = object.getInt("code");
                            loginBean.Msg = object.getString("Msg");
                            LoginBean.LoginContentBean bean = new LoginBean.LoginContentBean();
                            if(loginBean.code == 1){
                                JSONObject object1 = object.getJSONObject("data");
                                bean.id = object1.getString("id");
                                bean.name = object1.getString("name");
                                bean.idcard = object1.getString("idcard");
                                bean.loginname = object1.getString("loginname");
                                bean.password = object1.getString("password");
                                bean.photo  = UrlDomainUtil.photoHeader+object1.getString("photo");
                                loginBean.bean = bean;
                                callBackListener.success(loginBean);
                            }else {
                                loginBean.bean = new LoginBean.LoginContentBean();
                                loginBean.bean.errorMessage = object.getString("data");
                                callBackListener.success(loginBean);
                            }

                        } catch (JSONException e) {

                            e.printStackTrace();
                        }
                    }


                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("zhouzhuo","volleyError:"+volleyError.toString());
                Log.d("zhouzhuo","volleyError:"+volleyError.getMessage());
            }
        }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        MyApplication.getHttpQueue().add(request);

       /* OkHttpUtils
                .get()
                .url(url)
                .addParams("tel", phone)
                .addParams("pwd", password)
                .build()
                .execute(new RegisterCallBack() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        // callBackListener.success();
                        Log.d("zhouzhuo","请求失败");
                    }

                    @Override
                    public void onResponse(RegisterBean response, int id) {
                        callBackListener.success(response);
                        Log.d("zhouzhuo","请求成功:"+response.toString());
                    }
                });*/
        /*HashMap<String,String> map = new HashMap<>();
        map.put("tel",phone);
        map.put("pwd",password);
        OkHttpUtils//
                .post()//
                .url(url)//
                .params(map)
                .build()//
                .execute(new Callback() {
                    @Override
                    public Object parseNetworkResponse(Response response, int id) throws Exception {
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("zhouzhuo","登录失败:"+e.getMessage());
                    }

                    @Override
                    public void onResponse(Object response, int id) {
                        Log.d("zhouzhuo","登录成功:"+response +"=====");
                    }
                });*/
    }

    /*private abstract class  RegisterCallBack extends Callback<RegisterBean> {
        @Override
        public RegisterBean parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            RegisterBean registerBean = new Gson().fromJson(string,RegisterBean.class);
            return registerBean;
        }


    }*/

}
