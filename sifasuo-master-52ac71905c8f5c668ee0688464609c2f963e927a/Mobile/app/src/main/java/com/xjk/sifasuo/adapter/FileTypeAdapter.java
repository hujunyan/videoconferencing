package com.xjk.sifasuo.adapter;

import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.bean.FileBean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class FileTypeAdapter extends BaseAdapter {
    private ArrayList<FileBean.FileItem> list;
    private FragmentActivity activity;
    public FileTypeAdapter(FragmentActivity activity, ArrayList<FileBean.FileItem> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder holder;
        if(convertView == null){
            holder = new MyViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.file_item,parent,false);
            holder.tv_detail = (TextView) convertView.findViewById(R.id.tv_detail);
            holder.tv_date = (TextView) convertView.findViewById(R.id.tv_date);
            holder.tv_title = (TextView) convertView.findViewById(R.id.tv_title);
            convertView.setTag(holder);
        }else {
            holder = (MyViewHolder) convertView.getTag();
        }
        final FileBean.FileItem bean = list.get(position);
        holder.tv_title.setText(bean.filename);
        holder.tv_date.setText(bean.date);
        holder.tv_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(detailsInterface!=null){
                    detailsInterface.details(bean.id,bean.status);
                }
            }
        });
        return convertView;
    }

    public interface  DetailsInterface{
        void details(String id,String status);
    }
    public DetailsInterface detailsInterface;

    public void setDetailsInterface(DetailsInterface detailsInterface){
        this.detailsInterface = detailsInterface;
    }

    class MyViewHolder {
        TextView tv_title;
        TextView tv_date;
        TextView tv_detail;

    }
}
