package com.xjk.sifasuo.bean;

/**
 * Created by zhouzhuo on 2017/6/30.
 */

public class FileDetailBean {
    public String Msg;
    public String code;
    public ChildBean bean;
    public  static class ChildBean{
        public String case_id;
        public String issue;
        public String id;
        public String image_url;
        public String filename;
        public String casetype;
    }
}
