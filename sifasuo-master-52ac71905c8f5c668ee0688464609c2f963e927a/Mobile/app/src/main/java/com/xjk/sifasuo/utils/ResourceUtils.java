package com.xjk.sifasuo.utils;

import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;

import com.xjk.sifasuo.MyApplication;

/**
 * Created by zhouzhuo on 2017/6/15.
 */

class ResourceUtils {
    public static int getColor(int id)
    {
        try {
            return MyApplication.getInstance().getResources().getColor(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return 0x0000;
    }

    public static int getDimen(int id)
    {
        try {
            return (int) MyApplication.getInstance().getResources().getDimension(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static Drawable getDrawable(int id, int def)
    {
        try
        {
            return MyApplication.getInstance().getResources().getDrawable(id);
        }
        catch(Exception e)
        {
            LogUtils.i("VLResourceUtils->getDrawable()", "not fount resId"+id);
        }
        return MyApplication.getInstance().getResources().getDrawable(def);
    }

    public static String getString(int id)
    {
        return getString(id, "");
    }

    public static String getString(int id, String def)
    {
        try {
            return MyApplication.getInstance().getResources().getString(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return def;
    }

    public static int getInt(int id)
    {
        return getInt(id, -1);
    }

    public static int getInt(int id, int def)
    {
        try {
            return MyApplication.getInstance().getResources().getInteger(id);
        } catch (NotFoundException e) {
            e.printStackTrace();
        }
        return def;
    }
}
