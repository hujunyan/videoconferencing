package com.xjk.sifasuo.utils.swipemenulistview;

public interface SwipeMenuCreator {

    void create(SwipeMenu menu);
}
