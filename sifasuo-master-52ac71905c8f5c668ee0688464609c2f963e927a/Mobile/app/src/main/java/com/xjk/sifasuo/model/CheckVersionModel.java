package com.xjk.sifasuo.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xjk.sifasuo.MyApplication;
import com.xjk.sifasuo.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class CheckVersionModel extends BaseModel{
    public void check(String code){
        String url = UrlDomainUtil.urlHeader+"/User/getResourceSoft/type/2/version/"+code;
        Log.d("zhouzhuo","url==="+url);
        StringRequest stringRequest = new StringRequest(url,
                new com.android.volley.Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("zhouzhuo","检查更新内容:"+response);
                        if(callBackListener!=null){
                            if(!TextUtils.isEmpty(response)){
                                callBackListener.success(response);
                            }

                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        });
        MyApplication.getHttpQueue().add(stringRequest);
       /* OkHttpUtils
                .get()
                .url(url)
                .addParams("type", "2")
                .addParams("Version",code)
                .build()
                .execute(new RegisterCallBack() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                       // callBackListener.success();
                        Log.d("zhouzhuo","请求失败");
                    }

                    @Override
                    public void onResponse(RegisterBean response, int id) {
                        callBackListener.success(response);
                        Log.d("zhouzhuo","请求成功:"+response.toString());
                    }
                });


*/


    }

   /* private abstract class  RegisterCallBack extends Callback<RegisterBean>{
        @Override
        public RegisterBean parseNetworkResponse(Response response, int id) throws Exception {
            String string = response.body().string();
            RegisterBean registerBean = new Gson().fromJson(string,RegisterBean.class);
            return registerBean;
        }


    }*/
}
