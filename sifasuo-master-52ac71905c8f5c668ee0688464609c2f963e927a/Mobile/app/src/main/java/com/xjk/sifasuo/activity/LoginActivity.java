package com.xjk.sifasuo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.xjk.sifasuo.MainActivity;
import com.xjk.sifasuo.R;
import com.xjk.sifasuo.bean.LoginBean;
import com.xjk.sifasuo.model.BaseModel;
import com.xjk.sifasuo.model.LoginModel;
import com.xjk.sifasuo.utils.IntentUtils;
import com.xjk.sifasuo.utils.NewWorkUtils;
import com.xjk.sifasuo.utils.SharePreferenceUtils;
import com.xjk.sifasuo.utils.ToastUtils;

import static com.xjk.sifasuo.utils.ConstantsUtils.bean;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class LoginActivity extends BaseActivity {
    private EditText et_number;
    private EditText et_password;
    private TextView tv_forget;
    private Button btn_login;
    private TextView tv_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        et_number = (EditText) findViewById(R.id.et_number);
        et_password = (EditText) findViewById(R.id.et_password);
        tv_forget = (TextView) findViewById(R.id.tv_forget);
        tv_forget.setOnClickListener(this);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        tv_register= (TextView) findViewById(R.id.tv_register);
        tv_register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.tv_forget:
                Intent intent = new Intent(this,FindPasswordActivity.class);
                IntentUtils.startActivity(this,intent);
                break;
            case R.id.btn_login:
                if(!NewWorkUtils.isConnected(LoginActivity.this)){
                    ToastUtils.showShort(LoginActivity.this,"当前网络不好");
                    return;
                }
                btn_login.setClickable(false);
                LoginModel model = new LoginModel();
                model.login(et_number.getText().toString().toString(),et_password.getText().toString().trim());
                model.setCallBackListener(new BaseModel.CallBackListener<LoginBean>() {
                    @Override
                    public void success(LoginBean loginBean) {
                        if(loginBean!=null){
                            if(loginBean.code ==1){
                                Intent  intent = new Intent(LoginActivity.this,MainActivity.class);
                                bean = loginBean.bean;
                                intent.putExtra("login","login");
                                SharePreferenceUtils.put(LoginActivity.this,"id",bean.id);
                                SharePreferenceUtils.put(LoginActivity.this,"name",bean.name);
                                SharePreferenceUtils.put(LoginActivity.this,"photo",bean.photo);
                                SharePreferenceUtils.put(LoginActivity.this,"password",bean.password);
                                SharePreferenceUtils.put(LoginActivity.this,"loginname",bean.loginname);
                                SharePreferenceUtils.put(LoginActivity.this,"idcard",bean.idcard);
                                IntentUtils.startActivity(LoginActivity.this,intent);
                                finish();
                            }else {
                                ToastUtils.showShort(LoginActivity.this,loginBean.bean.errorMessage);
                                btn_login.setClickable(true);
                            }
                        }else {
                            btn_login.setClickable(true);
                        }
                    }
                    @Override
                    public void failed() {

                    }
                });


                break;
            case R.id.tv_register:
                intent = new Intent(this,RegisterActivity.class);
                IntentUtils.startActivity(this,intent);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

}
