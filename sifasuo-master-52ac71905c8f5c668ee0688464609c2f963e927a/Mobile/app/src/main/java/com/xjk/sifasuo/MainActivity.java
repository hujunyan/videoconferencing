package com.xjk.sifasuo;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.widget.TextView;

import com.xjk.sifasuo.activity.BaseActivity;
import com.xjk.sifasuo.bean.LoginBean;
import com.xjk.sifasuo.fragment.MainFragment;
import com.xjk.sifasuo.fragment.MessageFragment;
import com.xjk.sifasuo.fragment.MineFragment;
import com.yuyh.library.imgsel.ImgSelActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;


public class MainActivity extends BaseActivity {
    private TextView tv_title;
    private MainFragment mainFragment;
    private MessageFragment messageFragment;
    private MineFragment mineFragment;
    public LoginBean.LoginContentBean bean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        tv_title = (TextView) findViewById(R.id.tv_title);
        tv_title.setText("首页");
        mainFragment = new MainFragment();
        messageFragment = new MessageFragment();
        mineFragment = new MineFragment();
        // 初始化加载第一个fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, mainFragment).commit();
    }

    public void changeFragment(int prePosition,int curPosition) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        hideFragment(ft,prePosition);
        switch (curPosition){
            case 0:
                if (mainFragment.isAdded()) {
                    ft.show(mainFragment);
                } else {
                    ft.add(R.id.fragment, mainFragment);
                }
                tv_title.setText("首页");
                break;
            case 1:
                if (messageFragment.isAdded()) {
                    ft.show(messageFragment);
                } else {
                    ft.add(R.id.fragment, messageFragment);
                }
                tv_title.setText("我要留言");
                break;
            case 2:
                if (mineFragment.isAdded()) {
                    ft.show(mineFragment);
                    mineFragment.initData();
                } else {
                    ft.add(R.id.fragment, mineFragment);
                }
                tv_title.setText("我");
                break;
        }
        ft.commit();
    }

    private void hideFragment(FragmentTransaction ft ,int position){
        switch (position){
            case 0:
                ft.hide(mainFragment);
                break;
            case 1:
                ft.hide(messageFragment);
                break;
            case 2:
                ft.hide(mineFragment);
                break;
            default:
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null){
            if(requestCode ==0 && resultCode == -1){
                List<String> pathList = data.getStringArrayListExtra(ImgSelActivity.INTENT_RESULT);
              //  mineFragment.setIcon(pathList.get(0));
                String path = pathList.get(0);
                mineFragment.compressFile(path);
                saveToLocal(path);

            }
        }
        Log.d("zhouzhuo","调用了=="+resultCode);
        if(resultCode ==10){
            mineFragment.checkMessage();
        }
    }

    private void saveToLocal(String path){
        // 其次把文件插入到系统图库
        File file = new File(path);
        try {
            MediaStore.Images.Media.insertImage(getContentResolver(),
                    file.getAbsolutePath(), file.getName(), null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // 最后通知图库更新
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path)));

    }

}
