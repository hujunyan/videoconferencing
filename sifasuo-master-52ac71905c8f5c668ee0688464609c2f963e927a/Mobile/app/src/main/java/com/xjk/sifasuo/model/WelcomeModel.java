package com.xjk.sifasuo.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xjk.sifasuo.MyApplication;
import com.xjk.sifasuo.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/8/17.
 */

public class WelcomeModel extends BaseModel {
    public void getImage(){
      //  "http://59.110.156.74/Uploads/image/2017-08-17/5994fbbc6c257.jpg"
        String url = UrlDomainUtil.urlHeader+"/Staff/getStartUpImg/type/2/";
        Log.d("zhouzhuo","welcome==="+url);
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(callBackListener!=null){
                            if(!TextUtils.isEmpty(response)){
                                Log.d("zhouzhuo","==welcome="+response);
                                callBackListener.success(response);
                            }else {
                                callBackListener.failed();
                            }

                        }else {
                            callBackListener.failed();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("zhouzhuo","开机页面");
                callBackListener.failed();
            }
        });
        MyApplication.getHttpQueue().add(stringRequest);

    }
}
