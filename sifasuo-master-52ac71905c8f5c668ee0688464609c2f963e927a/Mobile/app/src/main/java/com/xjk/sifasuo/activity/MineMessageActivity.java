package com.xjk.sifasuo.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import com.xjk.sifasuo.R;
import com.xjk.sifasuo.adapter.MessageAdapter;
import com.xjk.sifasuo.bean.MessageBean;
import com.xjk.sifasuo.model.BaseModel;
import com.xjk.sifasuo.model.MessageModel;
import com.xjk.sifasuo.utils.SharePreferenceUtils;
import com.xjk.sifasuo.utils.pulltorefresh.PullToRefreshBase;
import com.xjk.sifasuo.utils.pulltorefresh.PullToRefreshListView;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class MineMessageActivity extends BaseActivity{
    private ImageView iv_back;
    private ListView mListView;
    private PullToRefreshListView mRefreshListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine_message);
        initView();
        initData();

    }

    private void initData() {
        MessageModel model = new MessageModel();
        String tel = (String) SharePreferenceUtils.get(this,"loginname","");
        model.getMessage(tel);
        model.setCallBackListener(new BaseModel.CallBackListener<ArrayList<MessageBean.MessageChildBean>>() {
            @Override
            public void success(ArrayList<MessageBean.MessageChildBean> list ) {
               mListView.setAdapter(new MessageAdapter(MineMessageActivity.this,list));
            }

            @Override
            public void failed() {

            }
        });
    }

    private void initView() {
        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        mRefreshListView = (PullToRefreshListView) findViewById(R.id.lv_list);
        mListView = mRefreshListView.getRefreshableView();
        mListView.setDivider(getResources().getDrawable(R.drawable.listview_dash));
        mListView.setDividerHeight(10);
        mListView.setVerticalScrollBarEnabled(false);
        mRefreshListView.setPullLoadEnabled(false);
        mRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                initData();
                mRefreshListView.onPullDownRefreshComplete();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mRefreshListView.onPullUpRefreshComplete();
            }
        });
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        setResult(10,intent);
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.iv_back:
                Intent intent = getIntent();
                setResult(10,intent);
                finish();
                break;
        }
    }
}
