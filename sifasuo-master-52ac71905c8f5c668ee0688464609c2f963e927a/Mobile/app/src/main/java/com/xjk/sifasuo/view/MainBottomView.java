package com.xjk.sifasuo.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.xjk.sifasuo.MainActivity;
import com.xjk.sifasuo.R;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class MainBottomView extends LinearLayout implements View.OnClickListener{
    private int currentIndex;
    private LinearLayout ll_main,ll_message,ll_mine;
    private ImageView iv_main,iv_message,iv_mine;
    private MainActivity mainActivity;

    public MainBottomView(Context context) {
        this(context,null);
    }

    public MainBottomView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.bottom_main, this, true);
        ll_main = (LinearLayout) view.findViewById(R.id.ll_main);
        ll_main.setOnClickListener(this);
        iv_main = (ImageView) view.findViewById(R.id.iv_main);
        ll_message = (LinearLayout) view.findViewById(R.id.ll_message);
        ll_message.setOnClickListener(this);
        iv_message = (ImageView) view.findViewById(R.id.iv_message);
        ll_mine = (LinearLayout) view.findViewById(R.id.ll_mine);
        ll_mine.setOnClickListener(this);
        iv_mine = (ImageView) view.findViewById(R.id.iv_mine);
        mainActivity = (MainActivity) getContext();


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ll_main:
                if(currentIndex!=0){
                    changeTab(currentIndex,0);
                    mainActivity.changeFragment(currentIndex,0);
                    currentIndex = 0;

                }
                break;
            case R.id.ll_message:
                if(currentIndex!=1){
                    changeTab(currentIndex,1);
                    mainActivity.changeFragment(currentIndex,1);
                    currentIndex = 1;
                }
                break;
            case R.id.ll_mine:
                if(currentIndex!=2){
                    changeTab(currentIndex,2);
                    mainActivity.changeFragment(currentIndex,2);
                    currentIndex = 2;
                }
                break;
            default:
                break;
        }

    }

    /**
     * 改变tab的图标
     * @param currentIndex
     */
    private void changeTab(int preIndex,int currentIndex) {
        switch (currentIndex){
            case 0:
                switch (preIndex){
                    case 1:
                        iv_message.setImageResource(R.drawable.message_unselected);
                        break;
                    case 2:
                        iv_mine.setImageResource(R.drawable.photo_unselected);
                        break;
                }
                iv_main.setImageResource(R.drawable.home_selected);
                break;
            case 1:
                switch (preIndex){
                    case 0:
                        iv_main.setImageResource(R.drawable.home_unselected);
                        break;
                    case 2:
                        iv_mine.setImageResource(R.drawable.photo_unselected);
                        break;
                }
                iv_message.setImageResource(R.drawable.message_selected);
                break;
            case 2:
                switch (preIndex){
                    case 0:
                        iv_main.setImageResource(R.drawable.home_unselected);
                        break;
                    case 1:
                        iv_message.setImageResource(R.drawable.message_unselected);
                        break;
                }
                iv_mine.setImageResource(R.drawable.photo_selected);
                break;
        }

    }
}
