package com.xjk.sifasuo.utils;

import android.Manifest;
import android.annotation.TargetApi;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.View;
import android.widget.ImageView;

import com.nostra13.universalimageloader.cache.disc.naming.HashCodeFileNameGenerator;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.xjk.sifasuo.MyApplication;
import com.xjk.sifasuo.R;

/**
 * Created by zhouzhuo on 2017/6/15.
 */

public class ImageLoaderModel {
    private static final String[] PERMISSIONS = new String[]
            {
                    Manifest.permission.INTERNET,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
            };

    private ImageLoader mImageLoader;
    private DisplayImageOptions mOptions;

    //单个图片加载使用option.
    private static  DisplayImageOptions.Builder singleImageoptions = new DisplayImageOptions.Builder();
    private static  DisplayImageOptions loadSingleImageOption = null;

    private static class LazyHolder
    {
        private static final ImageLoaderModel INSTANCE = new ImageLoaderModel();
    }

    public static synchronized ImageLoaderModel instance()
    {
        return LazyHolder.INSTANCE;
    }

    private ImageLoaderModel()
    {

        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(MyApplication.getInstance());
        DisplayImageOptions.Builder options = new DisplayImageOptions.Builder();

        singleImageoptions.cacheInMemory(true);
        singleImageoptions.cacheOnDisk(true);
        loadSingleImageOption = singleImageoptions.build();
        //配置全局config
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();

        config.memoryCache(new WeakMemoryCache());
        config.memoryCacheSize(2 * 1024 * 1024);
        config.diskCacheFileNameGenerator(new HashCodeFileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024).threadPoolSize(Thread.NORM_PRIORITY - 2);
        config.tasksProcessingOrder(QueueProcessingType.LIFO);
        //提供默认options
        options .cacheInMemory(true);
        options.imageScaleType(ImageScaleType.EXACTLY);
        options.cacheOnDisk(true);
        options.resetViewBeforeLoading(true);
        options.delayBeforeLoading(150);
        options.imageScaleType(ImageScaleType.IN_SAMPLE_INT);
        options.bitmapConfig(Bitmap.Config.RGB_565);

        options.showImageOnLoading(R.drawable.login_logo);
        options.showImageForEmptyUri(R.drawable.login_logo);
        options.showImageOnFail(R.drawable.login_logo);

        mOptions = options.build();

        //真正初始化ImageLoader
        mImageLoader = ImageLoader.getInstance();
        mImageLoader.init(config.build());
    }

    private static DisplayImageOptions mDefaultOptions;
    public DisplayImageOptions getOptions(){
        if(mDefaultOptions==null){
            mDefaultOptions = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .showImageForEmptyUri(R.drawable.login_logo)
                    .showImageOnFail(R.drawable.login_logo).build();
        }
        return mDefaultOptions;
    }

    public ImageLoader getImageLoader()
    {
        return mImageLoader;
    }

    public DisplayImageOptions getDefaultOptions()
    {
        return mOptions;
    }

    public static DisplayImageOptions getSingleImageOption()
    {
        return loadSingleImageOption;
    }
    public void displayImage(String uri, ImageView imageView)
    {
        displayImage(uri, imageView,mOptions);
    }

    public void displayImage(String uri, ImageView imageView,DisplayImageOptions options)
    {
        mImageLoader.displayImage(uri, imageView, options);
    }

    public void displayImage(String uri, ImageView imageView,DisplayImageOptions mOptions, ImageLoadingListener listener)
    {
        mImageLoader.displayImage(uri, imageView,mOptions,listener);
    }

    public void displayImage(String uri, ImageView imageView,ImageLoadingListener listener)
    {
        mImageLoader.displayImage(uri, imageView,mOptions,listener);
    }

    public static class ImageLoadingListenerBlur implements ImageLoadingListener
    {
        private View  mView;
        public ImageLoadingListenerBlur(View view)
        {
            mView = view;
        }
        @Override
        public void onLoadingStarted(String s, View view)
        {
            if(mView!=null) mView.setVisibility(View.GONE);
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onLoadingFailed(String s, View view, FailReason failReason)
        {
            Drawable bitmapDrawable  =new BitmapDrawable(FastBlur.getBlurBitmap(BitmapUtils.drawableToBitmap(ResourceUtils.getDrawable(R.drawable.login_logo,R.drawable.login_logo)),5,5));
            if(mView!=null) mView.setBackgroundDrawable(bitmapDrawable);
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public void onLoadingComplete(String s, View view, Bitmap bitmap)
        {
            if(mView!=null) mView.setVisibility(View.VISIBLE);
               /* Drawable bitmapDrawable = new BitmapDrawable(FastBlur.getBlurBitmap(bitmap));
                if(mView!=null)mView.setBackgroundDrawable(bitmapDrawable);*/
        }

        @Override
        public void onLoadingCancelled(String s, View view)
        {}
    }


}
