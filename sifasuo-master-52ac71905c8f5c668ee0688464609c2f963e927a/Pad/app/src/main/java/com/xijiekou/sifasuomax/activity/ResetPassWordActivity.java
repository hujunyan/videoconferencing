package com.xijiekou.sifasuomax.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.ResetPasswordBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.ResetPasswordModel;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;

/**
 * Created by zhouzhuo on 2017/8/8.
 */

public class ResetPassWordActivity extends BaseActivity {
    private EditText /* et_number,*/
            et_password ,
            et_new_password_one,
            et_new_password_two;
    private TextView  btn_reset  ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);
        initView();
    }

    private void initView() {
       // et_number = (EditText) findViewById(R.id.et_number);
        et_password = (EditText) findViewById(R.id.et_password);
        et_new_password_one = (EditText) findViewById(R.id.et_new_password_one);
        et_new_password_two = (EditText) findViewById(R.id.et_new_password_two);
        btn_reset = (TextView) findViewById(R.id.btn_reset);
        btn_reset.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_reset:
               /* if(TextUtils.isEmpty(et_number.getText().toString().trim())){
                    ToastUtils.showShort(ResetPassWordActivity.this,"请输入用户名");
                    break;
                }*/
                if(TextUtils.isEmpty(et_password.getText().toString().trim())){
                    ToastUtils.showShort(ResetPassWordActivity.this,"请输入当前密码");
                    break;
                }
                if(TextUtils.isEmpty(et_new_password_one.getText().toString().trim())){
                    ToastUtils.showShort(ResetPassWordActivity.this,"请输入新密码");
                    break;
                }
                if(!et_new_password_one.getText().toString().trim().equals(et_new_password_two.getText().toString().trim())){
                    ToastUtils.showShort(ResetPassWordActivity.this,"两次密码不一致");
                    break;
                }
                ResetPasswordModel model = new ResetPasswordModel();
                model.setCallBackListener(new BaseModel.CallBackListener<ResetPasswordBean>() {
                    @Override
                    public void success(ResetPasswordBean bean) {
                        ToastUtils.showShort(ResetPassWordActivity.this,bean.data);
                        if(bean.code == 1){
                            finish();
                        }
                    }

                    @Override
                    public void failed() {

                    }
                });
                Log.d("zhouzhuo","id:"+SharePreferenceUtils.get(ResetPassWordActivity.this,"id",""));
                model.resetPassword(et_password.getText().toString().trim(),
                        et_new_password_one.getText().toString().trim(),(String)SharePreferenceUtils.get(ResetPassWordActivity.this,"id",""));
                break;
        }
    }
}
