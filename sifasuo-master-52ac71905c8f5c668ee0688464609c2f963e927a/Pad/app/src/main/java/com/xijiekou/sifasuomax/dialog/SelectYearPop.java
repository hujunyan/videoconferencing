package com.xijiekou.sifasuomax.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.adapter.SelectYearAdapter;
import com.xijiekou.sifasuomax.utils.DensityUtils;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by zhouzhuo on 2017/7/11.
 */

public class SelectYearPop extends PopupWindow{
    //private TextView tv_state_doing,tv_state_success,tv_state_fail;
    private Activity activity;
    //private
    public ArrayList<String> list ;


    public SelectYearPop(Activity activity){
        this.activity = activity;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.select_month_pop, null);
        this.setWidth(DensityUtils.dp2px(activity,88));
        // 设置弹出窗体的高
        this.setHeight(DensityUtils.dp2px(activity,100));
        this.setBackgroundDrawable(new BitmapDrawable());
        // 设置弹出窗体可点击
        //this.setTouchable(true);
        //this.setFocusable(true);
        // 设置点击是否消失
        this.setOutsideTouchable(true);
        ListView listView = (ListView) view.findViewById(R.id.lv_list);
        initListView(listView);
        this.setContentView(view);// 设置弹出窗体的宽

    }

    private void initListView(ListView listView) {
        list = new ArrayList<>();
        Calendar now = Calendar.getInstance();
        int  year = now.get(Calendar.YEAR);
        int start = year-5;
        int end = year+5;
        for (int i= start;i<end;i++){
            list.add(i+"");
        }
        SelectYearAdapter adapter = new SelectYearAdapter(list,activity);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listener!=null){
                    listener.select(list.get(position));
                    if(isShowing()){
                        dismiss();
                    }
                }
            }
        });
        listView.setAdapter(adapter);
    }


    public void showPopupWindow(View parent) {
       if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
             // x y
            int[] location = new int[2];
            //获取在整个屏幕内的绝对坐标
            parent.getLocationOnScreen(location);
           this.showAsDropDown(parent,0,0);
        } else {
           this.dismiss();
        }
    }






    public SelectItemListener listener;
    public void setListener(SelectItemListener listener){
        this.listener = listener;
    }
    public interface  SelectItemListener {
        void select(String year);
    }

}
