package com.xijiekou.sifasuomax.bean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/8/4.
 */

public class NewOralBeans {
    public String isAudit;//区别保存和创建多加的一个参数   保存时才需要传
    public ArrayList<NewOralBean> list;
    public String id;
    public String fileName;//卷名
    public String argue;//争议内容
    public String agreement;
    public String performance;//
    public String signature;
    public String date;
    public String company;
    public String status;
    public String num;
}
