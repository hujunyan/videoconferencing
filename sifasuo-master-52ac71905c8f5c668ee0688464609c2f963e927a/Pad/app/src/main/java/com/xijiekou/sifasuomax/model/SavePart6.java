package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.bean.NewProtocolBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SavePart6 extends BaseModel{
    public void save(NewProtocolBean beans) {
        final JSONArray array = new JSONArray();
        ArrayList<NewOralBean> list = beans.list;
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                NewOralBean bean = list.get(i);
                JSONObject object = new JSONObject();
                putString(object, "name", bean.name);
                putString(object, "sex", bean.sex);
                putString(object, "nation", bean.nation);
                putString(object, "age", bean.age);
                putString(object, "job", bean.job);
                putString(object, "tel", bean.tel);
                putString(object, "type",bean.type);
                putString(object, "address", bean.address);
                putString(object,"idcard",bean.idcard);
                array.put(object);
            }

        }
        String url = UrlDomainUtil.urlHeader + "/Staff/saveCasePart6";
        // String postJson = "{\"arr\":"+array.toString()+"}";
        JSONObject object3 = null;
        try {
            object3 = new JSONObject();
            object3.putOpt("arr", array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("zhouzhuo", "url:" + url);
        Log.d("zhouzhuo", "postJson:" + object3.toString());
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
            if (!TextUtils.isEmpty(beans.case_id)) {
                builder.addParams("case_id", beans.case_id);
            }
            if (!TextUtils.isEmpty(beans.issue)) {
                builder.addParams("issue", beans.issue);
            }
            if (!TextUtils.isEmpty(beans.content)) {
                builder.addParams("content", beans.content);
            }
            if (!TextUtils.isEmpty(beans.method)) {
                builder.addParams("method", beans.method);
            }
            if (!TextUtils.isEmpty(beans.num)) {
                builder.addParams("num", beans.num);
            }
            if (!TextUtils.isEmpty(beans.litigant_sign)) {
                builder.addParams("litigant_sign", beans.litigant_sign);
            }
            if (!TextUtils.isEmpty(beans.mediate_sign)) {
                builder.addParams("mediate_sign", beans.mediate_sign);
            }
            if (!TextUtils.isEmpty(beans.record_sign)) {
                builder.addParams("record_sign", beans.record_sign);
            }
            if (!TextUtils.isEmpty(beans.date)) {
                builder.addParams("date", beans.date);
            }
        if (array.length() > 0) {
            builder.addParams("persons", object3.toString());
        }
        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo", " 新建协议书请求失败:" + e.getMessage());
                Log.d("zhouzhuo", " 新建协议书请求失败:" + e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if (response != null) {
                   // callBackListener.success(response);
                    Log.d("zhouzhuo", " 修改协议书 请求成功:" + response);
                    PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);
                }
            }
        });

    }

    public void putString(JSONObject object, String key, String value) {
        if (!TextUtils.isEmpty(value)) {
            try {
                object.put(key, value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
