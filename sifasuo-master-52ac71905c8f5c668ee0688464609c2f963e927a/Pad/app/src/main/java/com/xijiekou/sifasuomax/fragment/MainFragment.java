package com.xijiekou.sifasuomax.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.FileMenuActivity;
import com.xijiekou.sifasuomax.activity.NewOralActivity;
import com.xijiekou.sifasuomax.adapter.MainFragmentAdapter;
import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.MainFragmentCaseListModel;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshBase;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshListView;
import com.xijiekou.sifasuomax.view.MainFragmentHeadView;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/4.
 */
public class MainFragment extends BaseFragment{
    private PullToRefreshListView  refreshList;
    private ListView  listView ;
    private MainFragmentAdapter adapter;
    //为0=有下一页，为1=最后一页
    private String isLastpage;
    private int currentPage = 1;
    private String content;
    private ArrayList<MainFragmentItemBeans.MainFragmentItemBean> totalList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_main, null);
        initView(view);
        requestData(null);
        return view;
    }

    public void refreshData(){
        Log.d("zhouzhuo","refreshData==");
        currentPage =1;
        adapter = null;
        viewHeader.loadUrl();
        requestData(content);
    }


    private void requestData(String content) {
        Log.d("zhouzhuo","首页上拉刷新===");
        String id = (String) SharePreferenceUtils.get(getActivity(),"id","");
        String company = (String) SharePreferenceUtils.get(getActivity(),"company","");
        MainFragmentCaseListModel model = MainFragmentCaseListModel.getInstance();
        model.getList(id,company,content,currentPage);
        model.setCallBackListener(new BaseModel.CallBackListener<MainFragmentItemBeans>() {

            @Override
            public void success(MainFragmentItemBeans beans ) {
                if (beans!=null) {
                    if(currentPage == 1){
                        totalList = beans.list;
                    }else {
                        totalList.addAll(beans.list);
                    }
                    currentPage = beans.currentPage;
                    isLastpage = beans.isLastpage;
                    setAdapter();
                }else {
                   ToastUtils.showShort(getActivity(),"没有查找到相关数据");
                    if(totalList!=null){
                        totalList.clear();
                        setAdapter();
                    }
                }
            }

            @Override
            public void failed() {

            }
        });

    }


    private void setAdapter(){
        if(adapter == null){
            adapter = new MainFragmentAdapter(getActivity(),totalList);
            listView.setAdapter(adapter);
        }else {
            adapter.notifyDataSetChanged();
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private  MainFragmentHeadView viewHeader;

    private void initView(View view) {
        refreshList = (PullToRefreshListView) view.findViewById(R.id.lv_list);
        listView = refreshList.getRefreshableView();
       // listView.setDescendantFocusability(FOCUS_AFTER_DESCENDANTS);
        listView.setVerticalScrollBarEnabled(false);
        listView.setDivider(getContext().getResources().getDrawable(R.drawable.main_listview_drawable));
        listView.setDividerHeight(10);
        //viewHeader  = new MainFragmentHeadView(getContext());
        viewHeader = (MainFragmentHeadView) view.findViewById(R.id.viewHeader);
        viewHeader.setSearchListener(new MainFragmentHeadView.SearchListener() {
            @Override
            public void search(String content2) {
                currentPage =1;
                adapter = null;
                content = content2;
                requestData(content2);
            }
        });
        viewHeader.setAddTextListener(new MainFragmentHeadView.AddTextListener() {
            @Override
            public void search(String content1) {
                content = content1;
            }
        });


        refreshList.setPullLoadEnabled(true);
        refreshList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                Log.d("zhouzhuo","下拉==");
                currentPage = 1;
                adapter = null;
                requestData(content);
                refreshList.onPullDownRefreshComplete();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if(isLastpage.equals("0")){
                    currentPage++;
                    requestData(content);
                }else {
                    ToastUtils.showShort(getActivity(),"没有更多数据");
                }
                refreshList.onPullUpRefreshComplete();
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainFragmentItemBeans.MainFragmentItemBean bean = totalList.get(position);
                String type = bean.casetype;
                Intent intent ;
                if(type.equals("0")){
                    intent= new Intent(getActivity(),NewOralActivity.class);
                    intent.putExtra("caseId",bean.id);
                    IntentUtils.startFragmentToActivity(MainFragment.this,intent,true,0);
                }else {
                    intent= new Intent(getActivity(),FileMenuActivity.class);
                    intent.putExtra("caseId",bean.id);
                    IntentUtils.startFragmentToActivity(MainFragment.this,intent,true,1);
                }
            }
        });
    }

    public void updatePhoto() {
        viewHeader.updatePhoto();
    }
}
