package com.xijiekou.sifasuomax.bean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/9/12.
 */

public class CreateCheckCreateBean {
    public String filename;
    public String filenum;
    public String audit;
    public ArrayList<NewOralBean> list;
    public String argue;
    /**
     * (1  可创建 申请书和受理登记表
     2  可创建 调解记录 调查记录 证据材料 其他材料
     3 可创建书面协议  4 可创建结案报告 5 可创建回访记录)

     */
    public int state;
    public String id;
    public String case_id;
    //0 未创建
    public String sqs,
            sldjb,
            tjjl,
            dcjl,
            zjcl,
            qtcl,
            smxy,
            jabg,
            hfjl;
}
