package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.EvidenceMateriaBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SavePart4 extends BaseModel{
    public void registration(EvidenceMateriaBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/saveCasePart4";
        Log.d("zhouzhuo","调查记录:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(bean!=null){
            if(!TextUtils.isEmpty(bean.case_id)){
                builder.addParams("case_id", bean.case_id);
                Log.d("zhouzhuo","case Id="+bean.case_id);
            }
            if(!TextUtils.isEmpty(bean.auditor)){
                builder.addParams("auditor", bean.auditor);
            }
            if(!TextUtils.isEmpty(bean.litigant)){
                builder.addParams("litigant",bean.litigant);
            }
            if(!TextUtils.isEmpty(bean.date)){
                builder.addParams("date",bean.date);
            }

        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","新建证据材料请求失败:"+e.getMessage());
                Log.d("zhouzhuo","新建证据材料请求失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    //callBackListener.success(response);
                    //callBackListener.success(response);
                    Log.d("zhouzhuo","新建证据材料请求成功:"+response);
                    PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);
                }
            }
        });
    }
}
