package com.xijiekou.sifasuomax.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.DensityUtils;

/**
 * Created by zhouzhuo on 2017/7/11.
 */

public class SelectPerformancePop extends PopupWindow implements View.OnClickListener{
    private TextView tv_one,tv_two;
    private Activity activity;
    private int selectPosition;


    public SelectPerformancePop(Activity activity, int selectPosition){
        this.activity = activity;
        this.selectPosition = selectPosition;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.main_fragment_select_performance_pop, null);

        this.setWidth(DensityUtils.dp2px(activity,100));
        // 设置弹出窗体的高
        this.setHeight(DensityUtils.dp2px(activity,90));
        this.setBackgroundDrawable(new BitmapDrawable());
        // 设置弹出窗体可点击
        //this.setTouchable(true);
        //this.setFocusable(true);
        tv_one = (TextView) view.findViewById(R.id.tv_one);
        tv_one.setOnClickListener(this);
        tv_two = (TextView) view.findViewById(R.id.tv_two);
        tv_two.setOnClickListener(this);

        // 设置点击是否消失
        this.setOutsideTouchable(true);
        this.setContentView(view);
        initTextColor(tv_one,tv_two,selectPosition);

    }

   private void initTextColor(TextView tv_one, TextView tv_two,int selectPosition) {
        switch (selectPosition){
            case 0:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv_one.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));
                    tv_two.setTextColor(activity.getResources().getColor(R.color.popText,null));
                }else {
                    tv_one.setTextColor(activity.getResources().getColor(R.color.colorCommon));
                    tv_two.setTextColor(activity.getResources().getColor(R.color.popText));
                }
                break;
            case 1:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv_one.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_two.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));
                }else {
                    tv_one.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_two.setTextColor(activity.getResources().getColor(R.color.colorCommon));
                }
                break;
        }
    }

    public void showPopupWindow(View parent) {
       if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
             // x y
            int[] location = new int[2];
            //获取在整个屏幕内的绝对坐标
            parent.getLocationOnScreen(location);
           this.showAsDropDown(parent,0,0);
        } else {
           this.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_one:
                selectPosition = 0;
                break;
            case R.id.tv_two:
                selectPosition = 1;
                break;

        }
        if(listener!=null){
            listener.select(selectPosition);
            this.dismiss();
        }

    }



    public SelectItemListener listener;
    public void setListener(SelectItemListener listener){
        this.listener = listener;
    }
    public interface  SelectItemListener {
        void select(int type);
    }

}
