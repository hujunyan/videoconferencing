package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.NewOralBean;

/**
 * Created by zhouzhuo on 2017/8/2.
 * 法人
 */

public class AddTwoPersonActivity extends BaseActivity {
    TextView tv_person;
    TextView tv_person_select;
    EditText et_one;
    EditText et_two;
    EditText et_three;
    EditText et_four;
    EditText et_five;
    EditText et_six;
    private String search;
    private int selectPosition;


    // 0  自然人 Natural   1 法人 Corporation  2非法人组织  Litigant
    private NewOralBean bean;

    private Button btn_confirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person_two);
        initView();
        initData();

    }

    private void initData() {
        bean = (NewOralBean) getIntent().getSerializableExtra("bean");
        selectPosition = getIntent().getIntExtra("position",-1);
        if(bean == null){
            bean = new NewOralBean();
        }else {
                search = "search";
                String name = bean.faren;
                setTextNotNull(et_one,name);
                String address = bean.address;
                setTextNotNull(et_two,address);
                String codeNumber = bean.tongyi;
                setTextNotNull(et_three,codeNumber);
                String legalRepresentative = bean.fading;
                setTextNotNull(et_four,legalRepresentative);
                String job = bean.tongyi;
                setTextNotNull(et_five,job);
                String mobile = bean.tel;
                setTextNotNull(et_six,mobile);
        }

    }

    private void initView() {
        tv_person = (TextView) findViewById(R.id.tv_person);
        tv_person_select = (TextView) findViewById(R.id.tv_person_select);
        tv_person_select.setOnClickListener(this);
        et_one = (EditText) findViewById(R.id.et_one);
        et_two = (EditText) findViewById(R.id.et_two);
        et_three = (EditText) findViewById(R.id.et_three);
        et_four = (EditText) findViewById(R.id.et_four);
        et_five= (EditText) findViewById(R.id.et_five);
        et_six = (EditText) findViewById(R.id.et_six);


        et_one.addTextChangedListener(new MyTextWatcher(0));
        et_two.addTextChangedListener(new MyTextWatcher(1));
        et_three.addTextChangedListener(new MyTextWatcher(2));
        et_four.addTextChangedListener(new MyTextWatcher(3));
        et_five.addTextChangedListener(new MyTextWatcher(4));
        et_six.addTextChangedListener(new MyTextWatcher(5));

        btn_confirm = (Button) findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(this);
    }


    private class  MyTextWatcher implements TextWatcher {
        private int position;
        public MyTextWatcher(int position){
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }
        /***
         *  public static class Corporation implements Serializable{
         public String name;
         public String address;
         public String codeNumber;
         public String legalRepresentative;//法定代表人
         public String personInCharge; //负责人
         public String mobile;
         }
         */

        @Override
        public void afterTextChanged(Editable s) {
            switch (position){
                case 0:
                    bean.faren = s.toString();
                    break;
                case 1:
                    bean.address = s.toString();
                    break;
                case 2:
                    bean.tongyi = s.toString();
                    break;
                case 3:
                    bean.fading= s.toString();
                    break;
                case 4:
                    bean.weituoren = s.toString();
                    break;
                case 5:
                    bean.tel = s.toString();
                    break;
            }

        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_confirm:
                Intent intent = getIntent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("bean",bean);
                bundle.putInt("position",selectPosition);
               // ToastUtils.showShort(AddTwoPersonActivity.this,"tel:"+bean.tel);
                intent.putExtras(bundle);
                if(TextUtils.isEmpty(search)){
                    setResult(2,intent);
                }else {
                    setResult(20,intent);
                }

                finish();
                break;
        }
    }

    private void setTextNotNull(EditText tv,String text){
        if(!TextUtils.isEmpty(text)){
            tv.setText(text);
        }
    }
}
