package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhouzhuo on 2017/8/8.
 * 上传文件路径 保存
 */

public class TemporaryModel extends BaseModel {
    public void uploadPath(String case_id,String type,String uploadUrl){
        String url = UrlDomainUtil.urlHeader + "/Staff/addLocal";
        Log.d("zhouzhuo","url==="+url);
        final HashMap<String, String> map = new HashMap<>();
        map.put("case_id",case_id);
        map.put("type",type);
        map.put("url",uploadUrl);
        Log.d("zhouzhuo","upLoadUrl==="+uploadUrl);
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (callBackListener != null) {
                    if (!TextUtils.isEmpty(s)) {
                        Log.d("zhouzhuo","上传本地路径:"+s);
                         try {
                            JSONObject object = new JSONObject(s);
                            int  code = object.getInt("code");
                             if(code == 1){
                                 JSONObject object1 = object.getJSONObject("data");
                                 String file_id = object1.getString("file_id");
                                 callBackListener.success(file_id);
                             }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                callBackListener.failed();
                Log.d("zhouzhuo","上传本地路径 :"+volleyError.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        MyApp.getHttpQueue().add(request);
    }

}
