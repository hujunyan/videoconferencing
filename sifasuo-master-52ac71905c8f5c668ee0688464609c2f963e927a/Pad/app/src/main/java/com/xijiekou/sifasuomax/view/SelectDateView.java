package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.codbking.widget.DatePickDialog;
import com.codbking.widget.OnChangeLisener;
import com.codbking.widget.OnSureLisener;
import com.codbking.widget.bean.DateType;
import com.xijiekou.sifasuomax.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zhouzhuo on 2017/7/10.
 */

public class SelectDateView extends LinearLayout implements View.OnClickListener{
    private ImageView iv_select;

    private String date;

    public SelectDateView(Context context,GetDateListener listener) {
        super(context);
        this.listener = listener;
        initView(context);
    }



    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.select_date_view,this,true);
        iv_select = (ImageView) view.findViewById(R.id.iv_select);
        iv_select.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_select:
                showDatePickDialog(DateType.TYPE_YMD);
                break;
        }

    }

    /***
     * TYPE_ALL--年、月、日、星期、时、分
     TYPE_YMDHM--年、月、日、时、分
     TYPE_YMDH--年、月、日、时
     TYPE_YMD--年、月、日
     TYPE_HM--时、分
     * @param type
     */
    private void showDatePickDialog(DateType type) {
        DatePickDialog dialog = new DatePickDialog(getContext());
        //设置上下年分限制
        dialog.setYearLimt(5);
        //设置标题
        dialog.setTitle("选择时间");
        //设置类型
        dialog.setType(type);
        //设置消息体的显示格式，日期格式
        dialog.setMessageFormat("yyyy-MM-dd HH:mm");
        //设置选择回调
        dialog.setOnChangeLisener(new OnChangeLisener() {
            @Override
            public void onChanged(Date date) {
                if(listener!=null){
                    String message = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
                    Toast.makeText(getContext(),"data:"+11111,Toast.LENGTH_SHORT).show();
                    listener.getDate(message);
                }
            }
        });
        //设置点击确定按钮回调
        dialog.setOnSureLisener(new OnSureLisener() {
            @Override
            public void onSure(Date date) {
               String message = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
                Log.d("zhouzhuo","Date:"+date.toString());
                if(listener!=null){
                    Toast.makeText(getContext(),"data:"+11111,Toast.LENGTH_SHORT).show();
                    listener.getDate(message);
                }
                Toast.makeText(getContext(),"listener:"+listener,Toast.LENGTH_SHORT).show();
               // Toast.makeText(getContext(),"data:"+message,Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    public interface  GetDateListener{
        void getDate(String date);
    }
    public GetDateListener listener;
    public void setDateListener(GetDateListener listener){
        this.listener = listener;
    }


}
