package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/9/12.
 */

public class PrintModel extends BaseModel {
    public void print(String id, final String content){
        String url = UrlDomainUtil.urlHeader+"/Print/printsqs/"+content;
        Log.d("zhouzhuo","打印："+url+"=id="+id);
        // String postJson = "{\"arr\":"+array.toString()+"}";

        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(!TextUtils.isEmpty(id)){
            builder.addParams("case_id",id);
        }


        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","打印"+content+"==失败:"+e.getMessage());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    //callBackListener.success(response);
                    Log.d("zhouzhuo","打印"+content+"==请求成功:"+response);
                    try {
                        PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                        callBackListener.success(postRequestBean);
                    }catch (Exception e){
                        Log.d("zhouzhuo","请求成功:"+response);
                    }

                }
            }
        });

    }
}
