package com.xijiekou.sifasuomax.adapter;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xijiekou.sifasuomax.activity.EvidenceEnteringActivity;
import com.xijiekou.sifasuomax.bean.MultimediaBean;
import com.xijiekou.sifasuomax.holder.BaseHolder;
import com.xijiekou.sifasuomax.holder.EvidenceEnteringHolder;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/4.
 */
public class EvidenceEnteringAdapter extends BaseAdapter{
    private int type;
    private ArrayList<MultimediaBean> list;
    private EvidenceEnteringActivity activity;
    public EvidenceEnteringAdapter(EvidenceEnteringActivity activity,ArrayList<MultimediaBean> list,int type){
        this.activity = activity;
        this.list = list;
        this.type =type;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseHolder<MultimediaBean> holder;
        if(convertView == null){
           holder = new EvidenceEnteringHolder(activity,type,position);
        }else {
            holder = (EvidenceEnteringHolder) convertView.getTag();
        }
        holder.setData(list.get(position));
        return holder.getContentView();
    }

    public interface  SelectEvidenceListener{
        void selectEvidence(int type);
    }
    public void setListener(SelectEvidenceListener listener){
        this.listener = listener;
    }
    public SelectEvidenceListener listener;

}
