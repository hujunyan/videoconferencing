package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.RegistrationFormBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SearchPart2 extends BaseModel{
    public void searchPart2(String id){
        String url = UrlDomainUtil.urlHeader + "/Staff/getCasePart2/case_id/"+id;
        Log.d("zhouzhuo","-==="+url);
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(!TextUtils.isEmpty(response)){
                            Gson gson = new Gson();
                            try {
                                JSONObject object = new JSONObject(response);
                                int code = object.getInt("code");
                                if(code == 1){
                                    JSONObject object1 = object.getJSONObject("data");
                                    RegistrationFormBean bean = gson.fromJson(object1.toString(),RegistrationFormBean.class);
                                    callBackListener.success(bean);
                                }else if(code == 4012){
                                    callBackListener.success(null);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }


                        }
                        Log.d("zhouzhuo","受理表=="+response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }
}
