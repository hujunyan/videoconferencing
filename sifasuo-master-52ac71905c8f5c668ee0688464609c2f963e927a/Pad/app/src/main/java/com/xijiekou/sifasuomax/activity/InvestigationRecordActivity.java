package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.InvestigationBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.InvestigationFormModel;
import com.xijiekou.sifasuomax.model.PrintModel2;
import com.xijiekou.sifasuomax.model.SavePart3;
import com.xijiekou.sifasuomax.model.SearchPart3;
import com.xijiekou.sifasuomax.utils.BeanToJsonUtils;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.SignNameView;
import com.xijiekou.sifasuomax.view.YearMonthDayMorningHourView;

/**
 * Created by zhouzhuo on 2017/7/27.
 * 新建调查记录  3-5-2
 */

public class InvestigationRecordActivity extends VoiceBaseActivity {
    private String lastResult="";
    private YearMonthDayMorningHourView rl_date;//时间
    private EditText et_place;//地点
    private EditText et_attender;//c参加人
    private EditText et_attended;//c被调查人
    private RelativeLayout rl_voice;//
    private EditText et_detail;
    private SignNameView tv_person_one;
    private SignNameView tv_person_two;
    private SignNameView tv_person_three;
    private String caseId;
    private TextView tv_evidence_entering;
    private MyHandler handler;
    private String search;
    private String fileName;
    private String filenumber;
    private TextView tv_file_number;
    private BottomConfirmView bottom_confirm_view;
    private String audit;


    private class  MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    InvestigationBean bean = (InvestigationBean) msg.obj;
                    rl_date.setData(bean.date);
                    setTextNotNull(bean.location,et_place);
                    setTextNotNull(bean.attend,et_attender);
                    setTextNotNull(bean.surveyed,et_attended);
                    setTextNotNull(bean.record,et_detail);
                    tv_person_one.setImage(bean.litigant_sign);
                    tv_person_two.setImage(bean.litiganted_sign);
                    tv_person_three.setImage(bean.record_sign);
                    break;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_investigation_record);
        initView();
        iniData();

    }

    private void iniData() {
        Log.d("zhouzhuo", "caseId===" + caseId);
            Log.d("zhouzhuo", "caseId===" + caseId);
            SearchPart3 part = new SearchPart3();
            part.setCallBackListener(new BaseModel.CallBackListener<InvestigationBean>() {
                @Override
                public void success(InvestigationBean message) {
                    if(message!=null){
                        Message message1 = Message.obtain();
                        message1.what = 0;
                        message1.obj = message;
                        handler.sendMessage(message1);
                        search = "search";
                    }else {
                        bottom_confirm_view.hidePrint();
                        search = "";
                    }

                }

                @Override
                public void failed() {

                }
            });
            part.searchPart3(caseId);
        }


    private void initView() {
        handler = new MyHandler();
        rl_voice = (RelativeLayout) findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        et_detail = (EditText) findViewById(R.id.et_detail);
        et_detail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult = s.toString();
            }
        });
        rl_date = (YearMonthDayMorningHourView) findViewById(R.id.rl_date);
        et_place = (EditText) findViewById(R.id.et_place);
        et_attender = (EditText) findViewById(R.id.et_attender);
        et_attended = (EditText) findViewById(R.id.et_attended);
        tv_person_one = (SignNameView) findViewById(R.id.tv_person_one);
        tv_person_two = (SignNameView) findViewById(R.id.tv_person_two);
        tv_person_three = (SignNameView) findViewById(R.id.tv_person_three);
        tv_evidence_entering = (TextView) findViewById(R.id.tv_evidence_entering);
        tv_evidence_entering.setOnClickListener(this);
        tv_file_number = (TextView) findViewById(R.id.tv_file_number);
        caseId = getIntent().getStringExtra("caseId");
        search = getIntent().getStringExtra("search");
        fileName = getIntent().getStringExtra("filename");
        filenumber = getIntent().getStringExtra("filenumber");
        audit = getIntent().getStringExtra("audit");

        if(!TextUtils.isEmpty(filenumber)){
            tv_file_number.setText(filenumber);
        }
        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setSaveListener(new BottomConfirmView.SaveListener() {
            @Override
            public void save() {
               /* InvestigationBean bean = getBean();
                bean.isAudit = "1";
                saveAndCreate(bean);*/
            }
        });
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {
                PrintModel2 model = new PrintModel2();
                InvestigationBean bean = getBean();
                String content = BeanToJsonUtils.part4(bean);
                Log.d("zhouzhuo","content:"+content);
                model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)&&message.equals("success")){
                            ToastUtils.showShort(InvestigationRecordActivity.this,"打印成功,请勿重复点击");
                        }else {
                            ToastUtils.showShort(InvestigationRecordActivity.this,"打印失败");
                        }
                    }

                    @Override
                    public void failed() {
                        ToastUtils.showShort(InvestigationRecordActivity.this,"打印失败,请检查打印服务是否开启");
                    }
                });
                String ip = (String) SharePreferenceUtils.get(InvestigationRecordActivity.this,"printIp","");
                if(TextUtils.isEmpty(ip)||ip.equals("1")){
                    ToastUtils.showShort(InvestigationRecordActivity.this,"暂不支持打印");
                }else {
                    model.print(content,"printB4",ip);
                }
            }
        });

    }
    private  InvestigationBean getBean(){
        final InvestigationBean bean = new InvestigationBean();
        bean.case_id = caseId;
        bean.date = rl_date.getDate();
        // bean.time_range = rl_date.getType();
        // bean.time = rl_date.getHour()+":"+rl_date.getMinute();
        bean.location = et_place.getText().toString().trim();
        bean.attend = et_attender.getText().toString().trim();
        bean.surveyed = et_attended.getText().toString().trim();
        bean.record = et_detail.getText().toString().trim();
        bean.litigant_sign = tv_person_one.getFilePath();
        bean.litiganted_sign = tv_person_two.getFilePath();
        bean.record_sign = tv_person_three.getFilePath();
        return bean;
    }

    private void  saveAndCreate(InvestigationBean bean){
        InvestigationFormModel model = new InvestigationFormModel();
        model.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
            @Override
            public void success(PostRequestBean bean) {
                ToastUtils.showShort(InvestigationRecordActivity.this,bean.data);
                if(bean.code==1){
                    Intent intent = getIntent();
                    setResult(0,intent);
                    finish();
                }
            }

            @Override
            public void failed() {

            }
        });
        model.registration(bean);

    }
    private void next() {
        if(!TextUtils.isEmpty(audit)&&audit.equals("1")){
            ToastUtils.showShort(InvestigationRecordActivity.this,"审核已经通过,不能修改");
            return;
        }
        InvestigationBean bean = getBean();
        if(TextUtils.isEmpty(search)){
            saveAndCreate(bean);
        }else {
                SavePart3 savePart3 = new SavePart3();
                savePart3.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                    @Override
                    public void success(PostRequestBean bean) {
                        ToastUtils.showShort(InvestigationRecordActivity.this,bean.data);
                        if(bean.code==1){
                            Intent intent = getIntent();
                            setResult(1,intent);
                            finish();
                        }
                    }
                    @Override
                    public void failed() {
                    }
                });
                savePart3.save(bean);
            }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.rl_voice:
                requestVoicePermission();
                setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        Log.d("zhouzhuo","返回结果:"+result);
                        et_detail.getText().insert(et_detail.getSelectionStart(),result);
                    }
                });
                break;
            case R.id.tv_evidence_entering:
                Intent intent = new Intent(this,EvidenceEnteringActivity.class);
                intent.putExtra("id",caseId);
                IntentUtils.startActivity(InvestigationRecordActivity.this,intent);
                break;

        }
    }
}
