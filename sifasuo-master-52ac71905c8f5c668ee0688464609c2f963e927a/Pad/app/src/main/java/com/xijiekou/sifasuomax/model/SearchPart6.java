package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.bean.NewProtocolBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SearchPart6 extends BaseModel{
    public void searchPart6(String id){
        String url = UrlDomainUtil.urlHeader + "/Staff/getCasePart6/case_id/"+id;
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("zhouzhuo","查询协议书"+response);
                        if(response !=null){
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                int code = jsonObject.getInt("code");
                                if(code == 1){
                                    JSONObject object = jsonObject.getJSONObject("data");
                                    Gson gson = new Gson();
                                    NewProtocolBean bean = new NewProtocolBean();
                                    bean.id = object.getString("id");
                                    bean.case_id = object.getString("case_id");
                                    try {
                                        JSONArray array = object.getJSONArray("persons");
                                        ArrayList<NewOralBean> list = new ArrayList<>();
                                        for (int i= 0;i<array.length();i++){
                                            NewOralBean bean1 = gson.fromJson(array.getJSONObject(i).toString(),NewOralBean.class);
                                            list.add(bean1);
                                        }
                                        bean.list = list;
                                    }catch (Exception e){
                                        e.toString();
                                    }
                                    bean.issue = object.getString("issue");
                                    bean.content = object.getString("content");
                                    bean.method = object.getString("method");
                                    bean.num = object.getString("num");
                                    bean.litigant_sign = object.getString("litigant_sign");
                                    bean.mediate_sign = object.getString("mediate_sign");
                                    bean.record_sign = object.getString("record_sign");
                                    bean.date = object.getString("date");
                                    callBackListener.success(bean);
                                }else if(code == 4012){
                                    callBackListener.success(null);
                                }
                            } catch (JSONException e) {
                                Log.d("zhouzhuo","=查询协议书="+e.toString());
                                e.printStackTrace();
                            }
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }
}
