package com.xijiekou.sifasuomax.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.LoginActivity;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.LogoutModel;
import com.xijiekou.sifasuomax.utils.ConstantsUtils;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;


/**
 * Created by zhouzhuo on 2017/7/4.
 */
public class MainTopView extends RelativeLayout implements View.OnClickListener{
    private TextView tv_title;
    private CircleImageView iv_photo;
    private TextView tv_number;
    private ImageView iv_go;
    private TextView tv_back;
    private TextView tv_login_out;
    private RelativeLayout rl_back;
    private Activity activity;

    public MainTopView(Context context) {
        this(context,null);
    }

    public MainTopView(Context context, AttributeSet attrs) {
        super(context, attrs);
        activity = (Activity) context;
        initView(context,attrs);
    }

    private void initView(Context context,AttributeSet attrs) {
        View view  = LayoutInflater.from(context).inflate(R.layout.activity_top_view,this,true);
        tv_title = (TextView) view.findViewById(R.id.tv_title);
        iv_photo = (CircleImageView) view.findViewById(R.id.iv_photo);
        tv_number = (TextView) view.findViewById(R.id.tv_number);
        String loginName = (String) SharePreferenceUtils.get(getContext(),"loginname","");
        if(!TextUtils.isEmpty(loginName)){
            tv_number.setText(loginName);
        }
        String photo =(String) SharePreferenceUtils.get(getContext(),"photo","");

        if(!TextUtils.isEmpty(photo)){
            Glide.with(getContext()).load(photo).into(iv_photo);
        }
        iv_go = (ImageView) view.findViewById(R.id.iv_go);
        iv_go.setOnClickListener(this);
        rl_back = (RelativeLayout) view.findViewById(R.id.rl_back);
        rl_back.setOnClickListener(this);
        tv_login_out = (TextView) view.findViewById(R.id.tv_login_out);
        tv_login_out.setOnClickListener(this);

    }

    public void hideBack(){
        rl_back.setVisibility(View.INVISIBLE);
        iv_photo.setVisibility(View.INVISIBLE);
        tv_number.setVisibility(View.INVISIBLE);
        iv_go.setVisibility(View.INVISIBLE);
        tv_login_out.setVisibility(View.INVISIBLE);
    }

    public void changeTab(int position){
       // tv_title.setText(titles[position]);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rl_back:
                ((Activity)getContext()).finish();
                break;
            case R.id.iv_go:
            case R.id.tv_login_out:
                LogoutModel logoutModel = new LogoutModel();
                String number = (String) SharePreferenceUtils.get(activity,"loginname","");
                logoutModel.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)){
                            PostRequestBean postRequestBean = new Gson().fromJson(message,PostRequestBean.class);
                            if(postRequestBean.code == 1){
                                Intent intent = new Intent(getContext(),LoginActivity.class);
                                IntentUtils.startActivity(getContext(),intent,false,0);
                                ConstantsUtils.bean = null;
                                SharePreferenceUtils.put(getContext(),"login","");
                                activity.finish();
                            }
                        }
                    }

                    @Override
                    public void failed() {

                    }
                });
                logoutModel.logout(number);
                break;
        }
    }

}
