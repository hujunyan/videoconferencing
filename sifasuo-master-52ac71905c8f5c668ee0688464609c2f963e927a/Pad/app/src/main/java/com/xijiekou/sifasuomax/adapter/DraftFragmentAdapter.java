package com.xijiekou.sifasuomax.adapter;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.holder.BaseHolder;
import com.xijiekou.sifasuomax.holder.DraftFragmentHolder;
import com.xijiekou.sifasuomax.utils.ToastUtils;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/7.
 */
public class DraftFragmentAdapter extends BaseAdapter{
    private ArrayList<MainFragmentItemBeans.MainFragmentItemBean> list;
    private Activity activity;
   public DraftFragmentAdapter(FragmentActivity activity, ArrayList<MainFragmentItemBeans.MainFragmentItemBean> list) {
        this.list = list;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        BaseHolder<MainFragmentItemBeans.MainFragmentItemBean> holder;
        if(convertView == null){
            holder = new DraftFragmentHolder(activity);
        }else {
            holder = (BaseHolder<MainFragmentItemBeans.MainFragmentItemBean>) convertView.getTag();
        }
        ((DraftFragmentHolder)holder).setDeleteListener(new DraftFragmentHolder.DeleterListener() {
            @Override
            public void delete() {
                list.remove(position);
                notifyDataSetChanged();
            }
        });
        holder.setData(list.get(position));
        holder.changeItemBg(position);
        return holder.getContentView();
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
