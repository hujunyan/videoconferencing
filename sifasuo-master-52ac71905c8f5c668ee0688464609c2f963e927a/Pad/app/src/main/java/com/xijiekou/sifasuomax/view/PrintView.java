package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/9/5.
 */

public class PrintView extends RelativeLayout{
    private TextView tv_print;
    public PrintView(Context context) {
        this(context,null);
    }

    public PrintView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
       View view = LayoutInflater.from(context).inflate(R.layout.print_view,this,true);
       tv_print = (TextView) view.findViewById(R.id.tv_print);
    }

    public void setEnclick(){
       // tv_print.setBackgroundResource(R.drawable.btn_unselect_month);
        //tv_print.setClickable(false);
    }

}
