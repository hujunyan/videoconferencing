package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.MainActivity;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.NewExplainBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.NewFileExplainModel;
import com.xijiekou.sifasuomax.model.SavePart9;
import com.xijiekou.sifasuomax.model.SearchPart9;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.PrintView;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * 新建卷宗情况说明
 * 3-11
 */

public class NewFileExplainActivity extends VoiceBaseActivity {
    private RelativeLayout rl_voice ;
    private EditText et_detail;
    private TextView tv_confirm;
    private String lastResult="";
    private EditText et_examineYear;
    private String caseId;
    private String search;
    private TextView tv_file_number;
    private String fileName;
    private String filenumber;
    private PrintView printView;
    private MyHandler handler;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    NewExplainBean bean = (NewExplainBean) msg.obj;
                    setTextNotNull(bean.case_info,et_detail);
                    setTextNotNull(bean.auditor,et_examineYear);
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_file_explain);
        initView();
        initData();
    }

    private void initData() {
        SearchPart9 part = new SearchPart9();
        part.setCallBackListener(new BaseModel.CallBackListener<NewExplainBean>() {
            @Override
            public void success(NewExplainBean message) {
                if(message!=null){
                    Message message1 = Message.obtain();
                    message1.what = 0;
                    message1.obj = message;
                    handler.sendMessage(message1);
                }else {
                    search = "";
                }

            }

            @Override
            public void failed() {

            }
        });
        part.searchPart9(caseId);
    }


    private void initView() {
        handler = new MyHandler();
        rl_voice = (RelativeLayout) findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        et_detail = (EditText) findViewById(R.id.et_detail);
        et_detail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult = s.toString();
            }
        });
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);
        tv_confirm.setOnClickListener(this);
        et_examineYear = (EditText) findViewById(R.id.et_examineYear);
        caseId = getIntent().getStringExtra("id");
        search = getIntent().getStringExtra("search");
        fileName = getIntent().getStringExtra("filename");
        filenumber = getIntent().getStringExtra("filenumber");
        tv_file_number = (TextView) findViewById(R.id.tv_file_number);
        if(!TextUtils.isEmpty(filenumber)){
            tv_file_number.setText(filenumber);
        }
        printView = (PrintView) findViewById(R.id.printView);
        printView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.rl_voice:
                requestVoicePermission();
                setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            lastResult+=result;
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail.setText(lastResult);
                        }
                    }
                });
                break;
            case R.id.tv_confirm:

                NewExplainBean bean = new NewExplainBean();
                bean.case_id = caseId;
                bean.case_info = lastResult;
                bean.auditor = et_examineYear.getText().toString().trim();
                if(TextUtils.isEmpty(search)){
                    NewFileExplainModel model = new NewFileExplainModel();
                    model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                        @Override
                        public void success(String message) {
                            if(!TextUtils.isEmpty(message)){
                                try {
                                    JSONObject object = new JSONObject(message);
                                    ToastUtils.showShort(NewFileExplainActivity.this,object.getString("data"));
                                    if(object.getInt("code")==1){
                                        Intent intent = new Intent(NewFileExplainActivity.this, MainActivity.class);
                                        IntentUtils.startActivity(NewFileExplainActivity.this,intent);
                                        finish();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }

                        @Override
                        public void failed() {

                        }
                    });
                    model.doPost(bean);
                }else {
                    SavePart9 savePart9 = new SavePart9();
                    savePart9.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                        @Override
                        public void success(PostRequestBean bean) {
                            ToastUtils.showLong(NewFileExplainActivity.this,bean.data);
                            if(bean.code==1){
                                Intent intent = new Intent(NewFileExplainActivity.this, MainActivity.class);
                                IntentUtils.startActivity(NewFileExplainActivity.this,intent);
                                finish();
                                finish();
                            }
                        }

                        @Override
                        public void failed() {

                        }
                    });
                    savePart9.save(bean);
                }
                break;
        }
    }
}
