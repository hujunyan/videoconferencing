package com.xijiekou.sifasuomax.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.fragment.WavePlayFragment;

/**
 * Created by zhouzhuo on 2017/9/18.
 */

public class WavePlayActivity extends BaseActivity  {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wave_play);

        WavePlayFragment newFragment = new WavePlayFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frameLayout, newFragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }
}
