package com.xijiekou.sifasuomax.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.SeekBar;

import com.piterwilson.audio.MP3RadioStreamDelegate;
import com.piterwilson.audio.MP3RadioStreamPlayer;
import com.shuyu.waveview.AudioWaveView;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.WavePlayActivity;
import com.xijiekou.sifasuomax.utils.DensityUtils;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import static com.xijiekou.sifasuomax.fragment.RecordVoiceFragment.dip2px;

/**
 * Created by zhouzhuo on 2017/9/18.
 */

public class WavePlayFragment extends BaseFragment implements MP3RadioStreamDelegate {
    private WavePlayActivity activity;
    private final static String TAG = "WavePlayActivity";
    AudioWaveView audioWave;
    RelativeLayout activityWavePlay;
    Button playBtn;
    SeekBar seekBar;
    MP3RadioStreamPlayer player;

    Timer timer;

    boolean playeEnd;

    boolean seekBarTouch;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_wave_play, container, false);
        //ButterKnife.bind(this, view);
        initView(view);
        return view;
    }



    private void initView(View view) {
        activity = (WavePlayActivity) getActivity();
        audioWave = (AudioWaveView) view.findViewById(R.id.audioWave);
        playBtn = (Button) view.findViewById(R.id.playBtn);
        playBtn.setOnClickListener(this);
        seekBar = (SeekBar) view.findViewById(R.id.seekBar);


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                play();
            }
        }, 1000);
        playBtn.setEnabled(false);
        seekBar.setEnabled(false);


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBarTouch = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBarTouch = false;
                if (!playeEnd) {
                    player.seekTo(seekBar.getProgress());
                }
            }
        });

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (playeEnd || player == null || !seekBar.isEnabled()) {
                    return;
                }
                long position = player.getCurPosition();
                if (position > 0 && !seekBarTouch) {
                    seekBar.setProgress((int) position);
                }
            }
        }, 1000, 1000);
    }

    private void play() {
        if (player != null) {
            player.stop();
            player.release();
            player = null;
        }
        player = new MP3RadioStreamPlayer();
        //player.setUrlString(this, true, "http://www.stephaniequinn.com/Music/Commercial%20DEMO%20-%2005.mp3");
        player.setUrlString(activity.getIntent().getStringExtra("url"));
        player.setDelegate(this);

        int size = DensityUtils.getScreenWidth(activity)/dip2px(getActivity(),1);//控件默认的间隔是1
        player.setDataList(audioWave.getRecList(), size);

        //player.setStartWaveTime(5000);
        //audioWave.setDrawBase(false);
        audioWave.setBaseRecorder(player);
        audioWave.startView();
        try {
            player.play();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDestroyView() {
        stop();
        super.onDestroyView();
    }

    public void stop() {
        player.stop();
    }




    @Override
    public void onClick(View view) {
        super.onClick(view);
    }

    @Override
    public void onRadioPlayerPlaybackStarted(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                playeEnd = false;
                playBtn.setEnabled(true);
                seekBar.setMax((int) player.getDuration());
                seekBar.setEnabled(true);
            }
        });
    }

    @Override
    public void onRadioPlayerStopped(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                playeEnd = true;
                playBtn.setText("播放");
                playBtn.setEnabled(true);
                seekBar.setEnabled(false);
            }
        });

    }

    @Override
    public void onRadioPlayerError(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                playeEnd = false;
                playBtn.setEnabled(true);
                seekBar.setEnabled(false);
            }
        });

    }


    @Override
    public void onRadioPlayerBuffering(MP3RadioStreamPlayer mp3RadioStreamPlayer) {
        activity.runOnUiThread(new Runnable() {

            @Override
            public void run() {
                playBtn.setEnabled(false);
                seekBar.setEnabled(false);
            }
        });
    }
}
