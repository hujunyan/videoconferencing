package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.ResetPasswordBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by zhouzhuo on 2017/8/8.
 */

public class ResetPasswordModel extends BaseModel{
    public void resetPassword(String oldPwd,String newPwd,String id){
        String url = UrlDomainUtil.urlHeader + "/Staff/changePwd";
        Log.d("zhouzhuo","url==="+url);
        Log.d("zhouzhuo","old==="+oldPwd+"   newPassWord:"+newPwd);
        final HashMap<String, String> map = new HashMap<>();
        map.put("id",id);
        map.put("oldPwd",oldPwd);
        map.put("newPwd",newPwd);
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (callBackListener != null) {
                    if (!TextUtils.isEmpty(s)) {
                        Log.d("zhouzhuo","重置密码:"+s);
                        ResetPasswordBean bean = new Gson().fromJson(s,ResetPasswordBean.class);
                        callBackListener.success(bean);

                    }


                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("zhouzhuo","login :"+volleyError.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        MyApp.getHttpQueue().add(request);
    }
}
