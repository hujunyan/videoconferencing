package com.xijiekou.sifasuomax.model;

import android.content.Context;
import android.util.Log;

import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static java.lang.String.valueOf;

/**
 * Created by zhouzhuo on 2017/7/12.
 */

public class VideoUploadModel extends BaseModel{
    public void videoUpload(Context context,String caseId, String path,String photoId) {
        String url = UrlDomainUtil.urlHeader +"/Staff/vedioUpload";
       // Log.d("zhouzhuo","视频 path:"+path);
        //path = "/storage/emulated/0/videorecord/recording1025804478.mp4";
        File file = new File(path);
        OkHttpClient client = new OkHttpClient();
        // form 表单形式上传
        final MultipartBody.Builder requestBody = new MultipartBody.Builder().setType(MultipartBody.FORM);
        // MediaType.parse() 里面是上传的文件类型。
        RequestBody body = RequestBody.create(MediaType.parse("*/*"), file);
        // 参数分别为， 请求key ，文件名称 ， RequestBody
        requestBody.addFormDataPart("file", file.getName(), body);
       Map<String,String> map  = new HashMap<>();
        map.put("file_id",photoId);
        map.put("case_id",caseId);
        // map 里面是请求中所需要的 key 和 value
        for (Map.Entry entry : map.entrySet()) {
            requestBody.addFormDataPart(valueOf(entry.getKey()), valueOf(entry.getValue()));
        }

        Request request = new Request.Builder().url(url).post(requestBody.build()).tag(context).build();
        // readTimeout("请求超时时间" , 时间单位);
        client.newBuilder().readTimeout(5000, TimeUnit.MILLISECONDS).build().newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.d("zhouzhuo","失败==111="+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {

                    String str = response.body().string();
                    callBackListener.success(str);
                } else {
                    Log.d("zhouzhuo","失败=2222=="+response.toString());
                    callBackListener.failed();
                }
            }
        });

       /* OkHttpUtils
                .postFile()
                .url(url)
                .file(new File(path))
                .build()
                .execute(new Callback() {
                    @Override
                    public Object parseNetworkResponse(Response response, int id) throws Exception {
                        return null;
                    }

                    @Override
                    public void onError(Call call, Exception e, int id) {

                    }

                    @Override
                    public void onResponse(Object response, int id) {
                        Log.d("zhouzhuo","上传图片成功:"+response);

                    }
                });*/
    }



}
