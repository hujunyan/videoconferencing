package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.MediationRecordBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SearchPart5 extends BaseModel{
    public void searchPart5(String id){
        String url = UrlDomainUtil.urlHeader + "/Staff/getCasePart5/case_id/"+id;
        Log.d("zhozhuo","查询part5=="+url);
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        //MediationRecordBean
                        if(response !=null){
                            try {
                                JSONObject object = new JSONObject(response);
                                int code = object.getInt("code");
                                if(code == 1){
                                    Gson gson = new Gson();
                                    MediationRecordBean bean = gson.fromJson(object.getJSONObject("data").toString(),MediationRecordBean.class);
                                    callBackListener.success(bean);
                                }else if(code == 4012){
                                    callBackListener.success(null);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                        Log.d("zhouzhuo","查询调解记录=="+response);


                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }
}
