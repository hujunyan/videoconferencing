package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.adapter.NewOralAdapter;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.bean.NewProtocolBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.dialog.CommonDialog;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.NewProtocolModel;
import com.xijiekou.sifasuomax.model.PrintModel2;
import com.xijiekou.sifasuomax.model.SavePart6;
import com.xijiekou.sifasuomax.model.SearchPart6;
import com.xijiekou.sifasuomax.utils.BeanToJsonUtils;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.DoubleVoiceEditTextView;
import com.xijiekou.sifasuomax.view.NoScrollListView;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/28.
 * 新建协议书 3-8
 */

public class NewProtocolBookActivity extends VoiceBaseActivity {
    private TextView tv_evidence_entering;
    private NoScrollListView listView;
    private ArrayList<NewOralBean> list;
    private TextView tv_add;
    private NewOralAdapter adapter;
    private String caseId;
    private DoubleVoiceEditTextView rl_voiceView;
    private MyHandler handler;
    private String search;
    private TextView tv_file_number;
    private String fileName;
    private String filenumber;
    private BottomConfirmView bottom_confirm_view;
    private String audit;


    public class MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    NewProtocolBean message = (NewProtocolBean) msg.obj;
                    rl_voiceView.setData(message);
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_book_protocol);
        initView();
        initData();
        initPerson();
    }
    private String argue;

    private void initPerson() {
       // if(TextUtils.isEmpty(search)){
            list = (ArrayList<NewOralBean>) getIntent().getSerializableExtra("person");
            if(list!=null&&list.size()>0){
                setAdapter();
            }
            argue = getIntent().getStringExtra("argue");
            Log.d("zhouzhuo",argue);
            rl_voiceView.setArgue(argue);

       // }
    }


    private void initView() {
        handler = new MyHandler();
        tv_evidence_entering = (TextView) findViewById(R.id.tv_evidence_entering);
        tv_evidence_entering.setOnClickListener(this);
        listView = (NoScrollListView) findViewById(R.id.lv_list);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String type =list.get(position).type;
                switch (type){
                    case "1":
                        Intent intent1 = new Intent(NewProtocolBookActivity.this,AddOnePersonActivity.class);
                        intent1.putExtra("bean",list.get(position));
                        intent1.putExtra("position",position);
                        IntentUtils.startActivity(NewProtocolBookActivity.this,intent1,true,1);
                        break;
                    case "2":
                        intent1 = new Intent(NewProtocolBookActivity.this,AddTwoPersonActivity.class);
                        intent1.putExtra("bean",list.get(position));
                        intent1.putExtra("position",position);
                        IntentUtils.startActivity(NewProtocolBookActivity.this,intent1,true,2);
                        break;
                    case "3":
                        intent1 = new Intent(NewProtocolBookActivity.this,AddThreePersonActivity.class);
                        intent1.putExtra("bean",list.get(position));
                        intent1.putExtra("position",position);
                        IntentUtils.startActivity(NewProtocolBookActivity.this,intent1,true,3);
                        break;
                }
            }
        });
        listView.setDivider(getResources().getDrawable(R.drawable.new_oral_listview_divider));
        listView.setDividerHeight(15);
        tv_add = (TextView) findViewById(R.id.tv_add);
        tv_add.setOnClickListener(this);
        caseId = getIntent().getStringExtra("caseId");
        search = getIntent().getStringExtra("search");
        audit = getIntent().getStringExtra("audit");

        rl_voiceView = (DoubleVoiceEditTextView) findViewById(R.id.rl_voiceView);
        tv_file_number = (TextView) findViewById(R.id.tv_file_number);

        fileName = getIntent().getStringExtra("filename");
        filenumber = getIntent().getStringExtra("filenumber");
        if(!TextUtils.isEmpty(filenumber)){
            tv_file_number.setText(filenumber);
        }
        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setSaveListener(new BottomConfirmView.SaveListener() {
            @Override
            public void save() {
               /* NewProtocolBean bean = getBean();
                bean.isAudit = "1";
                saveAndCreate(bean);*/
            }
        });
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {
                PrintModel2 model = new PrintModel2();
                NewProtocolBean bean = getBean();
                bean.issue = argue+"\n"+rl_voiceView.getLastResult();
                Log.d("zhouzhuo","issue:"+bean.issue);
                String content = BeanToJsonUtils.part7(bean);
                model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)&&message.equals("success")){
                            ToastUtils.showShort(NewProtocolBookActivity.this,"打印成功,请勿重复点击");
                        }else {
                            ToastUtils.showShort(NewProtocolBookActivity.this,"打印失败");
                        }
                    }

                    @Override
                    public void failed() {
                        ToastUtils.showShort(NewProtocolBookActivity.this,"打印失败,请检查打印服务是否开启");
                    }
                });
                String ip = (String) SharePreferenceUtils.get(NewProtocolBookActivity.this,"printIp","");
                if(TextUtils.isEmpty(ip)||ip.equals("1")){
                    ToastUtils.showShort(NewProtocolBookActivity.this,"暂不支持打印");
                }else {
                    model.print(content,"printB5",ip);
                }
            }
        });


    }

    private NewProtocolBean getBean(){
        NewProtocolBean bean = new NewProtocolBean();
        bean.list = list;
        bean.case_id = caseId;
        bean.issue = rl_voiceView.getLastResult();
        bean.content = rl_voiceView.getLastResult2();
        bean.method = rl_voiceView.getPerformance();
        bean.num = rl_voiceView.getNumber();
        bean.filenumber = filenumber;
        bean.litigant_sign = rl_voiceView.getSignOne();
        bean.mediate_sign = rl_voiceView.getSignTwo();
        bean.record_sign = rl_voiceView.getSignThree();
        bean.date = rl_voiceView.getDate();
        return bean;
    }
    private void  saveAndCreate(NewProtocolBean bean){
        NewProtocolModel model = new NewProtocolModel();
        model.setCallBackListener(new BaseModel.CallBackListener() {
            @Override
            public void success(Object message) {
                Intent intent = getIntent();
                setResult(0,intent);
                finish();
            }

            @Override
            public void failed() {

            }
        });
        model.postWritten(bean);
    }

    private void next() {
        if(!TextUtils.isEmpty(audit)&&audit.equals("1")){
            ToastUtils.showShort(NewProtocolBookActivity.this,"审核已经通过，不能修改");
            return;
        }
        NewProtocolBean bean = getBean();
        if(TextUtils.isEmpty(search)){
            saveAndCreate(bean);
        }else {
                SavePart6 savePart6 = new SavePart6();
                savePart6.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                    @Override
                    public void success(PostRequestBean bean) {
                        ToastUtils.showLong(NewProtocolBookActivity.this,bean.data);
                        if(bean.code==1){
                            Intent intent = getIntent();
                            setResult(1,intent);
                            finish();
                        }
                    }

                    @Override
                    public void failed() {

                    }
                });
                savePart6.save(bean);
        }
    }


    private void initData() {
        SearchPart6 part = new SearchPart6();
        part.setCallBackListener(new BaseModel.CallBackListener<NewProtocolBean>() {
            @Override
            public void success(NewProtocolBean message) {
                if(message!=null){
                    search = "search";
                    Message message1 = Message.obtain();
                    message1.what = 0;
                    message1.obj = message;
                    handler.sendMessage(message1);
                }else {
                    bottom_confirm_view.hidePrint();
                }
            }

            @Override
            public void failed() {

            }
        });
        part.searchPart6(caseId);

    }

    @Override
    public void onClick(View v) {//VisitRecordActivityVisitRecordActivity
        super.onClick(v);
        switch (v.getId()){
            case R.id.tv_evidence_entering:
                Intent intent = new Intent(this,EvidenceEnteringActivity.class);
                intent.putExtra("id",caseId);
                IntentUtils.startActivity(NewProtocolBookActivity.this,intent);
                break;
            case R.id.tv_add:
                CommonDialog dialog = new CommonDialog(NewProtocolBookActivity.this);
                dialog.setListener(new CommonDialog.SelectTypeListener() {
                    @Override
                    public void click(int type) {
                        switch (type){
                            case 0:
                                Intent intent1 = new Intent(NewProtocolBookActivity.this,AddOnePersonActivity.class);
                                IntentUtils.startActivity(NewProtocolBookActivity.this,intent1,true,1);
                                break;
                            case 1:
                                intent1 = new Intent(NewProtocolBookActivity.this,AddTwoPersonActivity.class);
                                IntentUtils.startActivity(NewProtocolBookActivity.this,intent1,true,2);
                                break;
                            case 2:
                                intent1 = new Intent(NewProtocolBookActivity.this,AddThreePersonActivity.class);
                                IntentUtils.startActivity(NewProtocolBookActivity.this,intent1,true,3);
                                break;
                        }
                    }
                });
                dialog.show();
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(data!=null){
            switch (requestCode){
                case 1:
                    switch (resultCode){
                        case 1:
                            NewOralBean bean = (NewOralBean) data.getSerializableExtra("bean");
                            bean.type = "1";
                            updateList(bean);
                            break;
                        case 10:
                            bean = (NewOralBean) data.getSerializableExtra("bean");
                            int position = data.getIntExtra("position",-1);
                            if(position!=-1){
                                update(position,bean);
                            }
                            break;
                    }
                    break;
                case 2:
                    switch (resultCode){
                        case 2:
                            NewOralBean bean = (NewOralBean) data.getSerializableExtra("bean");
                            bean.type = "2";
                            updateList(bean);
                            break;
                        case 20:
                            bean = (NewOralBean) data.getSerializableExtra("bean");
                            int position = data.getIntExtra("position",-1);
                            if(position!=-1){
                                update(position,bean);
                            }
                            break;
                    }

                    break;
                case 3:
                    switch (resultCode){
                        case 3:
                            NewOralBean bean = (NewOralBean) data.getSerializableExtra("bean");
                            bean.type = "3";
                            updateList(bean);
                            break;
                        case 30:
                            bean = (NewOralBean) data.getSerializableExtra("bean");
                            int position = data.getIntExtra("position",-1);
                            if(position!=-1){
                                update(position,bean);
                            }
                            break;
                    }
                    break;
            }
        }
    }

    private void update(int position,NewOralBean bean){
        list.remove(position);
        list.add(position,bean);
        NewOralAdapter  adapter = new NewOralAdapter(this,list);
        listView.setAdapter(adapter);

    }

    private void setAdapter(){
            adapter = new NewOralAdapter(this,list);
            listView.setAdapter(adapter);
    }

    private void updateList(NewOralBean bean) {
        if(list == null){
            list = new ArrayList<>();
        }
        list.add(bean);
        setAdapter();

    }
}
