package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/7/4.
 */
public class MultimediaBean {
    public String id;//编号
    public String type;//照片,音频,视频
    public String case_id;
    public String url;
    public String remark;
    public String datetime;
    public String create_time;
    public String status ="1";//状态  1 为上传  2 已上传

}
