package com.xijiekou.sifasuomax.holder;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.utils.UIUtils;

/**
 * Created by zhouzhuo on 2017/7/31.
 */

public class MainFragmentHolder extends BaseHolder<MainFragmentItemBeans.MainFragmentItemBean> {
    private TextView tv_fileNumber;//卷号
    private TextView tv_fileName;//卷名
    private TextView tv_event;//
    private TextView tv_state;//人民调解员
    private TextView tv_mediationTime;//调解时间
    private LinearLayout ll_bg;

    public MainFragmentHolder(Activity activity) {
        super(activity);
    }

    /**
     * TextView tv_fileNumber;//卷号
     TextView tv_fileName;//卷名
     TextView tv_units;//所属单位
     TextView tv_mediators;//人民调解员
     TextView tv_mediationTime;//调解时间
     * @return
     */

    @Override
    protected View initView() {
        View view = UIUtils.inflate(R.layout.main_fragment_item);
        ll_bg = (LinearLayout) view.findViewById(R.id.ll_bg);
        tv_fileNumber = (TextView) view.findViewById(R.id.tv_fileNumber);
        tv_fileName = (TextView) view.findViewById(R.id.tv_fileName);
        tv_event = (TextView) view.findViewById(R.id.tv_event);
        tv_state = (TextView) view.findViewById(R.id.tv_state);
        tv_mediationTime = (TextView) view.findViewById(R.id.tv_mediationTime);
        return view;
    }

    @Override
    protected void refreshView() {
        MainFragmentItemBeans.MainFragmentItemBean bean = getInfo();
        tv_fileNumber.setText(bean.filenum);
        tv_fileName.setText(bean.filename);
       // String type = bean.casetype;
        /*if("0".equals(type)){
            tv_event.setText("");
        }else {
            tv_event.setText(bean.argue);
        }*/

        changState(bean.audit);
        if(!TextUtils.isEmpty(bean.date)){
            tv_mediationTime.setText(bean.date.split("\\ ")[0]);
        }

    }
    //改变状态
    private void changState(String audit) {
        if(audit.equals("0")){
            tv_state.setText("未审核");
        }else {
            tv_state.setText("已审核");
        }
    }

    @Override
    public void changeItemBg(int position) {
        super.changeItemBg(position);
        if(position%2 ==0){
            ll_bg.setBackgroundColor(Color.parseColor("#daf0fd"));
        }else {
            ll_bg.setBackgroundColor(Color.parseColor("#f2ffe3"));
        }
    }


}
