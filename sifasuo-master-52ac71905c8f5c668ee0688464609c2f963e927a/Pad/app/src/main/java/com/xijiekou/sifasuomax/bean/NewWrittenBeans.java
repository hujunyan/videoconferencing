package com.xijiekou.sifasuomax.bean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/8/4.
 */

public class NewWrittenBeans {
    public String isAudit;
    public ArrayList<NewOralBean> list;
    public String case_id;
    public String  filing;
    public String   simple;
    public String   content;
    public String   source;
    public String   issuetype;
    public String   signurl;
    public String   date;


}
