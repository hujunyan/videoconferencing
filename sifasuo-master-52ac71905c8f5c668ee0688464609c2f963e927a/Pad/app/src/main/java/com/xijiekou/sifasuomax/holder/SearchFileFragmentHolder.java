package com.xijiekou.sifasuomax.holder;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.FileMenuActivity;
import com.xijiekou.sifasuomax.activity.NewOralActivity;
import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.UIUtils;

/**
 * Created by zhouzhuo on 2017/7/26.
 */

public class SearchFileFragmentHolder extends BaseHolder<MainFragmentItemBeans.MainFragmentItemBean> implements View.OnClickListener{
    LinearLayout ll_bg;
    TextView tv_fileNumber;//卷号
    TextView tv_fileName;//卷名
    TextView tv_unit;//所属单位
    TextView tv_mediators;//人民调解员
    TextView tv_mediationTime;//调解时间
    TextView tv_operator;//操作
    TextView tv_status;//状态
    private String id;

    public SearchFileFragmentHolder(Activity activity) {
        super(activity);
    }


    @Override
    protected View initView() {
        View convertView = UIUtils.inflate(R.layout.search_file_fragment_item);
        ll_bg = (LinearLayout) convertView.findViewById(R.id.ll_bg);
        tv_fileNumber = (TextView) convertView.findViewById(R.id.tv_fileNumber);
        tv_fileName = (TextView) convertView.findViewById(R.id.tv_fileName);
        tv_unit = (TextView) convertView.findViewById(R.id.tv_unit);
        tv_mediators = (TextView) convertView.findViewById(R.id.tv_mediators);
        tv_mediationTime = (TextView) convertView.findViewById(R.id.tv_mediationTime);
        tv_operator = (TextView) convertView.findViewById(R.id.tv_operator);
        tv_operator.setOnClickListener(this);
        tv_status = (TextView) convertView.findViewById(R.id.tv_status);
        return convertView;
    }
    private String type;

    @Override
    protected void refreshView() {
        MainFragmentItemBeans.MainFragmentItemBean bean = getInfo();
        id = bean.id;
        type = bean.casetype;
        tv_fileNumber.setText(bean.filenum);
        tv_fileName.setText(bean.filename);
        //tv_unit.setText(bean.company);
       // tv_mediators.setText(bean.signature);
        if(!TextUtils.isEmpty(bean.date)){
            tv_mediationTime.setText(bean.date.split("\\ ")[0]);
        }
            String status = bean.status;
            if(!TextUtils.isEmpty(status)){
                switch (status){
                    case "1":
                        tv_status.setText("调解成功");
                        break;
                    case "2":
                        tv_status.setText("调解失败");
                        break;
                    case "0":
                        tv_status.setText("有待继续沟通");
                        break;
                    default:
                        tv_status.setText("调解成功");
                        break;
                }
            }
    }

    @Override
    public void changeItemBg(int position) {
        super.changeItemBg(position);
        if(position%2 ==0){
            ll_bg.setBackgroundColor(Color.parseColor("#daf0fd"));
        }else {
            ll_bg.setBackgroundColor(Color.parseColor("#f2ffe3"));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
          case R.id.tv_operator:
                Intent intent;
                if(type.equals("0")){
                    intent= new Intent(activity,NewOralActivity.class);
                    intent.putExtra("caseId",id);
                    IntentUtils.startActivity(activity,intent,false,0);
                }else {
                    intent= new Intent(activity,FileMenuActivity.class);
                    intent.putExtra("caseId",id);
                    IntentUtils.startActivity(activity,intent,false,1);
                }
                break;
        }
    }
}
