package com.xijiekou.sifasuomax.holder;

import android.app.Activity;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.SelectMonthBean;
import com.xijiekou.sifasuomax.utils.UIUtils;

/**
 * Created by zhouzhuo on 2017/8/7.
 */

public class SelectMonthHolder extends BaseHolder<SelectMonthBean>{
    private TextView tv_content;
    private RelativeLayout rl_bg;
    private ImageView iv_add;
    //0 开始
    public SelectMonthHolder(Activity activity) {
        super(activity);
        this.activity = activity;
    }

    @Override
    protected View initView() {
        View view = UIUtils.inflate(R.layout.select_month_item);
        tv_content = (TextView) view.findViewById(R.id.tv_content);
        rl_bg = (RelativeLayout) view.findViewById(R.id.rl_bg);
        iv_add = (ImageView) view.findViewById(R.id.iv_add);
        return view;
    }

    @Override
    protected void refreshView() {
        SelectMonthBean bean = getInfo();
        String content = bean.month;
        if(!TextUtils.isEmpty(content)){
            tv_content.setText(content);
        }
        int state = bean.state;
        if(bean.isClick){
            //已经添加过了
            if(state ==0){
                iv_add.setVisibility(View.VISIBLE);
            }else {
                iv_add.setVisibility(View.INVISIBLE);
            }
            tv_content.setTextColor(Color.parseColor("#ffffff"));
            rl_bg.setBackgroundResource(R.drawable.btn_select_month);
        }else {
            iv_add.setVisibility(View.INVISIBLE);
            rl_bg.setBackgroundResource(R.drawable.btn_unselect_month);
            tv_content.setTextColor(Color.parseColor("#999999"));
        }
    }


}
