package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.EvidenceMateriaBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.JudicialConfirmModel;
import com.xijiekou.sifasuomax.model.PrintModel2;
import com.xijiekou.sifasuomax.model.SavePart8;
import com.xijiekou.sifasuomax.model.SearchPart8;
import com.xijiekou.sifasuomax.utils.BeanToJsonUtils;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.YearMonthDayView;

/**
 * 其他材料
 * 3-10
 */

public class JudicialConfirmActivity extends VoiceBaseActivity {
    private String caseId;
    private EditText et_one;//立卷人
    private EditText et_two;//审核人
    private YearMonthDayView rl_date;//
    private TextView tv_evidence_entering;
    private String search;
    private TextView tv_file_number;
    private String filenumber;
    private EditText et_detail;
    private String lastResult="";
    private RelativeLayout rl_voice;
    private BottomConfirmView bottom_confirm_view;
    private String audit;


    private MyHandler handler;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    EvidenceMateriaBean bean = (EvidenceMateriaBean) msg.obj;
                    if(!TextUtils.isEmpty(bean.filing)){
                        et_one.setText(bean.filing);
                    }
                    if(!TextUtils.isEmpty(bean.auditor)){
                        et_two.setText(bean.auditor);
                    }
                    if(!TextUtils.isEmpty(bean.explain)){
                        et_detail.setText(bean.explain);
                        lastResult = bean.explain;
                    }
                    rl_date.setData(bean.datetime);
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_judical_confirm);
        initView();
        initData();
    }

    private void initView() {
        handler = new MyHandler();
        caseId = getIntent().getStringExtra("caseId");
        search = getIntent().getStringExtra("search");
        audit = getIntent().getStringExtra("audit");
        et_one = (EditText) findViewById(R.id.et_one);
        et_two = (EditText) findViewById(R.id.et_two);
        rl_voice = (RelativeLayout) findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        rl_date = (YearMonthDayView) findViewById(R.id.rl_date);
        tv_evidence_entering = (TextView) findViewById(R.id.tv_evidence_entering);
        tv_evidence_entering.setOnClickListener(this);
        et_detail = (EditText) findViewById(R.id.et_detail);
        et_detail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult = s.toString();
            }
        });

        tv_file_number = (TextView) findViewById(R.id.tv_file_number);
        filenumber = getIntent().getStringExtra("filenumber");
        if(!TextUtils.isEmpty(filenumber)){
            tv_file_number.setText(filenumber);
        }
        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setSaveListener(new BottomConfirmView.SaveListener() {
            @Override
            public void save() {
               /* EvidenceMateriaBean bean = getBean();
                bean.isAudit = "1";
                saveAndCreate(bean);*/
            }
        });
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {
                PrintModel2 model = new PrintModel2();
                EvidenceMateriaBean bean = getBean();
                String content = BeanToJsonUtils.part6(bean);
                Log.d("zhouzhuo","content:"+content);
                model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)&&message.equals("success")){
                            ToastUtils.showShort(JudicialConfirmActivity.this,"打印成功,请勿重复点击");
                        }else {
                            ToastUtils.showShort(JudicialConfirmActivity.this,"打印失败");
                        }
                    }

                    @Override
                    public void failed() {
                        ToastUtils.showShort(JudicialConfirmActivity.this,"打印失败,请检查打印服务是否开启");
                    }
                });
                String ip = (String) SharePreferenceUtils.get(JudicialConfirmActivity.this,"printIp","");
                if(TextUtils.isEmpty(ip)||ip.equals("1")){
                    ToastUtils.showShort(JudicialConfirmActivity.this,"暂不支持打印");
                }else {
                    model.print(content,"printB9",ip);
                }
            }
        });

    }

    private  EvidenceMateriaBean getBean(){
        EvidenceMateriaBean bean = new EvidenceMateriaBean();
        bean.case_id = caseId;
        bean.auditor = et_one.getText().toString().trim();
        bean.filing  = et_two.getText().toString().trim();
        bean.explain = lastResult;
        bean.datetime = rl_date.getDate();
        return bean;
    }
    private void saveAndCreate(EvidenceMateriaBean bean){
        JudicialConfirmModel materialModel = new JudicialConfirmModel();
        materialModel.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                Intent intent = getIntent();
                setResult(0,intent);
                finish();
            }

            @Override
            public void failed() {

            }
        });
        materialModel.registration(bean);
    }

    private void next() {
        if(!TextUtils.isEmpty(audit)&&audit.equals("1")){
            ToastUtils.showShort(JudicialConfirmActivity.this,"审核已经通过，不能修改");
            return;
        }
        EvidenceMateriaBean bean = getBean();
        if(TextUtils.isEmpty(search)){
            Log.d("zhouzhuo","创建");
            saveAndCreate(bean);
        }else {
            Log.d("zhouzhuo","保存");
                SavePart8 savePart8 = new SavePart8();
                savePart8.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                    @Override
                    public void success(PostRequestBean bean) {

                        // ToastUtils.showLong(JudicialConfirmActivity.this,bean.data);
                        if(bean.code==1){
                            Intent intent = getIntent();
                            setResult(1,intent);
                            finish();
                        }
                    }

                    @Override
                    public void failed() {

                    }
                });
                savePart8.save(bean);
            }
    }

    private void initData() {
        SearchPart8 part = new SearchPart8();
        part.setCallBackListener(new BaseModel.CallBackListener<EvidenceMateriaBean>() {
            @Override
            public void success(EvidenceMateriaBean message) {
                Log.d("zhouzhuo","查询==="+message);
                if(message!=null){
                    Message message1 = Message.obtain();
                    message1.what = 0;
                    message1.obj = message;
                    handler.sendMessage(message1);
                    search = "search";
                }else {
                    bottom_confirm_view.hidePrint();
                }

            }

            @Override
            public void failed() {

            }
        });
        part.searchPart8(caseId);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.tv_evidence_entering:
                Intent intent = new Intent(this,EvidenceEnteringActivity.class);
                intent.putExtra("id",caseId);
                IntentUtils.startActivity(JudicialConfirmActivity.this,intent);
                break;
            case R.id.rl_voice:
                requestVoicePermission();
                setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail.getText().insert(et_detail.getSelectionStart(),result);
                        }

                    }
                });
                break;
        }
    }
}
