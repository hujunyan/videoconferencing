package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/8/5.
 */

public class MediationRecordBean {
    public String case_id;
    public String date;
    public String time_range;
    public String time;
    public String location;
    public String litigant;
    public String attend;
    public String record;
    public String result;
    public String litigant_sign;
    public String attend_sign;
    public String record_sign;
    public String formdate;
    public String  create_time;
    public String isAudit;
}
