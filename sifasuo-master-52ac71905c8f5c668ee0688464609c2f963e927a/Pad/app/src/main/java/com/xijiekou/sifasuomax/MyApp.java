package com.xijiekou.sifasuomax;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.graphics.Color;
import android.os.Handler;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.yuyh.library.imgsel.ImageLoader;
import com.yuyh.library.imgsel.ImgSelConfig;
import com.zhy.http.okhttp.OkHttpUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Created by zhouzhuo on 2017/7/5.
 */
public class MyApp extends Application{
    public static ImgSelConfig config;
    private List<Activity> activities = new ArrayList<Activity>();
    public static MyApp myApp;
    public static RequestQueue queue;
    private static int myTid;
    private static Handler handler;

    @Override
    public void onCreate() {
        super.onCreate();
        myApp = this;
        myTid = android.os.Process.myTid();
        handler=new Handler();
        queue = Volley.newRequestQueue(this);

        new Thread(new Runnable() {
            @Override
            public void run() {
                CrashHandler.getInstance().init(myApp);
                OkHttpClient okHttpClient = new OkHttpClient.Builder()
//                .addInterceptor(new LoggerInterceptor("TAG"))
                        .connectTimeout(10000L, TimeUnit.MILLISECONDS)
                        .readTimeout(10000L, TimeUnit.MILLISECONDS)
                        //其他配置
                        .build();

                OkHttpUtils.initClient(okHttpClient);
                ImageLoader loader = new ImageLoader() {
                    @Override
                    public void displayImage(Context context, String path, ImageView imageView) {
                        Glide.with(context).load(path).into(imageView);
                    }
                };

                config = new ImgSelConfig.Builder(MyApp.this, loader)
                        // 是否多选
                        .multiSelect(false)
                        .btnText("Confirm")
                        // 确定按钮背景色
                        //.btnBgColor(Color.parseColor(""))
                        // 确定按钮文字颜色
                        .btnTextColor(Color.WHITE)
                        // 使用沉浸式状态栏
                        .statusBarColor(Color.parseColor("#3F51B5"))
                        // 返回图标ResId
                        .backResId(R.drawable.back_white)
                        .title("照片")
                        .titleColor(Color.WHITE)
                        .titleBgColor(Color.parseColor("#3F51B5"))
                        .allImagesText("All Images")
                        .needCrop(false)
                        .cropSize(1, 1, 200, 200)
                        // 第一个是否显示相机
                        .needCamera(true)
                        // 最大选择图片数量
                        .maxNum(9)
                        .build();
            }
        }).start();

    }


    public static RequestQueue getHttpQueue() {
        return queue;
    }

    public void add(Activity activity){
        activities.add(0,activity);
    }

    public void removeActivity(Activity activity){
        activities.remove(activity);
    }

    public void exit(){
        for (int i = 0;i<activities.size();i++){
            activities.get(i).finish();
            Log.d("zhouzhuo","activity:"+activities.get(i).toString());
            System.exit(0);
        }
    }
    public static int getMyTid() {
        return myTid;
    }
    public static Handler getHandler() {
        return handler;
    }

    public ImgSelConfig getImageLoaderConfig(){
        return config;
    }

}
