package com.xijiekou.sifasuomax.utils;

import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.xijiekou.sifasuomax.MyApp;

public class UIUtils {
	private UIUtils(){}
	public static int dip2px(int dip) {
		final float scale = getContext().getResources().getDisplayMetrics().density;
		return (int) (dip * scale + 0.5f);
	}

	/** pxzת��dip */

	public static int px2dip(int px) {
		final float scale = getContext().getResources().getDisplayMetrics().density;
		return (int) (px / scale + 0.5f);
	}

	public static String[] getStringArray(int id) {
		return getResource().getStringArray(id);
	}

	public static Resources getResource() {
		return getContext().getResources();
	}

	public static MyApp getContext() {
		return MyApp.myApp;
	}

	public static int getColor(int id) {
		return getResource().getColor(id);
	}

	public static void showToast(String str) {
		Toast.makeText(getContext(), str, Toast.LENGTH_SHORT).show();
	}

	public static void showToast(int str_id) {
		Toast.makeText(getContext(), str_id, Toast.LENGTH_SHORT).show();
	}

	/**
	 * ����view����
	 * 
	 * @param layout_id
	 *            ����id
	 * @return
	 */
	public static View inflate(int layout_id) {
		return View.inflate(getContext(), layout_id, null);
	}

	/**
	 * ��֤���������߳�������
	 * 
	 * @param runnable
	 */
	public static void runOnUiThread(Runnable runnable) {
		if (isOnMainThread()) {
			runnable.run();
		} else {
			execute(runnable);
		}
	}

	/**
	 * ��ȡ���߳��е�Handler
	 * 
	 * @return
	 */
	private static Handler getMainHandler() {
		return MyApp.getHandler();
	}

	public static void execute(Runnable runnable) {
		getMainHandler().post(runnable);
	}

	public static void executeDelay(Runnable runnable, long delayTime) {
		getMainHandler().postDelayed(runnable, delayTime);
	}

	public static void cancelRunnable(Runnable runnable) {
		getMainHandler().removeCallbacks(runnable);
	}

	/**
	 * �жϳ����Ƿ������߳�����
	 * 
	 * @return
	 */
	private static boolean isOnMainThread() {
		// ���Ȼ�ȡ�����̵߳�tid == ���жϵ�ǰ�̵߳�tid
		return MyApp.getMyTid() == android.os.Process.myTid();
	}

	public static Drawable getDrawable(int id) {
		return getResource().getDrawable(id);
	}


	public static String getString(int id) {
		return getResource().getString(id);
	}

}
