package com.xijiekou.sifasuomax.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.holder.BaseHolder;
import com.xijiekou.sifasuomax.holder.SearchFileFragmentHolder;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/7.
 */
public class SearchFileFragmentAdapter extends BaseAdapter{
    private ArrayList<MainFragmentItemBeans.MainFragmentItemBean> list;
    private Activity activity;
   public SearchFileFragmentAdapter(Activity activity,ArrayList<MainFragmentItemBeans.MainFragmentItemBean> list) {
        this.list = list;
        this.activity =activity;
    }

    @Override
    public int getCount() {
        if(list==null) return 0;
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseHolder<MainFragmentItemBeans.MainFragmentItemBean> holder;
        if(convertView == null){
            holder = new SearchFileFragmentHolder(activity);
            // convertView.setTag(holder);
        }else {
            holder = (BaseHolder<MainFragmentItemBeans.MainFragmentItemBean>) convertView.getTag();
        }
        holder.setData(list.get(position));
        holder.changeItemBg(position);
        return holder.getContentView();
    }

}
