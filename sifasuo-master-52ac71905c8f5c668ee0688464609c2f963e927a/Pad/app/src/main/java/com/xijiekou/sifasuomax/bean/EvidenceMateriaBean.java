package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/8/5.
 */

public class EvidenceMateriaBean {
    public String case_id;
    public String auditor;
    public String litigant;
    public String date;
    public String filing;
    public String explain;
    public String create_time;
    public String datetime;
    public String isAudit;
}
