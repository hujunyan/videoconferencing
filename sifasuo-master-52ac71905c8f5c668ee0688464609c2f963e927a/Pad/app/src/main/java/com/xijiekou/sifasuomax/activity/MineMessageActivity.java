package com.xijiekou.sifasuomax.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.adapter.MessageAdapter;
import com.xijiekou.sifasuomax.bean.MessageBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.MessageModel;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshBase;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshListView;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/8/21.
 */

public class MineMessageActivity extends BaseActivity {
    private ListView mListView;
    private PullToRefreshListView mRefreshListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mine_message);
        initView();
        initData();
    }
    private void initData() {
        MessageModel model = new MessageModel();
        model.getMessage();
        model.setCallBackListener(new BaseModel.CallBackListener<ArrayList<MessageBean.MessageChildBean>>() {
            @Override
            public void success(ArrayList<MessageBean.MessageChildBean> list ) {
                mListView.setAdapter(new MessageAdapter(MineMessageActivity.this,list));
            }

            @Override
            public void failed() {

            }
        });
    }

    private void initView() {
        mRefreshListView = (PullToRefreshListView) findViewById(R.id.lv_list);
        mListView = mRefreshListView.getRefreshableView();
        mListView.setDivider(getResources().getDrawable(R.drawable.listview_dash));
        mListView.setDividerHeight(10);
        mListView.setVerticalScrollBarEnabled(false);
        mRefreshListView.setPullLoadEnabled(false);
        mRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                mRefreshListView.onPullDownRefreshComplete();
                initData();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {

            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            }
        });
    }
}
