package com.xijiekou.sifasuomax.holder;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.UIUtils;

/**
 * Created by zhouzhuo on 2017/7/26.
 */

public class SelectYearHolder extends BaseHolder<String> {
    private TextView tv_year;
    public SelectYearHolder(Activity activity) {
        super(activity);

    }

    @Override
    protected View initView() {
        View view = UIUtils.inflate(R.layout.select_year_pop);
        tv_year = (TextView) view.findViewById(R.id.tv_year);
        return view;
    }


    @Override
    protected void refreshView() {
        String year = getInfo();
        tv_year.setText(year);
    }
}
