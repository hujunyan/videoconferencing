package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ListView;

/**
 * Created by zhouzhuo on 2017/7/25.
 */

public class NoScrollListView extends ListView {
        public NoScrollListView(Context context) {
            super(context);
        }

        public NoScrollListView(Context context, AttributeSet attrs) {
            super(context, attrs);
        }

        public NoScrollListView(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
        }


        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            int expandSpec = MeasureSpec.makeMeasureSpec(Integer.MAX_VALUE >> 2,MeasureSpec.AT_MOST);
            super.onMeasure(widthMeasureSpec, expandSpec);
        }
}
