package com.xijiekou.sifasuomax.activity;

import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.MediaController;
import android.widget.RelativeLayout;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.FileUtils;
import com.xijiekou.sifasuomax.video.PlayView;

/**
 * Created by zhouzhuo on 2017/8/22.
 */

public class VideoPlayActivity extends BaseActivity {

    public final static String DATA = "URL";

    PlayView playView;
    Button playBtn;
    RelativeLayout activityPlay;

    private long playPostion = -1;
    private long duration = -1;
    String uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);
        initView();
        uri = getIntent().getStringExtra(DATA);
        Log.d("zhouzhuo","播放ship:"+uri.toString());
        MediaController controller;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            controller = new MediaController(VideoPlayActivity.this);
            playView.setMediaController(controller);
        }

        playView.setVideoURI(Uri.parse(uri));
        playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
               // playView.seekTo(1);
              //  startVideo();
                Log.d("zhouzhuo","视频播放完毕==");
                playView.seekTo(1);
                playView.pause();

            }
        });

        playView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                //获取视频资源的宽度
                int videoWidth = mp.getVideoWidth();
                //获取视频资源的高度
                int videoHeight = mp.getVideoHeight();
                playView.setSizeH(videoHeight);
                playView.setSizeW(videoWidth);
                playView.requestLayout();
                duration = mp.getDuration();
                play();
            }
        });

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        boolean isScreenOn = pm.isScreenOn();//如果为true，则表示屏幕“亮”了，否则屏幕“暗”了。
        if (!isScreenOn) {
            pauseVideo();
        }
    }

    private void initView() {
        playView = (PlayView) findViewById(R.id.playView);

       /* MediaController controller = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            controller = new MediaController(VideoPlayActivity.this);
            playView.setMediaController(controller);
            playView.setVideoPath(uri);

        }

        playView.setOnCompletionListener(new MediaPlayer.OnCompletionListener()
        {
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                //播放结束后的动作
                Log.d("zhouzhuo","播放结束了===");

            }
        });*/

        playBtn = (Button) findViewById(R.id.playBtn);
        playBtn.setOnClickListener(this);
        activityPlay = (RelativeLayout) findViewById(R.id.activity_play);
    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.playBtn:
                play();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (playPostion > 0) {
            pauseVideo();
        }
        playView.seekTo((int) ((playPostion > 0 && playPostion < duration) ? playPostion : 1));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        playView.stopPlayback();
    }

    @Override
    protected void onPause() {
        super.onPause();
        playView.pause();
        playPostion = playView.getCurrentPosition();
        pauseVideo();

    }

    @Override
    public void onBackPressed() {
        FileUtils.deleteFile(uri);
        finish();
    }


    private void pauseVideo() {
        playView.pause();
        playBtn.setText("播放");
    }

    private void startVideo() {
        playView.start();
        playBtn.setText("停止");

    }

    /**
     * 播放
     */
    private void play() {
        if (playView.isPlaying()) {
            pauseVideo();
        } else {
            if (playView.getCurrentPosition() == playView.getDuration()) {
                //playView.seekTo(0);
                pauseVideo();
            }else {
                startVideo();
            }

        }
    }


}
