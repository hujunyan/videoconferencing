package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.xijiekou.sifasuomax.MainActivity;
import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/7/4.
 */
public class MainLeftView extends LinearLayout implements View.OnClickListener{
    private LinearLayout ll_home, ll_new_file ,ll_drafts ,
            ll_search_file, ll_file_report, ll_mine;
    private MainActivity mainActivity;

    private int currentIndex;
    private View[] views = new View[6];
    public MainLeftView(Context context) {
        this(context,null);
    }

    public MainLeftView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.activity_left_view,this,true);
        ll_home = (LinearLayout) view.findViewById(R.id.ll_home);
        ll_home.setOnClickListener(this);
        ll_new_file = (LinearLayout) view.findViewById(R.id.ll_new_file);
        ll_new_file.setOnClickListener(this);
        ll_drafts = (LinearLayout) view.findViewById(R.id.ll_drafts);
        ll_drafts.setOnClickListener(this);
        ll_search_file = (LinearLayout) view.findViewById(R.id.ll_search_file);
        ll_search_file.setOnClickListener(this);
        ll_file_report = (LinearLayout) view.findViewById(R.id.ll_file_report);
        ll_file_report.setOnClickListener(this);
        ll_mine = (LinearLayout) view.findViewById(R.id.ll_mine);
        ll_mine.setOnClickListener(this);

        views[0] = view.findViewById(R.id.view_home);
        views[1] = view.findViewById(R.id.view_new_file);
        views[2] = view.findViewById(R.id.view_drafts);
        views[3] = view.findViewById(R.id.view_search_file);
        views[4] = view.findViewById(R.id.view_report);
        views[5] = view.findViewById(R.id.view_mine);

        mainActivity = (MainActivity) getContext();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_home:
                if(currentIndex!=0){
                    changTab(currentIndex,0);
                    mainActivity.changeFragment(currentIndex,0);
                    currentIndex =0;
                }
                break;
            case R.id.ll_new_file:
                if(currentIndex!=1){
                    changTab(currentIndex,1);
                    mainActivity.changeFragment(currentIndex,1);
                    currentIndex =1;
                }
                break;
            case R.id.ll_drafts:
                if(currentIndex!=2){
                    changTab(currentIndex,2);
                    mainActivity.changeFragment(currentIndex,2);
                    currentIndex =2;
                }
                break;
            case R.id.ll_search_file:
                if(currentIndex!=3){
                    changTab(currentIndex,3);
                    mainActivity.changeFragment(currentIndex,3);
                    currentIndex =3;
                }
                break;
            case R.id.ll_file_report:
                if(currentIndex!=4){
                    changTab(currentIndex,4);
                    mainActivity.changeFragment(currentIndex,4);
                    currentIndex =4;
                }
                break;
            case R.id.ll_mine:
                if(currentIndex!=5){
                    changTab(currentIndex,5);
                    mainActivity.changeFragment(currentIndex,5);
                    currentIndex =5;
                }
                break;
        }

    }

    private void changTab(int pre, int current) {
        views[pre].setVisibility(View.GONE);
        views[current].setVisibility(View.VISIBLE);
    }
}
