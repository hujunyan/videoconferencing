package com.xijiekou.sifasuomax.bean;

import java.io.Serializable;

/**
 * Created by zhouzhuo on 2017/7/25.
 */

public class NewWrittenBean implements Serializable{
    public String id;
    public String type;
    public String name;
    public String sex = "0";
    public String nation;//民族
    public String age;
    public String job;
    public String mobile;
    public String address;
    public String create_time;
    public String isdelete;
    public String faren;
    public String tongyi;
    public String weituoren;
    public String fading;

}
