package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.bean.RegistrationFormBean;
import com.xijiekou.sifasuomax.dialog.SelectDisputePop;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.PrintModel2;
import com.xijiekou.sifasuomax.model.RegistrationFormModel;
import com.xijiekou.sifasuomax.model.SavePart2;
import com.xijiekou.sifasuomax.model.SearchPart2;
import com.xijiekou.sifasuomax.utils.BeanToJsonUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.RegistrationFormCaseIncommingView;
import com.xijiekou.sifasuomax.view.RegistrationVoiceView;
import com.xijiekou.sifasuomax.view.SignNameView;
import com.xijiekou.sifasuomax.view.YearMonthDayMorningHourView;
import com.xijiekou.sifasuomax.view.YearMonthDayView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/7/27.
 * 新建受理书
 *   3-4-1
 */

public class RegistrationFormActivity extends VoiceBaseActivity {
    private TextView tv_number_content,tv_file_name_content;
    private YearMonthDayMorningHourView yearMonthDayMorningView;
    private EditText et_personContent;//当事人
    private TextView et_issuetype;//纠纷类型
    private RegistrationFormCaseIncommingView et_source_content;//案件来源
    private RegistrationVoiceView et_brief_content;//纠纷情况介绍
    private SignNameView tv_sign_one, tv_sign_two;
    private EditText tv_sign_unit_content;//调解委员会
    private YearMonthDayMorningHourView select_date;
    private YearMonthDayView sign_date;//填表时间
    private MyHandler handler;
    private RegistrationFormBean  bean;
    private String fileName;
    private String filenumber;
    private final String[] types = {
            "婚姻家庭纠纷","邻里纠纷","房屋宅基地纠纷","合同纠纷","生产经营纠纷",
            "损害赔偿纠纷","征地拆迁纠纷","环境污染纠纷","拖欠农民工工资纠纷","其他劳动争议纠纷"
            ,"旅游纠纷","电子商务纠纷","其他消费纠纷","道路交通事故纠纷","物业纠纷","其他纠纷"};
    private int currentDispute = 0;
    private String caseId;
    private String search;
    private BottomConfirmView bottom_confirm_view;
    private String audit;

    private class  MyHandler extends Handler{
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    bean = (RegistrationFormBean) msg.obj;
                    yearMonthDayMorningView.setData(bean.date);
                    et_personContent.setText(bean.litigant);
                    if(!TextUtils.isEmpty(bean.issuetype)){
                        currentDispute = Integer.parseInt(bean.issuetype);
                        et_issuetype.setText(types[currentDispute]);
                    }


                    if(!TextUtils.isEmpty(bean.source)){
                        et_source_content.setCurrent(Integer.parseInt(bean.source));
                    }
                    sign_date.setData(bean.formdate);
                   // et_source_content.setText(bean.source);
                    et_brief_content.setText(bean.issueinfo);
                    tv_sign_one.setImage(bean.litiganturl);
                    tv_sign_two.setImage(bean.checkinurl);
                    tv_sign_unit_content.setText(bean.committee);
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_form);
        initView();
        iniData();
    }




    private void iniData() {
            SearchPart2 part = new SearchPart2();
            part.setCallBackListener(new BaseModel.CallBackListener<RegistrationFormBean >() {
                @Override
                public void success(RegistrationFormBean  bean) {
                    if(bean!=null){
                        Message message = Message.obtain();
                        message.what = 0;
                        message.obj = bean;
                        handler.sendMessage(message);
                        search = "search";
                    }else {
                        bottom_confirm_view.hidePrint();
                    }

                }

                @Override
                public void failed() {

                }
            });
            part.searchPart2(caseId);
        }


    private void initView() {
        handler = new MyHandler();
        caseId = getIntent().getStringExtra("caseId");
        search = getIntent().getStringExtra("search");
        fileName = getIntent().getStringExtra("filename");
        filenumber = getIntent().getStringExtra("filenumber");
        audit = getIntent().getStringExtra("audit");
        yearMonthDayMorningView = (YearMonthDayMorningHourView) findViewById(R.id.select_date);
        tv_sign_one = (SignNameView) findViewById(R.id.tv_sign_one);
        tv_sign_one.setSignTitle("当事人:(签名)");
        tv_sign_two = (SignNameView) findViewById(R.id.tv_sign_two);
        tv_sign_two.setSignTitle("登记人:(签名)");
        select_date = (YearMonthDayMorningHourView) findViewById(R.id.select_date);
        sign_date = (YearMonthDayView) findViewById(R.id.sign_date);
        et_personContent = (EditText) findViewById(R.id.et_personContent);
        et_issuetype = (TextView) findViewById(R.id.et_issuetype);
        et_issuetype.setOnClickListener(this);

        et_source_content = (RegistrationFormCaseIncommingView) findViewById(R.id.et_source_content);
        et_brief_content = (RegistrationVoiceView) findViewById(R.id.et_brief_content);
        tv_sign_unit_content = (EditText) findViewById(R.id.tv_sign_unit_content);
        select_date = (YearMonthDayMorningHourView) findViewById(R.id.select_date);
        tv_number_content = (TextView) findViewById(R.id.tv_number_content);
        tv_file_name_content = (TextView) findViewById(R.id.tv_file_name_content);
        Log.d("zhouzhuo","fileName=="+fileName);
        Log.d("zhouzhuo","fileNUmber=="+filenumber);
        if(!TextUtils.isEmpty(fileName)){
            tv_file_name_content.setText(fileName);
        }
        if(!TextUtils.isEmpty(filenumber)){
            tv_number_content.setText(filenumber);
        }
        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setSaveListener(new BottomConfirmView.SaveListener() {
            @Override
            public void save() {
               /* RegistrationFormBean bean = getBean();
                bean.isAudit = "1";
                saveAndCreate(bean);*/
            }
        });
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {
                PrintModel2 model = new PrintModel2();
                RegistrationFormBean bean = getBean();
                int type = Integer.parseInt(bean.issuetype);
                bean.issuetype = types[type];
                String content = BeanToJsonUtils.part2(bean);
                model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)&&message.equals("success")){
                            ToastUtils.showShort(RegistrationFormActivity.this,"打印成功,请勿重复点击");
                        }else {
                            ToastUtils.showShort(RegistrationFormActivity.this,"打印失败");
                        }
                    }

                    @Override
                    public void failed() {
                        ToastUtils.showShort(RegistrationFormActivity.this,"打印失败,请检查打印服务是否开启");
                    }
                });
                String ip = (String) SharePreferenceUtils.get(RegistrationFormActivity.this,"printIp","");
                if(TextUtils.isEmpty(ip)||ip.equals("1")){
                    ToastUtils.showShort(RegistrationFormActivity.this,"暂不支持打印");
                }else {
                    model.print(content,"printB2",ip);
                }
            }
        });

    }

    private RegistrationFormBean getBean(){
        RegistrationFormBean bean = new RegistrationFormBean();
        bean.case_id = caseId;
        bean.date = yearMonthDayMorningView.getDate();
        bean.formdate = sign_date.getDate();
        bean.litigant = et_personContent.getText().toString().toString();
        bean.issuetype = currentDispute+"";
        bean.source = et_source_content.getCurrentType()+"";
        bean.issueinfo = et_brief_content.getText().toString();
        bean.litiganturl = tv_sign_one.getFilePath();
        Log.d("zhouzhuo","litigantUrl:"+bean.litiganturl);
        bean.checkinurl = tv_sign_two.getFilePath();
        bean.committee = tv_sign_unit_content.getText().toString().trim();
        return bean;
    }

    private void  saveAndCreate(RegistrationFormBean bean){
        RegistrationFormModel model = new RegistrationFormModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                JSONObject object ;
                try {
                    object = new JSONObject(message);
                    ToastUtils.showLong(RegistrationFormActivity.this,object.getString("data"));
                    if(object.getInt("code")==1){
                        Intent intent = getIntent();
                        setResult(0,intent);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failed() {
                ToastUtils.showShort(RegistrationFormActivity.this,"操作失败");
            }
        });
        model.registration(bean);
    }

    private void next() {
        if(!TextUtils.isEmpty(audit)&&audit.equals("1")){
            ToastUtils.showShort(RegistrationFormActivity.this,"审核已经通过，不能修改");
            return;
        }
        RegistrationFormBean bean = getBean();
        if(TextUtils.isEmpty(search)){
            saveAndCreate(bean);
        }else {
                SavePart2 savePart2 = new SavePart2();
                savePart2.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                    @Override
                    public void success(PostRequestBean bean) {
                        ToastUtils.showLong(RegistrationFormActivity.this,bean.data);
                        if(bean.code==1){
                            Intent intent = getIntent();
                            setResult(1,intent);
                            finish();
                        }
                    }

                    @Override
                    public void failed() {

                    }
                });
                savePart2.save(bean);
            }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.et_issuetype:
                SelectDisputePop pop = new SelectDisputePop(RegistrationFormActivity.this,currentDispute);
                pop.setListener(new SelectDisputePop.SelectItemListener() {
                    @Override
                    public void select(int type) {
                        currentDispute = type;
                        et_issuetype.setText(types[type]);
                    }
                });
                pop.showPopupWindow(et_issuetype);
                break;

        }
    }
}
