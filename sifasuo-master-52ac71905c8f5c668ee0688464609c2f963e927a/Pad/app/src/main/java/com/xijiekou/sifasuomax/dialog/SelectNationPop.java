package com.xijiekou.sifasuomax.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.CommonUtils;


/**
 * Created by zhouzhuo on 2017/7/11.
 */

public class SelectNationPop extends PopupWindow {
    private Activity activity;
    private final String[] nations = {"汉族","满族","回族","蒙古族","藏族","其他"};

    public SelectNationPop(Activity activity, int selectPosition){
        this.activity = activity;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.select_nation, null);

        this.setWidth(CommonUtils.dp2px(activity,123));
        // 设置弹出窗体的高
        this.setHeight(CommonUtils.dp2px(activity,90));
        this.setBackgroundDrawable(new BitmapDrawable());
        // 设置弹出窗体可点击
        //this.setTouchable(true);
        //this.setFocusable(true);
        // 设置点击是否消失
        this.setOutsideTouchable(true);

        ListView listView = (ListView) view.findViewById(R.id.lv_list);
        listView.setAdapter(new NationAdapter(selectPosition));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listener!=null){
                    listener.select(position);
                    SelectNationPop.this.dismiss();
                }
            }
        });
        this.setContentView(view);// 设置弹出窗体的宽
       // initTextColor(tv_nation_one,tv_nation_two,tv_nation_three,tv_nation_four,tv_nation_five,tv_nation_six,selectPosition);

    }

    private class NationAdapter extends BaseAdapter{
        private int selectPosition;
        private NationAdapter(int selectPosition){
            this.selectPosition = selectPosition;
        }

        @Override
        public int getCount() {
            return nations.length;
        }

        @Override
        public Object getItem(int position) {
            return nations[position];
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view =  LayoutInflater.from(activity).inflate(R.layout.select_nation_item, null);
            TextView textView = (TextView) view.findViewById(R.id.tv_nation_one);
            textView.setText(nations[position]);
            if(position == selectPosition){
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    textView.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));
                }else {
                    textView.setTextColor(activity.getResources().getColor(R.color.colorCommon));
                }
            }else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    textView.setTextColor(activity.getResources().getColor(R.color.popText,null));
                }else {
                    textView.setTextColor(activity.getResources().getColor(R.color.popText));
                }
            }
            View view_bottom = view.findViewById(R.id.view_bottom);
            if(position == nations.length-1){
                view_bottom.setVisibility(View.VISIBLE);
            }else {
                view_bottom.setVisibility(View.INVISIBLE);
            }
            return view;
        }
    }



    public void showPopupWindow(View parent) {
       if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
             // x y
            int[] location = new int[2];
            //获取在整个屏幕内的绝对坐标
            parent.getLocationOnScreen(location);
            this.showAtLocation(parent, 0, location[0]-20, location[1] + parent.getHeight());
        } else {
           this.dismiss();
        }
    }

    public SelectItemListener listener;
    public void setListener(SelectItemListener listener){
        this.listener = listener;
    }
    public interface  SelectItemListener {
        void select(int type);
    }

}
