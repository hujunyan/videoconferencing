package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/7/5.
 */
public class UserMessageItemView extends LinearLayout{
    private ImageView iv_type;
    private TextView tv_type;
    private TextView tv_version;
    private final String[] title = {"公告信息","检查更新","清除缓存","修改密码"};
    private final int[] ids = {R.drawable.mine_message,R.drawable.mine_update
            ,R.drawable.mine_clear_cache,R.drawable.mine_modify_password};
    public UserMessageItemView(Context context) {
        this(context,null);
    }

    public UserMessageItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.activity_usermessage_item,this,true);
        iv_type = (ImageView) view.findViewById(R.id.iv_type);
        tv_type = (TextView) findViewById(R.id.tv_type);
        tv_version = (TextView) view.findViewById(R.id.tv_version);
    }

    public void setVsesion(String versionName){
        tv_version.setText(versionName);
        Log.d("zhouzhuo","versionname=="+versionName);
    }


    public void setData(int position){
        tv_type.setText(title[position]);
        iv_type.setImageResource(ids[position]);
        if(position ==1){
            tv_version.setVisibility(VISIBLE);

        }else {
            tv_version.setVisibility(INVISIBLE);
        }
    }

}
