package com.xijiekou.sifasuomax.adapter;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.HistoryReportBean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/7.
 */
public class HistoryReportAdapter extends BaseAdapter{
    private Activity activity;
    private ArrayList<HistoryReportBean> list;
   public HistoryReportAdapter(FragmentActivity activity, ArrayList<HistoryReportBean> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder holder;
        if(convertView == null){
            holder = new MyViewHolder();
            convertView = LayoutInflater.from(activity).inflate(R.layout.history_report_item,parent,false);
            holder.fileName = (TextView) convertView.findViewById(R.id.tv_name);
            holder.file_time = (TextView) convertView.findViewById(R.id.tv_time);
            convertView.setTag(holder);
        }else {
            holder = (MyViewHolder) convertView.getTag();
        }
        HistoryReportBean bean = list.get(position);
        holder.fileName.setText(bean.name);
        holder.file_time.setText(bean.create_time);
        return convertView;
    }
    class  MyViewHolder{
        TextView fileName;
        TextView file_time;

    }
}
