package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.bean.NewOralBeans;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/4.
 * 口头协议
 */

public class NewOralModel extends BaseModel {
    private  NewOralBeans beans;
    public NewOralModel(NewOralBeans beans){
        this.beans = beans;
    }

    public void postOral(){
        String url = UrlDomainUtil.urlHeader+"/Staff/createOralCasePart1";
        PostFormBuilder builder =OkHttpUtils
                .post()
                .url(url);
        JSONArray array = new JSONArray();
        ArrayList<NewOralBean> list = beans.list;

        if(list!=null && list.size()>0){
            for (int i =0;i<list.size();i++){
                NewOralBean bean = list.get(i);
                Log.d("zhouzhuo","bean ==="+bean.toString());
                String type = bean.type;
                switch (type){
                    case "1":
                        JSONObject object = new JSONObject();
                        putString(object,"type","1");
                        putString(object,"name",bean.name);
                        putString(object,"sex",bean.sex);
                        putString(object,"nation",bean.nation);
                        putString(object,"age",bean.age);
                        putString(object,"job",bean.job);
                        putString(object,"tel",bean.tel);
                        putString(object,"address",bean.address);
                        putString(object,"idcard",bean.idcard);
                        array.put(object);
                        break;
                    case "2":
                        JSONObject object1 = new JSONObject();
                        putString(object1,"type","2");
                        putString(object1,"faren",bean.faren);
                        putString(object1,"address",bean.address);
                        putString(object1,"tongyi",bean.tongyi);
                        putString(object1,"fading",bean.fading);
                        putString(object1,"weituoren",bean.weituoren);
                        putString(object1,"tel",bean.tel);
                        array.put(object1);
                        break;
                    case "3":
                        JSONObject object2 = new JSONObject();
                        putString(object2,"type","3");
                        putString(object2,"faren",bean.faren);
                        putString(object2,"address",bean.address);
                        putString(object2,"tongyi",bean.tongyi);
                        putString(object2,"fading",bean.fading);
                        putString(object2,"weituoren",bean.weituoren);
                        putString(object2,"tel",bean.tel);
                        array.put(object2);
                        break;
                }
            }

        }

        JSONObject objectLast = new JSONObject();
        try {
            objectLast.putOpt("arr",array);
        } catch (JSONException e) {
            e.printStackTrace();
        }



        if(beans!=null){

            if(!TextUtils.isEmpty(beans.argue)){
                builder.addParams("argue", beans.argue);
            }
            if(!TextUtils.isEmpty(beans.agreement)){
                builder.addParams("agreement",beans.agreement);
            }
            if(!TextUtils.isEmpty(beans.performance)){
                builder.addParams("performance",beans.performance);
            }
            if(!TextUtils.isEmpty(beans.signature)){
                builder.addParams("signature",beans.signature);
            }
            Log.d("zhouzhuo","signature=="+beans.signature);
            if(!TextUtils.isEmpty(beans.id)) {
                builder.addParams("case_id",beans.id);
            }
            if(!TextUtils.isEmpty(beans.date)){
                builder.addParams("date",beans.date);
            }
            if(!TextUtils.isEmpty(beans.isAudit)){
                builder.addParams("isAudit",beans.isAudit);
            }
            if(!TextUtils.isEmpty(beans.num)){
                builder.addParams("num",beans.num);
            }
        }
        if(array.length()>0){
            builder.addParams("persons",objectLast.toString());
        }
        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","请求失败:"+e.getMessage());
                Log.d("zhouzhuo","请求失败:"+e.toString());
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    Log.d("zhouzhuo","请求成功:"+response);
                   PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);
                }
            }
        });

       /* OkHttpUtils
                .post()
                .url(url)
                .addParams("fileName", beans.fileName)
                .addParams("argue", beans.argue)
                .addParams("performance",beans.performance)
                .addParams("signature",beans.signature)
                .addParams("date",beans.date)
               // .addParams("persons",postJson)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("zhouzhuo","请求失败:"+e.getMessage());
                        Log.d("zhouzhuo","请求失败:"+e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if(response !=null){
                            Log.d("zhouzhuo","请求成功:"+response);
                            PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                            callBackListener.success(postRequestBean);
                        }

                    }*/
              //  });
    }

    public void putString(JSONObject object,String key,String value){
        if(!TextUtils.isEmpty(value)){
            try {
                object.put(key,value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
