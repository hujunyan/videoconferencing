package com.xijiekou.sifasuomax.bean;

import java.io.Serializable;

/**
 * Created by zhouzhuo on 2017/7/24.
 */

public class HistoryReportBean implements Serializable{
    public String id;
    public String name;
    public String type;
    public String fillin;
    public String audit;
    public String create_time;
    public String zongshu;
    public String hunyinjiating;
    public String linliguanxi;
    public String sunhaipeichang;
    public String wuyejiufen;
    public String qita;
    public String haibaoguatu;
    public String banbaozhanban;
    public String zheyecailiao;
    public String gongzuopeixun1;
    public String gongzuopeixun2;
    public String falvjiangzuo1;
    public String falvjiagnzuo2;
    public String beizhu;
    public String c1;
    public String c2;
    public String c3;
    public String c4;
    public String c5;
    public String c6;
    public String c7;
    public String c8;
    public String c9;
    public String c10;
    public String c11;
    public String c12;
    public String c13;
    public String c14;
    public String c15;
    public String c16;
    public String c17;
    public String c18;
    public String c19;
    public String c20;
    public String c21;
    public String c22;
    public String c23;
    public String c24;
    public String c25;
    public String c26;
    public String c27;
    public String c28;
    public String c29;
    public String c30;
    public String c31;
    public String c32;
    public String c33;
    public String c34;
    public String c35;
    public String c36;
    public String c37;
    public String c38;
    public String c39;
    public String c40;
    public String c41;
    public String c42;
    public String c43;
    public String c44;
    public String c45;
    public String c46;
    public String c47;
    public String year;
    public String month;
    public String company;
}
