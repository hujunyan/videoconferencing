package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.CreateCheckCreateBean;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.CreateCheckCreateModel;
import com.xijiekou.sifasuomax.model.CreateOralWrittenModel;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.view.MenuTabView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/9/6.
 */

public class FileMenuActivity extends BaseActivity {
    private MenuTabView tab_one,tab_two,tab_three,tab_four,tab_five,
     tab_six,tab_seven,tab_eight,tab_nine;
    private String caseId;
    private String search;
    private MyHandler handler;
    private String audit;
    private CreateCheckCreateBean bean;
    private class MyHandler extends Handler{
        private WeakReference weakReference;
        public MyHandler(FileMenuActivity activity){
            weakReference = new WeakReference(activity);
        }
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(weakReference.get()!=null){
                switch (msg.what){
                    case 1:
                        bean = (CreateCheckCreateBean) msg.obj;
                        audit = bean.audit;
                        Log.d("zhouzhuo","bean=="+bean);
                        if(bean!=null){
                            int state = bean.state;
                            Log.d("zhouzhuo","status=="+state);
                            for (int i=1;i<=state;i++){
                                switch (i){
                                    case 1:
                                        tab_one.setBg(true);
                                        tab_two.setBg(true);
                                        break;
                                    case 2:
                                        tab_three.setBg(true);
                                        tab_four.setBg(true);
                                        tab_five.setBg(true);
                                        tab_six.setBg(true);
                                        break;
                                }
                                if(state ==3){
                                    tab_seven.setBg(true);
                                    tab_eight.setBg(false);
                                }
                                if(state == 4){
                                    tab_seven.setBg(false);
                                    tab_eight.setBg(true);
                                }
                                if(state ==5){
                                    tab_seven.setBg(true);
                                    tab_eight.setBg(false);
                                    tab_nine.setBg(true);
                                }

                            }


                        }
                        break;
                }
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_menu);
        initView();
        initData();
    }

    private void initData() {
        caseId = getIntent().getStringExtra("caseId");
        if(TextUtils.isEmpty(caseId)){
            //新建
            createCaseId();
        }else {
            search = "search";
            initStatus(caseId);

        }
    }

    private void initStatus(String caseId){
        Log.d("zhouzhuo","caseId=="+caseId);
        CreateCheckCreateModel model = new CreateCheckCreateModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                if(!TextUtils.isEmpty(message)){
                    try {
                        JSONObject object = new JSONObject(message);
                        int code = object.getInt("code");
                        if(code == 1){
                            JSONObject object1 = object.getJSONObject("data");
                            CreateCheckCreateBean bean = new CreateCheckCreateBean();
                            try {
                                bean.audit = object1.getString("audit");
                                bean.filename = object1.getString("filename");
                                bean.filenum = object1.getString("filenum");
                                bean.argue = object1.getString("argue");
                                bean.case_id = object1.getString("case_id");
                                bean.state = object1.getInt("state");
                                bean.sqs = object1.getString("sqs");
                                bean.sldjb = object1.getString("sldjb");
                                bean.tjjl = object1.getString("tjjl");
                                bean.dcjl = object1.getString("dcjl");
                                bean.zjcl = object1.getString("zjcl");
                                bean.qtcl = object1.getString("qtcl");
                                bean.smxy = object1.getString("smxy");
                                bean.jabg = object1.getString("jabg");
                                bean.hfjl = object1.getString("hfjl");
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            try {
                                JSONArray array = object1.getJSONArray("persons");
                                if(array.length()>0){
                                    ArrayList<NewOralBean> list = new ArrayList<>();
                                    for (int i=0;i<array.length();i++){
                                        Gson gson = new Gson();
                                        JSONObject object2 = array.getJSONObject(i);
                                        NewOralBean bean1 = gson.fromJson(object2.toString(),NewOralBean.class);
                                        list.add(bean1);
                                    }
                                    bean.list = list;
                                }
                            }catch (Exception e){
                                e.toString();
                            }
                            Message message1 = Message.obtain();
                            message1.what =1;
                            message1.obj = bean;
                            handler.sendMessage(message1);
                        }
                    } catch (JSONException e) {
                        Log.d("zhouzhuo","json=异常="+e.toString());
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failed() {

            }
        });
        model.getStatue(caseId);

    }

    private void createCaseId() {
        //创建
        String userId = (String) SharePreferenceUtils.get(this,"id","");
        String company = (String) SharePreferenceUtils.get(this,"company","");
        CreateOralWrittenModel model = new CreateOralWrittenModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                caseId = message;
                initStatus(caseId);
            }

            @Override
            public void failed() {

            }
        });
        model.getCaseId(userId,company);
    }

    private void initView() {
        handler = new MyHandler(this);
        tab_one = (MenuTabView) findViewById(R.id.tab_one);
        tab_one.setTab(0);
        tab_one.setOnClickListener(this);
        tab_two = (MenuTabView) findViewById(R.id.tab_two);
        tab_two.setTab(1);
        tab_two.setOnClickListener(this);
        tab_three = (MenuTabView) findViewById(R.id.tab_three);
        tab_three.setTab(2);
        tab_three.setOnClickListener(this);
        tab_four = (MenuTabView) findViewById(R.id.tab_four);
        tab_four.setTab(3);
        tab_four.setOnClickListener(this);
        tab_five = (MenuTabView) findViewById(R.id.tab_five);
        tab_five.setTab(4);
        tab_five.setOnClickListener(this);
        tab_six = (MenuTabView) findViewById(R.id.tab_six);
        tab_six.setTab(5);
        tab_six.setOnClickListener(this);
        tab_seven = (MenuTabView) findViewById(R.id.tab_seven);
        tab_seven.setTab(6);
        tab_seven.setOnClickListener(this);
        tab_eight = (MenuTabView) findViewById(R.id.tab_eight);
        tab_eight.setTab(7);
        tab_eight.setOnClickListener(this);
        tab_nine = (MenuTabView) findViewById(R.id.tab_nine);
        tab_nine.setTab(8);
        tab_nine.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("zhouzhuo","requestCode=="+requestCode);
        Log.d("zhouzhuo","onActivityResult=="+resultCode);
        switch (requestCode){
            case 0:
                //创建回来刷新状态
                if(resultCode == 0||resultCode==1){
                    requestStatus();
                }
                break;
            case 1:
                if(resultCode ==0){
                    requestStatus();
                }
                break;
            case 2:
                if(resultCode ==0||resultCode==1){
                    requestStatus();
                }
                break;
            case 3:
                if(resultCode ==0){
                    requestStatus();
                }
                break;
            case 4:
                if(resultCode ==0){
                    requestStatus();
                }
                break;
            case 5:
                if(resultCode ==0){
                    requestStatus();
                }
                break;
            case 6:
                if(resultCode ==0){
                    requestStatus();
                }
                break;
            case 7:
                if(resultCode ==0){
                    requestStatus();
                }
                break;
            case 8:
                if(resultCode ==0){
                    requestStatus();
                }
                break;
        }
    }

    private void requestStatus(){
        initStatus(caseId);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.tab_one:
                //cj
                changStatus(bean.sqs);
                startIntent(NewWrittenActivity.class,0);
                break;
            case R.id.tab_two:
                    changStatus(bean.sldjb);
                    startIntent(RegistrationFormActivity.class,1);
                break;
            case R.id.tab_three:
                if(bean.state>=2){
                    changStatus(bean.tjjl);
                    startIntent(MediationRecordActivity.class,2);
                }
                break;
            case R.id.tab_four:
                if(bean.state>=2){
                    changStatus(bean.dcjl);
                    startIntent(InvestigationRecordActivity.class,3);
                }
                break;
            case R.id.tab_five:
                if(bean.state>=2){
                    changStatus(bean.zjcl);
                    startIntent(EvidenceMaterialActivity.class,4);
                }
                break;
            case R.id.tab_six:
                if(bean.state>=2){
                    changStatus(bean.qtcl);
                    startIntent(JudicialConfirmActivity.class,5);
                }
                break;
            case R.id.tab_seven:
                if(bean.state==3||bean.state==5){
                    changStatus(bean.smxy);
                    startIntent(NewProtocolBookActivity.class,6);
                }
                break;
            case R.id.tab_eight:
                if(bean.state==4){
                    changStatus(bean.jabg);
                    startIntent(ProjectFinalReportActivity.class,7);
                }
                break;
            case R.id.tab_nine:
                if(bean.state==5){
                    changStatus(bean.hfjl);
                    startIntent(VisitRecordActivity.class,8);
                }
                break;
        }
    }

    private void changStatus(String status){
        if(status.equals("0")){
            search = "";
        }else {
            search ="search";
        }
    }

    private void startIntent(Class<?> activity, int requestCode) {
        Intent intent = new Intent(this,activity);
        intent.putExtra("search",search);
        intent.putExtra("caseId",caseId);
        intent.putExtra("audit",audit);
        if(requestCode ==1){
            intent.putExtra("filename",bean.filename);
            intent.putExtra("filenumber",bean.filenum);
        }
        if(requestCode ==6){
            intent.putExtra("person",bean.list);
            intent.putExtra("argue",bean.argue);
        }
        IntentUtils.startActivity(this,intent,true,requestCode);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(handler!=null){
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }

    }
}
