package com.xijiekou.sifasuomax.view;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.CommonUtils;


/**
 * Created by zhouzhuo on 2017/7/11.
 */

public class SelectSexPop extends PopupWindow implements View.OnClickListener{
    //private TextView tv_state_doing,tv_state_success,tv_state_fail;
    private Activity activity;


    public SelectSexPop(Activity activity,int selectPosition){
        this.activity = activity;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.select_sex, null);

        this.setWidth(CommonUtils.dp2px(activity,123));
        // 设置弹出窗体的高
        this.setHeight(CommonUtils.dp2px(activity,90));
        this.setBackgroundDrawable(new BitmapDrawable());
        // 设置弹出窗体可点击
        //this.setTouchable(true);
        //this.setFocusable(true);
        // 设置点击是否消失
        this.setOutsideTouchable(true);
        TextView tv_boy = (TextView) view.findViewById(R.id.tv_boy);
        tv_boy.setOnClickListener(this);
        TextView tv_girl = (TextView) view.findViewById(R.id.tv_girl);
        tv_girl.setOnClickListener(this);

        this.setContentView(view);// 设置弹出窗体的宽
        initTextColor(tv_boy,tv_girl,selectPosition);

    }

    private void initTextColor(TextView tv_boy, TextView tv_girl,
                               int selectPosition) {
        switch (selectPosition){
            case 0:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv_boy.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));
                    tv_girl.setTextColor(activity.getResources().getColor(R.color.popText,null));
                }else {
                    tv_boy.setTextColor(activity.getResources().getColor(R.color.colorCommon));
                    tv_girl.setTextColor(activity.getResources().getColor(R.color.popText));
                }
                break;
            case 1:
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tv_boy.setTextColor(activity.getResources().getColor(R.color.popText,null));
                    tv_girl.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));

                }else {
                    tv_boy.setTextColor(activity.getResources().getColor(R.color.popText));
                    tv_girl.setTextColor(activity.getResources().getColor(R.color.colorCommon));
                }
                break;

        }
    }

    public void showPopupWindow(View parent) {
       if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
             // x y
            int[] location = new int[2];
            //获取在整个屏幕内的绝对坐标
            parent.getLocationOnScreen(location);
            this.showAtLocation(parent, 0, location[0]-20, location[1] + parent.getHeight());
        } else {
           this.dismiss();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.tv_boy:
                if(listener!= null){
                    listener.select(0);
                }
                dismiss();
                break;
            case R.id.tv_girl:
                if(listener!= null){
                    listener.select(1);
                }
                dismiss();
                break;

        }

    }



    public SelectItemListener listener;
    public void setListener(SelectItemListener listener){
        this.listener = listener;
    }
    public interface  SelectItemListener {
        void select(int type);
    }

}
