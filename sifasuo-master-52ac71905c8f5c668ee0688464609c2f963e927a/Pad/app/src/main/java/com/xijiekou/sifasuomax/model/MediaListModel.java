package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.MultimediaBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/8/8.
 */

public class MediaListModel extends BaseModel{
    public void getList(String id){
        String url = UrlDomainUtil.urlHeader+"/Staff/getLocal/case_id/"+id;
        Log.d("zhouzhuo","midialist==="+url);
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("zhouzhuo","Media list==="+response);
                        if(!TextUtils.isEmpty(response)){
                            try {
                                JSONObject object = new JSONObject(response);
                                int code = object.getInt("code");

                                if(code ==1){
                                    ArrayList<MultimediaBean> list = new ArrayList<>();
                                    JSONObject object1 = object.getJSONObject("data");
                                    JSONArray array = object1.getJSONArray("content");
                                    Gson gson = new Gson();
                                    for (int i= 0;i<array.length();i++){
                                        MultimediaBean bean = gson.fromJson(array.getJSONObject(i).toString(),MultimediaBean.class);
                                        list.add(bean);
                                    }
                                    callBackListener.success(list);
                                }
                            } catch (JSONException e) {
                                Log.d("zhouzhuo","json 异常"+e.getMessage());
                                e.printStackTrace();
                            }

                        }
                        //callBackListener.success(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }
}
