package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.HistoryReportBean;
import com.xijiekou.sifasuomax.bean.MediationMonthFormBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.MediationMonthFormModel;
import com.xijiekou.sifasuomax.model.ModifyMonthModel;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.MediationMonthFormItemView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/7/10.
 * 人民调解月报表
 */

public class MediationMonthFormActivity extends BaseActivity {
    private MediationMonthFormItemView id_0,id_1,id_2,id_3,id_4,id_5,id_6,id_7,id_8,
            id_9,id_10,id_11,id_12,id_13,id_14,id_15,id_16,id_17,id_18,id_19,id_20,
            id_21,id_22,id_23,id_24,id_25,id_26,id_27,id_28,id_29,id_30,id_31,id_32,
            id_33,id_34,id_35,id_36,id_37,id_38,id_39,id_40,id_41,id_42,id_43,id_44,id_45,
            id_46;
    private HistoryReportBean bean;
    private String search ;
    private String id;
    private BottomConfirmView bottom_confirm_view;
    private String year;
    private String company;
    private String month;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mediation_month_form);
        intView();
        initData();
    }

    private void initData() {
        search = getIntent().getStringExtra("search");
        year = getIntent().getStringExtra("year");
        month = getIntent().getStringExtra("month");
        company = getIntent().getStringExtra("company");
        if(!TextUtils.isEmpty(search)){
            bean = (HistoryReportBean) getIntent().getSerializableExtra("bean");
            setData(bean);
        }else {
            bottom_confirm_view.hidePrint();
        }
    }

    private void setData(HistoryReportBean bean) {
        if(bean!=null){
            id = bean.id;
            id_0.setText(bean.c1);
            id_1.setText(bean.c2);
            id_2.setText(bean.c3);
            id_3.setText(bean.c4);
            id_4.setText(bean.c5);
            id_5.setText(bean.c6);
            id_6.setText(bean.c7);
            id_7.setText(bean.c8);
            id_8.setText(bean.c9);
            id_9.setText(bean.c10);
            id_10.setText(bean.c11);
            id_11.setText(bean.c12);
            id_12.setText(bean.c13);
            id_13.setText(bean.c14);
            id_14.setText(bean.c15);
            id_15.setText(bean.c16);
            id_16.setText(bean.c17);
            id_17.setText(bean.c18);
            id_18.setText(bean.c19);
            id_19.setText(bean.c20);
            id_20.setText(bean.c21);
            id_21.setText(bean.c22);
            id_22.setText(bean.c23);
            id_23.setText(bean.c24);
            id_24.setText(bean.c25);
            id_25.setText(bean.c26);
            id_26.setText(bean.c27);
            id_27.setText(bean.c28);
            id_28.setText(bean.c29);
            id_29.setText(bean.c30);
            id_30.setText(bean.c31);
            id_31.setText(bean.c32);
            id_32.setText(bean.c33);
            id_33.setText(bean.c34);
            id_34.setText(bean.c35);
            id_35.setText(bean.c36);
            id_36.setText(bean.c37);
            id_37.setText(bean.c38);
            id_38.setText(bean.c39);
            id_39.setText(bean.c40);
            id_40.setText(bean.c41);
            id_41.setText(bean.c42);
            id_42.setText(bean.c43);
            id_43.setText(bean.c44);
            id_44.setText(bean.c45);
            id_45.setText(bean.c46);
            id_46.setText(bean.c47);
        }
    }



    private void intView() {
        id_0 = (MediationMonthFormItemView) findViewById(R.id.id_0);
        id_1 = (MediationMonthFormItemView) findViewById(R.id.id_1);
        id_2 = (MediationMonthFormItemView) findViewById(R.id.id_2);
        id_3 = (MediationMonthFormItemView) findViewById(R.id.id_3);
        id_4 = (MediationMonthFormItemView) findViewById(R.id.id_4);
        id_5 = (MediationMonthFormItemView) findViewById(R.id.id_5);
        id_6 = (MediationMonthFormItemView) findViewById(R.id.id_6);
        id_7 = (MediationMonthFormItemView) findViewById(R.id.id_7);
        id_8 = (MediationMonthFormItemView) findViewById(R.id.id_8);
        id_9 = (MediationMonthFormItemView) findViewById(R.id.id_9);
        id_10 = (MediationMonthFormItemView) findViewById(R.id.id_10);
        id_11 = (MediationMonthFormItemView) findViewById(R.id.id_11);
        id_12 = (MediationMonthFormItemView) findViewById(R.id.id_12);
        id_13 = (MediationMonthFormItemView) findViewById(R.id.id_13);
        id_14 = (MediationMonthFormItemView) findViewById(R.id.id_14);
        id_15 = (MediationMonthFormItemView) findViewById(R.id.id_15);
        id_16 = (MediationMonthFormItemView) findViewById(R.id.id_16);
        id_17 = (MediationMonthFormItemView) findViewById(R.id.id_17);
        id_18 = (MediationMonthFormItemView) findViewById(R.id.id_18);
        id_19 = (MediationMonthFormItemView) findViewById(R.id.id_19);
        id_20 = (MediationMonthFormItemView) findViewById(R.id.id_20);
        id_21 = (MediationMonthFormItemView) findViewById(R.id.id_21);
        id_22 = (MediationMonthFormItemView) findViewById(R.id.id_22);
        id_23 = (MediationMonthFormItemView) findViewById(R.id.id_23);
        id_24 = (MediationMonthFormItemView) findViewById(R.id.id_24);
        id_25 = (MediationMonthFormItemView) findViewById(R.id.id_25);
        id_26 = (MediationMonthFormItemView) findViewById(R.id.id_26);
        id_27 = (MediationMonthFormItemView) findViewById(R.id.id_27);
        id_28 = (MediationMonthFormItemView) findViewById(R.id.id_28);
        id_29 = (MediationMonthFormItemView) findViewById(R.id.id_29);
        id_30 = (MediationMonthFormItemView) findViewById(R.id.id_30);
        id_31 = (MediationMonthFormItemView) findViewById(R.id.id_31);
        id_32 = (MediationMonthFormItemView) findViewById(R.id.id_32);
        id_33 = (MediationMonthFormItemView) findViewById(R.id.id_33);
        id_34 = (MediationMonthFormItemView) findViewById(R.id.id_34);
        id_35 = (MediationMonthFormItemView) findViewById(R.id.id_35);
        id_36 = (MediationMonthFormItemView) findViewById(R.id.id_36);
        id_37 = (MediationMonthFormItemView) findViewById(R.id.id_37);
        id_38 = (MediationMonthFormItemView) findViewById(R.id.id_38);
        id_39 = (MediationMonthFormItemView) findViewById(R.id.id_39);
        id_40 = (MediationMonthFormItemView) findViewById(R.id.id_40);
        id_41 = (MediationMonthFormItemView) findViewById(R.id.id_41);
        id_42 = (MediationMonthFormItemView) findViewById(R.id.id_42);
        id_43 = (MediationMonthFormItemView) findViewById(R.id.id_43);
        id_44 = (MediationMonthFormItemView) findViewById(R.id.id_44);
        id_45 = (MediationMonthFormItemView) findViewById(R.id.id_45);
        id_46 = (MediationMonthFormItemView) findViewById(R.id.id_46);

        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {

            }
        });


    }

    private void next() {
        MediationMonthFormBean bean = new MediationMonthFormBean();
        bean.id_0 = id_0.getNumber();
        bean.id_1 = id_1.getNumber();
        bean.id_2 = id_2.getNumber();
        bean.id_3 = id_3.getNumber();
        bean.id_4 = id_4.getNumber();
        bean.id_5 = id_5.getNumber();
        bean.id_6 = id_6.getNumber();
        bean.id_7 = id_7.getNumber();

        bean.id_8 = id_8.getNumber();
        bean.id_9 = id_9.getNumber();
        bean.id_10 = id_10.getNumber();
        bean.id_11 = id_11.getNumber();
        bean.id_12 = id_12.getNumber();
        bean.id_13 = id_13.getNumber();
        bean.id_14 = id_14.getNumber();
        bean.id_15 = id_15.getNumber();

        bean.id_16 = id_16.getNumber();
        bean.id_17 = id_17.getNumber();
        bean.id_18 = id_18.getNumber();
        bean.id_19 = id_19.getNumber();
        bean.id_20 = id_20.getNumber();
        bean.id_21 = id_21.getNumber();
        bean.id_22 = id_22.getNumber();
        bean.id_23 = id_23.getNumber();


        bean.id_24 = id_24.getNumber();
        bean.id_25 = id_25.getNumber();
        bean.id_26 = id_26.getNumber();
        bean.id_27 = id_27.getNumber();
        bean.id_28 = id_28.getNumber();
        bean.id_29 = id_29.getNumber();
        bean.id_30 = id_30.getNumber();
        bean.id_31 = id_31.getNumber();


        bean.id_32 = id_32.getNumber();
        bean.id_33 = id_33.getNumber();
        bean.id_34 = id_34.getNumber();
        bean.id_35 = id_35.getNumber();
        bean.id_36 = id_36.getNumber();
        bean.id_37 = id_37.getNumber();
        bean.id_38 = id_38.getNumber();
        bean.id_39 = id_39.getNumber();


        bean.id_40 = id_40.getNumber();
        bean.id_41 = id_41.getNumber();
        bean.id_42 = id_42.getNumber();
        bean.id_43 = id_43.getNumber();
        bean.id_44 = id_44.getNumber();
        bean.id_45 = id_45.getNumber();
        bean.id_46 = id_46.getNumber();
        bean.id = id;

        if (TextUtils.isEmpty(search)) {
            bean.year = year;
            bean.month = month ;
            bean.company = company;
            MediationMonthFormModel model = new MediationMonthFormModel();
            model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                @Override
                public void success(String message) {
                    if (!TextUtils.isEmpty(message)) {
                        try {
                            JSONObject object = new JSONObject(message);
                            int code = object.getInt("code");
                            ToastUtils.showShort(MediationMonthFormActivity.this,
                                    object.getString("data"));
                            if (code == 1) {
                                Intent intent = getIntent();
                                setResult(0, intent);
                                finish();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failed() {

                }
            });
            model.post(bean);
        }else {
            ModifyMonthModel modifyMonthModel = new ModifyMonthModel();
            modifyMonthModel.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                @Override
                public void success(PostRequestBean bean) {
                    ToastUtils.showShort(MediationMonthFormActivity.this,
                            bean.data);
                    if(bean.code == 1){
                        finish();
                    }

                }

                @Override
                public void failed() {

                }
            });
            modifyMonthModel.savePeople(bean);

        }

    }


}
