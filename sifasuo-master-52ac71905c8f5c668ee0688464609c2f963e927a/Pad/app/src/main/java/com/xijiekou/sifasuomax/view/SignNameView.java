package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.BaseActivity;
import com.xijiekou.sifasuomax.dialog.WritePadDialog;
import com.xijiekou.sifasuomax.utils.BitmapUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by zhouzhuo on 2017/7/27.
 */

public class SignNameView extends RelativeLayout implements View.OnClickListener{
    private TextView tv_sign_start;
    private ImageView iv_sign;
    private BaseActivity activity;
    private File file;
    private TextView tv_sign_name;
    public SignNameView(Context context) {
        this(context,null);
    }

    public SignNameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (BaseActivity) context;
        initView(context);
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.sign_name_view,this,true);
        tv_sign_start = (TextView) view.findViewById(R.id.tv_sign_start);
        tv_sign_start.setOnClickListener(this);
        iv_sign = (ImageView) view.findViewById(R.id.iv_sign);
        tv_sign_name = (TextView) view.findViewById(R.id.tv_sign_name);
    }

    public void setSignTitle(String text){
        if(!TextUtils.isEmpty(text)){
            tv_sign_name.setText(text);
        }
    }

    public String getFilePath(){
        return file!=null&&file.exists()?file.getPath():"";
    }

    public void setImage(String path){
        Log.d("zhouzhuo","path=="+path);
        if(!TextUtils.isEmpty(path)){
            Glide.with(activity).load(path).into(iv_sign);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_sign_start:
                WritePadDialog dialog = new WritePadDialog(activity);
                dialog.setDialogListener(new WritePadDialog.DialogListener() {
                    @Override
                    public void refreshActivity(Bitmap object)  {
                        iv_sign.setImageBitmap(object);
                        try {
                            file =  BitmapUtils.saveFile(object,System.currentTimeMillis()+".png");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                });
                dialog.show();
                break;
        }

    }
}
