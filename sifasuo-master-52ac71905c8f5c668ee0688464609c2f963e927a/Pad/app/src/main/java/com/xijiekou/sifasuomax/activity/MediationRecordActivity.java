package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.MediationRecordBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.MediationRecordModel;
import com.xijiekou.sifasuomax.model.PrintModel2;
import com.xijiekou.sifasuomax.model.SavePart5;
import com.xijiekou.sifasuomax.model.SearchPart5;
import com.xijiekou.sifasuomax.utils.BeanToJsonUtils;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.MediationRecordView;
import com.xijiekou.sifasuomax.view.YearMonthDayMorningHourView;

/**
 * 调解记录
 * 3-7-4
 */
public class MediationRecordActivity extends VoiceBaseActivity {
    private TextView tv_evidence_entering;
    private TextView tv_next;
    private String caseId;
    private MediationRecordView recordView;
    private MyHandler handler;
    private String search;
    private TextView tv_evidence_number;
    private String fileName;
    private String filenumber;
    private BottomConfirmView bottom_confirm_view;
    private String audit;

    private class  MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    MediationRecordBean bean = (MediationRecordBean) msg.obj;
                    recordView.setData(bean);
                    break;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mediation_record);
        initView();
        initData();
    }


    private void initData() {
        SearchPart5 part = new SearchPart5();
        part.setCallBackListener(new BaseModel.CallBackListener<MediationRecordBean>() {
            @Override
            public void success(MediationRecordBean message) {
                if(message!=null){
                    Message message1 = Message.obtain();
                    message1.what = 0;
                    message1.obj = message;
                    handler.sendMessage(message1);
                    search = "search";
                }else {
                    bottom_confirm_view.hidePrint();
                }

            }

            @Override
            public void failed() {
                bottom_confirm_view.hidePrint();
            }
        });
        part.searchPart5(caseId);

    }

    private void initView() {
        handler = new MyHandler();
        tv_evidence_entering = (TextView) findViewById(R.id.tv_evidence_entering);
        tv_evidence_entering.setOnClickListener(this);
        recordView = (MediationRecordView) findViewById(R.id.rl_mediation_record);
        recordView.setTitleOne("当事人(签名盖章或按指纹)");
        recordView.setTitleTwo("人民调解员(签名)");
        recordView.setTitleThree("记录人(签名)");
        caseId = getIntent().getStringExtra("caseId");
        search = getIntent().getStringExtra("search");
        audit = getIntent().getStringExtra("audit");
        Log.d("zhouzhuo","MediationRecordActivity:"+search);


        fileName = getIntent().getStringExtra("filename");
        filenumber = getIntent().getStringExtra("filenumber");

        tv_evidence_number = (TextView) findViewById(R.id.tv_evidence_number);
        if(!TextUtils.isEmpty(filenumber)){
            tv_evidence_number.setText(filenumber);
        }

        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setSaveListener(new BottomConfirmView.SaveListener() {
            @Override
            public void save() {
               /* MediationRecordBean bean = getBean();
                bean.isAudit = "1";
                saveAndCreate(bean);*/
            }
        });
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {
                PrintModel2 model = new PrintModel2();
                MediationRecordBean bean = getBean();
                String content = BeanToJsonUtils.part3(bean);
                model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)&&message.equals("success")){
                            ToastUtils.showShort(MediationRecordActivity.this,"打印成功,请勿重复点击");
                        }else {
                            ToastUtils.showShort(MediationRecordActivity.this,"打印失败");
                        }
                    }

                    @Override
                    public void failed() {
                        ToastUtils.showShort(MediationRecordActivity.this,"打印失败,请检查打印服务是否开启");
                    }
                });
                String ip = (String) SharePreferenceUtils.get(MediationRecordActivity.this,"printIp","");
                if(TextUtils.isEmpty(ip)||ip.equals("1")){
                    ToastUtils.showShort(MediationRecordActivity.this,"暂不支持打印");
                }else {
                    model.print(content,"printB3",ip);
                }
            }
        });

    }

    private MediationRecordBean getBean(){
        MediationRecordBean bean = new MediationRecordBean();
        YearMonthDayMorningHourView hourView = recordView.getYMDMV();
        bean.case_id = caseId;
        bean.date = hourView.getDate();
        // bean.time_range = hourView.getType()+"";
        //bean.time = hourView.getHour()+":"+hourView.getMinute();
        bean.location = recordView.getPlace();
        bean.litigant = recordView.getLitigant();
        bean.attend = recordView.getParticipant();
        bean.record = recordView.getVoiceResult();
        bean.result = recordView.getResult();
        bean.litigant_sign = recordView.getPathOne();
        bean.attend_sign = recordView.getPathTwo();
        bean.record_sign = recordView.getPathThree();
        bean.formdate = recordView.getFormDate();
        return bean;
    }

    private void saveAndCreate(MediationRecordBean bean){
        MediationRecordModel model = new MediationRecordModel();
        model.setCallBackListener(new BaseModel.CallBackListener() {
            @Override
            public void success(Object message) {
                Intent intent = getIntent();
                setResult(0,intent);
                finish();
            }

            @Override
            public void failed() {

            }
        });
        model.registration(bean);
    }

    private void next() {
        MediationRecordBean bean = getBean();
        if(!TextUtils.isEmpty(audit)&&audit.equals("1")){
            ToastUtils.showShort(MediationRecordActivity.this,"审核已经通过，不能修改");
            return;
        }
        if(TextUtils.isEmpty(search)){
            saveAndCreate(bean);
        }else {
                SavePart5 savePart5 = new SavePart5();
                savePart5.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                    @Override
                    public void success(PostRequestBean bean) {
                        ToastUtils.showLong(MediationRecordActivity.this,bean.data);
                        if(bean.code == 1){
                            Intent intent = getIntent();
                            setResult(1,intent);
                            finish();
                        }
                    }

                    @Override
                    public void failed() {

                    }
                });
                savePart5.save(bean);
            }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.tv_evidence_entering:
                Intent intent = new Intent(MediationRecordActivity.this,EvidenceEnteringActivity.class);
                intent.putExtra("id",caseId);
                IntentUtils.startActivity(this,intent);
                break;
        }
    }
}
