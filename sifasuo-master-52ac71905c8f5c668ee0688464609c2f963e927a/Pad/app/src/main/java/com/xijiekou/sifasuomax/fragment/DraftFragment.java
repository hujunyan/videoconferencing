package com.xijiekou.sifasuomax.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.adapter.DraftFragmentAdapter;
import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.DraftFragmentCaseModel;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshBase;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshListView;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/4.
 * 草稿 改为正在调解中
 */
public class DraftFragment extends BaseFragment{
    private PullToRefreshListView refreshList;
    private ListView listView ;
    //为0=有下一页，为1=最后一页
    private String isLastpage;
    private int currentPage = 1;
    private ArrayList<MainFragmentItemBeans.MainFragmentItemBean> totalList;
    private DraftFragmentAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_draft, null);
        initView(view);
        initData();
        return view;
    }
    private void initView(View view) {
        refreshList = (PullToRefreshListView) view.findViewById(R.id.lv_list);
        listView = refreshList.getRefreshableView();

        listView.setVerticalScrollBarEnabled(false);
        listView.setDivider(getContext().getResources().getDrawable(R.drawable.draft_listview_drawable));
        listView.setDividerHeight(15);
        refreshList.setPullLoadEnabled(true);
        //listView.setAdapter(new FileTypeAdapter(this,list));
        refreshList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                currentPage = 1;
                adapter = null;
                initData();
                refreshList.onPullDownRefreshComplete();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                Log.d("zhouzhuo","==onPullUpToRefresh=");
                if(isLastpage.equals("0")){
                    currentPage++;
                    initData();
                    refreshList.onPullUpRefreshComplete();
                }else {
                    ToastUtils.showShort(getActivity(),"没有更多数据");
                }

            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            }
        });
    }

    public void initData() {
        Log.d("zhouzhuo","DraftFragmentCaseModel initData===");
        DraftFragmentCaseModel model = DraftFragmentCaseModel.getInstance();
        String id = (String) SharePreferenceUtils.get(getActivity(),"id","");
         String company = (String) SharePreferenceUtils.get(getActivity(),"company","");
        model.getList(id,company,currentPage);
        model.setCallBackListener(new BaseModel.CallBackListener<MainFragmentItemBeans>() {
            @Override
            public void success(MainFragmentItemBeans beans) {
                if (beans!=null) {
                    if(currentPage == 1){
                        totalList = beans.list;
                    }else {
                        totalList.addAll(beans.list);
                    }
                    currentPage = beans.currentPage;
                    isLastpage = beans.isLastpage;
                    setAdapter();
                }else {
                    if(totalList!=null){
                        totalList.clear();
                        setAdapter();
                    }
                }

            }

            @Override
            public void failed() {

            }
        });
    }
    private void setAdapter(){
        if(adapter == null){
            adapter = new DraftFragmentAdapter(getActivity(),totalList);
            listView.setAdapter(adapter);
        }else {
            adapter.notifyDataSetChanged();
        }
    }

}
