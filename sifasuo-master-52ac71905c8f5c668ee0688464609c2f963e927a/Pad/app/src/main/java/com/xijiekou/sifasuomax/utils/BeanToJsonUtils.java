package com.xijiekou.sifasuomax.utils;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.EvidenceMateriaBean;
import com.xijiekou.sifasuomax.bean.InvestigationBean;
import com.xijiekou.sifasuomax.bean.MediationRecordBean;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.bean.NewOralBeans;
import com.xijiekou.sifasuomax.bean.NewProtocolBean;
import com.xijiekou.sifasuomax.bean.NewWrittenBeans;
import com.xijiekou.sifasuomax.bean.ProjectFinalReportBean;
import com.xijiekou.sifasuomax.bean.RegistrationFormBean;
import com.xijiekou.sifasuomax.bean.VisitRecordBean;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/9/26.
 */

public class BeanToJsonUtils {
    private BeanToJsonUtils(){

    }
    public static String get(NewWrittenBeans beans) {
        JSONObject object2 = new JSONObject();
        JSONArray array = new JSONArray();
        ArrayList<NewOralBean> list = beans.list;
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                NewOralBean bean = list.get(i);
                String type = bean.type;
                switch (type) {
                    case "1":
                        JSONObject object = new JSONObject();
                        putString(object, "type", "1");
                        putString(object, "name", bean.name);
                        if("1".equals(bean.sex)){
                            putString(object, "sex", "女");
                        }else {
                            putString(object, "sex", "男");
                        }

                        putString(object, "nation", bean.nation);
                        putString(object, "age", bean.age);
                        putString(object, "job", bean.job);
                        putString(object, "tel", bean.tel);
                        putString(object, "address", bean.address);
                        putString(object,"idcard",bean.idcard);
                        array.put(object);
                        break;
                    case "2":
                        object = new JSONObject();
                        putString(object, "type", "2");
                        putString(object, "faren", bean.faren);
                        putString(object, "address", bean.address);
                        putString(object, "tongyi", bean.tongyi);
                        putString(object, "fading", bean.fading);
                        putString(object, "weituoren", bean.weituoren);
                        putString(object, "tel", bean.tel);
                        array.put(object);
                        break;
                    case "3":
                        object = new JSONObject();
                        putString(object, "type", "3");
                        putString(object, "faren", bean.faren);
                        putString(object, "address", bean.address);
                        putString(object, "tongyi", bean.tongyi);
                        putString(object, "fading", bean.fading);
                        putString(object, "weituoren", bean.weituoren);
                        putString(object, "tel", bean.tel);
                        array.put(object);
                        break;
                }

            }
        }
       /* JSONObject object3 ;
        try {
           // object3 = new JSONObject();
           // object3.putOpt("arr",array);
            if(array.length()>0){
                Log.d("zhouzhuo","==length=="+array.length());

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }*/
        try {
            object2.putOpt("arr",array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // putString(object2,"persons",array.toString());
            putString(object2,"case_id",beans.case_id);
            putString(object2,"filing",beans.filing);
            putString(object2,"simple",beans.simple);
            putString(object2,"content",beans.content);
            putString(object2,"signurl",beans.signurl);
            putString(object2,"date",beans.date);
        return object2.toString();
    }


    public static  String part2(RegistrationFormBean bean){
        if(bean!=null){
            String content = new Gson().toJson(bean);
            return content;
        }
        return "";
    }


    public static String part3(MediationRecordBean bean){
        if(bean!=null){
            String content = new Gson().toJson(bean);
            return content;
        }
        return "";
    }

    public static String part4(InvestigationBean bean){
        if(bean!=null){
            String content = new Gson().toJson(bean);
            return content;
        }
        return "";
    }

    public static String part5(EvidenceMateriaBean bean){
        if(bean!=null){
            String content = new Gson().toJson(bean);
            return content;
        }
        return "";
    }

    public static  String part6(EvidenceMateriaBean bean){
        if(bean!=null){
            String content = new Gson().toJson(bean);
            return content;
        }
        return "";
    }

    public static String part7(NewProtocolBean beans){
        JSONObject object2 = new JSONObject();
        final JSONArray array = new JSONArray();
        ArrayList<NewOralBean> list = beans.list;
        if(list!=null &&list.size()>0){
            for (int i = 0; i < list.size(); i++) {
                NewOralBean bean = list.get(i);
                JSONObject object = new JSONObject();
                putString(object, "name", bean.name);
                if("1".equals(bean.sex)){
                    putString(object, "sex", "女");
                }else {
                    putString(object, "sex", "男");
                }
                putString(object, "nation", bean.nation);
                putString(object, "age", bean.age);
                putString(object, "job", bean.job);
                putString(object, "tel", bean.tel);
                putString(object, "type",bean.type);
                putString(object, "address", bean.address);
                putString(object,"idcard",bean.idcard);
                array.put(object);
            }

        }
        try {
            object2.putOpt("arr",array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        putString(object2,"case_id",beans.case_id);
        putString(object2,"issue",beans.issue);
        putString(object2,"content",beans.content);
        putString(object2,"method",beans.method);
        putString(object2,"filenumber",beans.filenumber);
        putString(object2,"num",beans.num);
        putString(object2,"litigant_sign",beans.litigant_sign);
        putString(object2,"mediate_sign",beans.mediate_sign);
        putString(object2,"record_sign",beans.record_sign);
        putString(object2,"date",beans.date);
        return object2.toString();
    }

    public static String part8(ProjectFinalReportBean bean){
        if(bean!=null){
            String content = new Gson().toJson(bean);
            return content;
        }
        return "";
    }

    public static String part9(VisitRecordBean bean){
        if(bean!=null){
            String content = new Gson().toJson(bean);
            return content;
        }
        return "";
    }
    //口头协议
    public static String part10(NewOralBeans beans){
        JSONObject object3 = new JSONObject();
        JSONArray array = new JSONArray();
        ArrayList<NewOralBean> list = beans.list;
        if(list!=null && list.size()>0){
            for (int i =0;i<list.size();i++){
                NewOralBean bean = list.get(i);
                Log.d("zhouzhuo","bean ==="+bean.toString());
                String type = bean.type;
                switch (type){
                    case "1":
                        JSONObject object = new JSONObject();
                        putString(object,"type","1");
                        putString(object,"name",bean.name);
                        if(bean.sex.equals("1")){
                            putString(object,"sex","女");
                        }else {
                            putString(object,"sex","男");
                        }
                        putString(object,"nation",bean.nation);
                        putString(object,"age",bean.age);
                        putString(object,"job",bean.job);
                        putString(object,"tel",bean.tel);
                        putString(object,"address",bean.address);
                        putString(object,"idcard",bean.idcard);
                        array.put(object);
                        break;
                    case "2":
                        JSONObject object1 = new JSONObject();
                        putString(object1,"type","2");
                        putString(object1,"faren",bean.faren);
                        putString(object1,"address",bean.address);
                        putString(object1,"tongyi",bean.tongyi);
                        putString(object1,"fading",bean.fading);
                        putString(object1,"weituoren",bean.weituoren);
                        putString(object1,"tel",bean.tel);
                        array.put(object1);
                        break;
                    case "3":
                        JSONObject object2 = new JSONObject();
                        putString(object2,"type","3");
                        putString(object2,"faren",bean.faren);
                        putString(object2,"address",bean.address);
                        putString(object2,"tongyi",bean.tongyi);
                        putString(object2,"fading",bean.fading);
                        putString(object2,"weituoren",bean.weituoren);
                        putString(object2,"tel",bean.tel);
                        array.put(object2);
                        break;
                }

            }
            try {
                object3.putOpt("arr",array);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            putString(object3,"argue",beans.argue);
            putString(object3,"agreement",beans.agreement);
            putString(object3,"performance",beans.performance);
            putString(object3,"signature",beans.signature);
            putString(object3,"case_id",beans.id);
            putString(object3,"date",beans.date);
            putString(object3,"num",beans.num);
        }
        return object3.toString();
    }


    public static void putString(JSONObject object,String key,String value){
        if(!TextUtils.isEmpty(value)){
            try {
                object.put(key,value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else {
            try {
                object.put(key,"");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }


}
