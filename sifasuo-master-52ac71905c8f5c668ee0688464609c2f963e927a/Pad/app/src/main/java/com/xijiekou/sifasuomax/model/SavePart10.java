package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.bean.ProjectFinalReportBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SavePart10 extends BaseModel{
    public void save(ProjectFinalReportBean beans){
        String url = UrlDomainUtil.urlHeader+"/Staff/saveCasePart10";
        Log.d("zhouzhuo","司法确认:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(beans!=null){
            if(!TextUtils.isEmpty(beans.case_id)){
                builder.addParams("case_id", beans.case_id);
            }
            Log.d("zhouzhuo","case===id==="+beans.case_id);
            if(!TextUtils.isEmpty(beans.begin_time)){
                builder.addParams("begin_time", beans.begin_time);
            }
            if(!TextUtils.isEmpty(beans.end_time)){
                builder.addParams("end_time",beans.end_time);
            }
            if(!TextUtils.isEmpty(beans.mediate)){
                builder.addParams("mediate",beans.mediate);
            }
            if(!TextUtils.isEmpty(beans.litigant)){
                builder.addParams("litigant",beans.litigant);
            }
            if(!TextUtils.isEmpty(beans.attend)){
                builder.addParams("attend",beans.attend);
            }

            if(!TextUtils.isEmpty(beans.record)){
                builder.addParams("record",beans.record);
            }
            if(!TextUtils.isEmpty(beans.content)){
                builder.addParams("content",beans.content);
            }
            if(!TextUtils.isEmpty(beans.result)){
                builder.addParams("result",beans.result);
            }
            if(!TextUtils.isEmpty(beans.resultcode)){
                builder.addParams("resultcode",beans.resultcode);
            }
        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","司法确认请求失败:"+e.getMessage());
                Log.d("zhouzhuo","司法确认请求失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    //callBackListener.success(response);
                  //  callBackListener.success(response);
                    Log.d("zhouzhuo","司法确认请求成功:"+response);
                    try {
                        PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                        callBackListener.success(postRequestBean);
                    }catch (Exception e){
                        Log.d("zhouzhuo","司法确认请求失败:"+e.toString());
                    }

                }
            }
        });
    }
}
