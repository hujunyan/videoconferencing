package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/9/7.
 */

public class MenuTabView extends RelativeLayout{
    private ImageView iv_type;
    private TextView tv_content;
    private LinearLayout ll_bg;
    private final String[] titles = {
            "申请书","受理登记表","调解记录","调查记录","证据材料","其他材料","书面协议","结案报告","回访记录"
    };
    private int[] resources = {R.drawable.tab_one,R.drawable.tab_two,R.drawable.tab_three
        ,R.drawable.tab_four,R.drawable.tab_five,R.drawable.tab_six,R.drawable.tab_selven
            ,R.drawable.tab_eight,R.drawable.tab_nine};

    public MenuTabView(Context context) {
        this(context,null);
    }

    public MenuTabView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public void setBg(boolean click){
        if(click){
            ll_bg.setBackgroundResource(R.drawable.btn_login);
            this.setClickable(true);
        }else {
            ll_bg.setBackgroundResource(R.drawable.btn_unselect_status);
            this.setClickable(false);
        }

    }

    private void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.menu_tab_view,this,true);
        iv_type = (ImageView) view.findViewById(R.id.iv_type);
        tv_content = (TextView) view.findViewById(R.id.tv_content);
        ll_bg = (LinearLayout) view.findViewById(R.id.ll_bg);
    }

    public void setTab(int position){
        tv_content.setText(titles[position]);
        iv_type.setImageResource(resources[position]);
    }
}
