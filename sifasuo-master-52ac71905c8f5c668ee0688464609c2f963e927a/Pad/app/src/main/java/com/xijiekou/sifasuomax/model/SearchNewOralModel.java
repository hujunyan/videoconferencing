package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SearchNewOralModel extends BaseModel {
    public void searchOral(String id){
        String url = UrlDomainUtil.urlHeader + "/Staff/getOralCase/case_id/"+id;
        Log.d("zhouzhuo","查询口头协议 url:"+url);
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        callBackListener.success(response);
                        Log.d("zhouzhuo","查询口头协议==="+response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }


}
