package com.xijiekou.sifasuomax.view.MainFragment;

import android.app.Activity;
import android.view.View;
import android.widget.ListView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.smoothListView.view.AbsHeaderView;

/**
 * Created by zhouzhuo on 2017/9/13.
 */

public class FiterViewTwo extends AbsHeaderView<String> implements View.OnClickListener{
    public FiterViewTwo(Activity activity) {
        super(activity);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void getView(String s, ListView listView) {
        View view = mInflate.inflate(R.layout.main_fragment_view_two, listView, false);
        listView.addHeaderView(view);
    }
}
