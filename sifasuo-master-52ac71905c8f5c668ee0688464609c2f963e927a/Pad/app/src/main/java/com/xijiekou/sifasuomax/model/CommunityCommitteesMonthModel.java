package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.xijiekou.sifasuomax.bean.CommunityCommitteesBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class CommunityCommitteesMonthModel extends BaseModel{

    public void post(CommunityCommitteesBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/createCommunityReport";
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(bean!=null){
            if(!TextUtils.isEmpty(bean.year)){
                builder.addParams("year", bean.year);
            }
            if(!TextUtils.isEmpty(bean.month)){
                builder.addParams("month", bean.month);
            }
            if(!TextUtils.isEmpty(bean.company)){
                builder.addParams("company", bean.company);
            }
            if(!TextUtils.isEmpty(bean.name)){
                builder.addParams("name", bean.name);
            }
            if(!TextUtils.isEmpty(bean.fillin)){
                builder.addParams("fillin", bean.fillin);
            }
            if(!TextUtils.isEmpty(bean.zongshu)){
                builder.addParams("zongshu",bean.zongshu);
            }
            if(!TextUtils.isEmpty(bean.hunyinjiating)){
                builder.addParams("hunyinjiating",bean.hunyinjiating);
            }

            if(!TextUtils.isEmpty(bean.linliguanxi)){
                builder.addParams("linliguanxi", bean.linliguanxi);
            }
            if(!TextUtils.isEmpty(bean.sunhaipeichang)){
                builder.addParams("sunhaipeichang", bean.sunhaipeichang);
            }
            if(!TextUtils.isEmpty(bean.wuyejiufen)){
                builder.addParams("wuyejiufen",bean.wuyejiufen);
            }
            if(!TextUtils.isEmpty(bean.qita)){
                builder.addParams("qita",bean.qita);
            }


            if(!TextUtils.isEmpty(bean.haibaoguatu)){
                builder.addParams("haibaoguatu", bean.haibaoguatu);
            }
            if(!TextUtils.isEmpty(bean.banbaozhanban)){
                builder.addParams("banbaozhanban", bean.banbaozhanban);
            }
            if(!TextUtils.isEmpty(bean.zheyecailiao)){
                builder.addParams("zheyecailiao",bean.zheyecailiao);
            }
            if(!TextUtils.isEmpty(bean.gongzuopeixun1)){
                builder.addParams("gongzuopeixun1",bean.gongzuopeixun1);
            }


            if(!TextUtils.isEmpty(bean.gongzuopeixun2)){
                builder.addParams("gongzuopeixun2", bean.gongzuopeixun2);
            }
            if(!TextUtils.isEmpty(bean.falvjiangzuo1)){
                builder.addParams("falvjiangzuo1", bean.falvjiangzuo1);
            }
            if(!TextUtils.isEmpty(bean.falvjiagnzuo2)){
                builder.addParams("falvjiagnzuo2",bean.falvjiagnzuo2);
            }
            if(!TextUtils.isEmpty(bean.beizhu)){
                builder.addParams("beizhu",bean.beizhu);
            }

        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    callBackListener.success(response);
                    Log.d("zhouzhuo","月报表请求成功:"+response);

                }
            }
        });
    }
}
