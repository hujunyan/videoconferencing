package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.CommunityCommitteesBean;
import com.xijiekou.sifasuomax.bean.HistoryReportBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.CommunityCommitteesMonthModel;
import com.xijiekou.sifasuomax.model.ModifyCommunityModel;
import com.xijiekou.sifasuomax.utils.NewWorkUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.MediationMonthFormItemView;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/7/10.
 * 社区居委会月报表
 */

public class CommunityCommitteesMonthActivity extends BaseActivity {
    private MediationMonthFormItemView one,two,three,four,five,six,seven,
            eight,nine,ten,eleven,twelve,thirteen,fourteen;

    private HistoryReportBean bean;
    private String search ;
    private String id;
    private BottomConfirmView bottom_confirm_view;
    private String year;
    private String company;
    private String month;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_community_committees_month);
        initView();
        initData();
    }



    private void initData() {
        search = getIntent().getStringExtra("search");
        year = getIntent().getStringExtra("year");
        month = getIntent().getStringExtra("month");
        company = getIntent().getStringExtra("company");
        if(!TextUtils.isEmpty(search)){
            bean = (HistoryReportBean) getIntent().getSerializableExtra("bean");
            setData(bean);
        }else {
            bottom_confirm_view.hidePrint();
        }
    }



    private void setData(HistoryReportBean bean) {
        if(bean!=null){
            id = bean.id;
            one.setText(bean.zongshu);
            two.setText(bean.hunyinjiating);
            three.setText(bean.linliguanxi);
            four.setText(bean.sunhaipeichang);
            five.setText(bean.wuyejiufen);
            six.setText(bean.qita);
            seven.setText(bean.haibaoguatu);
            eight.setText(bean.banbaozhanban);
            nine.setText(bean.zheyecailiao);
            ten.setText(bean.gongzuopeixun1);
            eleven.setText(bean.gongzuopeixun2);
            twelve.setText(bean.falvjiangzuo1);
            thirteen.setText(bean.falvjiagnzuo2);
            fourteen.setText(bean.beizhu);
        }
    }


    private void initView() {
        one = (MediationMonthFormItemView) findViewById(R.id.one);
        two = (MediationMonthFormItemView) findViewById(R.id.two);
        three = (MediationMonthFormItemView) findViewById(R.id.three);
        four = (MediationMonthFormItemView) findViewById(R.id.four);
        five = (MediationMonthFormItemView) findViewById(R.id.five);


        six = (MediationMonthFormItemView) findViewById(R.id.six);
        seven = (MediationMonthFormItemView) findViewById(R.id.seven);
        eight = (MediationMonthFormItemView) findViewById(R.id.eight);
        nine = (MediationMonthFormItemView) findViewById(R.id.nine);
        ten = (MediationMonthFormItemView) findViewById(R.id.ten);


        eleven = (MediationMonthFormItemView) findViewById(R.id.eleven);
        twelve = (MediationMonthFormItemView) findViewById(R.id.twelve);
        thirteen = (MediationMonthFormItemView) findViewById(R.id.thirteen);
        fourteen = (MediationMonthFormItemView) findViewById(R.id.fourteen);

        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {

            }
        });
    }

    private void next() {
        if(!NewWorkUtils.isConnected(CommunityCommitteesMonthActivity.this)){
            ToastUtils.showShort(CommunityCommitteesMonthActivity.this,"当前网络不好");
            return;
        }
        CommunityCommitteesBean bean = new CommunityCommitteesBean();
        bean.id = id;
        bean.zongshu = one.getNumber();
        bean.hunyinjiating = two.getNumber();
        bean.linliguanxi = three.getNumber();
        bean.sunhaipeichang = four.getNumber();
        bean.wuyejiufen = five.getNumber();
        bean.qita = six.getNumber();
        bean.haibaoguatu = seven.getNumber();
        bean.banbaozhanban = eight.getNumber();
        bean.zheyecailiao = nine.getNumber();
        bean.gongzuopeixun1 = ten.getNumber();
        bean.gongzuopeixun2 = eleven.getNumber();
        bean.falvjiangzuo1 = twelve.getNumber();
        bean.falvjiagnzuo2 = thirteen.getNumber();
        bean.beizhu = fourteen.getNumber();
        if(TextUtils.isEmpty(search)){
            bean.year = year;
            bean.month = month;
            bean.company = company;
            CommunityCommitteesMonthModel model = new CommunityCommitteesMonthModel();
            model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                @Override
                public void success(String message) {
                    if (!TextUtils.isEmpty(message)) {
                        try {
                            JSONObject object = new JSONObject(message);
                            int code = object.getInt("code");
                            ToastUtils.showShort(CommunityCommitteesMonthActivity.this,
                                    object.getString("data"));
                            if (code == 1) {
                                Intent intent = getIntent();
                                setResult(0,intent);
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void failed() {

                }
            });
            model.post(bean);
        }else {
            //查看  修改
            ModifyCommunityModel model = new ModifyCommunityModel();
            model.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                @Override
                public void success(PostRequestBean bean) {
                    ToastUtils.showShort(CommunityCommitteesMonthActivity.this,
                            bean.data);
                    if(bean.code == 1){
                        finish();
                    }
                }
                @Override
                public void failed() {

                }
            });
            model.saveCommunity(bean);
        }

    }


}
