package com.xijiekou.sifasuomax.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xijiekou.sifasuomax.holder.BaseHolder;
import com.xijiekou.sifasuomax.holder.SelectYearHolder;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/9/7.
 */

public class SelectYearAdapter extends BaseAdapter {
    private ArrayList<String> list;
    private Activity context;
    public SelectYearAdapter(ArrayList<String> list,Activity context){
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseHolder<String> holder;
        if(convertView == null){
            holder = new SelectYearHolder(context);
            // convertView.setTag(holder);
        }else {
            holder = (BaseHolder<String>) convertView.getTag();
        }
        holder.setData(list.get(position));
        return holder.getContentView();
    }
}
