package com.xijiekou.sifasuomax.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;

import java.util.List;

import static com.xijiekou.sifasuomax.R.id.tv_fileName;
import static com.xijiekou.sifasuomax.R.id.tv_fileNumber;
import static com.xijiekou.sifasuomax.R.id.tv_mediationTime;
import static com.xijiekou.sifasuomax.R.id.tv_state;

/**
 * Created by zhouzhuo on 2017/7/7.
 */
public class MainFragmentAdapterTwo extends BaseListAdapter<MainFragmentItemBeans.MainFragmentItemBean>{

    public MainFragmentAdapterTwo(Context context, List<MainFragmentItemBeans.MainFragmentItemBean> list) {
        super(context, list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MyViewHolder holder;
        if (convertView == null) {
            holder = new MyViewHolder();
            convertView = mInflate.inflate(R.layout.main_fragment_item, null);
            holder.ll_bg = (LinearLayout) convertView.findViewById(R.id.ll_bg);
            holder.tv_fileNumber = (TextView) convertView.findViewById(tv_fileNumber);
            holder.tv_fileName = (TextView) convertView.findViewById(tv_fileName);
            holder.tv_event = (TextView) convertView.findViewById(R.id.tv_event);
            holder.tv_state = (TextView) convertView.findViewById(tv_state);
            holder.tv_mediationTime = (TextView) convertView.findViewById(tv_mediationTime);
            convertView.setTag(holder);
        } else {
            holder = (MyViewHolder) convertView.getTag();
        }

        MainFragmentItemBeans.MainFragmentItemBean bean = getItem(position);
        holder.tv_fileNumber.setText(bean.filenum);
        holder.tv_fileName.setText(bean.filename);

        if(bean.audit.equals("0")){
            holder.tv_state.setText("未审核");
        }else {
            holder.tv_state.setText("已审核");
        }
        if (!TextUtils.isEmpty(bean.date)) {
            holder.tv_mediationTime.setText(bean.date.split("\\ ")[0]);
        }
        return convertView;
    }

    public class MyViewHolder{
        private TextView tv_fileNumber;//卷号
        private TextView tv_fileName;//卷名
        private TextView tv_event;//
        private TextView tv_state;//人民调解员
        private TextView tv_mediationTime;//调解时间
        private LinearLayout ll_bg;
    }
}
