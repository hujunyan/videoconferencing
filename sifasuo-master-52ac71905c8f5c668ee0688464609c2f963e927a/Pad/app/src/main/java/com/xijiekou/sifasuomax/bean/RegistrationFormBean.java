package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/8/4.
 * 新建受理书 对象
 */

public class RegistrationFormBean {
    public String id;
    public String case_id;
    public String date;
    public String time_range;
    public String time;
    public String litigant;
    public String issuetype;
    public String source;
    public String issueinfo;
    public String litiganturl;
    public String checkinurl;
    public String committee;
    public String formdate;
    public String  create_time;
    public String   isAudit;
}
