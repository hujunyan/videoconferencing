package com.xijiekou.sifasuomax.activity;

import android.Manifest;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.audioutils.MediaPlayerUtils;

import java.io.IOException;

/**
 * Created by zhouzhuo on 2017/9/15.
 */

public class VoicePlayActivity extends BaseActivity implements  MediaPlayerUtils.OnMediaPlayerUtilsInterfa {
    private TextView voice_rerecording;
    private ImageView voice_play;
    private TextView voice_ok;
    private String url;
    private  MediaPlayer mediaPlayer;
    private MediaPlayerUtils mMediaPlayerUtils;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_voice_play);
        initView();
        initData();
    }

    private void initData() {
        url = getIntent().getStringExtra("url");
        mMediaPlayerUtils = MediaPlayerUtils.getMediaPlayerUtils().init(this, url).setOnMediaPlayerUtilsInterfa(this);
    }

    private void initView() {
        voice_rerecording = (TextView) findViewById(R.id.voice_rerecording);
        voice_rerecording.setOnClickListener(this);
        voice_play = (ImageView) findViewById(R.id.voice_play);
        voice_play.setOnClickListener(this);
        voice_ok = (TextView) findViewById(R.id.voice_ok);
        voice_ok.setOnClickListener(this);
        mediaPlayer = new MediaPlayer();
    }


    private void play(String url){
        Log.d("zhouzhuo","开始播放===");
        requestPermission(new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE}, new PermissionHandler() {
            @Override
            public void onGranted() {
                if (null != mMediaPlayerUtils) {
                    mMediaPlayerUtils.start();
                }

            }

            @Override
            public void onDenied() {

            }

            @Override
            public boolean onNeverAsk() {
                return false;
            }
        });


    }

    private void stop2(){
        if (null != mMediaPlayerUtils)
            mMediaPlayerUtils.empty();
        Log.d("zhouzhuo","调用了停止===");
        finish();
    }



    public  void paly(final String url){
        // url = "http://59.110.156.74/Uploads/voice/2017-08-11/598d19a3315c3.wav";
        Log.d("zhouzhuo","play two==");
        requestPermission(new String[]{
                Manifest.permission.READ_EXTERNAL_STORAGE}, new PermissionHandler() {
            @Override
            public void onGranted() {
                try {
                    mediaPlayer.setDataSource(url);
                    mediaPlayer.prepareAsync();
                    mediaPlayer.start();
                } catch (IOException e) {
                    Log.d("zhouzhuo","play two=="+e.toString());
                    e.printStackTrace();
                }
            }

            @Override
            public void onDenied() {

            }

            @Override
            public boolean onNeverAsk() {
                return false;
            }
        });

    }

    public  void stop(){
        /*runOnUiThread(new Runnable() {
            @Override
            public void run() {

            }
        });*/
        if(mediaPlayer!=null){
            Log.d("zhouzhuo","=stop=111=");
            /*if(mediaPlayer.isPlaying()){
                Log.d("zhouzhuo","=stop=222=");
                mediaPlayer.stop();
            }*/
            Log.d("zhouzhuo","=stop=3333=");
            mediaPlayer.release();
            try {
                mediaPlayer.setDataSource("");
                mediaPlayer.prepare();
                mediaPlayer.start();
               // mediaPlayer = null;
            }catch (Exception e){
                Log.d("zhouzhuo","e==="+e.toString());
               // finish();
            }
            //finish();

        }
        Log.d("zhouzhuo","=stop==");

        /*try {
            if(mediaPlayer!=null&& mediaPlayer.isPlaying()){
                // mediaPlayer.release();

                mediaPlayer.stop();
                Log.d("zhouzhuo","stop==");
            }else {
                Log.d("zhouzhuo","mediaPlayer=111="+mediaPlayer);
            }
        }catch (Exception e){
            Log.d("zhouzhuo","停止异常："+e.toString());
        }
        try {
            Log.d("zhouzhuo","mediaPlayer=2222="+mediaPlayer);
            mediaPlayer.release();
        }catch (Exception e){
            Log.d("zhouzhuo","停止异常 111："+e.toString());
        }*/

    }


    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.voice_rerecording:
               // paly(url);
                play(url);
                break;
            case R.id.voice_ok:
                Log.d("zhouzhuo","点击返回===");
                stop2();
                break;
            case R.id.voice_play:
                play(url);
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        stop2();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onMediaPlayerTime(int time) {

    }

    @Override
    public void onMediaPlayerOk() {

    }

    @Override
    public void onMediaPlayerError() {

    }
}
