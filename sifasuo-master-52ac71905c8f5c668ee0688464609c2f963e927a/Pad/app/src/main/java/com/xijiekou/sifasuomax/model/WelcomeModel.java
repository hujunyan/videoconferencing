package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/8/17.
 */

public class WelcomeModel extends BaseModel {
    public void getImage(){
      //  "http://59.110.156.74/Uploads/image/2017-08-17/5994fbbc6c257.jpg"
        String url = UrlDomainUtil.urlHeader+"/Staff/getStartUpImg/type/1/";
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(callBackListener!=null){
                            if(!TextUtils.isEmpty(response)){
                                callBackListener.success(response);
                            }else {
                                callBackListener.failed();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("zhouzhuo","开机页面");
                if(callBackListener!=null){
                    callBackListener.failed();
                }
            }
        });
        MyApp.getHttpQueue().add(stringRequest);

    }
}
