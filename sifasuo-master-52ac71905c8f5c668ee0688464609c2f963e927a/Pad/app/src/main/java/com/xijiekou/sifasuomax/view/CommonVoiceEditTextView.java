package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.VoiceBaseActivity;

/**
 * Created by zhouzhuo on 2017/7/27.
 */

public class CommonVoiceEditTextView extends RelativeLayout implements View.OnClickListener{
    private RelativeLayout rl_voice;
    private EditText et_detail;
    private VoiceBaseActivity activity;
    private String lastResult="";
    private TextView tv_notice;

    public CommonVoiceEditTextView(Context context) {
        this(context,null);
    }

    public CommonVoiceEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (VoiceBaseActivity) context;
        initView(context);
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.common_voice_edit_view,this,true);
        rl_voice = (RelativeLayout) view.findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        et_detail = (EditText) view.findViewById(R.id.et_detail);
        et_detail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult = s.toString();
                Log.d("zhouzhuo","afterTextChanged:"+s);

            }
        });
        tv_notice = (TextView) view.findViewById(R.id.tv_notice);
    }

    public void setNotice(String text){
        if(!TextUtils.isEmpty(text)){
            tv_notice.setText(text);
        }
    }

    public String getVoiceContent(){
        return  et_detail.getText().toString().trim();
    }

    public void setText(String content){
        if(!TextUtils.isEmpty(content)){
            Log.d("zhouzhuo","不为空contnet:"+content);
            Log.d("zhouzhuo","不为空contnet length:"+content.length());
            et_detail.setText(content);
            lastResult = content;
        }else {
            Log.d("zhouzhuo","为空contnet:"+content);
        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_voice:
                activity.requestVoicePermission();
                activity.setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail.getText().insert(et_detail.getSelectionStart(),result);
                        }

                    }
                });
                break;
        }

    }
}
