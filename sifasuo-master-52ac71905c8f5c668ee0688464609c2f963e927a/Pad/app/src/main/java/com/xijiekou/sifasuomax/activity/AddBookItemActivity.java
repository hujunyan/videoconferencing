package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.NewWrittenBean;
import com.xijiekou.sifasuomax.view.SelectSexPop;

/**
 * Created by zhouzhuo on 2017/8/3.
 */

public class AddBookItemActivity extends BaseActivity {
    TextView tv_one;
    EditText et_one;
    TextView tv_two;
    TextView et_two;
    TextView tv_three;
    EditText et_three;
    TextView tv_four;
    EditText et_four;
    TextView tv_five;
    EditText et_five;
    TextView tv_six;
    EditText et_six;
    TextView tv_seven;
    EditText et_seven;
    private Button btn_confirm;
    private NewWrittenBean bean;
    private FrameLayout fl_two;
    private ImageView iv_arrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_book_item);
        initView();
        initData();
    }

    private void initData() {
        bean = (NewWrittenBean) getIntent().getSerializableExtra("bean");
        if(bean == null){
            bean = new NewWrittenBean();
        }else {
            String name = bean.name;
            setTextNotNull(et_one,name);
            setSex(bean.sex);
            String nation = bean.nation;
            setTextNotNull(et_three,nation);
            String age = bean.age;
            setTextNotNull(et_four,age);
            String job = bean.job;
            setTextNotNull(et_five,job);
            String mobile = bean.mobile;
            setTextNotNull(et_six,mobile);
            String address = bean.address;
            setTextNotNull(et_seven,address);
        }
    }

    //0  1
    private void setSex(String sex) {
        if(sex.equals("1")){
            et_two.setText("女");
        }else {
            et_two.setText("男");
        }
    }

    private void initView() {
        tv_one = (TextView) findViewById(R.id.tv_one);
        et_one = (EditText) findViewById(R.id.et_one);
        tv_two = (TextView) findViewById(R.id.tv_two);
        et_two = (TextView) findViewById(R.id.et_two);
        tv_three = (TextView) findViewById(R.id.tv_three);
        et_three = (EditText) findViewById(R.id.et_three);

        tv_four = (TextView) findViewById(R.id.tv_four);
        et_four = (EditText) findViewById(R.id.et_four);

        tv_five = (TextView) findViewById(R.id.tv_five);
        et_five= (EditText) findViewById(R.id.et_five);


        tv_six= (TextView) findViewById(R.id.tv_six);
        et_six = (EditText) findViewById(R.id.et_six);

        tv_seven = (TextView) findViewById(R.id.tv_seven);
        et_seven = (EditText) findViewById(R.id.et_seven);

        et_one.addTextChangedListener(new MyTextWatcher(0));
        et_three.addTextChangedListener(new MyTextWatcher(2));
        et_four.addTextChangedListener(new MyTextWatcher(3));
        et_five.addTextChangedListener(new MyTextWatcher(4));
        et_six.addTextChangedListener(new MyTextWatcher(5));
        et_seven.addTextChangedListener(new MyTextWatcher(6));

        btn_confirm = (Button) findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(this);

        fl_two = (FrameLayout) findViewById(R.id.fl_two);

        iv_arrow = (ImageView) findViewById(R.id.iv_arrow);
        iv_arrow.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_confirm:
                Intent intent = getIntent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("bean",bean);
                intent.putExtras(bundle);
                setResult(0,intent);
                finish();
                break;
            case R.id.iv_arrow:
                SelectSexPop pop = new SelectSexPop(AddBookItemActivity.this,Integer.parseInt(bean.sex));
                pop.setListener(new SelectSexPop.SelectItemListener() {
                    @Override
                    public void select(int type) {
                        bean.sex = type+"";
                        setSex(bean.sex);
                    }
                });
                pop.showPopupWindow(fl_two);
                break;
        }
    }


    private class  MyTextWatcher implements TextWatcher {
        private int position;
        public MyTextWatcher(int position){
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(bean!=null){
                switch (position){
                    case 0:
                        bean.name = s.toString();
                        break;
                    case 1:
                     //   bean.sex = s.toString();
                        break;
                    case 2:
                        bean.nation = s.toString();
                        break;
                    case 3:
                        bean.age = s.toString();
                        break;
                    case 4:
                        bean.job = s.toString();
                        break;
                    case 5:
                        bean.mobile = s.toString();
                        break;
                    case 6:
                        bean.address = s.toString();
                        break;
                }
            }
        }
    }
    private void setTextNotNull(EditText tv, String text){
        if(!TextUtils.isEmpty(text)){
            tv.setText(text);
        }
    }
}
