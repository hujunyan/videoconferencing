package com.xijiekou.sifasuomax.holder;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.utils.UIUtils;

/**
 * Created by zhouzhuo on 2017/7/26.
 */

public class NewOralHolder extends BaseHolder<NewOralBean> {
    TextView tv_person;
    TextView tv_person_select;
    TextView tv_one;
    TextView et_one;
    TextView tv_two;
    TextView et_two;
    TextView tv_three;
    TextView et_three;
    TextView tv_four;
    TextView et_four;
    TextView tv_five;
    TextView et_five;
    TextView tv_six;
    TextView et_six;
    TextView tv_seven;
    TextView et_seven;
    TextView tv_eight;
    TextView et_eight;
    // 0  自然人 Natural   1 法人 Corporation  2非法人组织  Litigant
    private final String[] titleNatural = {"当事人姓名","性别","民族","出生年月日","职业或职务","联系方式","身份证","单位或住址"};
    private final String[] titleCorporation = {"组织名称","单位地址","统一社会信用代码证","法定代表人","委托人或负责人","联系方式"};
    private final String[] titleLitigant = {"法人名称","单位地址","统一社会信用代码证","法定代表人","委托人或负责人","联系方式"};
    private TextView[] tvNatural= {tv_one,tv_two,tv_three,tv_four,tv_five,tv_six,tv_eight,tv_seven};
    private TextView[]  tvCorporation = {tv_one,tv_two,tv_three,tv_four,tv_five,tv_six};
    private TextView[]  thLitigant = {tv_one,tv_two,tv_three,tv_four,tv_five,tv_six};
    public NewOralBean bean ;

    public NewOralHolder(Activity activity) {
        super(activity);

    }

    @Override
    protected View initView() {
        View view = UIUtils.inflate(R.layout.activity_new_oral_item);
        tv_person = (TextView) view.findViewById(R.id.tv_person);
        tv_person_select = (TextView) view.findViewById(R.id.tv_person_select);
        tv_one = (TextView) view.findViewById(R.id.tv_one);
        et_one = (TextView) view.findViewById(R.id.et_one);


        tv_two = (TextView) view.findViewById(R.id.tv_two);
        et_two = (TextView) view.findViewById(R.id.et_two);

        tv_three = (TextView) view.findViewById(R.id.tv_three);
        et_three = (TextView) view.findViewById(R.id.et_three);

        tv_four = (TextView) view.findViewById(R.id.tv_four);
        et_four = (TextView) view.findViewById(R.id.et_four);

        tv_five = (TextView) view.findViewById(R.id.tv_five);
        et_five= (TextView) view.findViewById(R.id.et_five);


        tv_six= (TextView) view.findViewById(R.id.tv_six);
        et_six = (TextView) view.findViewById(R.id.et_six);

        tv_seven = (TextView) view.findViewById(R.id.tv_seven);
        et_seven = (TextView) view.findViewById(R.id.et_seven);
        tv_eight = (TextView) view.findViewById(R.id.tv_eight);
        et_eight = (TextView) view.findViewById(R.id.et_eight);
        return view;
    }

    private void setSex(String sex) {
        if(!TextUtils.isEmpty(sex)){
            if(sex.equals("1")){
                et_two.setText("女");
            }else {
                et_two.setText("男");
            }
        }

    }
    @Override
    protected void refreshView() {
        String type = info.type;
        bean = info;
        if(type.equals("1")){
            tv_seven.setVisibility(View.VISIBLE);
            et_seven.setVisibility(View.VISIBLE);
            tv_eight.setVisibility(View.VISIBLE);
            et_eight.setVisibility(View.VISIBLE);
        }else {
            tv_eight.setVisibility(View.GONE);
            et_eight.setVisibility(View.GONE);
            tv_seven.setVisibility(View.GONE);
            et_seven.setVisibility(View.GONE);
        }

        switch (type){
            case "1":
                tv_person_select.setText("自然人");
                for (int i=0;i<titleNatural.length;i++){
                    setTextNotNull(titleNatural[i],tvNatural[i]);
                }
                setTextNotNull(bean.name,et_one);
                setSex(bean.sex);
                setTextNotNull(bean.nation,et_three);
                setTextNotNull(
                        bean.age,et_four);
                setTextNotNull(bean.job,et_five);
                setTextNotNull(bean.tel,et_six);
                setTextNotNull(bean.address,et_seven);
                setTextNotNull(bean.idcard,et_eight);
                break;
            case "2":
                tv_person_select.setText("法人");
                for (int i=0;i<titleCorporation.length;i++){
                    if(!TextUtils.isEmpty(titleCorporation[i])){
                        tvCorporation[i].setText(titleCorporation[i]);
                    }
                }
                setTextNotNull(bean.faren,et_one);
                setTextNotNull(bean.address,et_two);
                setTextNotNull(bean.tongyi,et_three);
                setTextNotNull(bean.fading,et_four);
                setTextNotNull(bean.weituoren,et_five);
                setTextNotNull(bean.tel,et_six);
                break;
            case "3":
                tv_person_select.setText("非法人组织");
                for (int i=0;i<titleLitigant.length;i++){
                     thLitigant[i].setText(titleLitigant[i]);
                }
                setTextNotNull(bean.faren,et_one);
                setTextNotNull(bean.address,et_two);
                setTextNotNull(bean.tongyi,et_three);
                setTextNotNull(bean.fading,et_four);
                setTextNotNull(bean.weituoren,et_five);
                setTextNotNull(bean.tel,et_six);
                break;
        }
    }

    private void setTextNotNull(String text ,TextView tv){
        if(!TextUtils.isEmpty(text)){
            tv.setText(text);
        }
    }

}
