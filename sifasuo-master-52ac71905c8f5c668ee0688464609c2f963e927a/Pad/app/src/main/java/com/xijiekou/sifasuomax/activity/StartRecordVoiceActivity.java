package com.xijiekou.sifasuomax.activity;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lqr.audio.AudioPlayManager;
import com.lqr.audio.AudioRecordManager;
import com.lqr.audio.IAudioPlayListener;
import com.lqr.audio.IAudioRecordListener;
import com.xijiekou.sifasuomax.R;

import java.io.File;

/**
 * Created by zhouzhuo on 2017/8/9.
 */

public class StartRecordVoiceActivity extends BaseActivity {
    private Button mBtnVoice,btn_start_play,btn_stop_record, btn_stop_play,btn_continue_record;
    private File mAudioDir;
    private RelativeLayout rl_root;
    private AudioRecordManager recordManager;
    private AudioPlayManager playManager = AudioPlayManager.getInstance();
    private String url;
    private TextView tv_back;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_record_voice);
        recordManager =AudioRecordManager.getInstance(this) ;
        initView();
       /// initListener();
    }

    private void initView() {
        rl_root = (RelativeLayout) findViewById(R.id.rl_root);
        mBtnVoice = (Button) findViewById(R.id.btnVoice);
        mBtnVoice.setOnClickListener(this);
        btn_start_play = (Button) findViewById(R.id.btn_start_play);
        btn_start_play.setOnClickListener(this);
        btn_stop_play = (Button) findViewById(R.id.btn_stop_play);
        btn_stop_play.setOnClickListener(this);
        btn_stop_record = (Button) findViewById(R.id.btn_stop_record);
        btn_stop_record.setOnClickListener(this);
        btn_continue_record = (Button) findViewById(R.id.btn_continue_record);
        btn_continue_record.setOnClickListener(this);
        tv_back = (TextView) findViewById(R.id.tv_back);
        tv_back.setOnClickListener(this);

        recordManager.setMaxVoiceDuration(10);
        mAudioDir = new File(Environment.getExternalStorageDirectory(), "sifasuovoice");
        if (!mAudioDir.exists()) {
            mAudioDir.mkdirs();
        }
        recordManager.setAudioSavePath(mAudioDir.getAbsolutePath());
    }

    private void initListener() {
        /*mBtnVoice.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        AudioRecordManager.getInstance(StartRecordVoiceActivity.this).startRecord();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        if (isCancelled(v, event)) {
                            AudioRecordManager.getInstance(StartRecordVoiceActivity.this).willCancelRecord();
                        } else {
                            AudioRecordManager.getInstance(StartRecordVoiceActivity.this).continueRecord();
                        }
                        break;
                    case MotionEvent.ACTION_UP:
                        AudioRecordManager.getInstance(StartRecordVoiceActivity.this).stopRecord();
                        AudioRecordManager.getInstance(StartRecordVoiceActivity.this).destroyRecord();
                        break;
                }
                return false;
            }
        });*/


        recordManager.setAudioRecordListener(new IAudioRecordListener() {

            private TextView mTimerTV;
           // private TextView mStateTV;
            private ImageView mStateIV;
            private PopupWindow mRecordWindow;

            @Override
            public void initTipView() {
                View view = View.inflate(StartRecordVoiceActivity.this, R.layout.popup_audio_wi_vo, null);
                mStateIV = (ImageView) view.findViewById(R.id.rc_audio_state_image);
               // mStateTV = (TextView) view.findViewById(R.id.rc_audio_state_text);
                mTimerTV = (TextView) view.findViewById(R.id.rc_audio_timer);
                mRecordWindow = new PopupWindow(view, -1, -1);
                mRecordWindow.showAtLocation(rl_root, 17, 0, 0);
                mRecordWindow.setFocusable(true);
                mRecordWindow.setOutsideTouchable(false);
                mRecordWindow.setTouchable(false);
            }

            @Override
            public void setTimeoutTipView(int counter) {
                if (this.mRecordWindow != null) {
                    Log.d("zhouzhuo","counter=="+counter);
                    this.mTimerTV.setText(String.format("%s", new Object[]{Integer.valueOf(counter)}));
                    this.mTimerTV.setVisibility(View.VISIBLE);
                    /*this.mStateIV.setVisibility(View.GONE);
                    this.mStateTV.setVisibility(View.VISIBLE);
                    this.mStateTV.setText(R.string.voice_rec);
                    this.mStateTV.setBackgroundResource(R.drawable.bg_voice_popup);
                    this.mTimerTV.setText(String.format("%s", new Object[]{Integer.valueOf(counter)}));
                    this.mTimerTV.setVisibility(View.VISIBLE);*/
                }
            }

            @Override
            public void setRecordingTipView() {
                if (this.mRecordWindow != null) {
                    this.mStateIV.setVisibility(View.VISIBLE);
                    this.mStateIV.setImageResource(R.mipmap.ic_volume_1);
                   // this.mStateTV.setVisibility(View.VISIBLE);
                   // this.mStateTV.setText(R.string.voice_rec);
                   // this.mStateTV.setBackgroundResource(R.drawable.bg_voice_popup);
                    this.mTimerTV.setVisibility(View.GONE);
                }
            }

            @Override
            public void setAudioShortTipView() {
                if (this.mRecordWindow != null) {
                    mStateIV.setImageResource(R.mipmap.ic_volume_wraning);
                   // mStateTV.setText(R.string.voice_short);
                }
            }

            @Override
            public void setCancelTipView() {
                if (this.mRecordWindow != null) {
                    this.mTimerTV.setVisibility(View.GONE);
                    this.mStateIV.setVisibility(View.VISIBLE);
                    this.mStateIV.setImageResource(R.mipmap.ic_volume_cancel);
                   /// this.mStateTV.setVisibility(View.VISIBLE);
                   /// this.mStateTV.setText(R.string.voice_cancel);
                   // this.mStateTV.setBackgroundResource(R.drawable.corner_voice_style);
                }
            }

            @Override
            public void destroyTipView() {
                if (this.mRecordWindow != null) {
                    this.mRecordWindow.dismiss();
                    this.mRecordWindow = null;
                    this.mStateIV = null;
                   // this.mStateTV = null;
                    this.mTimerTV = null;
                }
            }

            @Override
            public void onStartRecord() {
                //开始录制
                Log.d("zhouzhuo","StartRecord===");
            }

            @Override
            public void onFinish(Uri audioPath, int duration) {
                if (this.mRecordWindow != null) {
                    this.mRecordWindow.dismiss();
                    this.mRecordWindow = null;
                    this.mStateIV = null;
                  //  this.mStateTV = null;
                    this.mTimerTV = null;
                }
                url = audioPath.getPath();

                //发送文件
                file = new File(audioPath.getPath());
                if (file.exists()) {
                    Toast.makeText(getApplicationContext(), "录制成功", Toast.LENGTH_SHORT).show();

                }
                Log.d("zhouzhuo","onFinish Record=url=="+url);
                Log.d("zhouzhuo","onFinish Record==="+audioPath);
            }

            @Override
            public void onAudioDBChanged(int db) {
                switch (db / 5) {
                    case 0:
                        this.mStateIV.setImageResource(R.mipmap.ic_volume_1);
                        break;
                    case 1:
                        this.mStateIV.setImageResource(R.mipmap.ic_volume_2);
                        break;
                    case 2:
                        this.mStateIV.setImageResource(R.mipmap.ic_volume_3);
                        break;
                    case 3:
                        this.mStateIV.setImageResource(R.mipmap.ic_volume_4);
                        break;
                    case 4:
                        this.mStateIV.setImageResource(R.mipmap.ic_volume_5);
                        break;
                    case 5:
                        this.mStateIV.setImageResource(R.mipmap.ic_volume_6);
                        break;
                    case 6:
                        this.mStateIV.setImageResource(R.mipmap.ic_volume_7);
                        break;
                    default:
                        this.mStateIV.setImageResource(R.mipmap.ic_volume_8);
                }
            }
        });
        recordManager.startRecord();
    }
    private File file;



    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btnVoice:
                requestPermission(new String[]{Manifest.permission.RECORD_AUDIO
                        , Manifest.permission.WRITE_EXTERNAL_STORAGE
                        , Manifest.permission.WAKE_LOCK
                        , Manifest.permission.READ_EXTERNAL_STORAGE}, new PermissionHandler() {
                    @Override
                    public void onGranted() {
                        initListener();
                    }

                    @Override
                    public void onDenied() {

                    }

                    @Override
                    public boolean onNeverAsk() {
                        return false;
                    }
                });
                break;
            case R.id.btn_start_play:
                startPlay();
                break;
            case R.id.btn_stop_play:
                playManager.stopPlay();
                break;
            case R.id.btn_continue_record:
                recordManager.continueRecord();
                //不需要
                break;
            case R.id.btn_stop_record:
                recordManager.stopRecord();
                recordManager.destroyRecord();
                break;
            case R.id.tv_back:
                Intent intent = getIntent();
                intent.putExtra("path",url);
                setResult(0,intent);
                finish();
                break;

        }
    }

    @Override
    public void onBackPressed() {
        Intent intent = getIntent();
        intent.putExtra("path",url);
        setResult(0,intent);
        super.onBackPressed();



    }

    private void startPlay(){
        AudioPlayManager.getInstance().stopPlay();
        if(file == null||!file.exists()){
            return;
        }
        Uri audioUri = Uri.fromFile(file);
        Log.d("zhouzhuo", audioUri.toString());
        playManager.startPlay(StartRecordVoiceActivity.this, audioUri, new IAudioPlayListener() {
            @Override
            public void onStart(Uri var1) {
                Log.d("zhouzhuo","Star play:"+var1);
            }

            @Override
            public void onStop(Uri var1) {
                Log.d("zhouzhuo","Stop play:"+var1);

            }

            @Override
            public void onComplete(Uri var1) {
                Log.d("zhouzhuo","Complete play:"+var1);
            }
        });
    }
}
