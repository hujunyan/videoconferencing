package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/8/3.
 * 调解结果的三种状态
 */

public class RecordResultView extends RelativeLayout implements View.OnClickListener{
    private FrameLayout fl_one,fl_two,fl_three;
    //1,2,3
    private int type = 1;
    private ImageView iv_result_success_selected,iv_result_failed_selected,iv_result_doing_selected;
    public RecordResultView(Context context) {
        this(context,null);
    }

    public RecordResultView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }
    public void initView(Context context){
        View view  = LayoutInflater.from(context).inflate(R.layout.record_result_view,this,true);
        fl_one = (FrameLayout) view.findViewById(R.id.fl_one);
        fl_one.setOnClickListener(this);
        fl_two = (FrameLayout) view.findViewById(R.id.fl_two);
        fl_two.setOnClickListener(this);
        fl_three = (FrameLayout) view.findViewById(R.id.fl_three);
        fl_three.setOnClickListener(this);
        iv_result_success_selected = (ImageView) view.findViewById(R.id.iv_result_success_selected);
        iv_result_failed_selected = (ImageView) view.findViewById(R.id.iv_result_failed_selected);
        iv_result_doing_selected = (ImageView) view.findViewById(R.id.iv_result_doing_selected);
    }

    public int getType(){
        return type;
    }

    public void setResult(String content){
        if(!TextUtils.isEmpty(content)){
            int result = Integer.parseInt(content);
            type = result;
            switch (result){
                case 1:
                    iv_result_success_selected.setVisibility(View.VISIBLE);
                    iv_result_failed_selected.setVisibility(View.INVISIBLE);
                    iv_result_doing_selected.setVisibility(View.INVISIBLE);
                    break;
                case 2:
                    iv_result_failed_selected.setVisibility(View.VISIBLE);
                    iv_result_success_selected.setVisibility(View.INVISIBLE);
                    iv_result_doing_selected.setVisibility(View.INVISIBLE);
                    break;
                case 3:
                    iv_result_doing_selected.setVisibility(View.VISIBLE);
                    iv_result_success_selected.setVisibility(View.INVISIBLE);
                    iv_result_failed_selected.setVisibility(View.INVISIBLE);
                    break;
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fl_one:
                iv_result_success_selected.setVisibility(View.VISIBLE);
                if(type == 2){
                    iv_result_failed_selected.setVisibility(View.INVISIBLE);
                }if(type == 3){
                    iv_result_doing_selected.setVisibility(View.INVISIBLE);
                }
                type = 1;
                break;
            case R.id.fl_two:
                iv_result_failed_selected.setVisibility(View.VISIBLE);
                if(type ==1){
                    iv_result_success_selected.setVisibility(View.INVISIBLE);
                }if(type == 3){
                    iv_result_doing_selected.setVisibility(View.INVISIBLE);
                }
                type = 2;
                break;
            case R.id.fl_three:
                iv_result_doing_selected.setVisibility(View.VISIBLE);
                if(type ==1){
                    iv_result_success_selected.setVisibility(View.INVISIBLE);
                }else if(type == 2){
                    iv_result_failed_selected.setVisibility(View.INVISIBLE);
                }
                type = 3;
                break;
        }

    }
}
