package com.xijiekou.sifasuomax.view;

import android.Manifest;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.BaseActivity;
import com.xijiekou.sifasuomax.activity.VoiceBaseActivity;
import com.xijiekou.sifasuomax.dialog.SelectPerformancePop;
import com.xijiekou.sifasuomax.dialog.WritePadDialog;
import com.xijiekou.sifasuomax.utils.BitmapUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by zhouzhuo on 2017/7/25.
 */

public class VoiceEditTextView extends RelativeLayout implements View.OnClickListener{
    private RelativeLayout rl_voice;
    private RelativeLayout rl_voice2;
    private EditText et_detail;
    private EditText et_detail2;
    private VoiceBaseActivity activity;
    private TextView tv_sign;
    private ImageView iv_sign;
    private YearMonthDayView yearMonthDayView;
    private EditText et_performance;
    private TextView tv_performance2;
    private int currentPerformance=0 ;
    private String perferenceResult;
    private File file;
    private EditText et_number_one2;//一式几份



    public VoiceEditTextView(Context context) {
        this(context,null);
    }

    public VoiceEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (VoiceBaseActivity) context;
        initView(context);

    }
    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.voice_edit_view,this,true);
        tv_sign = (TextView) view.findViewById(R.id.tv_sign);
        et_performance = (EditText) view.findViewById(R.id.et_performance);
        et_performance.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                    perferenceResult = s.toString();
            }
        });
        tv_performance2 = (TextView) findViewById(R.id.tv_performance2);
        tv_performance2.setOnClickListener(this);
        tv_sign.setOnClickListener(this);
        iv_sign = (ImageView) findViewById(R.id.iv_sign);
        rl_voice = (RelativeLayout) view.findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        rl_voice2 = (RelativeLayout) findViewById(R.id.rl_voice2);
        rl_voice2.setOnClickListener(this);
        et_detail = (EditText) view.findViewById(R.id.et_detail);

        et_detail2 = (EditText) view.findViewById(R.id.et_detail2);

        yearMonthDayView = (YearMonthDayView) view.findViewById(R.id.select_date);
        et_number_one2 = (EditText) view.findViewById(R.id.et_number_one2);
    }
    public String getDate(){
        return yearMonthDayView.getDate();
    }

    public void setDate(String data){
        if(!TextUtils.isEmpty(data)){
            String ymd = data.split(" ")[0];
            yearMonthDayView.setData(ymd);
        }
    }

    public String getArgue(){
        return et_detail.getText().toString();
    }
    public void setText(String text){
        if(!TextUtils.isEmpty(text)){
            et_detail.setText(text);
        }
    }

    public String getNumber(){
        String number = et_number_one2.getText().toString();
        if(TextUtils.isEmpty(number)){
            number = "2";
        }
        return number;
    }
    public void setNumber(String number){
        if(!TextUtils.isEmpty(number)){
            et_number_one2.setText(number);
        }
    }

    public String getArgue2(){
        return et_detail2.getText().toString();
    }
    public void setText2(String text){
        if(!TextUtils.isEmpty(text)){
            et_detail2.setText(text);
        }
    }

    public String getSignature(){
        if(file ==null ||!file.exists()){
            return null;
        }
        return file.getPath();
    }

    public void setSign(String url){
        Log.d("zhouzhuo","file path==="+url);
        if(!TextUtils.isEmpty(url)){
            Glide.with(activity).load(url).into(iv_sign);
        }
    }

    public String getPerformance(){
        Log.d("zhouzhuo","currentPerformance=="+currentPerformance);
        Log.d("zhouzhuo","currentPerformance=内容="+et_performance.getText().toString());
        if(currentPerformance ==0){
            return "即时履行";
        }else {
            return et_performance.getText().toString();
        }

    }

    public void setPerference(String perference){
        if((TextUtils.isEmpty(perference)&&currentPerformance==0)||
                (!TextUtils.isEmpty(perference)&&perference.equals("即时履行"))){
            et_performance.setVisibility(INVISIBLE);
            tv_performance2.setText("即时履行");
        }else {
            et_performance.setVisibility(VISIBLE);
            tv_performance2.setText("其他");
            if(!TextUtils.isEmpty(perference)){
                et_performance.setText(perference);
            }
        }
        Log.d("zhouzhuo","perferenceResult:"+perferenceResult);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_voice:
                activity.requestVoicePermission();
                activity.setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail.getText().insert(et_detail.getSelectionStart(),result);
                        }

                    }
                });
                break;
            case R.id.rl_voice2:
                activity.requestVoicePermission();
                activity.setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail2.getText().insert(et_detail2.getSelectionStart(),result);
                        }
                    }
                });
                break;
            case R.id.tv_sign:
                WritePadDialog dialog = new WritePadDialog(activity);
                dialog.setDialogListener(new WritePadDialog.DialogListener() {
                    @Override
                    public void refreshActivity(final Bitmap object)  {
                        iv_sign.setImageBitmap(object);
                        activity.requestPermission(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE}, new BaseActivity.PermissionHandler() {
                            @Override
                            public void onGranted() {
                                try {
                                    file =  BitmapUtils.saveFile(object,System.currentTimeMillis()+".png");
                                    Log.d("zhouzhuo","file:"+file);
                                    Log.d("zhouzhuo","file name:"+file.getName());
                                } catch (IOException e) {
                                    Log.d("zhouzhuo","file name:"+e.toString());
                                    e.printStackTrace();
                                }
                                Log.d("zhouzhuo","Bitmap object:"+object);
                            }

                            @Override
                            public void onDenied() {

                            }

                            @Override
                            public boolean onNeverAsk() {
                                return false;
                            }
                        });

                    }
                });
                dialog.show();
                break;
            case R.id.tv_performance2:
                SelectPerformancePop pop = new SelectPerformancePop(activity,currentPerformance);
                pop.setListener(new SelectPerformancePop.SelectItemListener() {
                    @Override
                    public void select(int type) {
                        currentPerformance = type;
                        if(currentPerformance ==0){
                            setPerference("即时履行");
                        }else {
                            setPerference(perferenceResult);
                        }
                    }
                });
                pop.showPopupWindow(tv_performance2);
               break;
        }

    }


}
