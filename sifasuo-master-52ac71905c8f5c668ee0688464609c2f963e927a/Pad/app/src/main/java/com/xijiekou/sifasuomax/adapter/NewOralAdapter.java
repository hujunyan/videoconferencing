package com.xijiekou.sifasuomax.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.holder.BaseHolder;
import com.xijiekou.sifasuomax.holder.NewOralHolder;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/25.
 */

public class NewOralAdapter extends BaseAdapter {
    public Activity activity;
    public ArrayList<NewOralBean> list;

    public NewOralAdapter(Activity activity,ArrayList<NewOralBean> list){
        this.activity = activity;
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseHolder<NewOralBean> holder;
        if(convertView == null){
            holder = new NewOralHolder(activity);
           // convertView.setTag(holder);
        }else {
            holder = (BaseHolder<NewOralBean>) convertView.getTag();
        }
        holder.setData(list.get(position));
        return holder.getContentView();
    }


}
