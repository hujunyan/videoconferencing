package com.xijiekou.sifasuomax.holder;

import android.content.Intent;
import android.graphics.Color;
import android.os.Environment;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.EvidenceEnteringActivity;
import com.xijiekou.sifasuomax.activity.VoicePlayActivity;
import com.xijiekou.sifasuomax.bean.MultimediaBean;
import com.xijiekou.sifasuomax.dialog.SearchBigPictureDialog;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.DeleteMediaModel;
import com.xijiekou.sifasuomax.model.PhotoUploadModel;
import com.xijiekou.sifasuomax.model.VideoUploadModel;
import com.xijiekou.sifasuomax.model.VoiceUploadModel;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.utils.UIUtils;
import com.xijiekou.sifasuomax.video.PlayActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by zhouzhuo on 2017/8/7.
 */

public class EvidenceEnteringHolder extends BaseHolder<MultimediaBean> implements View.OnClickListener{
    private TextView tv_number;
    private TextView btn_upload;
    private TextView btn_delete;
    private TextView tv_time;
    private ImageView iv_type;
    private TextView tv_type;
    private TextView tv_state;
    private MultimediaBean bean;
    private ImageView iv_detail;
    private static final int REQUEST_CODE_PHOTO = 0;
    private static final int REQUEST_CODE_VOICE = 1;
    private static final int REQUEST_CODE_VIDEO = 2;
    private static final String AUDIO_FILE_PATH =
            Environment.getExternalStorageDirectory().getPath() + "/recorded_audio.wav";
    private int position;


    private int type;
    private EvidenceEnteringActivity activity;
    private String url;

    private Handler handler;

    public EvidenceEnteringHolder(EvidenceEnteringActivity activity, int type,int position) {
        super(activity);
        this.activity = activity;
        this.type = type;
        this.position = position;
        handler = new Handler();
        initData();
    }



    @Override
    protected View initView() {
        View view = UIUtils.inflate(R.layout.evidnece_entering_item);
        tv_number = (TextView) view.findViewById(R.id.tv_number);
        btn_upload = (TextView) view.findViewById(R.id.btn_upload);
        btn_upload.setOnClickListener(this);
        btn_delete = (TextView) view.findViewById(R.id.btn_delete);
        btn_delete.setOnClickListener(this);
        tv_time = (TextView) view.findViewById(R.id.tv_time);
        iv_type = (ImageView) view.findViewById(R.id.iv_type);
        tv_type = (TextView) view.findViewById(R.id.tv_type);
        tv_state = (TextView) view.findViewById(R.id.tv_state);
        iv_detail = (ImageView) view.findViewById(R.id.iv_detail);
        iv_detail.setOnClickListener(this);
        return view;
    }

    private void initData() {
        switch (type){
            case 0:
                iv_type.setImageResource(R.drawable.picture);
                tv_type.setText("照片");
                break;
            case 1:
                iv_type.setImageResource(R.drawable.voice);
                tv_type.setText("录音");
                break;
            case 2:
                iv_type.setImageResource(R.drawable.video);
                tv_type.setText("录像");
                break;
        }

    }


    public void uploadVoice(final String path) {
        VoiceUploadModel model = new VoiceUploadModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                Log.d("zhouzhuo","音频上传成功=111="+message);
                try {
                    JSONObject object = new JSONObject(message);
                    int code = object.getInt("code");
                    if(code == 1){
                        url = object.getJSONObject("data").getString("url");
                        new Thread(){
                            public void run(){
                                handler.post(runnableUi);
                            }
                        }.start();
                        deleteFile(path);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void failed() {
                Log.d("zhouzhuo","音频上传失败==");
            }
        });
        model.voiceUpload(activity,activity.getCaseId(),path,bean.id);

    }

    // 构建Runnable对象，在runnable中更新界面
    Runnable   runnableUi=new  Runnable(){
        @Override
        public void run() {
            //更新界面
            tv_state.setText("已上传");
            bean.status = "2";
            tv_state.setTextColor(Color.parseColor("#cdcecf"));
        }

    };

    public void uploadPhoto(final String path) {
        if(TextUtils.isEmpty(path)){
            ToastUtils.showShort(activity,"文件不存在或已被删除");
            return;
        }
        PhotoUploadModel model = new PhotoUploadModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {

                try {
                    JSONObject object = new JSONObject(message);
                    int code = object.getInt("code");
                    if(code == 1){
                        url = object.getJSONObject("data").getString("url");
                        new Thread(){
                            public void run(){
                                handler.post(runnableUi);
                            }
                        }.start();
                     //   deleteFile(path);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("zhouzhuo","图片上传成功=="+message);
                //TODO

            }

            @Override
            public void failed() {
                Log.d("zhouzhuo","图片上传失败==");


            }
        });
        model.photoUpload(activity,activity.getCaseId(),path,bean.id);
    }

    @Override
    protected void refreshView() {
        bean =getInfo();
        url = bean.url;
        String number = bean.id;
        if(!TextUtils.isEmpty(number)){
            tv_number.setText(number);
        }
        String time = bean.datetime;
        if(!TextUtils.isEmpty(time)){
            tv_time.setText(time);
        }
        String state = bean.status;
        Log.d("zhozhuo","status=="+state);
        if(!TextUtils.isEmpty(state)){
            if(state.equals("1")){
                tv_state.setText("未上传");
                tv_state.setTextColor(Color.parseColor("#2b6aaa"));
            }else {
                tv_state.setText("已上传");
                tv_state.setTextColor(Color.parseColor("#cdcecf"));
            }
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_upload:
                if(!bean.status.equals("1")){
                    ToastUtils.showShort(activity,"已经上传过了");
                }else {
                    switch (type){
                        case 0:
                            uploadPhoto(bean.url);
                            break;
                        case 1:
                            uploadVoice(bean.url);
                            break;
                        case 2:
                            uploadVideo(bean.url);
                            break;
                    }
                }
                break;
            case R.id.iv_detail:
                switch (type){
                    case 0:
                        if(TextUtils.isEmpty(url)){
                            ToastUtils.showShort(activity,"文件不存在或已被删除");
                        }else {
                            /*if(!bean.status.equals("1")){
                                url = UrlDomainUtil.voiceUrl+ url;
                            }*/
                            Log.d("zhouzhuo","url=="+url);
                            SearchBigPictureDialog dialog = new SearchBigPictureDialog(activity,url);
                            dialog.show();
                        }
                        break;
                    case 1:
                        //有bug
                       if(!TextUtils.isEmpty(url)){
                           Log.d("zhouzhuo","voice url2222=="+url);
                           /*SearchVoiceDialog dialog = new SearchVoiceDialog(activity,url);
                           dialog.show();*/
                           Intent intent = new Intent(activity, VoicePlayActivity.class);
                           Log.d("zhouzhuo","录音路径路径==="+url);
                           intent.putExtra("url", url);
                           IntentUtils.startActivity(activity,intent);
                        }
                        break;
                    case 2:
                        Intent intent = new Intent(activity, PlayActivity.class);
                        Log.d("zhouzhuo","视频路径路径==="+url);
                        intent.putExtra(PlayActivity.DATA, url);
                        IntentUtils.startActivity(activity,intent);
                        break;
                }
                break;
            case R.id.btn_delete:
                DeleteMediaModel mediaModel = new DeleteMediaModel();
                mediaModel.setCallBackListener(new BaseModel.CallBackListener<int[]>() {
                    @Override
                    public void success(int[] message) {
                        activity.deleteItem(message[0],message[1]);
                    }
                    @Override
                    public void failed() {

                    }
                });
                mediaModel.delete(bean.id,type,position);
                break;
        }

    }

    private void uploadVideo(final String path) {
        if(TextUtils.isEmpty(path)){
            ToastUtils.showShort(activity,"文件不存在或已被删除");
            return;
        }
        VideoUploadModel model = new VideoUploadModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                Log.d("zhouzhuo","视频上传成功=="+message);
                //TODO
                try {
                    JSONObject object = new JSONObject(message);
                    int code = object.getInt("code");
                    if(code == 1){
                        url = object.getJSONObject("data").getString("url");
                        new Thread(){
                            public void run(){
                                handler.post(runnableUi);
                            }
                        }.start();
                        deleteFile(path);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failed() {
                Log.d("zhouzhuo","视频上传失败==");


            }
        });
        model.videoUpload(activity,activity.getCaseId(),path,bean.id);
    }


    private void deleteFile(String path){
        Log.d("zhouzhuo","deleteFile 文件路径:"+path);
        File file = new File(path);
        if (file.exists()){
            file.delete();
            Log.d("zhouzhuo","删除文件成功");
        }else {
            Log.d("zhouzhuo","删除文件失败");
        }

    }
}
