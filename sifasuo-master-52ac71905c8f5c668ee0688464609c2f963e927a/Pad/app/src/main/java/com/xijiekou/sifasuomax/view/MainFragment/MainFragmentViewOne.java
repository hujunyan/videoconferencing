package com.xijiekou.sifasuomax.view.MainFragment;

import android.app.Activity;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.xijiekou.sifasuomax.utils.smoothListView.view.AbsHeaderView;

/**
 * Created by zhouzhuo on 2017/9/12.
 */

public class MainFragmentViewOne extends AbsHeaderView<String> implements View.OnClickListener{
    private WebView wevView;
    private EditText et_content;
    private TextView tv_more;
    public MainFragmentViewOne(Activity activity) {
        super(activity);
    }

    @Override
    public void onClick(View v) {

    }


    public void loadUrl(){
        WebSettings settings = wevView.getSettings();
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setJavaScriptEnabled(true);// 开启js功能, 同时, 百度网页不再跳转到浏览器
        settings.setDefaultTextEncodingName("UTF-8") ;
        wevView.loadUrl(UrlDomainUtil.urlHeader+"/index/news.html");
    }

    @Override
    protected void getView(String url, ListView listView) {
        View view = mInflate.inflate(R.layout.main_fragment_view_one, listView, false);
        wevView = (WebView) view.findViewById(R.id.web_view);
        wevView.setScrollContainer(false);
        wevView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        loadUrl();
        listView.addHeaderView(view);

    }
}
