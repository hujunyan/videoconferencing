package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.bean.VisitRecordBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/5.
 */

public class VisitRecordModel extends BaseModel{
    public void registration(VisitRecordBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/createCasePart7";
        Log.d("zhouzhuo","回访记录:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(bean!=null){
            if(!TextUtils.isEmpty(bean.case_id)){
                builder.addParams("case_id", bean.case_id);
            }
            if(!TextUtils.isEmpty(bean.person)){
                builder.addParams("person", bean.person);
            }
            if(!TextUtils.isEmpty(bean.number)){
                builder.addParams("number",bean.number);
            }
            if(!TextUtils.isEmpty(bean.visit_reason)){
                builder.addParams("visit_reason",bean.visit_reason);
            }
            if(!TextUtils.isEmpty(bean.date)){
                builder.addParams("date", bean.date);
            }
            if(!TextUtils.isEmpty(bean.time_range)){
                builder.addParams("time_range",bean.time_range);
            }
            if(!TextUtils.isEmpty(bean.datetime)){
                builder.addParams("formdate",bean.datetime);
            }
            if(!TextUtils.isEmpty(bean.visit_state)){
                builder.addParams("visit_state",bean.visit_state);
            }
            if(!TextUtils.isEmpty(bean.committee)){
                builder.addParams("committee",bean.committee);
            }
            if(!TextUtils.isEmpty(bean.visit_url)){
                builder.addParams("visit_url",bean.visit_url);
            }

        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","回访记录请求失败:"+e.getMessage());
                Log.d("zhouzhuo","回访记录请求失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                   // callBackListener.success(response);
                    Log.d("zhouzhuo","回访记录请求成功 另存为或者创建:"+response);
                    PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);
                }
            }
        });
    }
}
