package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class MediationMonthFormBean {
    public String id_0;
    public String id_1;
    public String id_2;
    public String id_3;
    public String id_4;
    public String id_5;
    public String id_6;
    public String id_7;
    public String id_8;
    public String id_9;
    public String id_10;
    public String id_11;
    public String id_12;
    public String year;
    public String month;
    public String company;

    @Override
    public String toString() {
        return "MediationMonthFormBean{" +
                "id_0='" + id_0 + '\'' +
                ", id_1='" + id_1 + '\'' +
                ", id_2='" + id_2 + '\'' +
                ", id_3='" + id_3 + '\'' +
                ", id_4='" + id_4 + '\'' +
                ", id_5='" + id_5 + '\'' +
                ", id_6='" + id_6 + '\'' +
                ", id_7='" + id_7 + '\'' +
                ", id_8='" + id_8 + '\'' +
                ", id_9='" + id_9 + '\'' +
                ", id_10='" + id_10 + '\'' +
                ", id_11='" + id_11 + '\'' +
                ", id_12='" + id_12 + '\'' +
                ", id_13='" + id_13 + '\'' +
                ", id_14='" + id_14 + '\'' +
                ", id_15='" + id_15 + '\'' +
                ", id_16='" + id_16 + '\'' +
                ", id_17='" + id_17 + '\'' +
                ", id_18='" + id_18 + '\'' +
                ", id_19='" + id_19 + '\'' +
                ", id_20='" + id_20 + '\'' +
                ", id_21='" + id_21 + '\'' +
                ", id_22='" + id_22 + '\'' +
                ", id_23='" + id_23 + '\'' +
                ", id_24='" + id_24 + '\'' +
                ", id_25='" + id_25 + '\'' +
                ", id_26='" + id_26 + '\'' +
                ", id_27='" + id_27 + '\'' +
                ", id_28='" + id_28 + '\'' +
                ", id_29='" + id_29 + '\'' +
                ", id_30='" + id_30 + '\'' +
                ", id_31='" + id_31 + '\'' +
                ", id_32='" + id_32 + '\'' +
                ", id_33='" + id_33 + '\'' +
                ", id_34='" + id_34 + '\'' +
                ", id_35='" + id_35 + '\'' +
                ", id_36='" + id_36 + '\'' +
                ", id_37='" + id_37 + '\'' +
                ", id_38='" + id_38 + '\'' +
                ", id_39='" + id_39 + '\'' +
                ", id_40='" + id_40 + '\'' +
                ", id_41='" + id_41 + '\'' +
                ", id_42='" + id_42 + '\'' +
                ", id_43='" + id_43 + '\'' +
                ", id_44='" + id_44 + '\'' +
                ", id_45='" + id_45 + '\'' +
                ", id_46='" + id_46 + '\'' +
                ", name='" + name + '\'' +
                ", id='" + id + '\'' +
                '}';
    }

    public String id_13;
    public String id_14;
    public String id_15;
    public String id_16;
    public String id_17;
    public String id_18;
    public String id_19;
    public String id_20;
    public String id_21;
    public String id_22;
    public String id_23;
    public String id_24;
    public String id_25;
    public String id_26;
    public String id_27;
    public String id_28;
    public String id_29;
    public String id_30;
    public String id_31;
    public String id_32;
    public String id_33;
    public String id_34;
    public String id_35;
    public String id_36;
    public String id_37;
    public String id_38;
    public String id_39;
    public String id_40;
    public String id_41;
    public String id_42;
    public String id_43;
    public String id_44;
    public String id_45;
    public String id_46;
    public String name = "人民调解月报表";
    public String id;
}
