package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.NewOralBean;

/**
 * Created by zhouzhuo on 2017/8/2.
 * 当事人详情
 */

public class AddThreePersonActivity extends BaseActivity {
    EditText et_one;
    EditText et_two;
    EditText et_three;
    EditText et_four;
    EditText et_five;
    EditText et_six;
    private String serach;
    private Button btn_confirm;


    // 0  自然人 Natural   1 法人 Corporation  2非法人组织  Litigant
    private NewOralBean bean;
    private int selectPosition;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person_three);
        initView();
        initData();

    }

    private void initData() {
        bean = (NewOralBean) getIntent().getSerializableExtra("bean");
        selectPosition = getIntent().getIntExtra("position",-1);
        if(bean == null){
            bean = new NewOralBean();
        }else {
            serach = "search";
            String faren = bean.faren;
            setTextNotNull(et_one,faren);
            String address = bean.address;
            setTextNotNull(et_two,address);
            String tongyi = bean.tongyi;
            setTextNotNull(et_three,tongyi);
            String fading = bean.fading;
            setTextNotNull(et_four,fading);
            String weituo = bean.weituoren;
            setTextNotNull(et_five,weituo);
            String mobile = bean.tel;
            setTextNotNull(et_six,mobile);
        }
    }

    private void initView() {
        et_one = (EditText) findViewById(R.id.et_one);
        et_two = (EditText) findViewById(R.id.et_two);
        et_three = (EditText) findViewById(R.id.et_three);
        et_four = (EditText) findViewById(R.id.et_four);
        et_five= (EditText) findViewById(R.id.et_five);
        et_six = (EditText) findViewById(R.id.et_six);
        et_one.addTextChangedListener(new MyTextWatcher(0));
        et_two.addTextChangedListener(new MyTextWatcher(1));
        et_three.addTextChangedListener(new MyTextWatcher(2));
        et_four.addTextChangedListener(new MyTextWatcher(3));
        et_five.addTextChangedListener(new MyTextWatcher(4));
        et_six.addTextChangedListener(new MyTextWatcher(5));


        btn_confirm = (Button) findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(this);

    }


    private class  MyTextWatcher implements TextWatcher {
        private int position;
        public MyTextWatcher(int position){
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            switch (position){
                case 0:
                    bean.faren = s.toString();
                    break;
                case 1:
                    bean.address = s.toString();
                    break;
                case 2:
                    bean.tongyi = s.toString();
                    break;
                case 3:
                    bean.fading = s.toString();
                    break;
                case 4:
                    bean.weituoren = s.toString();
                    break;
                case 5:
                    bean.tel = s.toString();
                    break;

            }

        }
    }
    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_confirm:
                Intent intent = getIntent();
                Bundle bundle = new Bundle();
                bundle.putSerializable("bean",bean);
                bundle.putInt("position",selectPosition);
                intent.putExtras(bundle);
                if(TextUtils.isEmpty(serach)){
                    setResult(3,intent);
                }else {
                    setResult(30,intent);
                }
                finish();
                break;
        }
    }


    private void setTextNotNull(EditText tv,String text){
        if(!TextUtils.isEmpty(text)){
            tv.setText(text);
        }
    }
}
