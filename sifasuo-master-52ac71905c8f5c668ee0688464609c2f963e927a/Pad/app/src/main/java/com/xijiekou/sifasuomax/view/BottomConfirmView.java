package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/9/8.
 * 底部打印确定按钮
 */

public class BottomConfirmView extends RelativeLayout implements View.OnClickListener{
    private TextView tv_next;
    private PrintView printView;
    private TextView tv_save;
    private LinearLayout ll_save;
    private TextView ll_holder;

    public BottomConfirmView(Context context) {
        this(context,null);
    }

    public BottomConfirmView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.bottom_confirm_view,this,true);
        ll_save = (LinearLayout) view.findViewById(R.id.ll_save);
        ll_holder = (TextView) view.findViewById(R.id.ll_holder);
        tv_next = (TextView) view.findViewById(R.id.tv_next);
        tv_next.setOnClickListener(this);
        printView = (PrintView) view.findViewById(R.id.printView);
        printView.setOnClickListener(this);
        tv_save = (TextView) view.findViewById(R.id.tv_save);
        tv_save.setOnClickListener(this);

    }

    public void showSaveView(){
        ll_save.setVisibility(VISIBLE);
        ll_holder.setVisibility(GONE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_next:
                Log.d("zhouzhuo","确认=====");
                if(confirmListener!=null){
                    confirmListener.confirm();
                }
                break;
            case R.id.printView:
                if(printListener!=null){
                    printListener.print();
                }
                break;
            case R.id.tv_save:
                Log.d("zhouzhuo","保存=====");
                if(saveListener!=null){
                    saveListener.save();
                }
                break;
        }
    }
    public interface SaveListener{
        void save();
    }

    public void setSaveListener(SaveListener saveListener){
        this.saveListener = saveListener;
    }

    public SaveListener saveListener;


    public interface ConfirmListener{
        void confirm();
    }

    public void setConfirmListener(ConfirmListener confirmListener) {
        this.confirmListener = confirmListener;
    }

    public void setPrintListener(PrintListener printListener) {
        this.printListener = printListener;
    }

    public ConfirmListener confirmListener;

    public interface PrintListener{
        void print();
    }
    public PrintListener printListener;


    public void hidePrint(){
        printView.setEnclick();
    }
}
