package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/9/12.
 */

public class ProjectFinalReportBean {
    public String case_id;
    public String begin_time;
    public String end_time;
    public String mediate;
    public String litigant;
    public String attend;
    public String record;
    public String content;
    public String result;
    public String resultcode;
    public String create_time;
    public String isAudit;
}
