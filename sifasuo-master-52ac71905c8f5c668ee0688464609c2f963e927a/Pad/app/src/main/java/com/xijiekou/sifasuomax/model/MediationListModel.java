package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/8/14.
 *
 */

public class MediationListModel extends BaseModel{
    public void getList(String page,String type,String company){
        String url = UrlDomainUtil.urlHeader+"/Staff/getReportList";
        if(!TextUtils.isEmpty(company)){
            url+="/company/"+company;
        }
        Log.d("zhouzhuo","company:"+company);
        if(!TextUtils.isEmpty(page)){
           url+="/page/"+page;
        }
        if(!TextUtils.isEmpty(type)){
            url+="/type/"+type;
        }
        Log.d("zhouzhuo","请求月报表 list==="+url);

        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response == null){
                            return;
                        }

                        Log.d("zhouzhuo"," 请求月报表 list==="+response);

                        callBackListener.success(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);

    }
}
