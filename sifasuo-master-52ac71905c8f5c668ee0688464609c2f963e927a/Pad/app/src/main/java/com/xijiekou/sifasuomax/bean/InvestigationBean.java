package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/8/4.
 *
 */

public class InvestigationBean {
    public String id;
    public String case_id;
    public String date;
    public String time_range;
    public String time;
    public String location;
    public String attend;
    public String surveyed;
    public String record;
    public String litigant_sign;
    public String litiganted_sign;
    public String record_sign;
    public String create_time;
    public String isAudit;

}
