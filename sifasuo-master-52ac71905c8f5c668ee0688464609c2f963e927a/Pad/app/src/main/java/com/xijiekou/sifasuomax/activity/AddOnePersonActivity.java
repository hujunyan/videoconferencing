package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.codbking.widget.DatePickDialog;
import com.codbking.widget.OnChangeLisener;
import com.codbking.widget.OnSureLisener;
import com.codbking.widget.bean.DateType;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.dialog.SelectNationPop;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.SelectSexPop;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zhouzhuo on 2017/8/2.
 * 当事人详情  自然人
 */

public class AddOnePersonActivity extends BaseActivity {
    TextView tv_person;
    TextView tv_person_select;
    EditText et_one;
    TextView et_two;
    TextView et_three;
    TextView et_four;
    EditText et_five;
    EditText et_six;
    EditText et_seven;
    EditText et_eight;
    private Button btn_confirm;

    private ImageView iv_arrow;
    private ImageView iv_nation;

    private FrameLayout fl_two;
    private FrameLayout fl_three;
    private String serach;
    private EditText et_mingzu;
    private String mingzu;

    // 0  自然人 Natural   1 法人 Corporation  2非法人组织  Litigant
    NewOralBean bean;
    private int selectPosition;
    private int currentPosition;
    private final  String[] nations = {"汉族","满族","回族","蒙古族","藏族","其他"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_person_one);
        initView();
        initData();

    }

    private void initData() {
        bean = (NewOralBean) getIntent().getSerializableExtra("bean");
        selectPosition = getIntent().getIntExtra("position",-1);
        if(bean == null){
            bean = new NewOralBean();
            bean.sex = "0";
        }else {
                serach = "search";
                String name = bean.name;
                setTextNotNull(et_one,name);
                setSex(bean.sex);
                String nation = bean.nation;
                setTextNation(nation);
                String age = bean.age;
                if(!TextUtils.isEmpty(age)){
                    et_four.setText(age);
                }

                //setTextNotNull(et_four,age);
                String job = bean.job;
                setTextNotNull(et_five,job);
                String mobile = bean.tel;
                setTextNotNull(et_six,mobile);
                String address = bean.address;
                setTextNotNull(et_seven,address);
                String idcard = bean.idcard;
                setTextNotNull(et_eight,idcard);
        }

    }

    private void setTextNation(String nation) {
            if(TextUtils.isEmpty(nation)){
                    currentPosition = 0;
                    et_mingzu.setVisibility(View.INVISIBLE);
                    et_three.setText("汉族");
                }else if(nation.equals("汉族")){
                    currentPosition = 0;
                    et_three.setText("汉族");
                    et_mingzu.setVisibility(View.INVISIBLE);
                }else if(nation.equals("满族")){
                    currentPosition = 1;
                    et_three.setText("满族");
                    et_mingzu.setVisibility(View.INVISIBLE);
                }else if(nation.equals("回族")){
                    currentPosition = 2;
                    et_three.setText("回族");
                    et_mingzu.setVisibility(View.INVISIBLE);
                }else if(nation.equals("蒙古族")){
                    currentPosition = 3;
                    et_three.setText("蒙古族");
                    et_mingzu.setVisibility(View.INVISIBLE);
                }else if(nation.equals("藏族")){
                    currentPosition = 4;
                    et_three.setText("藏族");
                    et_mingzu.setVisibility(View.INVISIBLE);
                }else {
                    currentPosition = 5;
                    et_three.setText("其他");
                    et_mingzu.setVisibility(View.VISIBLE);
                    if(!TextUtils.isEmpty(mingzu)){
                      et_mingzu.setText(mingzu);
                    }else {
                        et_mingzu.setText(bean.nation);
                    }
                }
            }

    //0  1
    private void setSex(String sex) {
        if(sex.equals("1")){
            et_two.setText("女");
        }else {
            et_two.setText("男");
        }
    }

    private void initView() {
        tv_person = (TextView) findViewById(R.id.tv_person);
        tv_person_select = (TextView) findViewById(R.id.tv_person_select);
        tv_person_select.setOnClickListener(this);
        et_one = (EditText) findViewById(R.id.et_one);
        et_two = (TextView) findViewById(R.id.et_two);
        et_three = (TextView) findViewById(R.id.et_three);

        et_four = (TextView) findViewById(R.id.et_four);
        et_five= (EditText) findViewById(R.id.et_five);


        et_six = (EditText) findViewById(R.id.et_six);

        et_seven = (EditText) findViewById(R.id.et_seven);
        et_eight = (EditText) findViewById(R.id.et_eight);
        et_mingzu = (EditText) findViewById(R.id.et_mingzu);

        et_one.addTextChangedListener(new MyTextWatcher(0));
        et_mingzu.addTextChangedListener(new MyTextWatcher(2));
        et_four.setOnClickListener(this);
        et_five.addTextChangedListener(new MyTextWatcher(4));
        et_six.addTextChangedListener(new MyTextWatcher(5));
        et_seven.addTextChangedListener(new MyTextWatcher(6));
        et_eight.addTextChangedListener(new MyTextWatcher(7));



        btn_confirm = (Button) findViewById(R.id.btn_confirm);
        btn_confirm.setOnClickListener(this);

        fl_two = (FrameLayout) findViewById(R.id.fl_two);
        fl_three = (FrameLayout) findViewById(R.id.fl_three);
        iv_nation = (ImageView) findViewById(R.id.iv_nation);
        iv_nation.setOnClickListener(this);

        iv_arrow = (ImageView) findViewById(R.id.iv_arrow);
        iv_arrow.setOnClickListener(this);
    }


    private class  MyTextWatcher implements TextWatcher {
        private int position;
        public MyTextWatcher(int position){
            this.position = position;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            if(bean!=null){
                switch (position){
                    case 0:
                        bean.name = s.toString();
                        break;
                    case 1:
                       // natural.sex = s.toString();
                        break;
                    case 2:
                        mingzu = s.toString();
                        bean.nation = s.toString();
                        break;
                    case 3:
                       // bean.age = s.toString();
                        break;
                    case 4:
                        bean.job = s.toString();
                        break;
                    case 5:
                        bean.tel = s.toString();
                        break;
                    case 6:
                        bean.address = s.toString();
                        break;
                    case 7:
                        bean.idcard = s.toString();
                        break;
                }
            }
        }
    }

    private void selectDate(){
        DatePickDialog dialog = new DatePickDialog(this);
        //设置上下年分限制
        dialog.setYearLimt(100);
        //设置标题
        dialog.setTitle("选择时间");
        //设置类型
        dialog.setType(DateType.TYPE_YMD);
        //设置消息体的显示格式，日期格式
        dialog.setMessageFormat("yyyy-MM-dd HH:mm");
        //设置选择回调
        dialog.setOnChangeLisener(new OnChangeLisener() {
            @Override
            public void onChanged(Date date) {

            }
        });
        //设置点击确定按钮回调
        dialog.setOnSureLisener(new OnSureLisener() {
            @Override
            public void onSure(Date date) {
                String message = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
                bean.age = message;
                et_four.setText(message);
                Log.d("zhouzhuo","Date:"+date.toString());

            }
        });
        dialog.show();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_confirm:
                Intent intent = getIntent();
                Bundle bundle = new Bundle();
                if(currentPosition==5){
                    bean.nation = mingzu;
                }else {
                    bean.nation = nations[currentPosition];
                }
                bundle.putSerializable("bean",bean);
                bundle.putInt("position",selectPosition);
                intent.putExtras(bundle);
                if(TextUtils.isEmpty(bean.name)){
                    ToastUtils.showShort(AddOnePersonActivity.this,"请输入姓名");
                }else {
                    if(TextUtils.isEmpty(serach)){
                        setResult(1,intent);
                    }else {
                        setResult(10,intent);
                    }
                    finish();
                }
                break;
            case R.id.iv_arrow:
                SelectSexPop pop = new SelectSexPop(AddOnePersonActivity.this,Integer.parseInt(bean.sex));
                pop.setListener(new SelectSexPop.SelectItemListener() {
                    @Override
                    public void select(int type) {
                        bean.sex = type+"";
                        setSex(bean.sex);
                    }
                });
                pop.showPopupWindow(fl_two);
                break;
            case R.id.iv_nation:
                SelectNationPop nationPop = new SelectNationPop(AddOnePersonActivity.this,currentPosition);
                nationPop.setListener(new SelectNationPop.SelectItemListener() {
                    @Override
                    public void select(int type) {
                        setTextNation(nations[type]);
                    }
                });
                nationPop.showPopupWindow(fl_three);
                break;
            case R.id.et_four:
                selectDate();
                break;
        }
    }


    private void setTextNotNull(EditText tv, String text){
        if(!TextUtils.isEmpty(text)){
            tv.setText(text);
        }
    }
}
