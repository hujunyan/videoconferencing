package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.InvestigationBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/4.
 */

public class InvestigationFormModel extends BaseModel{
    public void registration(InvestigationBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/createCasePart3";
        Log.d("zhouzhuo","调查记录:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(bean!=null){
            if(!TextUtils.isEmpty(bean.case_id)){
                builder.addParams("case_id", bean.case_id);
                Log.d("zhouzhuo","case Id="+bean.case_id);
            }
            if(!TextUtils.isEmpty(bean.date)){
                builder.addParams("date", bean.date);
            }
            if(!TextUtils.isEmpty(bean.time_range)){
                builder.addParams("time_range",bean.time_range);
            }
            if(!TextUtils.isEmpty(bean.time)){
                builder.addParams("time",bean.time);
            }
            if(!TextUtils.isEmpty(bean.location)){
                builder.addParams("location",bean.location);
            }
            if(!TextUtils.isEmpty(bean.attend)){
                builder.addParams("attend",bean.attend);
            }
            if(!TextUtils.isEmpty(bean.surveyed)){
                builder.addParams("surveyed", bean.surveyed);
            }
            if(!TextUtils.isEmpty(bean.record)){
                builder.addParams("record", bean.record);
            }
            if(!TextUtils.isEmpty(bean.litigant_sign)){
                builder.addParams("litigant_sign",bean.litigant_sign);
            }
            if(!TextUtils.isEmpty(bean.litiganted_sign)){
                builder.addParams("litiganted_sign",bean.litiganted_sign);
            }
            if(!TextUtils.isEmpty(bean.record_sign)){
                builder.addParams("record_sign",bean.record_sign);
            }
            if(!TextUtils.isEmpty(bean.isAudit)){
                builder.addParams("isAudit",bean.isAudit);
            }

        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","调查记录请求失败:"+e.getMessage());
                Log.d("zhouzhuo","调查记录请求失败:"+e.toString());
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    Log.d("zhouzhuo","请求成功:"+response);
                    try {
                     PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                        callBackListener.success(postRequestBean);
                    }catch (Exception e){
                       Log.d("zhouzhuo","解析数据异常=="+e.toString());
                    }

                }
            }
        });
    }
}
