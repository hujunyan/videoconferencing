package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.MultimediaBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.MediaListModel;
import com.xijiekou.sifasuomax.model.TemporaryModel;
import com.xijiekou.sifasuomax.utils.TimeUtils;
import com.xijiekou.sifasuomax.view.EvidenceEnteringView;
import com.yuyh.library.imgsel.ImgSelActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhouzhuo on 2017/7/4.
 * 证据录入
 */
public class EvidenceEnteringActivity extends BaseActivity{
    private EvidenceEnteringView ll_evidence_one,ll_evidence_two,ll_evidence_three;
    private ArrayList<MultimediaBean> listOne = new ArrayList<>();
    private ArrayList<MultimediaBean> listTwo = new ArrayList<>();
    private ArrayList<MultimediaBean> listThree = new ArrayList<>();
    private String caseId;
    private  MyHandler handler;
    private class  MyHandler extends Handler{
        private WeakReference<EvidenceEnteringActivity> reference;
        public MyHandler(EvidenceEnteringActivity activity){
            reference = new WeakReference<>(activity);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(reference.get()!=null){
                switch (msg.what){
                    case 4://请求列表
                        if(listOne.size()>0){
                            ll_evidence_one.setAdapter(listOne,0);
                        }
                        if(listTwo.size()>0){
                            ll_evidence_two.setAdapter(listTwo,1);
                        }
                        if(listThree.size()>0){
                            ll_evidence_three.setAdapter(listThree,2);
                        }
                        break;
                    case 0://增加
                        if(listOne.size()>=0){
                            ll_evidence_one.setAdapter(listOne,0);
                        }
                        break;
                    case 1:
                        if(listTwo.size()>=0){
                            ll_evidence_two.setAdapter(listTwo,1);
                        }
                        break;
                    case 2:
                        if(listThree.size()>=0){
                            ll_evidence_three.setAdapter(listThree,2);
                        }
                    case 10://删除
                        ll_evidence_one.setAdapter(listOne,0);
                        break;
                    case 11:
                        ll_evidence_two.setAdapter(listTwo,1);
                        break;
                    case 12:
                        ll_evidence_three.setAdapter(listThree,2);
                        break;
                }
            }
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evidence_entering);
        initView();
        initData();
    }

    private void initData() {
        MediaListModel model = new MediaListModel();
        model.setCallBackListener(new BaseModel.CallBackListener<ArrayList<MultimediaBean>>() {
            @Override
            public void success(ArrayList<MultimediaBean> list) {
                if(list.size()>0){
                    for (int i=0;i<list.size();i++){
                        MultimediaBean bean = list.get(i);
                        switch (bean.type){
                            case "0":
                                listOne.add(bean);
                                break;
                            case "1":
                                listTwo.add(bean);
                                break;
                            case "2":
                                listThree.add(bean);
                                break;
                        }
                    }
                    handler.sendEmptyMessage(4);
                }

            }

            @Override
            public void failed() {

            }
        });
        model.getList(caseId);
    }

    private void initView() {
        caseId = getIntent().getStringExtra("id");
        ll_evidence_one = (EvidenceEnteringView) findViewById(R.id.ll_evidence_one);
        ll_evidence_two = (EvidenceEnteringView) findViewById(R.id.ll_evidence_two);
        ll_evidence_three = (EvidenceEnteringView) findViewById(R.id.ll_evidence_three);
        handler = new MyHandler(this);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("zhouzhuo","request code:"+requestCode+"resultCode:"+resultCode);
        Log.d("zhouzhuo","data=="+data);
        if(data!= null){
            switch (requestCode){
                case 0:
                    switch (resultCode){
                        case RESULT_OK:
                            List<String> pathList = data.getStringArrayListExtra(ImgSelActivity.INTENT_RESULT);
                            Log.d("zhouzhuo","拍照的路径:"+pathList.toString());
                            if(pathList.size()>0){
                                String path = pathList.get(0);
                                uploadLocalFile("0",path);
                                saveToLocal(path);
                            }
                            break;
                    }
                    break;
                case 1:
                    switch (resultCode){
                        case 0:
                            Log.d("zhouzhuo","date==="+data);
                            String path = data.getStringExtra("path");
                            if(!TextUtils.isEmpty(path)){
                                uploadLocalFile("1",path);
                            }
                            Log.d("zhouzhuo","录音的path:"+path);
                            break;
                    }
                    break;
                case 2:
                    switch (resultCode){
                        case 0:
                           String path = data.getStringExtra("path");
                            uploadLocalFile("2",path);
                            Log.d("zhouzhuo","录像的path:"+path);
                            break;
                    }
                    break;

            }
        }

    }

    private void saveToLocal(String path){
        // 其次把文件插入到系统图库
        File file = new File(path);
        try {
            MediaStore.Images.Media.insertImage(getContentResolver(),
                    file.getAbsolutePath(), file.getName(), null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // 最后通知图库更新
        sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path)));

    }

    private MultimediaBean getBean(String path,String type,String file_id){
        MultimediaBean bean = new MultimediaBean();
        bean.url = path;
        bean.status = "1";
        bean.type = type;
        bean.id = file_id;
        bean.datetime = TimeUtils.getFormatTime();
        return bean;
    }



    public void uploadLocalFile(final String type, final String url){
        TemporaryModel model = new TemporaryModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String fileId) {
                Message message = Message.obtain();
                switch (type){
                    case "0":
                        message.what = 0;
                        listOne.add(getBean(url,type,fileId));
                        handler.sendEmptyMessage(0);
                        break;
                    case "1":
                        message.what = 1;
                        listTwo.add(getBean(url,type,fileId));
                        handler.sendMessage(message);
                        break;
                    case "2":
                        message.what = 2;
                        listThree.add(getBean(url,type,fileId));
                        handler.sendMessage(message);
                        break;
                }
            }

            @Override
            public void failed() {

            }
        });
        model.uploadPath(caseId,type,url);

    }
    public void deleteItem(int type,int position){
        switch (type){
            case 0:
                if(listOne.size()>0){
                    listOne.remove(position);
                    handler.sendEmptyMessage(10);
                }
                break;
            case 1:
                if(listTwo.size()>0){
                    listTwo.remove(position);
                    handler.sendEmptyMessage(11);
                }
                break;
            case 2:
                if(listThree.size()>0){
                    listThree.remove(position);
                    handler.sendEmptyMessage(12);
                }
                break;
        }
    }

    public String getCaseId(){
        return caseId;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(handler!=null){
            handler.removeCallbacksAndMessages(null);
        }
    }
}
