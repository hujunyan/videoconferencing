package com.xijiekou.sifasuomax.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/8/10.
 */

public class SearchBigPictureDialog extends Dialog implements View.OnClickListener{
    private WindowManager.LayoutParams p;
    private Context context;
    private String url;
    private ImageView iv_picture ;
    public SearchBigPictureDialog(Context context,String url) {
        super(context);
        this.context = context;
        this.url = url;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_see_big_picture);
        /*WindowManager wm = (WindowManager) getContext()
                .getSystemService(Context.WINDOW_SERVICE);
        int width = wm.getDefaultDisplay().getWidth();
        int height = wm.getDefaultDisplay().getHeight();
        p = getWindow().getAttributes(); // 获取对话框当前的参数值
        p.height = (int)(height* 0.4); //高度设置为屏幕的0.4
        p.width = (int) (width * 0.6); //宽度设置为屏幕的0.6
        int statusBarHeight = getStatusBarHeight(context);
        p.height = (int)(height - statusBarHeight + 0.5);   //减去系统的宽高
        p.width = (int)(width + 0.5);
        getWindow().setAttributes(p); // 设置生效,全屏填充*/

        iv_picture = (ImageView) findViewById(R.id.iv_picture);
        Glide.with(context).load(url).into(iv_picture);
    }


    /**
     * 获取通知栏的高度
     * @param context
     * @return
     */
    public int getStatusBarHeight(Context context){
        int result = 0;
        int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = context.getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void onClick(View v) {

    }


}
