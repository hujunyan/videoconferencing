package com.xijiekou.sifasuomax.holder;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.FileMenuActivity;
import com.xijiekou.sifasuomax.activity.NewOralActivity;
import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.dialog.CommonDeleteDialog;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.DeleteFileModel;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.utils.UIUtils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/7/26.
 */

public class DraftFragmentHolder extends BaseHolder<MainFragmentItemBeans.MainFragmentItemBean> implements View.OnClickListener{
    private TextView tv_id;
    private TextView tv_fileNumber;//卷号
    private TextView tv_fileName;//卷名
    private TextView tv_unit;//
    private TextView tv_mediators;//人民调解员
    private TextView tv_mediationTime;//调解时间
    private TextView tv_modify;
    private TextView tv_delete;
    private LinearLayout ll_bg;
    private TextView tv_operator;
    private String id;
    private Activity activity;

    public DraftFragmentHolder(Activity activity) {
        super(activity);
        this.activity = activity;
    }


    @Override
    protected View initView() {
        View view = UIUtils.inflate(R.layout.draft_fragment_item);
        ll_bg = (LinearLayout) view.findViewById(R.id.ll_bg);
        tv_fileNumber = (TextView) view.findViewById(R.id.tv_fileNumber);
        tv_fileName = (TextView) view.findViewById(R.id.tv_fileName);
        tv_unit = (TextView) view.findViewById(R.id.tv_unit);
        tv_mediators = (TextView) view.findViewById(R.id.tv_mediators);
        tv_mediationTime = (TextView) view.findViewById(R.id.tv_mediationTime);
        tv_delete = (TextView) view.findViewById(R.id.tv_delete);
        tv_delete.setOnClickListener(this);
        tv_operator = (TextView) view.findViewById(R.id.tv_operator);
        tv_operator.setOnClickListener(this);
        return view;
    }
    private String type;

    @Override
    protected void refreshView() {
        MainFragmentItemBeans.MainFragmentItemBean bean = getInfo();
        type = bean.casetype;
        id = bean.id;
        if(!TextUtils.isEmpty(bean.filenum)){
            tv_fileNumber.setText(bean.filenum);
        }
        if(!TextUtils.isEmpty(bean.filename)){
            tv_fileName.setText(bean.filename);
        }
        if(!TextUtils.isEmpty(bean.signature)){
            tv_mediators.setText(bean.signature);
        }
        if(!TextUtils.isEmpty(bean.date)){
            tv_mediationTime.setText(bean.date.split("\\ ")[0]);
        }
        if(!TextUtils.isEmpty(bean.company)){
            tv_unit.setText(bean.company);
        }

    }

    @Override
    public void changeItemBg(int position) {
        super.changeItemBg(position);
        if(position%2 ==0){
            ll_bg.setBackgroundColor(Color.parseColor("#daf0fd"));
        }else {
            ll_bg.setBackgroundColor(Color.parseColor("#f2ffe3"));
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_delete:
                //提示框
                CommonDeleteDialog commonDialog = new CommonDeleteDialog(activity);
                commonDialog.setDeleteListener(new CommonDeleteDialog.DeleteListener() {
                    @Override
                    public void delete() {
                        //请求删除接口 成功之后刷新列表
                        DeleteFileModel model = new DeleteFileModel();
                        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                            @Override
                            public void success(String message)  {
                                try {
                                    JSONObject object = new JSONObject(message);
                                    int code = object.getInt("code");
                                    String data = object.getString("data");
                                    ToastUtils.showShort(activity,data);
                                    if(code == 1){
                                        if(deleterListener!=null){
                                            deleterListener.delete();
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }


                            }

                            @Override
                            public void failed() {

                            }
                        });
                        model.delete(id);

                    }
                });
                commonDialog.show();
                break;
            case R.id.tv_operator:
                Intent intent ;
                if(type.equals("0")){
                    intent= new Intent(activity,NewOralActivity.class);
                    intent.putExtra("caseId",id);
                    intent.putExtra("type","0");
                    IntentUtils.startActivity(activity,intent,false,0);
                }else {
                    intent= new Intent(activity,FileMenuActivity.class);
                    intent.putExtra("caseId",id);
                    IntentUtils.startActivity(activity,intent,false,1);
                }
                break;
        }
    }

    public interface DeleterListener{
        void delete();
    }
    public DeleterListener deleterListener;

    public void setDeleteListener(DeleterListener deleterListener){
        this.deleterListener = deleterListener;
    }
}
