package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.xijiekou.sifasuomax.bean.EvidenceMateriaBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/5.
 */

public class JudicialConfirmModel extends BaseModel {
    public void registration(EvidenceMateriaBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/createCasePart8";
        Log.d("zhouzhuo","司法确认:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(bean!=null){
            if(!TextUtils.isEmpty(bean.case_id)){
                builder.addParams("case_id", bean.case_id);
                Log.d("zhouzhuo","case Id="+bean.case_id);
            }
            Log.d("zhouzhuo","auditor:"+bean.auditor);
            if(!TextUtils.isEmpty(bean.auditor)){
                builder.addParams("auditor", bean.auditor);
            }
            Log.d("zhouzhuo","filing:"+bean.litigant);
            if(!TextUtils.isEmpty(bean.filing)){
                builder.addParams("filing",bean.filing);
            }
            if(!TextUtils.isEmpty(bean.datetime)){
                builder.addParams("datetime",bean.datetime);
            }
            if(!TextUtils.isEmpty(bean.explain)){
                builder.addParams("explain",bean.explain);
            }
            if(!TextUtils.isEmpty(bean.isAudit)){
                builder.addParams("isAudit",bean.isAudit);
            }

        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","司法确认请求失败:"+e.getMessage());
                Log.d("zhouzhuo","司法确认请求失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    //callBackListener.success(response);
                    callBackListener.success(response);
                    Log.d("zhouzhuo","司法确认请求成功:"+response);

                }
            }
        });
    }
}
