package com.xijiekou.sifasuomax.activity;

import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/8/23.
 */

public class WebViewMoreActivity extends BaseActivity{
    private WebView web_view;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview_more);
        web_view = (WebView) findViewById(R.id.web_view);
        WebSettings settings = web_view.getSettings();
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setJavaScriptEnabled(true);// 开启js功能, 同时, 百度网页不再跳转到浏览器
        settings.setDefaultTextEncodingName("UTF-8") ;
        web_view.loadUrl(UrlDomainUtil.urlHeader+"/index/news_list.html");
    }
}
