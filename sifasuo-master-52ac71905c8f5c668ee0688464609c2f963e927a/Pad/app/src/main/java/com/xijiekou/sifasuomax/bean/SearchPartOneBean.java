package com.xijiekou.sifasuomax.bean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/8/16.
 */

public class SearchPartOneBean {
    public String id;
    public String case_id;
    public String filing;
    public String simple;
    public String content;
    public String source = "1";
    public String issuetype;
    public String signurl;
    public String datetime;
    public String date;
    public String filename;
    public String filenum;
    public ArrayList<NewOralBean> list;
}
