package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/7/10.
 */

public class MediationMonthFormItemView extends RelativeLayout implements View.OnClickListener{
    private ImageView iv_add,iv_reduce;
    private EditText et_number;
    private int number;
    private TextView tv_type;
    private TextView tv_content;
    private final String[] titles = {"调解案件总数","涉及当事人数","调解成功","疑难复杂案件",
                       "协议涉及金额","居调委会调解案件数","街道调委会调解案件数","企事业单位调委会调解案件数",
                        "社会团体和其他组织调委会调解案件数","主动调解","依申请调解","接受委托移送调解",
            "公安机关委托移送","信访部门委托移送","其他部门委托移送","婚姻家庭纠纷","邻里纠纷","房屋宅基地纠纷"
            ,"合同纠纷","生产经营纠纷","损害赔偿纠纷","征地拆迁纠纷","环境污染纠纷","拖欠农民工工资纠纷",
            "其他劳动争议纠纷","旅游纠纷","电子商务纠纷","其他消费纠纷","道路交通事故纠纷","物业纠纷",
            "其他纠纷","口头协议","书面协议","履行","司法确认","达成协议后起诉","法院判决维持",
            "排查纠纷","预防纠纷","防止民间纠纷引起自杀件数","防止民间纠纷引起自杀人数",
            "防止民间纠纷转化为刑事案件数","防止民间纠纷转化为刑事案件人数","防止群体性上访人数","防止群体性上访件数",
            "防止群体性械斗人数","防止群体性械斗件数"
    };
    private final String[] contnetOne ={
        "件","人","件","件","万元","件","件","件","件","件","件","件","件",
            "件","件","件","件","件","件","件","件","件","件","件","件",
            "件","件","件","件","件","件","件","件","件","件","件","件",
            "次","件","件","人","件","人","件","人","件","人"
    };
    private final String[] titlesTwo ={"总数","婚姻家庭","邻里关系","损害赔偿","物业纠纷",
                        "其它","海报挂图","板报展报","折页材料","工作培训 次数","工作培训 参加人数",
                            "法律讲座 次数","法律讲座 参加人数","备注"};
    private final String[] contentTwo = {
            "件","件","件","件","件","件","件","件","件","件","人","件","人","件"
    };


    public MediationMonthFormItemView(Context context) {
        this(context,null);
    }

    public MediationMonthFormItemView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context,attrs);
    }

    private void initView(Context context,AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MediationMonthFormItemView);
        int type =  typedArray.getInt(R.styleable.MediationMonthFormItemView_type,-1);
        int totalType = typedArray.getInt(R.styleable.MediationMonthFormItemView_totalType,-1);
        typedArray.recycle();
        View view  = LayoutInflater.from(context).inflate(R.layout.mediation_month_form_item,this,true);
        tv_content = (TextView) view.findViewById(R.id.tv_content);
        iv_add = (ImageView) view.findViewById(R.id.iv_add);
        iv_add.setOnClickListener(this);
        iv_reduce = (ImageView) view.findViewById(R.id.iv_reduce);
        iv_reduce.setOnClickListener(this);
        et_number = (EditText) view.findViewById(R.id.et_number);
        tv_type = (TextView) view.findViewById(R.id.tv_type);
        et_number.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String afterNumber = et_number.getText().toString().trim();
               if(!TextUtils.isEmpty(afterNumber)){
                   //TODO Integer最大值 限制
                 if(Double.parseDouble(afterNumber)<Integer.MAX_VALUE){
                       number = Integer.parseInt(afterNumber);
                   }else {
                     number = Integer.MAX_VALUE;
                 }
               }else {
                   number = 0;
               }

            }
        });
        if(totalType ==0){
            tv_type.setText(titles[type]);
            tv_content.setText(contnetOne[type]);
        }else {
            tv_type.setText(titlesTwo[type]);
            tv_content.setText(contentTwo[type]);
        }

    }

    public void setText(String message){
        if(TextUtils.isEmpty(message)){
            et_number.setText("0");
        }else {
            et_number.setText(message);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.iv_add:
                number++;
                et_number.setText(number+"");
                break;
            case R.id.iv_reduce:
                if(number>0){
                    number--;
                    et_number.setText(number+"");
                }
                break;
        }
    }

    public String getNumber(){
        return  number+"";
    }
}
