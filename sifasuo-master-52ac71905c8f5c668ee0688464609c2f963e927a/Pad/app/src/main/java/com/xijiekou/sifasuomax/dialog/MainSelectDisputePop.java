package com.xijiekou.sifasuomax.dialog;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.DensityUtils;

/**
 * Created by zhouzhuo on 2017/7/11.
 */

public class MainSelectDisputePop extends PopupWindow {
    private Activity activity;
    private ListView listView;
    private String[] types = {
           "口头协议","书面协议","全部"};
    public MainSelectDisputePop(Activity activity, int selectPosition){
        this.activity = activity;
        LayoutInflater inflater = (LayoutInflater) activity
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout view = (LinearLayout) inflater.inflate(R.layout.main_fragment_select_dispute_pop, null);

        this.setWidth(DensityUtils.dp2px(activity,150));
        // 设置弹出窗体的高
        this.setHeight(DensityUtils.dp2px(activity,90));
        this.setBackgroundDrawable(new BitmapDrawable());
        // 设置弹出窗体可点击
        //this.setTouchable(true);
        //this.setFocusable(true);
        listView = (ListView) view.findViewById(R.id.lv_dispute);
        listView.setAdapter(new MyAdapter(selectPosition));
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listener!=null){
                    listener.select(position);
                    MainSelectDisputePop.this.dismiss();
                }
            }
        });
        // 设置点击是否消失
        this.setOutsideTouchable(true);
        this.setContentView(view);
    }


    public void showPopupWindow(View parent) {
       if (!this.isShowing()) {
            // 以下拉方式显示popupwindow
             // x y
            int[] location = new int[2];
            //获取在整个屏幕内的绝对坐标
            parent.getLocationOnScreen(location);
            this.showAsDropDown(parent,0,0);
        } else {
           this.dismiss();
        }
    }

    private class MyAdapter extends BaseAdapter{

       private int selectPosition ;
       public MyAdapter(int selectPosition){
           this.selectPosition = selectPosition;
       }

       @Override
       public int getCount() {
           return types.length;
       }

       @Override
       public Object getItem(int position) {
           return types[position];
       }

       @Override
       public long getItemId(int position) {
           return position;
       }

       @Override
       public View getView(int position, View convertView, ViewGroup parent) {
           View view =  LayoutInflater.from(activity).inflate(R.layout.select_dispute_pop, null);
           TextView textView = (TextView) view.findViewById(R.id.tv);
           textView.setText(types[position]);
           if(position == selectPosition){
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                   textView.setTextColor(activity.getResources().getColor(R.color.colorCommon,null));
               }else {
                   textView.setTextColor(activity.getResources().getColor(R.color.colorCommon));
               }
           }else {
               if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                   textView.setTextColor(activity.getResources().getColor(R.color.popText,null));
               }else {
                   textView.setTextColor(activity.getResources().getColor(R.color.popText));
               }
           }
           return view;
       }
   }



    public SelectItemListener listener;
    public void setListener(SelectItemListener listener){
        this.listener = listener;
    }
    public interface  SelectItemListener {
        void select(int type);
    }

}
