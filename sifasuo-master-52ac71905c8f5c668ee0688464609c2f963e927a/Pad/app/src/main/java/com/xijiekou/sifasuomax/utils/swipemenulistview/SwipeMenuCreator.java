package com.xijiekou.sifasuomax.utils.swipemenulistview;

public interface SwipeMenuCreator {

    void create(SwipeMenu menu);
}
