package com.xijiekou.sifasuomax.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/25.
 */

public class NewProtocolBean implements Serializable{
    public String id;
    public String case_id;

    public ArrayList<NewOralBean> list;
    public String   issue;
    public String   content;
    public String   method;
    public String   issuetype;
    public String   num;
    public String   litigant_sign;
    public String   mediate_sign;
    public String   record_sign;
    public String   date;
    public String   create_time;
    public String   filenumber;
    public String    isAudit;
}
