package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.TimeUtils;

/**
 * Created by zhouzhuo on 2017/7/26.
 */

public class YearMonthDayMorningHourView extends RelativeLayout implements View.OnClickListener{
    private EditText et_examineYear,et_month,et_day;
    private TextView tv_current_time;
    private String timeResult ;
    private String[] ymd = new String[3];
  //  private ImageView iv_morning_selected,iv_noon_selected;
    // 1  上午  2  下午
   /* private int type = 1;
    private EditText et_hour;
    private EditText et_minute;
    private FrameLayout fl_one,fl_two;*/
    public YearMonthDayMorningHourView(Context context) {
        this(context,null);
    }

    public YearMonthDayMorningHourView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.year_month_day_view3,this,true);
        et_examineYear = (EditText) view.findViewById(R.id.et_examineYear);
        et_examineYear.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!TextUtils.isEmpty(s)){
                    ymd[0] = s.toString();
                }

            }
        });
        et_month = (EditText) view.findViewById(R.id.et_month);
        et_month.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!TextUtils.isEmpty(s)){
                    ymd[1] = s.toString();
                }
            }
        });
        et_day = (EditText) view.findViewById(R.id.et_day);
        et_day.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if(!TextUtils.isEmpty(s)){
                    ymd[2] = s.toString();
                }
            }
        });
        tv_current_time = (TextView) view.findViewById(R.id.tv_current_time);
        tv_current_time.setOnClickListener(this);
      /*  fl_one = (FrameLayout) view.findViewById(fl_one);
        fl_one.setOnClickListener(this);
        et_hour  = (EditText) view.findViewById(R.id.et_hour);
        et_minute = (EditText) view.findViewById(R.id.et_minute);
        iv_morning_selected = (ImageView) view.findViewById(R.id.iv_morning_selected);
        iv_noon_selected = (ImageView) view.findViewById(R.id.iv_noon_selected);

        fl_two = (FrameLayout) view.findViewById(fl_two);
        fl_two.setOnClickListener(this);*/
    }
    public String getDate(){
        String date =  ymd[0]+"-"+ymd[1]+"-"+ymd[2];
        if(TimeUtils.isData(date)){
            return date;
        }else {
            return "";
        }
    }

    public void setData(String data){
        if(!TextUtils.isEmpty(data)&&!data.equals("null")){
            timeResult = data;
            ymd =timeResult.split("-");
            et_examineYear.setText(ymd[0]);
            et_month.setText(ymd[1]);
            et_day.setText(ymd[2].split("\\ ")[0]);
        }
    }

    /*public String getHour(){
        return et_hour.getText().toString().trim();
    }
    public void setHour(String hour){
        if(!TextUtils.isEmpty(hour)){
            et_hour.setText(hour);
        }
    }

    public String getMinute(){
        return  et_minute.getText().toString().trim();
    }

    public void setMinute(String minute){
        if(!TextUtils.isEmpty(minute)){
            et_minute.setText(minute);
        }
    }

    public String getType(){
        return  type+"";
    }

    public void setType(String type){
        if(!TextUtils.isEmpty(type)){
            this.type = Integer.parseInt(type);
            if(type.equals("1")){
                iv_morning_selected.setVisibility(VISIBLE);
                iv_noon_selected.setVisibility(GONE);
            }else {
                iv_morning_selected.setVisibility(GONE);
                iv_noon_selected.setVisibility(VISIBLE);
            }
        }
    }*/

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_current_time:
                timeResult = TimeUtils.getCurrentTime();
                ymd =timeResult.split("-");
                et_examineYear.setText(ymd[0]);
                et_month.setText(ymd[1]);
                et_day.setText(ymd[2]);
                break;
           /* case fl_one:
                if(type == 2){
                    iv_morning_selected.setVisibility(VISIBLE);
                    iv_noon_selected.setVisibility(GONE);
                    type =1;
                }
                break;
            case fl_two:
                if(type == 1){
                    iv_morning_selected.setVisibility(GONE);
                    iv_noon_selected.setVisibility(VISIBLE);
                    type =2;

                }
                break;*/
        }

    }
}
