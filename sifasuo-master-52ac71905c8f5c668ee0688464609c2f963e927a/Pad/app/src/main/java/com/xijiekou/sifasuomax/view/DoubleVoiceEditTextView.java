package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.VoiceBaseActivity;
import com.xijiekou.sifasuomax.bean.NewProtocolBean;
import com.xijiekou.sifasuomax.dialog.SelectPerformancePop;

/**
 * Created by zhouzhuo on 2017/7/25.
 */

public class DoubleVoiceEditTextView extends RelativeLayout implements View.OnClickListener{
    private RelativeLayout rl_voice,rl_voice2;
    private TextView et_detail;
    private EditText et_detail_add,et_detail2;
    private VoiceBaseActivity activity;
    private String lastResult="",lastResult2="";

    private EditText et_number_one,et_number_one2;

    private TextView tv_number_one;

    private SignNameView rl_sign_one, rl_sign_two, rl_sign_three;

    private YearMonthDayView yearMonthDayView;

   // private EditText et_number;

    private int currentPerformance = 2;

    private int currentSelect = 0;
    private String perferenceResult;

    public DoubleVoiceEditTextView(Context context) {
        this(context,null);
    }

    public DoubleVoiceEditTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (VoiceBaseActivity) context;
        initView(context);
        initData();
    }

    private void initData() {

    }

    public void setLastResult(String result){
        if(!TextUtils.isEmpty(result)){
            et_detail_add.setText(result);
        }
    }

  /*  public void setIssue(String content){
        if(!TextUtils.isEmpty(content)){
            et_detail.setText(content);
        }

    }*/

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.voice_edit_view2,this,true);
        rl_voice = (RelativeLayout) view.findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        rl_voice2 = (RelativeLayout) view.findViewById(R.id.rl_voice2);
        rl_voice2.setOnClickListener(this);
        tv_number_one = (TextView) view.findViewById(R.id.tv_number_one);
        tv_number_one.setOnClickListener(this);
        et_number_one2 = (EditText) view.findViewById(R.id.et_number_one2);
        et_detail_add = (EditText) view.findViewById(R.id.et_detail_add);
        et_detail = (TextView) view.findViewById(R.id.et_detail);

        et_detail2 = (EditText) view.findViewById(R.id.et_detail2);
        et_detail2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult2 = s.toString();

            }
        });



        et_number_one = (EditText) view.findViewById(R.id.et_number_one);
        et_number_one.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                perferenceResult = s.toString();
            }
        });
        rl_sign_one = (SignNameView) view.findViewById(R.id.rl_sign_one);
        rl_sign_one.setSignTitle("当事人(签名盖章或按指纹)");
        rl_sign_two = (SignNameView) view.findViewById(R.id.rl_sign_two);
        rl_sign_two.setSignTitle("人民调解员(签名)");
        rl_sign_three = (SignNameView) view.findViewById(R.id.rl_sign_three);
        rl_sign_three.setSignTitle("记录人(签名)");
        yearMonthDayView = (YearMonthDayView) view.findViewById(R.id.rl_date);
      //  et_number = (EditText) view.findViewById(R.id.et_number);
    }

    public String getLastResult(){
        String result = et_detail_add.getText().toString().trim();
        return result;

    }
    public String getLastResult2(){
        return  lastResult2;
    }

    public String getPerformance(){
        if(currentSelect ==0){
            return "即时履行";
        }else {
            return et_number_one.getText().toString();
        }
    }

    public String getSignOne(){
        return rl_sign_one.getFilePath();
    }

    public String getSignTwo(){
        return  rl_sign_two.getFilePath();
    }

    public String getSignThree(){
        return  rl_sign_three.getFilePath();
    }


    public String getNumber(){
        return  et_number_one2.getText().toString();
    }

    public String getDate(){
        return  yearMonthDayView.getDate();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_voice:
                activity.requestVoicePermission();
                activity.setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail_add.getText().insert(et_detail_add.getSelectionStart(),result);
                        }
                    }
                });
                break;
            case R.id.rl_voice2:
                activity.requestVoicePermission();
                activity.setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail2.getText().insert(et_detail2.getSelectionStart(),result);
                        }

                    }
                });
                break;
            case R.id.tv_number_one:
                SelectPerformancePop pop = new SelectPerformancePop(activity,currentPerformance);
                pop.setListener(new SelectPerformancePop.SelectItemListener() {
                    @Override
                    public void select(int type) {
                        currentSelect = type;
                        if(currentSelect ==0){
                            setPerference("即时履行");
                        }else {
                            setPerference(perferenceResult);
                        }
                    }
                });
                pop.showPopupWindow(tv_number_one);
                break;

        }

    }

    public void setPerference(String perference){
        if((TextUtils.isEmpty(perference)&&currentSelect==0)||
                (!TextUtils.isEmpty(perference)&&perference.equals("即时履行"))){
            et_number_one.setVisibility(GONE);
            tv_number_one.setText("即时履行");
        }else {
            et_number_one.setVisibility(VISIBLE);
            tv_number_one.setText("其他");
            if(!TextUtils.isEmpty(perference)){
                et_number_one.setText(perference);
            }
        }
        Log.d("zhouzhuo","perferenceResult:"+perferenceResult);
    }

    public void setData(NewProtocolBean message) {
        Log.d("zhouzhuo","协议书=="+message.issue);
        String issue = message.issue;
        //TODO 加上一个带过来的
        if(!TextUtils.isEmpty(issue)){
            et_detail_add.setText(issue);
        }
        lastResult = message.issue;
        setTextNotNull(message.content,et_detail2);
        lastResult2 = message.content;
       // setTextNotNull(message.content,et_performance);
        //setTextNotNull(message.num,et_number);
        yearMonthDayView.setData(message.date);
        rl_sign_one.setImage(message.litigant_sign);
        rl_sign_two.setImage(message.mediate_sign);
        rl_sign_three.setImage(message.record_sign);
        if(!TextUtils.isEmpty(message.num)){
            et_number_one2.setText(message.num);
        }else {
            et_number_one2.setText(currentPerformance+"");
        }
        setPerference(message.method);
    }

    public void setTextNotNull(String content, EditText editText){
        if(!TextUtils.isEmpty(content)){
            editText.setText(content);

        }
    }

    public void setArgue(String argue) {
        if(!TextUtils.isEmpty(argue)){
            et_detail.setText(argue);
        }
    }
}
