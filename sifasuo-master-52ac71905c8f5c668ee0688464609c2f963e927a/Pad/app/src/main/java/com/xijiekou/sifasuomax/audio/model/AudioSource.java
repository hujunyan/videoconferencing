package com.xijiekou.sifasuomax.audio.model;

import android.media.MediaRecorder;

/**
 * Created by zhouzhuo on 2017/6/16.
 */

public enum AudioSource {
    MIC,
    CAMCORDER;

    public int getSource(){
        switch (this){
            case CAMCORDER:
                return MediaRecorder.AudioSource.CAMCORDER;
            default:
                return MediaRecorder.AudioSource.MIC;
        }
    }
}
