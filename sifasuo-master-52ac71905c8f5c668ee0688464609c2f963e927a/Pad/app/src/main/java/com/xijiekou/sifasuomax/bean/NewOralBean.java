package com.xijiekou.sifasuomax.bean;

import java.io.Serializable;

/**
 * Created by zhouzhuo on 2017/7/25.
 */

public class NewOralBean implements Serializable{
    public String type;
    public String id;

    @Override
    public String toString() {
        return "NewOralBean{" +
                "type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", nation='" + nation + '\'' +
                ", age='" + age + '\'' +
                ", job='" + job + '\'' +
                ", tel='" + tel + '\'' +
                ", address='" + address + '\'' +
                ", create_time='" + create_time + '\'' +
                ", isdelete='" + isdelete + '\'' +
                ", faren='" + faren + '\'' +
                ", tongyi='" + tongyi + '\'' +
                ", weituoren='" + weituoren + '\'' +
                ", fading='" + fading + '\'' +
                '}';
    }

    public String name;
    public String sex="0";
    public String nation;
    public String age;
    public String job;
    public String tel;
    public String address;
    public String create_time;
    public String isdelete;
    public String faren;
    public String tongyi;
    public String weituoren;
    public String fading;
    public String idcard;
}
