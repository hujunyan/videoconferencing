package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.ProjectFinalReportActivity;
import com.xijiekou.sifasuomax.activity.VoiceBaseActivity;
import com.xijiekou.sifasuomax.bean.ProjectFinalReportBean;

/**
 * Created by zhouzhuo on 2017/7/6.
 * 人民调解结案报告记录
 */
public class ProjectFinalReportView extends RelativeLayout implements View.OnClickListener{
    private YearMonthDayMorningHourView rl_date;//立案时间
    private YearMonthDayMorningHourView rl_date2;//立案时间
    private EditText et_one;//调解员
    private EditText et_litigant;//当事人
    private EditText et_participant;//参加人
    private EditText et_ten;//记录人
    private EditText et_detail;//调解经过
    private RelativeLayout rl_voice;
    private EditText et_detail2;//调解结果
    private RelativeLayout rl_voice2;
    private RecordResultView rl_result;
    private String lastResult="";
    private String lastResult2="";
    private ProjectFinalReportActivity activity;
    private YearMonthDayView record_time;


    public void setData(ProjectFinalReportBean bean){
        if(bean!=null){
            rl_date.setData(bean.begin_time);
            rl_date2.setData(bean.end_time);
            setTextNotNull(bean.mediate,et_one);
            setTextNotNull(bean.litigant,et_litigant);
            setTextNotNull(bean.attend,et_participant);
            setTextNotNull(bean.record,et_ten);
            lastResult = bean.content;
            setTextNotNull(lastResult,et_detail);
            lastResult2 = bean.result;
            setTextNotNull(lastResult2,et_detail2);
            rl_result.setResult(bean.resultcode);
            record_time.setData(bean.create_time);
        }

    }


    public ProjectFinalReportView(Context context) {
        this(context,null);
    }

    public ProjectFinalReportView(Context context, AttributeSet attrs) {
        super(context, attrs);
        activity = (ProjectFinalReportActivity) context;
        initView(context);
    }


    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.project_final_report_view,
                this,true);
        rl_date = (YearMonthDayMorningHourView) view.findViewById(R.id.rl_date);
        rl_date2 = (YearMonthDayMorningHourView) view.findViewById(R.id.rl_date2);
        et_one = (EditText) view.findViewById(R.id.et_one);
        et_litigant = (EditText) view.findViewById(R.id.et_litigant);
        et_participant = (EditText) view.findViewById(R.id.et_participant);
        record_time = (YearMonthDayView) view.findViewById(R.id.record_time);
        et_ten = (EditText) view.findViewById(R.id.et_ten);
        et_detail = (EditText) view.findViewById(R.id.et_detail);
        et_detail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult = s.toString();
            }
        });
        et_detail2 = (EditText) view.findViewById(R.id.et_detail2);
        et_detail2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult2 = s.toString();

            }
        });
        rl_result = (RecordResultView) view.findViewById(R.id.rl_result);
        rl_voice = (RelativeLayout) view.findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        rl_voice2 = (RelativeLayout) view.findViewById(R.id.rl_voice2);
        rl_voice2.setOnClickListener(this);

    }

    public void setTextNotNull(String content, EditText editText){
        if(!TextUtils.isEmpty(content)){
            editText.setText(content);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_voice:
                activity.requestVoicePermission();
                activity.setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail.getText().insert(et_detail.getSelectionStart(),result);
                        }
                    }
                });
                break;
            case R.id.rl_voice2:
                activity.requestVoicePermission();
                activity.setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail2.getText().insert(et_detail2.getSelectionStart(),result);
                        }
                    }
                });
                break;
        }

    }

    public ProjectFinalReportBean getBean() {
        ProjectFinalReportBean bean = new ProjectFinalReportBean();
        bean.begin_time = rl_date.getDate();
        bean.end_time = rl_date2.getDate();
        bean.create_time = record_time.getDate();
        bean.mediate = et_one.getText().toString().trim();
        bean.litigant = et_litigant.getText().toString().toString();
        bean.attend = et_participant.getText().toString().trim();
        bean.record = et_ten.getText().toString().trim();
        bean.content = lastResult;
        bean.result = lastResult2;
        bean.resultcode = rl_result.getType()+"";
        return  bean;
    }
}
