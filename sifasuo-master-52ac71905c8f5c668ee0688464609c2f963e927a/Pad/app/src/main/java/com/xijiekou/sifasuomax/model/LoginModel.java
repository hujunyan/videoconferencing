package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.LoginBean;
import com.xijiekou.sifasuomax.utils.MD5;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.xijiekou.sifasuomax.utils.ConstantsUtils.bean;

/**
 * Created by zhouzhuo on 2017/7/10.
 */

public class LoginModel extends BaseModel{
    public void login(String phone, String password,String key) {
        String url = UrlDomainUtil.urlHeader + "/Staff/login";
        Log.d("zhouzhuo","url==="+url);
        final HashMap<String, String> map = new HashMap<>();
        map.put("tel", phone);
        map.put("pwd", MD5.MD5(password));
        map.put("key",key);
        StringRequest request = new StringRequest(Request.Method.POST, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                if (callBackListener != null) {
                    if (!TextUtils.isEmpty(s)) {
                        Log.d("zhouzhuo","登录成功信息:"+s);
                       LoginBean loginBean = new LoginBean();
                        try {
                            JSONObject object = new JSONObject(s);
                            loginBean.code = object.getInt("code");
                           // loginBean.Msg = object.getString("Msg");
                            //LoginBean.LoginContentBean bean = new LoginBean.LoginContentBean();
                            if (loginBean.code == 1) {
                                if(bean == null){
                                    bean = new LoginBean.LoginContentBean();
                                }
                                JSONObject object1 = object.getJSONObject("data");
                                bean.id = object1.getString("id");
                                bean.name = object1.getString("name");
                                bean.photo = object1.getString("photo");
                                bean.loginname = object1.getString("tel");
                                bean.password = object1.getString("pwd");
                                bean.company =  object1.getString("company");
                                bean.printIp = object1.getString("printIp");
                                //bean.photo  = UrlDomainUtil.voiceUrl+object1.getString("photo");
                                callBackListener.success(loginBean);
                            } else {
                                loginBean.bean = new LoginBean.LoginContentBean();
                                loginBean.bean.errorMessage = object.getString("data");
                                callBackListener.success(loginBean);
                            }

                        } catch (JSONException e) {
                            //Log.d()
                            e.printStackTrace();
                        }
                    }
                }

            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("zhouzhuo","login :"+volleyError.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                return map;
            }
        };
        MyApp.getHttpQueue().add(request);

    }
}