package com.xijiekou.sifasuomax.fragment;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.codbking.widget.DatePickDialog;
import com.codbking.widget.OnChangeLisener;
import com.codbking.widget.OnSureLisener;
import com.codbking.widget.bean.DateType;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.adapter.SearchFileFragmentAdapter;
import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.dialog.MainSelectDisputePop;
import com.xijiekou.sifasuomax.dialog.SelectStatePop;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.SearchFragmentCaseListModel;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshBase;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by zhouzhuo on 2017/7/4.
 */
public class SearchFileFragment extends BaseFragment implements View.OnClickListener{
    private PullToRefreshListView refreshList;
    private ListView listView ;
    private String isLastpage;
    private int currentPage = 1;
    private TextView tv_confirm;
    private ArrayList<MainFragmentItemBeans.MainFragmentItemBean> totalList;
    private SearchFileFragmentAdapter adapter;
    private String search;//要查找的文本
    private EditText et_keyword;
    private int type=3;//案件int类型
    private TextView et_filetype,et_filetype2;
    private ImageView iv_start_time,iv_end_time;
    private String beginTime;
    private String end;
    private EditText et_start_time,et_end_time;
    private final String[] status = { "有待继续沟通", "调解成功", "调解失败","全部"};
    private final String[] types = {
            "口头协议","书面协议","全部"};
    private int casetype = 2;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_file_search, null);
        initView(view);
        initData();
        return view;
    }

    private void initView(View view) {
        et_filetype = (TextView) view.findViewById(R.id.et_filetype);
        et_filetype.setOnClickListener(this);
        et_filetype2 = (TextView) view.findViewById(R.id.et_filetype2);
        et_filetype2.setOnClickListener(this);
        et_keyword = (EditText) view.findViewById(R.id.et_keyword);
        et_keyword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                search = s.toString();

            }
        });

        tv_confirm = (TextView) view.findViewById(R.id.tv_confirm);
        tv_confirm.setOnClickListener(this);
        iv_start_time = (ImageView) view.findViewById(R.id.iv_start_time);
        iv_start_time.setOnClickListener(this);
        iv_end_time = (ImageView) view.findViewById(R.id.iv_end_time);
        iv_end_time.setOnClickListener(this);

        et_start_time = (EditText) view.findViewById(R.id.et_start_time);
        et_start_time.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                beginTime = s.toString();

            }
        });
        et_end_time = (EditText) view.findViewById(R.id.et_end_time);
        et_end_time.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                end = s.toString();

            }
        });
        refreshList = (PullToRefreshListView) view.findViewById(R.id.lv_list);
        listView = refreshList.getRefreshableView();
        listView.setDivider(getContext().getResources().getDrawable(R.drawable.main_listview_drawable));
        listView.setDividerHeight(10);
        listView.setVerticalScrollBarEnabled(false);
        refreshList.setPullLoadEnabled(true);
        //listView.setAdapter(new FileTypeAdapter(this,list));
        refreshList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                currentPage = 1;
                adapter = null;
                initData();
                refreshList.onPullDownRefreshComplete();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if(isLastpage.equals("0")){
                    currentPage++;
                    initData();
                }else {
                    ToastUtils.showShort(getActivity(),"没有更多数据");
                }
                refreshList.onPullUpRefreshComplete();


            }
        });


    }

    @Override
    public void onClick(View view) {
        //super.onClick(view);
        switch (view.getId()){
            case R.id.iv_start_time:
                 DatePickDialog dialog = new DatePickDialog(getContext());
                //设置上下年分限制
                dialog.setYearLimt(5);
                //设置标题
                dialog.setTitle("选择时间");
                //设置类型
                dialog.setType(DateType.TYPE_YMD);
                //设置消息体的显示格式，日期格式
                dialog.setMessageFormat("yyyy-MM-dd HH:mm");
                //设置选择回调
                dialog.setOnChangeLisener(new OnChangeLisener() {
                    @Override
                    public void onChanged(Date date) {

                    }
                });
                //设置点击确定按钮回调
                dialog.setOnSureLisener(new OnSureLisener() {
                    @Override
                    public void onSure(Date date) {
                        String message = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
                        Log.d("zhouzhuo","Date:"+date.toString());
                        beginTime = message;
                        et_start_time.setText(message);
                    }
                });
               dialog.show();
                break;
            case R.id.iv_end_time:
                dialog = new DatePickDialog(getContext());
                //设置上下年分限制
                dialog.setYearLimt(5);
                //设置标题
                dialog.setTitle("选择时间");
                //设置类型
                dialog.setType(DateType.TYPE_YMD);
                //设置消息体的显示格式，日期格式
                dialog.setMessageFormat("yyyy-MM-dd HH:mm");
                //设置选择回调
                dialog.setOnChangeLisener(null);
                //设置点击确定按钮回调
                //设置点击确定按钮回调
                dialog.setOnSureLisener(new OnSureLisener() {
                    @Override
                    public void onSure(Date date) {
                        String message = (new SimpleDateFormat("yyyy-MM-dd")).format(date);
                        Log.d("zhouzhuo","Date:"+date.toString());
                        end= message;
                        et_end_time.setText(message);
                    }
                });
                dialog.show();
                break;
            case R.id.tv_confirm:
                search =et_keyword.getText().toString().trim();
                currentPage = 1;
                adapter = null;
                initData();
                break;
            case R.id.et_filetype:
                SelectStatePop selectStatePop =  new SelectStatePop(getActivity(),type);
                selectStatePop.setListener(new SelectStatePop.SelectItemListener() {
                    @Override
                    public void select(int types) {
                        et_filetype.setText(status[types]);
                        type = types;
                    }
                });
                selectStatePop.showPopupWindow(et_filetype);
                break;
            case R.id.et_filetype2:
                MainSelectDisputePop pop = new MainSelectDisputePop(getActivity(),casetype);
                pop.setListener(new MainSelectDisputePop.SelectItemListener() {
                    @Override
                    public void select(int type) {
                        casetype = type;
                        et_filetype2.setText(types[type]);
                    }
                });
                pop.showPopupWindow(et_filetype2);
                break;


        }
    }

    public void refreshData(){
        Log.d("zhouzhuo","=search==refreshData=");
        currentPage = 1;
        adapter = null;
        initData();
    }

    public void initData() {
        //Activity activity, String search,int page,String type,String beginTime,String end
        SearchFragmentCaseListModel model = SearchFragmentCaseListModel.getInstance();
        model.getList(getActivity(),search,currentPage,type,beginTime,end,casetype+"");
        model.setCallBackListener(new BaseModel.CallBackListener<MainFragmentItemBeans>() {
            @Override
            public void success(MainFragmentItemBeans beans) {
                if (beans!=null) {
                    if(currentPage == 1){
                        totalList = beans.list;
                    }else {
                        totalList.addAll(beans.list);
                    }
                    Log.d("zhouzhuo","size===="+totalList.size());
                    currentPage = beans.currentPage;
                    isLastpage = beans.isLastpage;
                    setAdapter();
                }else {
                    if(totalList!=null){
                        totalList.clear();
                    }
                    setAdapter();
                }
            }

            @Override
            public void failed() {

            }
        });


    }


    private void setAdapter(){
        if(adapter == null){
            adapter = new SearchFileFragmentAdapter(getActivity(),totalList);
            listView.setAdapter(adapter);
        }else {
            adapter.notifyDataSetChanged();
        }
    }

}
