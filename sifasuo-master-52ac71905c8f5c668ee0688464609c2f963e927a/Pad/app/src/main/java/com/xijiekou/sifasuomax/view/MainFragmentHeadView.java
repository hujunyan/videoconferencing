package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.MainActivity;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.LoginActivity;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.LogoutModel;
import com.xijiekou.sifasuomax.utils.ConstantsUtils;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/7/31.
 */

public class MainFragmentHeadView extends LinearLayout implements View.OnClickListener{
    private WebView wevView;
    private EditText et_content;
    private RelativeLayout rl_search;
    private MainFragmentUserInfo main_user_info;
    private TextView tv_more;
    private MainActivity activity;


    public MainFragmentHeadView(Context context) {
        this(context,null);
    }

    public MainFragmentHeadView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        activity = (MainActivity) context;
        initView(context);
        loadUrl();
    }

    public void updatePhoto(){
        main_user_info.updatePhoto();
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.main_fragment_header_view,this,true);
        wevView = (WebView) view.findViewById(R.id.web_view);
        wevView.setScrollContainer(false);
        wevView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction() == MotionEvent.ACTION_MOVE);
            }
        });
        main_user_info = (MainFragmentUserInfo) view.findViewById(R.id.main_user_info);
        main_user_info.setListener(new MainFragmentUserInfo.LogoutListener() {
            @Override
            public void logout() {
                LogoutModel logoutModel = new LogoutModel();
                String number = (String) SharePreferenceUtils.get(activity,"loginname","");
                logoutModel.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)){
                            PostRequestBean postRequestBean = new Gson().fromJson(message,PostRequestBean.class);
                            if(postRequestBean.code == 1){
                                Intent intent = new Intent(getContext(),LoginActivity.class);
                                IntentUtils.startActivity(getContext(),intent,false,0);
                                ConstantsUtils.bean = null;
                                SharePreferenceUtils.put(getContext(),"login","");
                                activity.finish();
                            }
                        }
                    }

                    @Override
                    public void failed() {

                    }
                });
              logoutModel.logout(number);

            }
        });
        et_content = (EditText) view.findViewById(R.id.et_content);
        et_content.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                    addTextlistener.search(s.toString());
            }
        });
        rl_search = (RelativeLayout) view.findViewById(R.id.rl_search);
        rl_search.setOnClickListener(this);
        tv_more = (TextView) view.findViewById(R.id.tv_more);
        tv_more.setOnClickListener(this);
    }

    public void loadUrl(){
        WebSettings settings = wevView.getSettings();
        settings.setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        settings.setJavaScriptEnabled(true);// 开启js功能, 同时, 百度网页不再跳转到浏览器
        settings.setDefaultTextEncodingName("UTF-8") ;
        wevView.loadUrl(UrlDomainUtil.urlHeader+"/index/news.html");
    }


    public AddTextListener  addTextlistener;

    public void setAddTextListener(AddTextListener listener){
        this.addTextlistener = listener;
    }

    public String getContent(){
        return et_content.getText().toString().trim();
    }
    public interface AddTextListener{
        void search(String content);
    }

    public interface SearchListener{
        void search(String content);
    }


    public SearchListener listener;

    public void setSearchListener(SearchListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_search:
                if(listener!=null){
                    String content = et_content.getText().toString().trim();
                    listener.search(content);
                }
                break;
            case R.id.tv_more:
                String url = UrlDomainUtil.urlHeader+"/index/news_list.html";
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                Uri content_url = Uri.parse(url);
                intent.setData(content_url);
                getContext().startActivity(intent);
                break;
        }
    }
}
