package com.xijiekou.sifasuomax.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by zhouzhuo on 2017/9/13.
 */

public abstract class BaseListAdapter<E> extends BaseAdapter{
    protected Context mContext;
    private List<E> mList = new ArrayList<>();
    protected LayoutInflater mInflate;

    public BaseListAdapter(Context context){
        mContext = context;
        mInflate = LayoutInflater.from(context);
    }
    public BaseListAdapter(Context context, List<E> list) {
        this(context);
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public E getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void removeEntity(E e){
        mList.remove(e);
    }

    public List<E> getData() {
        return mList;
    }

    public void clearAll() {
        mList.clear();
    }

}
