package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.bean.NewOralBeans;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/17.
 */

public class ModifyOralModel extends BaseModel {
    private NewOralBeans beans;
    public ModifyOralModel(NewOralBeans beans){
            this.beans = beans;
    }

    public void modify(){
        JSONArray array = new JSONArray();
        ArrayList<NewOralBean> list = beans.list;
        if(list!=null && list.size()>0){
            for (int i =0;i<list.size();i++){
                NewOralBean bean = list.get(i);
                String type = bean.type;
                switch (type){
                    case "1":
                        JSONObject object = new JSONObject();
                        putString(object,"type","1");
                        putString(object,"name",bean.name);
                        putString(object,"sex",bean.sex);
                        putString(object,"nation",bean.nation);
                        putString(object,"age",bean.age);
                        putString(object,"job",bean.job);
                        putString(object,"tel",bean.tel);
                        putString(object,"address",bean.address);
                        putString(object,"idcard",bean.idcard);
                        array.put(object);
                        break;
                    case "2":
                        object = new JSONObject();
                        putString(object,"type","2");
                        putString(object,"faren",bean.name);
                        putString(object,"address",bean.address);
                        putString(object,"tongyi",bean.tongyi);
                        putString(object,"fading",bean.fading);
                        putString(object,"weituoren",bean.weituoren);
                        putString(object,"tel",bean.tel);
                        array.put(object);
                        break;
                    case "3":
                        object = new JSONObject();
                        putString(object,"type","3");
                        putString(object,"faren",bean.faren);
                        putString(object,"address",bean.address);
                        putString(object,"tongyi",bean.tongyi);
                        putString(object,"fading",bean.fading);
                        putString(object,"weituoren",bean.weituoren);
                        putString(object,"tel",bean.tel);
                        array.put(object);
                        break;
                }
            }

        }
        String url = UrlDomainUtil.urlHeader+"/Staff/saveOralCase";
        JSONObject objectLast = new JSONObject();
        try {
            objectLast.put("arr",array.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("zhouzhuo","url:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(beans!=null){
            if(!TextUtils.isEmpty(beans.id)){
                builder.addParams("case_id", beans.id);
            }
            if(!TextUtils.isEmpty(beans.fileName)){
                builder.addParams("fileName", beans.fileName);
            }
            if(!TextUtils.isEmpty(beans.argue)){
                builder.addParams("argue", beans.argue);
            }
            if(!TextUtils.isEmpty(beans.performance)){
                builder.addParams("performance",beans.performance);
            }
            Log.d("zhouzhuo","beans.signature:"+beans.signature);
            if(!TextUtils.isEmpty(beans.signature)){
                builder.addParams("signature",beans.signature);
            }
            if(!TextUtils.isEmpty(beans.num)){
                builder.addParams("num",beans.num);
            }

            builder.addParams("date",beans.date);


        }
        if(array.length()>0){
            builder.addParams("persons",objectLast.toString());
        }
        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","请求失败:"+e.getMessage());
                Log.d("zhouzhuo","请求失败:"+e.toString());
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    Log.d("zhouzhuo","请求成功:"+response);
                    PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);
                }
            }
        });

       /* OkHttpUtils
                .post()
                .url(url)
                .addParams("fileName", beans.fileName)
                .addParams("argue", beans.argue)
                .addParams("performance",beans.performance)
                .addParams("signature",beans.signature)
                .addParams("date",beans.date)
               // .addParams("persons",postJson)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("zhouzhuo","请求失败:"+e.getMessage());
                        Log.d("zhouzhuo","请求失败:"+e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if(response !=null){
                            Log.d("zhouzhuo","请求成功:"+response);
                            PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                            callBackListener.success(postRequestBean);
                        }

                    }*/
        //  });
    }

    public void putString(JSONObject object,String key,String value){
        if(!TextUtils.isEmpty(value)){
            try {
                object.put(key,value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
