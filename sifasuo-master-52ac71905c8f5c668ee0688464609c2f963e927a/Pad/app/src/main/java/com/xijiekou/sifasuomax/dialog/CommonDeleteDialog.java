package com.xijiekou.sifasuomax.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class CommonDeleteDialog extends Dialog implements View.OnClickListener{
    private TextView tv_confirm ,tv_cancel;
    public CommonDeleteDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.deltedialog);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        tv_cancel.setOnClickListener(this);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);
        tv_confirm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.tv_cancel:
                dismiss();
                break;
            case R.id.tv_confirm:
                dismiss();
                if(listener!=null){
                    listener.delete();
                }
                break;
        }
    }

    public interface DeleteListener{
        void delete();
    }
    public DeleteListener listener;
    public void setDeleteListener(DeleteListener listener){
        this.listener = listener;
    }
}
