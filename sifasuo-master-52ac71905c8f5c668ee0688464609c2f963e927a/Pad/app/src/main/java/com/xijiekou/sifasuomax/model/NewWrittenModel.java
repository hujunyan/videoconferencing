package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.bean.NewWrittenBeans;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/4.
 * 书面协议
 */

public class NewWrittenModel extends BaseModel {
    private NewWrittenBeans beans;
    public NewWrittenModel(NewWrittenBeans beans){
        this.beans = beans;
    }

    public void postWritten(){
        final JSONArray array = new JSONArray();
        ArrayList<NewOralBean> list = beans.list;
        if(list!=null && list.size()>0){
            for (int i =0;i<list.size();i++){
                NewOralBean bean = list.get(i);
                String type = bean.type;
                switch (type){
                    case "1":
                        JSONObject object = new JSONObject();
                        putString(object,"type","1");
                        putString(object,"name",bean.name);
                        putString(object,"sex",bean.sex);
                        putString(object,"nation",bean.nation);
                        putString(object,"age",bean.age);
                        putString(object,"job",bean.job);
                        putString(object,"tel",bean.tel);
                        putString(object,"address",bean.address);
                        putString(object,"idcard",bean.idcard);
                        array.put(object);
                        break;
                    case "2":
                        object = new JSONObject();
                        putString(object,"type","2");
                        putString(object,"faren",bean.faren);
                        putString(object,"address",bean.address);
                        putString(object,"tongyi",bean.tongyi);
                        putString(object,"fading",bean.fading);
                        putString(object,"weituoren",bean.weituoren);
                        putString(object,"tel",bean.tel);
                        array.put(object);
                        break;
                    case "3":
                        object = new JSONObject();
                        putString(object,"type","3");
                        putString(object,"faren",bean.faren);
                        putString(object,"address",bean.address);
                        putString(object,"tongyi",bean.tongyi);
                        putString(object,"fading",bean.fading);
                        putString(object,"weituoren",bean.weituoren);
                        putString(object,"tel",bean.tel);
                        array.put(object);
                        break;
                }

            }

        }
        String url = UrlDomainUtil.urlHeader+"/Staff/createCasePart1";
       // String postJson = "{\"arr\":"+array.toString()+"}";
        JSONObject object3 = null;
        try {
             object3 = new JSONObject();
            object3.putOpt("arr",array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("zhouzhuo","url:"+url);
        PostFormBuilder builder =OkHttpUtils
                .post()
                .url(url);
        if(beans!=null){
            if(!TextUtils.isEmpty(beans.case_id)){
                builder.addParams("case_id", beans.case_id);
            }
            Log.d("zhouzhuo","case===id==="+beans.case_id);
            if(!TextUtils.isEmpty(beans.filing)){
                builder.addParams("filing", beans.filing);
            }
            if(!TextUtils.isEmpty(beans.simple)){
                builder.addParams("simple",beans.simple);
            }
            if(!TextUtils.isEmpty(beans.content)){
                builder.addParams("conteunt",beans.content);
            }
            if(!TextUtils.isEmpty(beans.signurl)){
                builder.addParams("signUrl",beans.signurl);
            }
            Log.d("zhouzhuo","sign url=="+beans.signurl);
            if(!TextUtils.isEmpty(beans.date)){
                builder.addParams("date",beans.date);
            }
            if(!TextUtils.isEmpty(beans.isAudit)){
                builder.addParams("isAudit",beans.isAudit);
                Log.d("zhouzhuo","isAudit:"+beans.isAudit);
            }

        }
        if(array.length()>0){
            Log.d("zhouzhuo","==length=="+array.length());
            builder.addParams("persons",object3.toString());
        }
        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","请求失败:"+e.getMessage());
                Log.d("zhouzhuo","请求失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    callBackListener.success(response);
                    Log.d("zhouzhuo","请求成功:"+response);
                   /* PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);*/
                }
            }
        });

    }

    public void putString(JSONObject object,String key,String value){
        if(!TextUtils.isEmpty(value)){
            try {
                object.put(key,value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
