package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.NewExplainBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SavePart9 extends BaseModel{
    public void save(NewExplainBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/saveCasePart9";
        Log.d("zhouzhuo","卷宗情况说明:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(bean!=null){
            if(!TextUtils.isEmpty(bean.case_id)){
                builder.addParams("case_id", bean.case_id);
            }
            Log.d("zhouzhuo","case_info:"+bean.case_info);
            Log.d("zhouzhuo","case_auditor:"+bean.auditor);
            if(!TextUtils.isEmpty(bean.case_info)){
                builder.addParams("case_info", bean.case_info);
            }
            if(!TextUtils.isEmpty(bean.auditor)){
                builder.addParams("auditor",bean.auditor);
            }


        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","卷宗情况说明失败:"+e.getMessage());
                Log.d("zhouzhuo","卷宗情况说明失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                   // callBackListener.success(response);
                    Log.d("zhouzhuo","卷宗情况说明求成功:"+response);
                    PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);
                }
            }
        });
    }
}
