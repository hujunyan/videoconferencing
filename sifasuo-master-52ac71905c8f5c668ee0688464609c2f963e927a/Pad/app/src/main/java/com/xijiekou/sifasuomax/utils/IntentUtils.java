package com.xijiekou.sifasuomax.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/7/4.
 */
public class IntentUtils {
    private IntentUtils(){}
    /**
     * 添加启动动画
     *
     * 非forResult
     * */
    public static void startActivity(Context activity, Intent intent)
    {
        startActivity(activity, intent, R.anim.push_int_from_right, R.anim.push_out_toright_no_translate, false, 0);
    }

    /**
     * 添加启动动画
     *
     * forResult
     * */
    public static void startActivity(Context activity, Intent intent, boolean forResult, int requestCode)
    {
        startActivity(activity, intent,R.anim.push_int_from_right, R.anim.push_out_toright_no_translate, forResult, requestCode);
    }

    /**
     * 添加启动动画
     * 可自定义动画
     * */
    public static void startActivity(Context activity, Intent intent, int inAnim, int outAnim)
    {
        startActivity(activity, intent, inAnim, outAnim, false, 0);
    }

    /**
     * 添加启动动画
     * 用于自定义动画 & forResult
     * */
    public static void startActivity(Context activity, Intent intent, int inAnim, int outAnim, boolean forResult, int requestCode)
    {
        try {
            if(!forResult)
            {
                activity.startActivity(intent);
            } else if (activity instanceof Activity)
            {
                ((Activity)activity).startActivityForResult(intent, requestCode);
            }

            if(activity instanceof Activity){
                ((Activity)activity).overridePendingTransition(inAnim, outAnim);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private static void gotoNativePage(Context context, String componentName){
        try {
            Intent intent = new Intent();
            intent.setClassName(context, componentName);
            IntentUtils.startActivity(context,intent);
        } catch (Exception e) {
        }
    }




    public static void startFragmentToActivity(Fragment fragment, Intent intent, boolean forResult, int requestCode)
    {
        startFragmentToActivity(fragment, intent,R.anim.push_int_from_right, R.anim.push_out_toright_no_translate, forResult, requestCode);
    }

    //从碎片跳转到activity
    private static void startFragmentToActivity(Fragment fragment, Intent intent, int inAnim, int outAnim, boolean forResult, int requestCode) {
        try {
            if (!forResult) {
                fragment.startActivity(intent);
            } else {
                fragment.startActivityForResult(intent, requestCode);
            }

            fragment.getActivity().overridePendingTransition(inAnim, outAnim);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
