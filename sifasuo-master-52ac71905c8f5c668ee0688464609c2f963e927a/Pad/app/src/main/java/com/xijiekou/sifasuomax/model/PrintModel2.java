package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.xijiekou.sifasuomax.utils.PrintSeverUtils;

import org.ksoap2.serialization.SoapObject;

import java.util.HashMap;

/**
 * Created by zhouzhuo on 2017/9/25.
 */

public class PrintModel2 extends BaseModel {
    public void print(String content,String method,String printIp){
        HashMap<String, String> properties = new HashMap<>();
        properties.put("paramJson",content);
       // printIp = "10.10.10.107";http://192.168.1.91:8080
       // printIp = "http://10.10.10.107:8080";
        Log.d("zhouzhuo","printid=="+printIp);
        PrintSeverUtils.callWebService(printIp+"/FormalPrintln/printBook",method, properties, new PrintSeverUtils.WebServiceCallBack() {
            @Override
            public void callBack(SoapObject result) {
                if(result!=null){
                    String show=result.getProperty(0).toString();
                    callBackListener.success(show);
                    Log.d("zhouzhuo","result=="+result);
                }else {
                    callBackListener.failed();
                }

            }
        });

    }

}
