package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.CommunityCommitteesBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/9/4.
 */

public class ModifyCommunityModel extends BaseModel{
    public void saveCommunity(CommunityCommitteesBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/saveCommunityReport";

        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        builder.addParams("id",bean.id);
        builder.addParams("zongshu",bean.zongshu);
        builder.addParams("hunyinjiating",bean.hunyinjiating);
        builder.addParams("linliguanxi",bean.linliguanxi);
        builder.addParams("sunhaipeichang",bean.sunhaipeichang);
        builder.addParams("wuyejiufen",bean.wuyejiufen);
        builder.addParams("qita",bean.qita);
        builder.addParams("haibaoguatu",bean.haibaoguatu);
        builder.addParams("banbaozhanban",bean.banbaozhanban);
        builder.addParams("zheyecailiao",bean.zheyecailiao);
        builder.addParams("gongzuopeixun1",bean.gongzuopeixun1);
        builder.addParams("gongzuopeixun2",bean.gongzuopeixun2);
        builder.addParams("falvjiangzuo1",bean.falvjiangzuo1);
        builder.addParams("falvjiagnzuo2",bean.falvjiagnzuo2);
        builder.addParams("beizhu",bean.beizhu);
        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {

                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    Log.d("zhouzhuo","修改社区请求成功:"+response);
                    PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);
                }
            }
        });



    }
}
