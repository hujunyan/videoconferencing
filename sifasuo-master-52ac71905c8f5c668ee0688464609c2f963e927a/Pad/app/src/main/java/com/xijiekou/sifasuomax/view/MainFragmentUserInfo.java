package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;

import java.lang.ref.WeakReference;

/**
 * Created by zhouzhuo on 2017/7/24.
 */

public class MainFragmentUserInfo extends RelativeLayout implements View.OnClickListener{
    private TextView tv_number;
    private TextView tv_date;
    private CircleImageView iv_photo;
    private MyHandler handler;
    private RelativeLayout rl_logout;

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_logout:
                if(logoutListener!=null){
                    logoutListener.logout();
                }
                break;
        }
    }

   public interface LogoutListener {
       void logout();
   }
   public LogoutListener logoutListener;

    public void setListener(LogoutListener logoutListener){
        this.logoutListener = logoutListener;
    }


    private class MyHandler extends Handler{
        private WeakReference<MainFragmentUserInfo> handlerWeakReference;
        public MyHandler(MainFragmentUserInfo info){
            handlerWeakReference = new WeakReference<>(info);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(handlerWeakReference.get()!=null){
                switch (msg.what){
                    case 0:
                        long sysTime = System.currentTimeMillis();
                        CharSequence sysTimeStr = DateFormat.format("yyyy-MM-dd HH:mm:ss", sysTime);
                        tv_date.setText(sysTimeStr);
                        break;
                }
            }

        }
    }
    public MainFragmentUserInfo(Context context) {
        this(context,null);
    }

    public MainFragmentUserInfo(Context context, AttributeSet attrs) {
        super(context, attrs);
        handler = new MyHandler(this);
        init(context);
        initTime();
    }

    private void initTime() {
        new TimeThread().start();
    }
    public void initData(){

        String number = (String) SharePreferenceUtils.get(getContext(),"loginname","");

        if(!TextUtils.isEmpty(number)) {
            tv_number.setText(number);
        }
        updatePhoto();
    }

    public void updatePhoto(){
        String photo =(String) SharePreferenceUtils.get(getContext(),"photo","");
        if(!TextUtils.isEmpty(photo)){
            Glide.with(getContext()).load(photo).into(iv_photo);
        }/*else {
            iv_photo.setImageResource(R.drawable.user_photo_default);
        }*/

    }

    private void init(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.main_fragment_user_info,this,true);
        tv_number = (TextView) view.findViewById(R.id.tv_number);
        tv_date = (TextView) view.findViewById(R.id.tv_date);
        iv_photo = (CircleImageView) view.findViewById(R.id.iv_photo);
        rl_logout = (RelativeLayout) view.findViewById(R.id.rl_logout);
        rl_logout.setOnClickListener(this);
        initData();
    }
    public class TimeThread extends Thread {
        @Override
        public void run () {
            do {
                try {
                    Thread.sleep(1000);
                    Message msg = new Message();
                    msg.what = 0;
                    handler.sendMessage(msg);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while(true);
        }
    }

}
