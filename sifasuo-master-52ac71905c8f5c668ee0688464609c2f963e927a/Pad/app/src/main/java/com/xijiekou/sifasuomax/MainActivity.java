package com.xijiekou.sifasuomax;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.xijiekou.sifasuomax.activity.BaseActivity;
import com.xijiekou.sifasuomax.bean.LoginBean;
import com.xijiekou.sifasuomax.fragment.DraftFragment;
import com.xijiekou.sifasuomax.fragment.FileReportFragment;
import com.xijiekou.sifasuomax.fragment.MainFragment;
import com.xijiekou.sifasuomax.fragment.MineFragment;
import com.xijiekou.sifasuomax.fragment.NewFileFragment;
import com.xijiekou.sifasuomax.fragment.SearchFileFragment;
import com.xijiekou.sifasuomax.utils.ConstantsUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.view.MainTopView;
import com.yuyh.library.imgsel.ImgSelActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity {
    private MainFragment mainFragment;
    private MainTopView mainTopView;
    private NewFileFragment newFileFragment;
    private DraftFragment draftFragment;
    private SearchFileFragment searchFileFragment;
    private FileReportFragment fileReportFragment;
    private MineFragment mineFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        AndroidBug5497Workaround.assistActivity(this);
        initView();
    }


    private void initView() {
        mainTopView = (MainTopView) findViewById(R.id.main_top);
        mainTopView.hideBack();
        mainFragment = new MainFragment();
        newFileFragment = new NewFileFragment();
        draftFragment = new DraftFragment();
        searchFileFragment = new SearchFileFragment();
        fileReportFragment = new FileReportFragment();
        mineFragment = new MineFragment();
        // 初始化加载第一个fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment, mainFragment).commit();
        String login = getIntent().getStringExtra("login");
        // bean = (LoginBean.LoginContentBean) bundle.get("bean");
        if(!TextUtils.isEmpty(login)){
            LoginBean.LoginContentBean bean = ConstantsUtils.bean;
            if(bean!=null){
                if(TextUtils.isEmpty(bean.name)){
                    return;
                }
                SharePreferenceUtils.put(this,"name",bean.name);
                SharePreferenceUtils.put(this,"photo",bean.photo);
                SharePreferenceUtils.put(this,"id",bean.id);
                SharePreferenceUtils.put(this,"company",bean.company);
                SharePreferenceUtils.put(this,"password",bean.password);
                SharePreferenceUtils.put(this,"loginname",bean.loginname);
                SharePreferenceUtils.put(this,"printIp",bean.printIp);
                Log.d("zhouzhuo","=printIp=="+bean.printIp);
            }
        }
    }

    public void changeFragment(int prePosition,int curPosition) {
        mainTopView.changeTab(curPosition);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        hideFragment(ft,prePosition);
        switch (curPosition){
            case 0:
                Log.d("zhouzhuo","changeFragment==");
                if (mainFragment.isAdded()) {
                    ft.show(mainFragment);
                    mainFragment.updatePhoto();
                    if(prePosition ==0||prePosition ==1||prePosition==2||prePosition==3 ){
                        mainFragment.refreshData();
                    }
                } else {
                    ft.add(R.id.fragment, mainFragment);
                    mainFragment.updatePhoto();
                }
                break;
            case 1:
                if (newFileFragment.isAdded()) {
                    ft.show(newFileFragment);
                } else {
                    ft.add(R.id.fragment, newFileFragment);
                }
                break;
            case 2:
                if (draftFragment.isAdded()) {
                    ft.show(draftFragment);
                } else {
                    ft.add(R.id.fragment, draftFragment);
                }
                break;
            case 3:
                if (searchFileFragment.isAdded()) {
                    ft.show(searchFileFragment);
                    if(prePosition ==0||prePosition ==1||prePosition==2||prePosition==3 ){
                        searchFileFragment.refreshData();
                    }
                } else {
                    ft.add(R.id.fragment, searchFileFragment);
                }
                break;
            case 4:
                if (fileReportFragment.isAdded()) {
                    ft.show(fileReportFragment);
                } else {
                    ft.add(R.id.fragment, fileReportFragment);
                }
                break;
            case 5:
                if (mineFragment.isAdded()) {
                    ft.show(mineFragment);
                    mineFragment.setMessage();
                } else {
                    ft.add(R.id.fragment, mineFragment);
                }
                break;
        }
        ft.commit();


    }

    private void hideFragment(FragmentTransaction ft ,int position){
        switch (position){
            case 0:
                ft.hide(mainFragment);
                break;
            case 1:
                ft.hide(newFileFragment);
                break;
            case 2:
                ft.hide(draftFragment);
                break;
            case 3:
                ft.hide(searchFileFragment);
                break;
            case 4:
                ft.hide(fileReportFragment);
                break;
            case 5:
                ft.hide(mineFragment);
                break;
            default:
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
       if(data!=null){
           if(requestCode ==0 && resultCode == -1){
               List<String> pathList = data.getStringArrayListExtra(ImgSelActivity.INTENT_RESULT);
               String path = pathList.get(0);
               mineFragment.setIcon(path);
               saveToLocal(path);
               Log.d("zhouzhuo","===拍完照的地址="+pathList.get(0));
           }
        }
        Log.d("zhouzhuo",requestCode+":"+resultCode);
        Log.d("zhouzhuo","data:"+data);
    }

    private void saveToLocal(String path){
        // 其次把文件插入到系统图库
        File file = new File(path);
        try {
            MediaStore.Images.Media.insertImage(getContentResolver(),
                    file.getAbsolutePath(), file.getName(), null);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        // 最后通知图库更新
       sendBroadcast(new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.parse("file://" + path)));

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {
        exitBy2Click(); //调用双击退出函数
    }

    private static Boolean isExit = false;

    protected void exitBy2Click()  {
        Timer tExit;
        if (isExit == false) {
            isExit = true; // 准备退出
            Toast.makeText(this,"再按一次退出程序",Toast.LENGTH_SHORT).show();
            tExit = new Timer();
            tExit.schedule(new TimerTask() {
                @Override
                public void run() {
                    isExit = false; // 取消退出

                }
            }, 2000); // 如果2秒钟内没有按下返回键，则启动定时器取消掉刚才执行的任务
        } else {
            finish();
            //System.exit(0);
        }
    }
}
