package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.adapter.SelectMonthAdapter;
import com.xijiekou.sifasuomax.bean.SelectMonthBean;
import com.xijiekou.sifasuomax.dialog.SelectYearPop;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.SearchMonthStatusModel;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by zhouzhuo on 2017/9/6.
 */

public class ReportFileSelectMonthActivity extends BaseActivity {
    private TextView tv_file_name;
    private GridView grid_view;
    private SelectMonthAdapter adapter;
    private ArrayList<SelectMonthBean> list;
    private final String[] month= {"一月","二月","三月","四月","五月","六月","七月",
            "八月","九月","十月","十一月","十二月"};
    private int currentMonth = Calendar.getInstance().get(Calendar.MONTH);
    private RelativeLayout rl_year;
    private TextView tv_year;
    private int type;
    private String selectYear = Calendar.getInstance().get(Calendar.YEAR)+"";
    private String company;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_file);
        initView();
        requestMonth();
    }

    private void initView() {
        tv_file_name = (TextView) findViewById(R.id.tv_file_name);
        grid_view = (GridView) findViewById(R.id.grid_view);
        tv_year = (TextView) findViewById(R.id.tv_year);
        rl_year = (RelativeLayout) findViewById(R.id.rl_year);
        rl_year.setOnClickListener(this);
        tv_file_name = (TextView) findViewById(R.id.tv_file_name);
        type = getIntent().getIntExtra("type",0);
        setFileName(type,selectYear);

    }

    private void  setFileName(int type,String selectYear){
        if(type ==0){
            tv_file_name.setText(selectYear+"年 人民调解月报表(请单击月份按钮添加该月的报表)");
        }else {
            tv_file_name.setText(selectYear+"年 社区委员会月报表(请单击月份按钮添加该月的报表)");
        }

    }


    private void requestMonth() {
        company =  (String) SharePreferenceUtils.get(this,"company","");
        SearchMonthStatusModel model = new SearchMonthStatusModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                try {
                    JSONObject object = new JSONObject(message);
                    int code = object.getInt("code");
                    if(code == 4014){
                        Toast.makeText(ReportFileSelectMonthActivity.this,object.getString("data"),Toast.LENGTH_SHORT).show();
                    }else if(code ==1){
                        JSONArray array =object.getJSONArray("data");
                        initArray(array);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void failed() {

            }
        });
        model.getSearchStatus(company,selectYear,type+"");
    }

    private void initArray(JSONArray array) {
        list = new ArrayList<>();
        ArrayList<String> haveList = new ArrayList<>();
        for (int j=0;j<array.length();j++){
            try {
                JSONObject object = array.getJSONObject(j);
                String month = object.getString("month");
                haveList.add(month);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        for (int i=0;i<month.length;i++) {
            SelectMonthBean bean = new SelectMonthBean();
            bean.month = month[i];
            if(i>currentMonth&&Integer.parseInt(selectYear)>=Calendar.getInstance().get(Calendar.YEAR)){
                bean.state = 1;
                bean.isClick = false;
            }else {
                bean.isClick = true;
                if (haveList.contains(i + 1 + "")) {
                    bean.state = 0;
                } else {
                    bean.state = 1;
                }
            }
            list.add(bean);
        }
            adapter = new SelectMonthAdapter(this,list);
            grid_view.setAdapter(adapter);
            grid_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    SelectMonthBean bean = list.get(position);
                    int state = bean.state;
                    if(bean.isClick){
                        if(state !=0){
                            if(type == 0){
                                Intent intent = new Intent(ReportFileSelectMonthActivity.this,MediationMonthFormActivity.class);
                                intent.putExtra("company",company);
                                intent.putExtra("year",selectYear);
                                intent.putExtra("month",position+1+"");
                                IntentUtils.startActivity(ReportFileSelectMonthActivity.this,intent,true,0);
                            }else {
                                Intent intent = new Intent(ReportFileSelectMonthActivity.this,CommunityCommitteesMonthActivity.class);
                                intent.putExtra("company",company);
                                intent.putExtra("year",selectYear);
                                intent.putExtra("month",position+1+"");
                                IntentUtils.startActivity(ReportFileSelectMonthActivity.this,intent,true,1);
                            }
                        }else {
                            ToastUtils.showShort(ReportFileSelectMonthActivity.this,"请勿重复添加");
                        }
                    }

                }
            });
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.rl_year:
                SelectYearPop selectYearPop = new SelectYearPop(this);
                selectYearPop.setListener(new SelectYearPop.SelectItemListener() {
                    @Override
                    public void select(String year) {
                        selectYear = year;
                        setFileName(type,selectYear);
                        tv_year.setText(year+"年");
                        requestMonth();
                    }
                });
                selectYearPop.showPopupWindow(rl_year);
                break;
        }
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        Intent intent = getIntent();
        setResult(0,intent);
        finish();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode){
            case 0:
            case 1:
                if(resultCode ==0){
                    requestMonth();
                }
                break;

        }

    }
}
