package com.xijiekou.sifasuomax.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.xijiekou.sifasuomax.MainActivity;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.LoginBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.LoginModel;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.NewWorkUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;

public class LoginActivity extends BaseActivity {
    private EditText et_number,et_password;
    private TextView btn_login;
    private LinearLayout ll_reset_password;
    private String DEVICE_ID;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initView();
    }

    private void initView() {
        et_number = (EditText) findViewById(R.id.et_number);
        et_password = (EditText) findViewById(R.id.et_password);
        btn_login = (TextView) findViewById(R.id.btn_login);
        btn_login.setOnClickListener(this);
        ll_reset_password = (LinearLayout) findViewById(R.id.ll_reset_password);
        ll_reset_password.setOnClickListener(this);
        requestPermission(new String[]{Manifest.permission.READ_PHONE_STATE}, new PermissionHandler() {
            @Override
            public void onGranted() {
                TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
                DEVICE_ID = tm.getDeviceId();
            }

            @Override
            public void onDenied() {

            }

            @Override
            public boolean onNeverAsk() {
                return false;
            }
        });
        Log.d("zhouzhuo","===DEVICE_ID ="+DEVICE_ID);

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.btn_login:
                if(!NewWorkUtils.isConnected(LoginActivity.this)){
                    ToastUtils.showShort(LoginActivity.this,"当前网络不好");
                    return;
                }
                LoginModel model = new LoginModel();
                model.login(et_number.getText().toString().trim(),
                        et_password.getText().toString().trim(),DEVICE_ID);
                model.setCallBackListener(new BaseModel.CallBackListener<LoginBean>() {
                    @Override
                    public void success(LoginBean loginBean) {
                        if(loginBean!=null){
                            if(loginBean.code ==1){
                                Intent intent = new Intent(LoginActivity.this,MainActivity.class);
                                intent.putExtra("login","login");
                                IntentUtils.startActivity(LoginActivity.this,intent);
                                SharePreferenceUtils.put(LoginActivity.this,"login","login");
                                Toast.makeText(LoginActivity.this,"登录成功",Toast.LENGTH_SHORT).show();
                                finish();
                            }else {
                                ToastUtils.showShort(LoginActivity.this,loginBean.bean.errorMessage);
                                btn_login.setClickable(true);
                            }
                        }else {
                            btn_login.setClickable(true);
                        }

                    }
                    @Override
                    public void failed() {

                    }
                });
                break;
            case R.id.ll_reset_password:
                ToastUtils.showShort(LoginActivity.this,"忘记密码请联系后台管理员");
                break;
        }

    }
}
