package com.xijiekou.sifasuomax.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xijiekou.sifasuomax.bean.SelectMonthBean;
import com.xijiekou.sifasuomax.holder.BaseHolder;
import com.xijiekou.sifasuomax.holder.SelectMonthHolder;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/9/6.
 */

public class SelectMonthAdapter extends BaseAdapter {
    private final String[] month= {"一月","二月","三月","四月","五月","六月","七月",
                "八月","九月","十月","十一月","十二月"};
    private ArrayList<SelectMonthBean> list;
    private Activity activity;
    public SelectMonthAdapter(Activity activity,
                              ArrayList<SelectMonthBean> list){
        this.list = list;
        this.activity = activity;

    }

    @Override
    public int getCount() {
        return month.length;
    }

    @Override
    public Object getItem(int position) {
        return month;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseHolder<SelectMonthBean> holder;
        if(convertView == null){
            holder = new SelectMonthHolder(activity);
        }else {
            holder = (BaseHolder<SelectMonthBean>) convertView.getTag();
        }

        holder.setData(list.get(position));
        holder.changeItemBg(position);
        return holder.getContentView();

    }
}
