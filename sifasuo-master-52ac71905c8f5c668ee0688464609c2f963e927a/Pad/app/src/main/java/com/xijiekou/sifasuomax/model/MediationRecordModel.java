package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.xijiekou.sifasuomax.bean.MediationRecordBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/4.
 */

public class MediationRecordModel extends BaseModel{
    public void registration(MediationRecordBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/createCasePart5";
        Log.d("zhouzhuo","调解记录:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(bean!=null){
            if(!TextUtils.isEmpty(bean.case_id)){
                builder.addParams("case_id", bean.case_id);
            }
            if(!TextUtils.isEmpty(bean.date)){
                builder.addParams("date", bean.date);
            }
            if(!TextUtils.isEmpty(bean.time_range)){
                builder.addParams("time_range",bean.time_range);
            }
            if(!TextUtils.isEmpty(bean.time)){
                builder.addParams("time",bean.time);
            }
            if(!TextUtils.isEmpty(bean.location)){
                builder.addParams("location",bean.location);
            }
            if(!TextUtils.isEmpty(bean.litigant)){
                builder.addParams("litigant",bean.litigant);
            }
            if(!TextUtils.isEmpty(bean.attend)){
                builder.addParams("attend",bean.attend);
            }

            if(!TextUtils.isEmpty(bean.record)){
                builder.addParams("record", bean.record);
            }
            if(!TextUtils.isEmpty(bean.result)){
                builder.addParams("result", bean.result);
            }
            if(!TextUtils.isEmpty(bean.litigant_sign)){
                builder.addParams("litigant_sign", bean.litigant_sign);
            }
            if(!TextUtils.isEmpty(bean.attend_sign)){
                builder.addParams("attend_sign",bean.attend_sign);
            }
            if(!TextUtils.isEmpty(bean.record_sign)){
                builder.addParams("record_sign",bean.record_sign);
            }
            if(!TextUtils.isEmpty(bean.formdate)){
                builder.addParams("formdate",bean.formdate);
            }

        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","调解记录请求失败:"+e.getMessage());
                Log.d("zhouzhuo","调解记录请求失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    callBackListener.success(response);
                    Log.d("zhouzhuo","调解记录请求成功:"+response);
                  /*  PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);*/
                }
            }
        });
    }
}
