package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/8/5.
 */

public class VisitRecordBean {
    public String id;
    public String case_id;
    public String person;
    public String number;
    public String visit_reason;
    public String date;
    public String time_range;
    public String datetime;
    public String visit_state;
    public String committee;
    public String visit_url;
    public String formdate;
    public String isAudit;
}
