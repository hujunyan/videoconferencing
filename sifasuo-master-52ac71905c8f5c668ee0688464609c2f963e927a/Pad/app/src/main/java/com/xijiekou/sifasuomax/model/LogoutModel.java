package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/7/10.
 */

public class LogoutModel extends BaseModel{
    public void logout(String phone) {
        String url = UrlDomainUtil.urlHeader + "/Staff/logoff/tel/"+phone;
        Log.d("zhouzhuo","url==="+url);

        StringRequest request = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                callBackListener.success(s);
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.d("zhouzhuo","logout :"+volleyError.toString());
            }
        });
        MyApp.getHttpQueue().add(request);

    }
}