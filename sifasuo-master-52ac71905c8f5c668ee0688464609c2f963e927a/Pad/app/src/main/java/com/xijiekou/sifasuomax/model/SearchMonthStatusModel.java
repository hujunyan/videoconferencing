package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

/**
 * Created by zhouzhuo on 2017/9/11.
 */

public class SearchMonthStatusModel extends BaseModel {
    //type 1.社区调委会月报表  0.人民调解月报表
    public void getSearchStatus(String company, String year, String type){
        String url = UrlDomainUtil.urlHeader+"/staff/getReportState/company/"+company+"/year/"+year+"/type/"+type;
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(!TextUtils.isEmpty(response)){
                            callBackListener.success(response);
                           Log.d("zhouzhuo","请求月报表状态=="+response);
                        }
                        //callBackListener.success(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }

}
