package com.xijiekou.sifasuomax.utils.audioutils.interfa;

/**
 * 时间格式转换算法接口
 * Created by LiQi on 2017/1/18.
 */
public interface OnTimeFormatTransition {
    public String onTimeFormatTransition(int duration);
}
