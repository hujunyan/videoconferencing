package com.xijiekou.sifasuomax.utils.audioutils.algorithm;


import com.xijiekou.sifasuomax.utils.audioutils.interfa.OnTimeFormatTransition;

/**
 * 把秒钟转换成特定时间格式对象
 * <p/>
 * 时间格式：00:00
 * Created by LiQi on 2017/1/18.
 */
public class SecondsTimeFormatText implements OnTimeFormatTransition {
    @Override
    public String onTimeFormatTransition(int duration) {
        int second , minute = 0;
        if (duration >= 60) {
            //秒
            second = duration % 60;
            //分
            minute =  duration / 60;
        } else
            second = duration;

        return String.format("%1$02d:%2$02d", minute, second);
    }
}
