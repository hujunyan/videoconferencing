package com.xijiekou.sifasuomax.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.CommunityCommitteesMonthActivity;
import com.xijiekou.sifasuomax.activity.MediationMonthFormActivity;
import com.xijiekou.sifasuomax.activity.ReportFileSelectMonthActivity;
import com.xijiekou.sifasuomax.adapter.HistoryReportAdapter;
import com.xijiekou.sifasuomax.bean.HistoryReportBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.MediationListModel;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshBase;
import com.xijiekou.sifasuomax.utils.pulltorefresh.PullToRefreshListView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/4.
 * 卷宗报表
 */
public class FileReportFragment extends BaseFragment{
    private PullToRefreshListView refreshList;
    private ListView listView ;
    private FrameLayout fl_one,fl_two;
    private int currentPage = 1;
    private int isLastpage;
    private HistoryReportAdapter adapter;
    private TextView tv_one,tv_two;
    private View view_one,view_two;
    //0人民调解月报表 1社区调委会月报表
    private String type="0";
    private ArrayList<HistoryReportBean> list = new ArrayList<>();
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_file_report, null);
        initView(view);
        initData();
        return view;
    }

    private void initData() {
        MediationListModel model = new MediationListModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                if(message!=null){
                    try {
                        JSONObject object = new JSONObject(message);
                        int code = object.getInt("code");
                        JSONObject object2 = object.getJSONObject("data");
                        isLastpage = object2.getInt("isLastpage");
                        currentPage = object2.getInt("currentPage");
                        if(code == 1){
                            ArrayList<HistoryReportBean> list1 = new ArrayList<>();
                            JSONArray array = object2.getJSONArray("content");
                            Gson gson = new Gson();
                            if(array.length()>0){
                                for (int i = 0;i<array.length();i++){
                                    JSONObject object1 = array.getJSONObject(i);
                                    HistoryReportBean bean = gson.fromJson(object1.toString(),HistoryReportBean.class);
                                    list1.add(bean);
                                }
                            }
                            list.addAll(list1);
                            refreshData();
                        }else {
                            list.clear();
                            refreshData();
                            ToastUtils.showShort(getActivity(),"没有查找到相关数据");
                        }
                    } catch (JSONException e) {
                        list.clear();
                        refreshData();
                        Log.d("zhouzhuo","异常==="+e.getMessage()+"==="+e.toString());
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failed() {

            }
        });
        model.getList(currentPage+"",type, (String) SharePreferenceUtils.get(getActivity(),"company",""));
    }

    private void refreshData(){
        if(adapter == null){
            adapter = new HistoryReportAdapter(getActivity(),list);
            listView.setAdapter(adapter);
        }else {
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("zhouzhuo","request code=="+requestCode);
        Log.d("zhouzhuo","resultCode code=="+resultCode);
        switch (requestCode){
            case 0:
                if(resultCode ==0){
                    if(type.equals("0")){
                        requestLeft();
                    }
                }
            case 1:
                if(resultCode ==0){
                    if(type.equals("1")){
                        requestRight();
                    }
                }
                break;
        }
    }



    private void initView(View view) {
        refreshList = (PullToRefreshListView) view.findViewById(R.id.lv_list);
        listView = refreshList.getRefreshableView();
        listView.setVerticalScrollBarEnabled(false);
        tv_one = (TextView) view.findViewById(R.id.tv_one);
        view_one = view.findViewById(R.id.view_one);
        tv_one.setOnClickListener(this);
        tv_two = (TextView) view.findViewById(R.id.tv_two);
        view_two = view.findViewById(R.id.view_two);
        tv_two.setOnClickListener(this);
        fl_one = (FrameLayout) view.findViewById(R.id.fl_one);
        fl_one.setOnClickListener(this);
        fl_two = (FrameLayout) view.findViewById(R.id.fl_two);
        fl_two.setOnClickListener(this);
        refreshList.setPullLoadEnabled(true);
        refreshList.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                currentPage = 1;
                adapter = null;
                if(list!=null){
                    list.clear();
                }
                initData();
                refreshList.onPullDownRefreshComplete();
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                if(isLastpage == 0){
                    currentPage++;
                    initData();
                }else {
                    ToastUtils.showShort(getActivity(),"没有更多数据");
                }
                refreshList.onPullUpRefreshComplete();
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                HistoryReportBean bean = list.get(position);
                String type = bean.type;
                Intent intent;
                if(type.equals("0")){
                    intent= new Intent(getActivity(),MediationMonthFormActivity.class);
                    intent.putExtra("bean",bean);
                    intent.putExtra("search","search");
                    IntentUtils.startFragmentToActivity(FileReportFragment.this,intent,true,0);
                }else {
                    intent= new Intent(getActivity(),CommunityCommitteesMonthActivity.class);
                    intent.putExtra("bean",bean);
                    intent.putExtra("search","search");
                    IntentUtils.startFragmentToActivity(FileReportFragment.this,intent,true,1);
                }


            }
        });

    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.fl_one:
                Intent intent = new Intent(getActivity(),ReportFileSelectMonthActivity.class);
                intent.putExtra("type",0);
                IntentUtils.startFragmentToActivity(FileReportFragment.this,intent,true,0);
                break;
            case R.id.fl_two:
                intent = new Intent(getActivity(),ReportFileSelectMonthActivity.class);
                intent.putExtra("type",1);
                IntentUtils.startFragmentToActivity(FileReportFragment.this,intent,true,1);
                break;
            case R.id.tv_one:
                if(type.equals("1")){
                    requestLeft();
                }
                break;
            case R.id.tv_two:
                if(type.equals("0")){
                   requestRight();
                }
                break;
        }
    }


    private void requestLeft(){
        type = "0";
        currentPage = 1;
        adapter = null;
        if(list!=null){
            list.clear();
        }
        initData();
        tv_one.setTextColor(Color.parseColor("#0b55ae"));
        view_one.setVisibility(View.VISIBLE);
        tv_two.setTextColor(Color.parseColor("#333333"));
        view_two.setVisibility(View.INVISIBLE);
    }

    private void requestRight(){
        adapter = null;
        if(list!=null){
            list.clear();
        }
        type = "1";
        initData();
        tv_one.setTextColor(Color.parseColor("#333333"));
        view_one.setVisibility(View.INVISIBLE);
        tv_two.setTextColor(Color.parseColor("#0b55ae"));
        view_two.setVisibility(View.VISIBLE);
    }

}
