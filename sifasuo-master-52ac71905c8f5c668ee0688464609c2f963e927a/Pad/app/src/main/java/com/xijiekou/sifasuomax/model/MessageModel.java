package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.MessageBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/8/21.
 */

public class MessageModel extends BaseModel{
    public void getMessage(){
        String url2 = UrlDomainUtil.urlHeader+"/User/getNotice/type/1";
        Log.d("zhouzhuo","url==="+url2);
        StringRequest stringRequest = new StringRequest(url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {

                            JSONObject object = new JSONObject(response);
                            int code = object.getInt("code");
                            if(code == 1){
                                ArrayList<MessageBean.MessageChildBean> list = new ArrayList<>();
                                JSONArray array = object.getJSONArray("data");
                                for (int i=0;i<array.length();i++){
                                    JSONObject object1 = array.getJSONObject(i);
                                    MessageBean.MessageChildBean bean1 = new MessageBean.MessageChildBean();
                                    bean1.message = object1.getString("message");
                                    bean1.subject = object1.getString("subject");
                                    list.add(bean1);
                                }
                                if(list.size()>0){
                                    callBackListener.success(list);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Log.d("zhouzhuo","我的消息=="+response);

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }
}
