package com.xijiekou.sifasuomax.activity;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.xijiekou.sifasuomax.utils.NewWorkUtils;
import com.xijiekou.sifasuomax.utils.PermissionUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;

/**
 * Created by zhouzhuo on 2017/6/29.
 */

public class BaseActivity extends FragmentActivity implements View.OnClickListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
      //  MyApp.myApp.add(this);
        if(!NewWorkUtils.isConnected(this)){
            ToastUtils.showShort(this,"当前网络不好");
            return;
        }

    }

    @Override
    public void onClick(View v) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
       // MyApp.myApp.removeActivity(this);
       // Log.d("zhouzhuo","remove==="+this);
    }



    public void exit(){
       // MyApp.myApp.exit();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
    }


    //下面是权限相关代码----------------------------------------!
    /**
     * 权限回调Handler
     */
    private PermissionHandler mHandler;

    /**
     * 请求权限
     *
     * @param permissions 权限列表
     * @param handler     回调
     */
    public void requestPermission(String[] permissions, PermissionHandler handler) {
        String permissionArr[] = PermissionUtils.hasSelfPermissions(this, permissions);
        if (permissionArr.length==0) {
            handler.onGranted();
        } else {
            mHandler = handler;
            ActivityCompat.requestPermissions(this, permissionArr, 001);
        }
    }




    /**
     * 权限请求结果
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (mHandler == null) return;

//        if (PermissionUtils.getTargetSdkVersion(this) < 23 && !PermissionUtils.hasSelfPermissions(this, permissions)) {
//            mHandler.onDenied();
//            return;
//        }

        if (PermissionUtils.verifyPermissions(grantResults)) {
            mHandler.onGranted();
        } else {
            if (!PermissionUtils.shouldShowRequestPermissionRationale(this, permissions)) {
                if (!mHandler.onNeverAsk()) {
                    Toast.makeText(this, "权限已被拒绝,请在设置-应用-权限中打开", Toast.LENGTH_SHORT).show();
                }

            } else {
                mHandler.onDenied();
            }
        }
    }



    /**
     * 权限回调接口
     */
    public interface PermissionHandler {
        /**
         * 权限通过
         */
        void onGranted();

        /**
         * 权限拒绝
         */
        void onDenied();

        /**
         * 不再询问
         *
         * @return 如果要覆盖原有提示则返回true
         */
        boolean onNeverAsk();
    }

    public void setTextNotNull(String content, EditText editText){
        if(!TextUtils.isEmpty(content)){
            editText.setText(content);
        }
    }

}
