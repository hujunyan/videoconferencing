package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.NewExplainBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SearchPart9 extends BaseModel{
    public void searchPart9(String id){
        String url = UrlDomainUtil.urlHeader + "/Staff/getCasePart9/case_id/"+id;
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(!TextUtils.isEmpty(response)){
                            try {
                                JSONObject object = new JSONObject(response);
                                int code = object.getInt("code");
                                if(code == 1){
                                    NewExplainBean bean = new Gson().fromJson(object.getJSONObject("data").toString(), NewExplainBean.class);
                                    callBackListener.success(bean);
                                }else if(code == 4012){
                                    callBackListener.success(null);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }

                        Log.d("zhouzhuo","search part 9=="+response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }
}
