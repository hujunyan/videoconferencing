package com.xijiekou.sifasuomax.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;

import com.lqr.audio.AudioPlayManager;
import com.lqr.audio.IAudioPlayListener;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.audioutils.MediaPlayerUtils;

import java.io.File;
import java.io.IOException;

/**
 * Created by zhouzhuo on 2017/8/10.
 */

public class SearchVoiceDialog extends Dialog implements View.OnClickListener,MediaPlayerUtils.OnMediaPlayerUtilsInterfa {
    private String url;
    private ImageView imageView;
    private Activity context;
    private MediaPlayerUtils mMediaPlayerUtils;
    public SearchVoiceDialog(Activity context,String url) {
        super(context);
        this.url = url;
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestWindowFeature(Window.FEATURE_PROGRESS);
        setContentView(R.layout.dialog_search_voice_dialog);
        imageView = (ImageView) findViewById(R.id.imageView);
        imageView.setOnClickListener(this);

    }

    public void playS(){
        Log.d("zhouzhuo","playS  url:"+url);
        mMediaPlayerUtils = MediaPlayerUtils.getMediaPlayerUtils().init(context, url).
                setOnMediaPlayerUtilsInterfa(this);
        mMediaPlayerUtils.start();
    }
    private MediaPlayer mediaPlayer;

    private void palyTwo(){
       // url = "http://59.110.156.74/Uploads/voice/2017-08-11/598d19a3315c3.wav";
        Log.d("zhouzhuo","play two==");
        mediaPlayer = new MediaPlayer();
        try {
            mediaPlayer.setDataSource(url);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        if(mediaPlayer!=null){
            mediaPlayer.stop();
            mediaPlayer = null;
        }
        super.dismiss();
    }

    public void voice(){
        Log.d("zhouzhuo","=voice11111=="+url);
        Uri audioUri = Uri.fromFile(new File(url));
        Log.d("zhouzhuo", audioUri.toString());
        AudioPlayManager.getInstance().startPlay(context, audioUri, new IAudioPlayListener() {
            @Override
            public void onStart(Uri var1) {
                if (imageView != null && imageView.getBackground() instanceof AnimationDrawable) {
                    AnimationDrawable animation = (AnimationDrawable) imageView.getBackground();
                    animation.start();
                }
            }

            @Override
            public void onStop(Uri var1) {
                if (imageView != null && imageView.getBackground() instanceof AnimationDrawable) {
                    AnimationDrawable animation = (AnimationDrawable)imageView.getBackground();
                    animation.stop();
                    animation.selectDrawable(0);
                }

            }

            @Override
            public void onComplete(Uri var1) {
                if (imageView != null && imageView.getBackground() instanceof AnimationDrawable) {
                    AnimationDrawable animation = (AnimationDrawable) imageView.getBackground();
                    animation.stop();
                    animation.selectDrawable(0);
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imageView:
               // voice();
                palyTwo();
                //playS();
                break;
        }

    }

    @Override
    public void onMediaPlayerTime(int time) {

    }

    @Override
    public void onMediaPlayerOk() {

    }

    @Override
    public void onMediaPlayerError() {

    }
}
