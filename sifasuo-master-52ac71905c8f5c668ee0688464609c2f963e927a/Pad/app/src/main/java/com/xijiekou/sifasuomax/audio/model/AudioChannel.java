package com.xijiekou.sifasuomax.audio.model;

import android.media.AudioFormat;

/**
 * Created by zhouzhuo on 2017/6/16.
 */

public enum AudioChannel {
    STEREO,
    MONO;

    public int getChannel(){
        switch (this){
            case MONO:
                return AudioFormat.CHANNEL_IN_MONO;
            default:
                return AudioFormat.CHANNEL_IN_STEREO;
        }
    }
}
