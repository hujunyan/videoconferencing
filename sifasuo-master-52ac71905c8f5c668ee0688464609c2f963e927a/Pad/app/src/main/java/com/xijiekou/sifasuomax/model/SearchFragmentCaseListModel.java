package com.xijiekou.sifasuomax.model;

import android.app.Activity;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/31.
 */

public class SearchFragmentCaseListModel extends BaseModel {
    private static SearchFragmentCaseListModel model;
    public static SearchFragmentCaseListModel getInstance(){
        if(model == null){
            model = new SearchFragmentCaseListModel();
        }
        return model;
    }
    public void getList(final Activity activity, String search, int page, int type, String beginTime, String end,String caseType) {
        String id = (String) SharePreferenceUtils.get(activity,"id","");
        String company = (String) SharePreferenceUtils.get(activity,"company","");
        String url2 = UrlDomainUtil.urlHeader + "/Staff/searchCase/page/"+page+"/id/"+id
                +"/company/"+company;
        if(!TextUtils.isEmpty(search)){
            url2 +="/search/"+search;
        }
        if(type!=3){
            url2 +="/type/"+type;
        }
        if(!TextUtils.isEmpty(beginTime)){
            url2 +="/begin/"+beginTime;
        }
        if(!TextUtils.isEmpty(end)){
            url2 +="/end/"+end;
        }
        if(!"2".equals(caseType)){
            url2 +="/casetype/"+caseType;
        }
        Log.d("zhouzhuo","url==="+url2);
        StringRequest stringRequest = new StringRequest(url2,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("zhouzhuo","Search list==="+response);
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.getInt("code");
                            if(code == 1){
                                MainFragmentItemBeans beans = new MainFragmentItemBeans();
                                JSONObject object1 = object.getJSONObject("data");
                                beans.currentPage = object1.getInt("currentPage");
                                beans.isLastpage = object1.getString("isLastpage");
                                JSONArray array = object1.getJSONArray("content");
                                if(array.length()>=0){
                                    Gson gson = new Gson();
                                    ArrayList<MainFragmentItemBeans.MainFragmentItemBean> list = new ArrayList<>();
                                    for (int i=0;i<array.length();i++){
                                        MainFragmentItemBeans.MainFragmentItemBean bean = gson.fromJson(array.get(i).toString(),MainFragmentItemBeans.MainFragmentItemBean.class);
                                        list.add(bean);
                                    }
                                    beans.list = list;
                                    callBackListener.success(beans);
                                }

                            }else if(code == 4005){
                                String message = object.getString("data");
                                ToastUtils.showShort(activity,message);
                                callBackListener.success(null);
                            }
                        } catch (JSONException e) {
                            Log.d("zhouzhuo","e==="+e.getMessage());

                            e.printStackTrace();
                        }
                        //callBackListener.success(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }
}
