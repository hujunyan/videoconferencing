package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/8/21.
 */

public class RegistrationFormCaseIncommingView extends RelativeLayout implements View.OnClickListener{
    private FrameLayout fl_one,fl_two;
    private ImageView two_select,one_select;
    private int current = 1;

    public RegistrationFormCaseIncommingView(Context context) {
        this(context,null);
    }

    public RegistrationFormCaseIncommingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.register_form_case_comming_view, this,true);
        fl_one = (FrameLayout) view.findViewById(R.id.fl_one);
        fl_one.setOnClickListener(this);
        fl_two = (FrameLayout) view.findViewById(R.id.fl_two);
        fl_two.setOnClickListener(this);
        two_select = (ImageView) view.findViewById(R.id.two_select);
        one_select = (ImageView) view.findViewById(R.id.one_select);
    }

    public int getCurrentType(){
        return  current;
    }
    public void setCurrent(int current){
        this.current = current;
        if(current == 1){
            one_select.setVisibility(VISIBLE);
            two_select.setVisibility(GONE);
        }else {
            one_select.setVisibility(GONE);
            two_select.setVisibility(VISIBLE);
        }

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fl_one:
                if(current !=1){
                    one_select.setVisibility(VISIBLE);
                    two_select.setVisibility(GONE);
                    current = 1;
                }
                break;
            case R.id.fl_two:
                if(current !=2){
                    one_select.setVisibility(GONE);
                    two_select.setVisibility(VISIBLE);
                    current = 2;
                }
                break;
        }

    }
}
