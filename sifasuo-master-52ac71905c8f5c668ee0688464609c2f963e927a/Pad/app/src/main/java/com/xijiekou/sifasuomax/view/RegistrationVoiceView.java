package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.VoiceBaseActivity;

/**
 * Created by zhouzhuo on 2017/8/21.
 * 受理书 纠纷简要 的语音输入
 */

public class RegistrationVoiceView extends RelativeLayout implements View.OnClickListener{
    private RelativeLayout rl_voice;
    private EditText et_detail;
    private VoiceBaseActivity activity;
    private String lastResult="";
    public RegistrationVoiceView(Context context) {
        this(context,null);
    }

    public RegistrationVoiceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (VoiceBaseActivity) context;
        init(context);
    }

    public void setText(String text){
        if(!TextUtils.isEmpty(text)){
            et_detail.setText(text);
            lastResult = text;
        }
    }
    public String getText(){
        return et_detail.getText().toString();
    }

    private void init(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.registration_voice_view,this,true);
        rl_voice = (RelativeLayout) view.findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        et_detail = (EditText) view.findViewById(R.id.et_detail);
        et_detail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult = s.toString();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_voice:
                activity.requestVoicePermission();
                activity.setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail.getText().insert(et_detail.getSelectionStart(),result);
                        }

                    }
                });
                break;
        }


    }
}
