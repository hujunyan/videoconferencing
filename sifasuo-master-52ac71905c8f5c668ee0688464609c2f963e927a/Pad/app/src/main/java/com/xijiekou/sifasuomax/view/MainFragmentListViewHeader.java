package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/7/24.
 */

public class MainFragmentListViewHeader extends RelativeLayout {
    public MainFragmentListViewHeader(Context context) {
        super(context,null);
    }

    public MainFragmentListViewHeader(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        LayoutInflater.from(context).inflate(R.layout.main_file_fragment_header_view,this,true);
    }
}
