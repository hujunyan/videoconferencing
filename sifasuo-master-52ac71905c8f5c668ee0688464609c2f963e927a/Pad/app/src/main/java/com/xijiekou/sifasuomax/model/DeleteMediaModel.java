package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONException;
import org.json.JSONObject;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/9.
 */

public class DeleteMediaModel extends BaseModel{
    public void delete(String file_id, final int type, final int position){
        String url = UrlDomainUtil.urlHeader+"/Staff/delLocal";
        Log.d("zhouzhuo","删除媒体文件:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
            if(!TextUtils.isEmpty(file_id)){
                builder.addParams("file_id", file_id);
            }
        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","删除文件:"+e.getMessage());
                Log.d("zhouzhuo","删除文件:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    //callBackListener.success(response);
                    try {
                        JSONObject object = new JSONObject(response);
                        int code = object.getInt("code");
                        if(code == 1){
                            int[] a = {type,position};
                            callBackListener.success(a);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    Log.d("zhouzhuo","删除文件成功:"+response);
                    //PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    //callBackListener.success(postRequestBean);
                }
            }
        });

    }
}
