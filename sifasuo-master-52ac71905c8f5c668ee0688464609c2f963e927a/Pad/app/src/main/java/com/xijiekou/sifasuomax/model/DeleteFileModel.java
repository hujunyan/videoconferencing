package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class DeleteFileModel extends BaseModel {
    public void delete(String id){
        String url = UrlDomainUtil.urlHeader+"/Staff/delCase";
        Log.d("zhouzhuo","删除文件:"+url);
        Log.d("zhouzhuo","删除文件 id:"+id);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
            builder.addParams("id",id);

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","删除正在调解中卷宗:"+e.getMessage());
                Log.d("zhouzhuo","删除正在调解中卷宗:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    //callBackListener.success(response);
                   callBackListener.success(response);
                    Log.d("zhouzhuo","删除文件成功:"+response);
                    //PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    //callBackListener.success(postRequestBean);
                }
            }
        });

    }
}
