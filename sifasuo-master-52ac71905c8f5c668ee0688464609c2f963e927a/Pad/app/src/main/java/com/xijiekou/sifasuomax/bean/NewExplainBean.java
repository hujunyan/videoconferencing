package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/8/5.
 */

public class NewExplainBean {
    public String id;
    public String case_id;
    public String case_info;
    public String auditor;
    public String filing;
    public String create_time;
}
