package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/8/14.
 * 社区委员会月报表
 */

public class CommunityCommitteesBean {
    public String id;
    public String name = "社区调委会月报表";
    public String fillin;
    public String audit;
    public String create_time;
    public String zongshu;
    public String hunyinjiating;
    public String linliguanxi;
    public String sunhaipeichang;
    public String wuyejiufen;
    public String qita;
    public String haibaoguatu;
    public String banbaozhanban;
    public String zheyecailiao;
    public String gongzuopeixun1;
    public String gongzuopeixun2;
    public String falvjiangzuo1;
    public String falvjiagnzuo2;
    public String beizhu;
    public String year;
    public String month;
    public String company;


}
