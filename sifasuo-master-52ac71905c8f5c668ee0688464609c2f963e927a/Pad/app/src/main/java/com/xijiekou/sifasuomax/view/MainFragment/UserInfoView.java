package com.xijiekou.sifasuomax.view.MainFragment;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.smoothListView.view.AbsHeaderView;
import com.xijiekou.sifasuomax.view.CircleImageView;

import java.lang.ref.WeakReference;

import static com.xijiekou.sifasuomax.utils.UIUtils.getContext;

/**
 * Created by zhouzhuo on 2017/9/11.
 */

public class UserInfoView extends AbsHeaderView<String> implements View.OnClickListener{
    private TextView tv_number;
    private TextView tv_date;
    private CircleImageView iv_photo;
    private MyHandler handler;
    private RelativeLayout rl_logout;

    @Override
    public void onClick(View v) {
        if(logoutListener!=null){
            logoutListener.logout();
        }
    }

    public interface LogoutListener {
        void logout();
    }
    public LogoutListener logoutListener;

    public void setListener(LogoutListener logoutListener){
        this.logoutListener = logoutListener;
    }



    private class MyHandler extends Handler {
        private WeakReference<UserInfoView> handlerWeakReference;
        public MyHandler(UserInfoView info){
            handlerWeakReference = new WeakReference<>(info);
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if(handlerWeakReference.get()!=null){
                switch (msg.what){
                    case 0:
                        long sysTime = System.currentTimeMillis();
                        CharSequence sysTimeStr = DateFormat.format("yyyy-MM-dd HH:mm:ss", sysTime);
                        tv_date.setText(sysTimeStr);
                        break;
                }
            }

        }
    }
    public UserInfoView(Activity activity) {
        super(activity);
        handler = new MyHandler(this);
    }
    private void initTime() {
        new TimeThread().start();
    }


    public void initData(){
        String number = (String) SharePreferenceUtils.get(getContext(),"loginname","");
        if(!TextUtils.isEmpty(number)) {
            tv_number.setText(number);
        }
        updatePhoto();
    }

    public void updatePhoto(){
        String photo =(String) SharePreferenceUtils.get(getContext(),"photo","");
        if(!TextUtils.isEmpty(photo)){
            Glide.with(getContext()).load(photo).into(iv_photo);
        }/*else {
            iv_photo.setImageResource(R.drawable.user_photo_default);
        }*/

    }


    public class TimeThread extends Thread {
        @Override
        public void run () {
            do {
                try {
                    Thread.sleep(1000);
                    Message msg = new Message();
                    msg.what = 0;
                    handler.sendMessage(msg);
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            } while(true);
        }
    }

    @Override
    protected void getView(String string, ListView listView) {
        View view = mInflate.inflate(R.layout.main_fragment_user_info, listView, false);
        tv_number = (TextView) view.findViewById(R.id.tv_number);
        tv_date = (TextView) view.findViewById(R.id.tv_date);
        iv_photo = (CircleImageView) view.findViewById(R.id.iv_photo);
        rl_logout = (RelativeLayout) view.findViewById(R.id.rl_logout);
        rl_logout.setOnClickListener(this);
        initData();
        initTime();
        listView.addHeaderView(view);
    }
}
