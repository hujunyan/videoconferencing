package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/9/12.
 */

public class CreateCheckCreateModel extends BaseModel {
    public void getStatue(String case_id){
        String url = UrlDomainUtil.urlHeader+"/Staff/checkCreate/";
        Log.d("zhouzhuo","获取状态:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
            if(!TextUtils.isEmpty(case_id)){
                builder.addParams("case_id",case_id);

            }
        Log.d("zhouzhuo","case_id:"+case_id);

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","获取状态请求失败:"+e.getMessage());
                Log.d("zhouzhuo","获取状态请求失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                   callBackListener.success(response);
                    Log.d("zhouzhuo","获取状态请求成功:"+response);

                }
            }
        });
    }
}
