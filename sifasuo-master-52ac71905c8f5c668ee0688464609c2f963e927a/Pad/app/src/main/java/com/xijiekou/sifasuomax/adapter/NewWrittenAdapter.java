package com.xijiekou.sifasuomax.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xijiekou.sifasuomax.bean.NewWrittenBean;
import com.xijiekou.sifasuomax.holder.BaseHolder;
import com.xijiekou.sifasuomax.holder.NewWrittenHolder;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/25.
 */

public class NewWrittenAdapter extends BaseAdapter {
    public Activity activity;
    public ArrayList<NewWrittenBean> list;

    public NewWrittenAdapter(Activity activity, ArrayList<NewWrittenBean> list){
        this.activity = activity;
        this.list = list;
    }
    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseHolder<NewWrittenBean> holder;
        if(convertView == null){
            holder = new NewWrittenHolder(activity);
           // convertView.setTag(holder);
        }else {
            holder = (BaseHolder<NewWrittenBean>) convertView.getTag();
        }
        holder.setData(list.get(position));
        return holder.getContentView();
    }


}
