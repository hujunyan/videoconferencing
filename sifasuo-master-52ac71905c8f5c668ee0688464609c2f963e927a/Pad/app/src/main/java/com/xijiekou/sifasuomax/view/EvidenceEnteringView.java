package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.EvidenceEnteringActivity;
import com.xijiekou.sifasuomax.activity.StartRecordVoiceActivityTwo;
import com.xijiekou.sifasuomax.adapter.EvidenceEnteringAdapter;
import com.xijiekou.sifasuomax.bean.MultimediaBean;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.video.RecordActivity;
import com.yuyh.library.imgsel.ImgSelActivity;

import java.util.ArrayList;


/**
 * Created by zhouzhuo on 2017/7/4.
 */
public class EvidenceEnteringView extends LinearLayout implements View.OnClickListener{
    private NoScrollListView lv_list;
    private TextView btn_add;
    private EvidenceEnteringActivity activity;
    private int type;
    private EvidenceEnteringAdapter adapter;
    private static final int REQUEST_CODE_PHOTO = 0;
    private static final int REQUEST_CODE_VOICE = 1;
    private static final int REQUEST_CODE_VIDEO = 2;


    public EvidenceEnteringView(Context context) {
        this(context,null);
    }

    public EvidenceEnteringView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context, attrs);
    }


    private void initView(Context context, AttributeSet attrs) {
        activity = (EvidenceEnteringActivity) context;
        View view  = LayoutInflater.from(context).inflate(R.layout.activity_evidence_entering_child_view,this,true);
        lv_list = (NoScrollListView) view.findViewById(R.id.lv_list);
        lv_list.setDivider(getContext().getResources().getDrawable(R.drawable.media_listview_drawable));
        lv_list.setDividerHeight(15);

        btn_add = (TextView) findViewById(R.id.btn_add);
        btn_add.setOnClickListener(this);
        TextView tv_type = (TextView) view.findViewById(R.id.tv_type);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.EvidenceEnteringView);
        type= typedArray.getInt(R.styleable.EvidenceEnteringView_evidenceType,0);
        typedArray.recycle();
        setTitle(type,tv_type);

    }


    public ListView getListView(){
        return lv_list;
    }

    private void setTitle(int type, TextView tv_type) {
        switch (type){
            case 0:
                tv_type.setText("照片证据");
                break;
            case 1:
                tv_type.setText("录音证据");
                break;
            case 2:
                tv_type.setText("录像证据");
                break;
            default:
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_add:
                switch (type){
                    case 0://跳到选择图片或者拍照的地方
                        picture();
                        break;
                    case 1://跳到录音的地方
                        voice();
                        break;
                    case 2://跳到录像的地方
                        video();
                        break;
                }
                break;
        }
    }


    private void picture(){
        ImgSelActivity.startActivity(activity, MyApp.config, REQUEST_CODE_PHOTO);
    }
    private void voice(){
        Intent intent = new Intent(activity, StartRecordVoiceActivityTwo.class);
        IntentUtils.startActivity(activity,intent,true,REQUEST_CODE_VOICE);
    }

    public void setAdapter(ArrayList<MultimediaBean> listOne,int type){
        if(adapter == null){
            adapter =new EvidenceEnteringAdapter(activity,listOne,type);
            lv_list.setAdapter(adapter);
        }else {
            adapter.notifyDataSetChanged();
        }

    }

    private void video(){
        Intent intent = new Intent(activity,RecordActivity.class);
        IntentUtils.startActivity(activity,intent,true,REQUEST_CODE_VIDEO);
    }


}
