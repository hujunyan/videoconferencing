package com.xijiekou.sifasuomax.view;

import android.app.Activity;
import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.dialog.SelectDisputePop;

/**
 * Created by zhouzhuo on 2017/7/27.
 */

public class CaseComingView extends RelativeLayout implements View.OnClickListener{
    private FrameLayout fl_one,fl_two;
    private ImageView two_select,one_select;

    private TextView et_dispute_type;
    private int current = 1;
    private int currentDispute = 0;

    private String[] types = {
            "婚姻家庭纠纷","邻里纠纷","房屋宅基地纠纷","合同纠纷","生产经营纠纷",
            "损害赔偿纠纷","征地拆迁纠纷","环境污染纠纷","拖欠农民工工资纠纷","其他劳动争议纠纷"
            ,"旅游纠纷","电子商务纠纷","其他消费纠纷","道路交通事故纠纷","物业纠纷","其他纠纷"};

    public CaseComingView(Context context) {
        this(context,null);
    }

    public CaseComingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.case_comming_view, this,true);
        fl_one = (FrameLayout) view.findViewById(R.id.fl_one);
        fl_one.setOnClickListener(this);
        fl_two = (FrameLayout) view.findViewById(R.id.fl_two);
        fl_two.setOnClickListener(this);
        two_select = (ImageView) view.findViewById(R.id.two_select);
        one_select = (ImageView) view.findViewById(R.id.one_select);
        et_dispute_type = (TextView) view.findViewById(R.id.et_dispute_type);
        et_dispute_type.setOnClickListener(this);
    }

    public int getCurrentType(){
        return  current;
    }
    public void setCurrent(int current){
        this.current = current;
        if(current == 1){
            one_select.setVisibility(VISIBLE);
            two_select.setVisibility(GONE);
        }else {
            one_select.setVisibility(GONE);
            two_select.setVisibility(VISIBLE);
        }

    }

    public String getDisputeType(){
        return  currentDispute+"";
    }

    public void setDistype(String type){
        Log.d("zhouzhuo","setDistype=="+type);
        if(!TextUtils.isEmpty(type)){
            currentDispute = Integer.parseInt(type);
            et_dispute_type.setText(types[currentDispute]);
            Log.d("zhouzhuo","==上面="+et_dispute_type.getText());
        }else {
            Log.d("zhouzhuo","==下面="+type);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fl_one:
                if(current !=1){
                    one_select.setVisibility(VISIBLE);
                    two_select.setVisibility(GONE);
                    current = 1;
                }
                break;
            case R.id.fl_two:
                if(current !=2){
                    one_select.setVisibility(GONE);
                    two_select.setVisibility(VISIBLE);
                    current = 2;
                }
                break;
            case R.id.et_dispute_type:
                SelectDisputePop pop = new SelectDisputePop((Activity) getContext(),currentDispute);
                pop.setListener(new SelectDisputePop.SelectItemListener() {
                    @Override
                    public void select(int type) {
                        currentDispute = type;
                        et_dispute_type.setText(types[type]);
                    }
                });
                pop.showPopupWindow(et_dispute_type);
                break;
        }

    }
}
