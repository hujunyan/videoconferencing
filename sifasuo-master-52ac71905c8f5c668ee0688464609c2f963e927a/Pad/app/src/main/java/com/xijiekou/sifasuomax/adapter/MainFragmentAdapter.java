package com.xijiekou.sifasuomax.adapter;

import android.app.Activity;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.holder.BaseHolder;
import com.xijiekou.sifasuomax.holder.MainFragmentHolder;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/7.
 */
public class MainFragmentAdapter extends BaseAdapter{
    private Activity activity;
    private ArrayList<MainFragmentItemBeans.MainFragmentItemBean> list;
   public MainFragmentAdapter(FragmentActivity activity, ArrayList<MainFragmentItemBeans.MainFragmentItemBean> list) {
        this.activity = activity;
        this.list = list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BaseHolder<MainFragmentItemBeans.MainFragmentItemBean> holder;
        if(convertView == null){
            holder = new MainFragmentHolder(activity);
            // convertView.setTag(holder);
        }else {
            holder = (BaseHolder<MainFragmentItemBeans.MainFragmentItemBean>) convertView.getTag();
        }
        holder.setData(list.get(position));
        holder.changeItemBg(position);
        return holder.getContentView();
    }

}
