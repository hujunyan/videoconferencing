package com.xijiekou.sifasuomax.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;


/**
 * Created by zhouzhuo on 16/9/7.
 */

public class CommonDialog extends Dialog implements View.OnClickListener{

    private TextView tv_confirm ,tv_cancel;

    private int type;
    private FrameLayout fl_one,fl_two,fl_three;
    private ImageView one_select,two_select,three_select;

    public CommonDialog(Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.appexitdailog);
        tv_confirm = (TextView) findViewById(R.id.tv_confirm);
        tv_cancel = (TextView) findViewById(R.id.tv_cancel);
        fl_one = (FrameLayout) findViewById(R.id.fl_one);
        fl_one.setOnClickListener(this);
        one_select = (ImageView) findViewById(R.id.one_select);
        fl_two = (FrameLayout) findViewById(R.id.fl_two);
        fl_two.setOnClickListener(this);
        two_select = (ImageView) findViewById(R.id.two_select);
        fl_three = (FrameLayout) findViewById(R.id.fl_three);
        fl_three.setOnClickListener(this);
        three_select = (ImageView) findViewById(R.id.three_select);

        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener!=null){
                    listener.click(type);
                }
                dismiss();
            }
        });
    }
    private SelectTypeListener listener;
    public void setListener(SelectTypeListener listener){
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fl_one:
                if(type!=0){
                    switch (type){
                        case 1:
                            two_select.setVisibility(View.INVISIBLE);
                            break;
                        case 2:
                            three_select.setVisibility(View.INVISIBLE);
                            break;
                    }
                    one_select.setVisibility(View.VISIBLE);
                    type = 0;
                }
                break;
            case R.id.fl_two:
                if(type!=1){
                    switch (type){
                        case 0:
                            one_select.setVisibility(View.INVISIBLE);
                            break;
                        case 2:
                            three_select.setVisibility(View.INVISIBLE);
                            break;
                    }
                    two_select.setVisibility(View.VISIBLE);
                    type = 1;
                }

                break;
            case R.id.fl_three:
                if(type!=2){
                    switch (type){
                        case 0:
                            one_select.setVisibility(View.INVISIBLE);
                            break;
                        case 1:
                            two_select.setVisibility(View.INVISIBLE);
                            break;
                    }
                    three_select.setVisibility(View.VISIBLE);
                    type = 2;
                }
                break;
        }

    }

    public interface SelectTypeListener{
        void click(int type);
    }

}
