package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.bean.ProjectFinalReportBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.NewProjectFinalReportModel;
import com.xijiekou.sifasuomax.model.PrintModel2;
import com.xijiekou.sifasuomax.model.SavePart10;
import com.xijiekou.sifasuomax.model.SearchPart10;
import com.xijiekou.sifasuomax.utils.BeanToJsonUtils;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.ProjectFinalReportView;

/**
 * Created by zhouzhuo on 2017/9/6.
 * 结案报告
 */

public class ProjectFinalReportActivity extends VoiceBaseActivity {
    private TextView tv_evidence_entering;
    private BottomConfirmView bottom_confirm_view;
    private ProjectFinalReportView rl_mediation_record;
    private String caseId;
    private String search;
    private MyHandler handler;
    private String audit;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    ProjectFinalReportBean bean  = (ProjectFinalReportBean) msg.obj;
                    rl_mediation_record.setData(bean);
                    break;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_final_report);
        initView();
        initData();
    }

    private void initData() {
        SearchPart10 part = new SearchPart10();
        part.setCallBackListener(new BaseModel.CallBackListener<ProjectFinalReportBean>() {
            @Override
            public void success(ProjectFinalReportBean bean) {
                if(bean!=null){
                    search = "search";
                    Message message = Message.obtain();
                    message.what = 0;
                    message.obj = bean;
                    handler.sendMessage(message);
                }else {
                    bottom_confirm_view.hidePrint();
                }
            }

            @Override
            public void failed() {

            }
        });
        part.searchPart10(caseId);
    }

    private void initView() {
        handler = new MyHandler();
        caseId = getIntent().getStringExtra("caseId");
        search = getIntent().getStringExtra("search");
        audit = getIntent().getStringExtra("audit");
        rl_mediation_record = (ProjectFinalReportView) findViewById(R.id.rl_mediation_record);
        tv_evidence_entering = (TextView) findViewById(R.id.tv_evidence_entering);
        tv_evidence_entering.setOnClickListener(this);
        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setSaveListener(new BottomConfirmView.SaveListener() {
            @Override
            public void save() {
                /*ProjectFinalReportBean bean = rl_mediation_record.getBean();
                bean.case_id = caseId;
                bean.isAudit = "1";
                saveAndCreate(bean);*/
            }
        });
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {
                PrintModel2 model = new PrintModel2();
                ProjectFinalReportBean bean = rl_mediation_record.getBean();
                String content = BeanToJsonUtils.part8(bean);
                Log.d("zhouzhuo","content:"+content);
                model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)&&message.equals("success")){
                            ToastUtils.showShort(ProjectFinalReportActivity.this,"打印成功,请勿重复点击");
                        }else {
                            ToastUtils.showShort(ProjectFinalReportActivity.this,"打印失败");
                        }
                    }

                    @Override
                    public void failed() {
                        ToastUtils.showShort(ProjectFinalReportActivity.this,"打印失败,请检查打印服务是否开启");
                    }
                });
                String ip = (String) SharePreferenceUtils.get(ProjectFinalReportActivity.this,"printIp","");
                if(TextUtils.isEmpty(ip)||ip.equals("1")){
                    ToastUtils.showShort(ProjectFinalReportActivity.this,"暂不支持打印");
                }else {
                    model.print(content,"printB10",ip);
                }
            }
        });

    }

    private void  saveAndCreate(ProjectFinalReportBean bean){
        NewProjectFinalReportModel model = new NewProjectFinalReportModel();
        model.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
            @Override
            public void success(PostRequestBean bean) {
                if(bean.code==1){
                    Intent intent = getIntent();
                    setResult(0,intent);
                    finish();
                }
            }

            @Override
            public void failed() {

            }
        });
        model.create(bean);
    }

    private void next() {
        if(!TextUtils.isEmpty(audit)&&audit.equals("1")){
            ToastUtils.showShort(ProjectFinalReportActivity.this,"审核已经通过，不能修改");
            return;
        }
        ProjectFinalReportBean bean = rl_mediation_record.getBean();
        bean.case_id = caseId;
        if(TextUtils.isEmpty(search)){
            saveAndCreate(bean);
        }else {
                SavePart10 savePart10 = new SavePart10();
                savePart10.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                    @Override
                    public void success(PostRequestBean bean) {
                        ToastUtils.showLong(ProjectFinalReportActivity.this,bean.data);
                        if(bean.code==1){
                            Intent intent = getIntent();
                            setResult(1,intent);
                            finish();
                        }
                    }
                    @Override
                    public void failed() {

                    }
                });
                savePart10.save(bean);
            }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.tv_evidence_entering:
                Intent intent = new Intent(this,EvidenceEnteringActivity.class);
                intent.putExtra("id",caseId);
                IntentUtils.startActivity(ProjectFinalReportActivity.this,intent);
                break;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(handler!=null){
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }
}
