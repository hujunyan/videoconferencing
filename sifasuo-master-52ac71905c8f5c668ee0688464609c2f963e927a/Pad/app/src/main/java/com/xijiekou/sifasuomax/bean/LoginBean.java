package com.xijiekou.sifasuomax.bean;

import java.io.Serializable;

/**
 * Created by zhouzhuo on 2017/7/21.
 */

public class LoginBean {
    public String Msg;
    public int code;
    public LoginContentBean bean;

    public static class LoginContentBean implements Serializable {
        public String id;
        public String name;
        public String idcard;
        public String loginname;
        public String password;
        public String company;
        public String role;
        public String photo;
        public String isdelete;
        public String errorMessage;
        public String printIp;
    }
}
