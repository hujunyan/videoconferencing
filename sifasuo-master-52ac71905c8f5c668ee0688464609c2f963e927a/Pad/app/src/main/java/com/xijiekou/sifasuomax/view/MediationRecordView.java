package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.VoiceBaseActivity;
import com.xijiekou.sifasuomax.bean.MediationRecordBean;

/**
 * Created by zhouzhuo on 2017/7/6.
 * 人民调解记录
 */
public class MediationRecordView extends RelativeLayout implements View.OnClickListener{
    private RelativeLayout rl_voice;
    private EditText et_detail;
    private VoiceBaseActivity activity;
    private String lastResult="";
    private YearMonthDayMorningHourView rl_date;//时间
    private EditText et_place;//地点
    private EditText et_litigant;//当事人
    private EditText et_participant;//参加人
    private RecordResultView rl_result;//调解结果
    private SignNameView tv_person_one;
    private SignNameView tv_person_two;
    private SignNameView tv_person_three;
    private YearMonthDayView record_time;

    public MediationRecordView(Context context) {
        this(context,null);
    }

    public MediationRecordView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.activity = (VoiceBaseActivity) context;
        initView(context);
    }

    private void initView(Context context) {
        View view  = LayoutInflater.from(context).inflate(R.layout.activity_mediation_record_view,this,true);
        rl_voice = (RelativeLayout) view.findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        et_detail = (EditText) view.findViewById(R.id.et_detail);
        et_detail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult = s.toString();
            }
        });
        rl_date = (YearMonthDayMorningHourView) findViewById(R.id.rl_date);

        et_place = (EditText) view.findViewById(R.id.et_place);
        et_participant = (EditText) view.findViewById(R.id.et_participant);
        rl_result = (RecordResultView) view.findViewById(R.id.rl_result);

        et_litigant = (EditText) view.findViewById(R.id.et_litigant);
        et_participant = (EditText) view.findViewById(R.id.et_participant);


        tv_person_one = (SignNameView) view.findViewById(R.id.tv_person_one);
        tv_person_two = (SignNameView) view.findViewById(R.id.tv_person_two);
        tv_person_three = (SignNameView) view.findViewById(R.id.tv_person_three);

        record_time = (YearMonthDayView) view.findViewById(R.id.record_time);
    }

    public String getResult(){
        return rl_result.getType()+"";
    }

    public void setTitleOne(String title){
        if(!TextUtils.isEmpty(title)){
            tv_person_one.setSignTitle(title);
        }
    }
    public void setTitleTwo(String title){
        if(!TextUtils.isEmpty(title)){
            tv_person_two.setSignTitle(title);
        }
    }

    public void setTitleThree(String title){
        if(!TextUtils.isEmpty(title)){
            tv_person_three.setSignTitle(title);
        }
    }


    public YearMonthDayMorningHourView getYMDMV(){
        return  rl_date;
    }

    public String getFormDate(){
        return record_time.getDate();
    }

    public String getPlace(){
        return  et_place.getText().toString().trim();
    }

    public String getParticipant(){
        return et_participant.getText().toString().trim();
    }

    public String getVoiceResult(){
        return lastResult;
    }

    public String getLitigant(){
        return et_litigant.getText().toString();
    }

    public String getPathOne(){
        return tv_person_one.getFilePath();
    }

    public String getPathTwo(){
        return  tv_person_two.getFilePath();
    }

    public String getPathThree(){
        return  tv_person_three.getFilePath();
    }

    public String getCurrentTime(){
        return record_time.getDate();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.rl_voice:
                activity.requestVoicePermission();
                activity.setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail.getText().insert(et_detail.getSelectionStart(),result);
                        }
                    }
                });
                break;
        }


    }

    public void setData(MediationRecordBean bean) {
        tv_person_one.setImage(bean.litigant_sign);
        tv_person_two.setImage(bean.attend_sign);
        tv_person_three.setImage(bean.record_sign);
        rl_date.setData(bean.date);
        record_time.setData(bean.formdate);
        setTextNotNull(bean.location,et_place);
        setTextNotNull(bean.litigant,et_litigant);
        setTextNotNull(bean.attend,et_participant);
        if(!TextUtils.isEmpty(bean.record)){
            et_detail.setText(bean.record);
            lastResult=bean.record;
        }
        if(!TextUtils.isEmpty(bean.result)){
            rl_result.setResult(bean.result);
        }
        tv_person_one.setImage(bean.litigant_sign);
        tv_person_two.setImage(bean.attend_sign);
        tv_person_three.setImage(bean.record_sign);

    }

    public void setTextNotNull(String content, EditText editText){
        if(!TextUtils.isEmpty(content)){
            editText.setText(content);
        }
    }
}
