package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/8/18.
 */

public class CreateOralCaseIdModel extends BaseModel {
    public void getCaseId(String userId, String company){
        String url = UrlDomainUtil.urlHeader + "/Staff/createOralCase/id/"+userId+"/company/"+company;
        Log.d("zhouzhuo", "url===" + url);
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response == null){
                            return;
                        }
                        try {
                            JSONObject object = new JSONObject(response);
                            int code = object.getInt("code");
                            if(code == 1){
                                callBackListener.success(object.getString("data"));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);

    }
}
