package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.xijiekou.sifasuomax.bean.RegistrationFormBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/4.
 */

public class RegistrationFormModel extends BaseModel{
    public void registration(RegistrationFormBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/createCasePart2";
        Log.d("zhouzhuo","受理书:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(bean!=null){
            if(!TextUtils.isEmpty(bean.case_id)){
                builder.addParams("case_id", bean.case_id);
            }
            Log.d("zhouzhuo","受理书 caseId:"+bean.case_id);
            if(!TextUtils.isEmpty(bean.date)){
                builder.addParams("date", bean.date);
            }
            if(!TextUtils.isEmpty(bean.time_range)){
                builder.addParams("time_range",bean.time_range);
            }
            if(!TextUtils.isEmpty(bean.time)){
                builder.addParams("time",bean.time);
            }
            if(!TextUtils.isEmpty(bean.litigant)){
                builder.addParams("litigant",bean.litigant);
            }
            if(!TextUtils.isEmpty(bean.issuetype)){
                builder.addParams("issuetype",bean.issuetype);
            }
            if(!TextUtils.isEmpty(bean.formdate)){
                builder.addParams("formdate",bean.formdate);
            }


            if(!TextUtils.isEmpty(bean.source)){
                builder.addParams("source", bean.source);
            }
            if(!TextUtils.isEmpty(bean.issueinfo)){
                builder.addParams("issueinfo", bean.issueinfo);
            }
            if(!TextUtils.isEmpty(bean.litiganturl)){
                builder.addParams("litigantUrl",bean.litiganturl);
            }
            Log.d("zhouzhuo","=litigantUrl=="+bean.litiganturl);
            if(!TextUtils.isEmpty(bean.checkinurl)){
                builder.addParams("checkinUrl",bean.checkinurl);
            }
            Log.d("zhouzhuo","=checkinUrl=="+bean.checkinurl);
            if(!TextUtils.isEmpty(bean.committee)){
                builder.addParams("committee",bean.committee);
            }
            if(!TextUtils.isEmpty(bean.create_time)){
                builder.addParams("create_time",bean.create_time);
            }
            if(!TextUtils.isEmpty(bean.isAudit)){
                builder.addParams("isAudit",bean.isAudit);
            }

        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","受理书请求失败:"+e.getMessage());
                Log.d("zhouzhuo","受理书请求失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    callBackListener.success(response);
                    Log.d("zhouzhuo","请求成功:"+response);
                  /*  PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);*/
                }
            }
        });
    }
}
