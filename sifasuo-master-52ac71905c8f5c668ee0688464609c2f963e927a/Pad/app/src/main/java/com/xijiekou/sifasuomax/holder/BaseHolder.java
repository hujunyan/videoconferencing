package com.xijiekou.sifasuomax.holder;

import android.app.Activity;
import android.view.View;

/**
 * Created by zhouzhuo on 2017/7/26.
 */

public abstract class BaseHolder<T> {
    protected View contentView;
    public Activity activity;

    public BaseHolder(Activity activity) {
        this.activity = activity;
        contentView = initView();
        contentView.setTag(this);
    }

    protected abstract View initView();

    protected T info;

    public void setData(T info) {
        this.info = info;
        refreshView();
    }

    protected abstract void refreshView();

    public T getInfo() {
        return info;
    }

    public View getContentView() {
        return contentView;
    }

    public void recycle() {

    }

    public  void changeItemBg(int position){

    }
}