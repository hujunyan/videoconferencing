package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.EvidenceMateriaBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.EvidenceMaterialModel;
import com.xijiekou.sifasuomax.model.PrintModel2;
import com.xijiekou.sifasuomax.model.SavePart4;
import com.xijiekou.sifasuomax.model.SearchPart4;
import com.xijiekou.sifasuomax.utils.BeanToJsonUtils;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.YearMonthDayView;

/**
 * 新建证据材料
 * 3-6-3
 */

public class EvidenceMaterialActivity extends BaseActivity {
    private EditText et_one;//立卷人
    private EditText et_two;//审核人
    private YearMonthDayView rl_date;//
    private TextView tv_evidence_entering;
    private TextView tv_file_number;
    private String caseId;
    private String search;
    private String fileName;
    private String filenumber;
    private BottomConfirmView bottom_confirm_view;
    private String audit;


    private MyHandler handler;


    private class  MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    EvidenceMateriaBean bean = (EvidenceMateriaBean) msg.obj;
                    setTextNotNull(bean.litigant,et_one);
                    setTextNotNull(bean.auditor,et_two);
                    rl_date.setData(bean.date);
                    break;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_evidence_material);
        initView();
        initData();
    }

    private void initView() {
        handler = new MyHandler();
        caseId = getIntent().getStringExtra("caseId");
        search = getIntent().getStringExtra("search");
        et_one = (EditText) findViewById(R.id.et_one);
        et_two = (EditText) findViewById(R.id.et_two);
        rl_date = (YearMonthDayView) findViewById(R.id.rl_date);
        tv_evidence_entering = (TextView) findViewById(R.id.tv_evidence_entering);
        tv_evidence_entering.setOnClickListener(this);
        tv_file_number = (TextView) findViewById(R.id.tv_file_number);
        fileName = getIntent().getStringExtra("filename");
        filenumber = getIntent().getStringExtra("filenumber");
        audit = getIntent().getStringExtra("audit");

        if(!TextUtils.isEmpty(filenumber)){
            tv_file_number.setText(filenumber);
        }
        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setSaveListener(new BottomConfirmView.SaveListener() {
            @Override
            public void save() {
               /* EvidenceMateriaBean bean = getBean();
                bean.isAudit ="1";
                saveAndCreate(bean);*/
            }
        });
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {
                PrintModel2 model = new PrintModel2();
                EvidenceMateriaBean bean = getBean();
                String content = BeanToJsonUtils.part5(bean);
                Log.d("zhouzhuo","content:"+content);
                model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)&&message.equals("success")){
                            ToastUtils.showShort(EvidenceMaterialActivity.this,"打印成功,请勿重复点击");
                        }else {
                            ToastUtils.showShort(EvidenceMaterialActivity.this,"打印失败");
                        }
                    }

                    @Override
                    public void failed() {
                        ToastUtils.showShort(EvidenceMaterialActivity.this,"打印失败,请检查打印服务是否开启");
                    }
                });
                String ip = (String) SharePreferenceUtils.get(EvidenceMaterialActivity.this,"printIp","");
                if(TextUtils.isEmpty(ip)||ip.equals("1")){
                    ToastUtils.showShort(EvidenceMaterialActivity.this,"暂不支持打印");
                }else {
                    model.print(content,"printB8",ip);
                }
            }
        });


    }

    private EvidenceMateriaBean getBean(){
        EvidenceMateriaBean bean = new EvidenceMateriaBean();
        bean.case_id = caseId;
        bean.litigant  = et_one.getText().toString().trim();
        bean.auditor = et_two.getText().toString().trim();
        bean.date = rl_date.getDate();
        return bean;
    }

    private void  saveAndCreate(EvidenceMateriaBean bean){
        EvidenceMaterialModel materialModel = new EvidenceMaterialModel();
        materialModel.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                Intent intent = getIntent();
                setResult(0,intent);
                finish();
            }

            @Override
            public void failed() {

            }
        });
        materialModel.registration(bean);

    }


    private void next() {
        if(!TextUtils.isEmpty(audit)&&audit.equals("1")){
            ToastUtils.showShort(EvidenceMaterialActivity.this,"审核已经通过，不能修改");
            return;
        }
        EvidenceMateriaBean bean = getBean();
        if(TextUtils.isEmpty(search)){
           saveAndCreate(bean);
        }else {
                SavePart4 savePart4 = new SavePart4();
                savePart4.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                    @Override
                    public void success(PostRequestBean bean) {
                        ToastUtils.showLong(EvidenceMaterialActivity.this,bean.data);
                        if(bean.code==1){
                            Intent intent = getIntent();
                            setResult(1,intent);
                            finish();
                        }
                    }

                    @Override
                    public void failed() {

                    }
                });
                savePart4.registration(bean);
            }
    }


    private void initData(){
        SearchPart4 part = new SearchPart4();
        part.setCallBackListener(new BaseModel.CallBackListener<EvidenceMateriaBean>() {
            @Override
            public void success(EvidenceMateriaBean message) {
                if(message!=null){
                    Message message1 = Message.obtain();
                    message1.what = 0;
                    message1.obj = message;
                    handler.sendMessage(message1);
                    search = "search";
                }else {
                    bottom_confirm_view.hidePrint();
                }
            }

            @Override
            public void failed() {

            }
        });
        part.searchPart4(caseId);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.tv_evidence_entering:
                Intent intent = new Intent(this,EvidenceEnteringActivity.class);
                intent.putExtra("id",caseId);
                IntentUtils.startActivity(EvidenceMaterialActivity.this,intent);
                break;


        }
    }
}
