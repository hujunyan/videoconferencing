package com.xijiekou.sifasuomax.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.activity.FileMenuActivity;
import com.xijiekou.sifasuomax.activity.NewOralActivity;
import com.xijiekou.sifasuomax.utils.IntentUtils;

/**
 * Created by zhouzhuo on 2017/7/4.
 * 新建卷宗
 */
public class NewFileFragment extends BaseFragment{
    private TextView tv_new_oral,tv_new_agreement;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_new_file, null);
        initView(view);
        return view;
    }

    private void initView(View view) {
        tv_new_oral = (TextView) view.findViewById(R.id.tv_new_oral);
        tv_new_oral.setOnClickListener(this);
        tv_new_agreement = (TextView) view.findViewById(R.id.tv_new_agreement);
        tv_new_agreement.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        super.onClick(view);
        switch (view.getId()){
            case R.id.tv_new_oral:
                Intent intent = new Intent(getContext(),NewOralActivity.class);
                IntentUtils.startFragmentToActivity(this,intent,false,0);
                break;
            case R.id.tv_new_agreement:
                intent = new Intent(getContext(), FileMenuActivity.class);
                IntentUtils.startFragmentToActivity(this,intent,false,0);
                break;
        }
    }
}
