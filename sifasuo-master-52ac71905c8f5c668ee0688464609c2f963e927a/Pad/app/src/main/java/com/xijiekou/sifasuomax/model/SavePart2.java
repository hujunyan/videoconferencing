package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.bean.RegistrationFormBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SavePart2 extends BaseModel{
    public void save(RegistrationFormBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/saveCasePart2";
        Log.d("zhouzhuo","受理书:"+url);
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(bean!=null){
            if(!TextUtils.isEmpty(bean.case_id)){
                builder.addParams("case_id", bean.case_id);
                Log.d("zhouzhuo","case_i===="+bean.case_id);
            }

            if(!TextUtils.isEmpty(bean.date)){
                builder.addParams("date", bean.date);
            }
            Log.d("zhouzhuo","===date=="+bean.date);
           /* if(!TextUtils.isEmpty(bean.time_range)){
                builder.addParams("time_range",bean.time_range);
            }
            if(!TextUtils.isEmpty(bean.time)){
                builder.addParams("time",bean.time);
            }*/
            if(!TextUtils.isEmpty(bean.litigant)){
                builder.addParams("litigant",bean.litigant);
            }
            if(!TextUtils.isEmpty(bean.issuetype)){
                builder.addParams("issuetype",bean.issuetype);
            }
            if(!TextUtils.isEmpty(bean.formdate)){
                builder.addParams("formdate",bean.formdate);
            }


            if(!TextUtils.isEmpty(bean.source)){
                builder.addParams("source", bean.source);
            }
            if(!TextUtils.isEmpty(bean.issueinfo)){
                builder.addParams("issueinfo", bean.issueinfo);
            }
            if(!TextUtils.isEmpty(bean.litiganturl)){
                builder.addParams("litigantUrl",bean.litiganturl);
            }
            if(!TextUtils.isEmpty(bean.checkinurl)){
                builder.addParams("checkinUrl",bean.checkinurl);
            }
            if(!TextUtils.isEmpty(bean.committee)){
                builder.addParams("committee",bean.committee);
            }

        }

        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("zhouzhuo","受理书请求失败:"+e.getMessage());
                Log.d("zhouzhuo","受理书请求失败:"+e.toString());
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    //callBackListener.success(response);
                    Log.d("zhouzhuo","请求成功:"+response);
                    PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);
                }
            }
        });
    }
}
