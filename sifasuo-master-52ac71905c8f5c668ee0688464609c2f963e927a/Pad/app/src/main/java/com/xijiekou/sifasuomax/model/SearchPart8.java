package com.xijiekou.sifasuomax.model;

import android.util.Log;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.xijiekou.sifasuomax.MyApp;
import com.xijiekou.sifasuomax.bean.EvidenceMateriaBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by zhouzhuo on 2017/8/14.
 */

public class SearchPart8 extends BaseModel{
    public void searchPart8(String id){
        String url = UrlDomainUtil.urlHeader + "/Staff/getCasePart8/case_id/"+id;
        Log.d("zhouzhuo","司法确认页"+url);
        StringRequest stringRequest = new StringRequest(url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        if(response !=null){
                            Log.d("zhouzhuo","查询请求司法确认页"+response);
                            try {
                                JSONObject object = new JSONObject(response);
                                int code = object.getInt("code");
                                if(code == 1){
                                    Gson gson = new Gson();
                                    EvidenceMateriaBean bean = gson.fromJson(object.getJSONObject("data").toString(),EvidenceMateriaBean.class);
                                    callBackListener.success(bean);
                                }else if(code == 4012){
                                    callBackListener.success(null);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("zhouzhuo", error.getMessage(), error);
            }
        });
        MyApp.getHttpQueue().add(stringRequest);
    }
}
