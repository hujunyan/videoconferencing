package com.xijiekou.sifasuomax.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;

import com.xijiekou.sifasuomax.R;

/**
 * Created by zhouzhuo on 2017/9/13.
 */

public class MainListTitleView extends LinearLayout {
    public MainListTitleView(Context context) {
        super(context);
        init();
    }

    private void init() {
        LayoutInflater.from(getContext()).inflate(R.layout.main_fragment_title_view,this,true);
    }

    public MainListTitleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }
}
