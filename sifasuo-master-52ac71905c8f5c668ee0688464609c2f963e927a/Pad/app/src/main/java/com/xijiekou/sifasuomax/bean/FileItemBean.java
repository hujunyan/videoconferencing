package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/7/7.
 */
public class FileItemBean {
    public String fileNumber;//卷号
    public String fileName;//卷名
    public String units;//所属单位
    public String mediators;//人民调解员
    public String mediationTime;//调解时间
}
