package com.xijiekou.sifasuomax.bean;

/**
 * Created by zhouzhuo on 2017/7/5.
 * 司法确认
 */
public class JudicialConfirmBean {
    public String case_id;//案件id
    public String auditor;//立卷人
    public String litigant;//审核人
    public String create_time;//立卷时间
}
