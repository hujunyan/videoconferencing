package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.widget.FrameLayout;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.fragment.RecordVoiceFragment;

/**
 * Created by zhouzhuo on 2017/9/18.
 */

public class StartRecordVoiceActivityThree extends BaseActivity{
    private FrameLayout mainFrameLayout;
    private String url;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_record_voice3);
        url = getIntent().getStringExtra("url");
        RecordVoiceFragment newFragment = new RecordVoiceFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.main_frameLayout, newFragment);
       // transaction.addToBackStack(null);
        transaction.commit();
    }

    public String getPath(){
        return url;
    }

    public void back(String path){
        Intent intent = getIntent();
        intent.putExtra("path",path);
        setResult(0, intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
