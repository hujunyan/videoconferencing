package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.xijiekou.sifasuomax.MainActivity;
import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.WelcomeModel;
import com.xijiekou.sifasuomax.utils.NewWorkUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by zhouzhuo on 2017/6/29.
 * 3-7
 */

public class WelcomeActivity extends BaseActivity{
    private ImageView rl_rootview;
    private static Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setTheme(R.style.AppTheme_Launcher);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);// 设置全屏
        // 选择支持半透明模式，在有surfaceview的activity中使用
        getWindow().setFormat(PixelFormat.TRANSLUCENT);
        setContentView(R.layout.activity_welcome);
        rl_rootview = (ImageView) findViewById(R.id.rl_rootview);
        initData();
    }

    private void initData() {
        if(!NewWorkUtils.isConnected(WelcomeActivity.this)){
            ToastUtils.showShort(WelcomeActivity.this,"当前网络不好");
            setLocalImage();
            return;
        }
        WelcomeModel model = new WelcomeModel();
        model.setCallBackListener(new BaseModel.CallBackListener<String>() {
            @Override
            public void success(String message) {
                if(!TextUtils.isEmpty(message)){
                    try {
                        JSONObject object = new JSONObject(message);
                        int code = object.getInt("code");
                        if(code == 1){
                            String url = object.getJSONObject("data").getString("img_url");
                            Log.d("zhouzhuo","pad=="+url);
                            if(rl_rootview!=null){
                                Glide.with(WelcomeActivity.this).load(url).into(rl_rootview);
                            }
                            init();
                        }else {
                            setLocalImage();
                        }
                    } catch (JSONException e) {
                        setLocalImage();
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void failed() {
                setLocalImage();
            }
        });
        model.getImage();
    }
    //设置本地图片
    private void setLocalImage(){
        if(rl_rootview!=null){
            rl_rootview.setImageResource(R.drawable.welcome);
        }
        init();
    }

    private void init() {
            handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Log.d("zhouzhuo","init()after"+System.currentTimeMillis());
                    if(TextUtils.isEmpty((String) SharePreferenceUtils.get(WelcomeActivity.this,"login",""))){
                        Intent intent = new Intent(WelcomeActivity.this,LoginActivity.class);
                        startActivity(intent);
                        finish();
                    }else{
                        Intent  intent2 =new Intent(WelcomeActivity.this,MainActivity.class);
                        startActivity(intent2);
                        finish();
                    }
                }
            },3000);
        //项目加载完后再去初始化三方组件
        Log.d("zhouzhuo","init pre=="+System.currentTimeMillis());
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(rl_rootview!=null){
            rl_rootview =null;
        }
        if(handler!=null){
            handler.removeCallbacksAndMessages(null);
            handler = null;
        }
    }
}
