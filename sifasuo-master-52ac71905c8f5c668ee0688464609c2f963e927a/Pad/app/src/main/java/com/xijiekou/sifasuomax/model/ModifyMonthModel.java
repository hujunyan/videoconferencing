package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.MediationMonthFormBean;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/9/4.
 */

public class ModifyMonthModel extends BaseModel{
    public void savePeople(MediationMonthFormBean bean){
        String url = UrlDomainUtil.urlHeader+"/Staff/savePeopleReport/";
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        builder.addParams("id",bean.id);
        if(!TextUtils.isEmpty(bean.id_0)){
            builder.addParams("c1", bean.id_0);
        }
        if(!TextUtils.isEmpty(bean.id_1)){
            builder.addParams("c2", bean.id_1);
        }

        if(!TextUtils.isEmpty(bean.id_2)){
            builder.addParams("c3", bean.id_2);
        }
        if(!TextUtils.isEmpty(bean.id_3)){
            builder.addParams("c4", bean.id_3);
        }
        if(!TextUtils.isEmpty(bean.id_4)){
            builder.addParams("c5", bean.id_4);
        }

        if(!TextUtils.isEmpty(bean.id_5)){
            builder.addParams("c6", bean.id_5);
        }
        if(!TextUtils.isEmpty(bean.id_6)){
            builder.addParams("c7", bean.id_6);
        }
        if(!TextUtils.isEmpty(bean.id_7)){
            builder.addParams("c8", bean.id_7);
        }

        if(!TextUtils.isEmpty(bean.id_8)){
            builder.addParams("c9", bean.id_8);
        }
        if(!TextUtils.isEmpty(bean.id_9)){
            builder.addParams("c10", bean.id_9);
        }
        if(!TextUtils.isEmpty(bean.id_10)){
            builder.addParams("c11", bean.id_10);
        }

        if(!TextUtils.isEmpty(bean.id_11)){
            builder.addParams("c12", bean.id_11);
        }
        if(!TextUtils.isEmpty(bean.id_12)){
            builder.addParams("c13", bean.id_12);
        }
        if(!TextUtils.isEmpty(bean.id_13)){
            builder.addParams("c14", bean.id_13);
        }

        if(!TextUtils.isEmpty(bean.id_14)){
            builder.addParams("c15", bean.id_14);
        } if(!TextUtils.isEmpty(bean.id_15)){
            builder.addParams("c16", bean.id_15);
        }
        if(!TextUtils.isEmpty(bean.id_16)){
            builder.addParams("c17", bean.id_16);
        }

        if(!TextUtils.isEmpty(bean.id_17)){
            builder.addParams("c18", bean.id_17);
        } if(!TextUtils.isEmpty(bean.id_18)){
            builder.addParams("c19", bean.id_18);
        }
        if(!TextUtils.isEmpty(bean.id_19)){
            builder.addParams("c20", bean.id_19);
        }

        if(!TextUtils.isEmpty(bean.id_20)){
            builder.addParams("c21", bean.id_20);
        }
        if(!TextUtils.isEmpty(bean.id_21)){
            builder.addParams("c22", bean.id_21);
        }
        if(!TextUtils.isEmpty(bean.id_22)){
            builder.addParams("c23", bean.id_22);
        }

        if(!TextUtils.isEmpty(bean.id_23)){
            builder.addParams("c24", bean.id_23);
        }
        if(!TextUtils.isEmpty(bean.id_24)){
            builder.addParams("c25", bean.id_24);
        }
        if(!TextUtils.isEmpty(bean.id_25)){
            builder.addParams("c26", bean.id_25);
        }

        if(!TextUtils.isEmpty(bean.id_26)){
            builder.addParams("c27", bean.id_26);
        }

        if(!TextUtils.isEmpty(bean.id_27)){
            builder.addParams("c28", bean.id_27);
        }
        if(!TextUtils.isEmpty(bean.id_28)){
            builder.addParams("c29", bean.id_28);
        }

        if(!TextUtils.isEmpty(bean.id_29)){
            builder.addParams("c30", bean.id_29);
        }
        if(!TextUtils.isEmpty(bean.id_30)){
            builder.addParams("c31", bean.id_30);
        }
        if(!TextUtils.isEmpty(bean.id_31)){
            builder.addParams("c32", bean.id_31);
        }

        if(!TextUtils.isEmpty(bean.id_32)){
            builder.addParams("c33", bean.id_32);
        }
        if(!TextUtils.isEmpty(bean.id_33)){
            builder.addParams("c34", bean.id_33);
        }
        if(!TextUtils.isEmpty(bean.id_34)){
            builder.addParams("c35", bean.id_34);
        }

        if(!TextUtils.isEmpty(bean.id_35)){
            builder.addParams("c36", bean.id_35);
        }
        if(!TextUtils.isEmpty(bean.id_36)){
            builder.addParams("c37", bean.id_36);
        }
        if(!TextUtils.isEmpty(bean.id_37)){
            builder.addParams("c38", bean.id_37);
        }

        if(!TextUtils.isEmpty(bean.id_38)){
            builder.addParams("c39", bean.id_38);
        }
        if(!TextUtils.isEmpty(bean.id_39)){
            builder.addParams("c40", bean.id_39);
        }
        if(!TextUtils.isEmpty(bean.id_40)){
            builder.addParams("c41", bean.id_40);
        }

        if(!TextUtils.isEmpty(bean.id_41)){
            builder.addParams("c42", bean.id_41);
        }
        if(!TextUtils.isEmpty(bean.id_42)){
            builder.addParams("c43", bean.id_42);
        }
        if(!TextUtils.isEmpty(bean.id_43)){
            builder.addParams("c44", bean.id_43);
        }

        if(!TextUtils.isEmpty(bean.id_44)){
            builder.addParams("c45", bean.id_44);
        }
        if(!TextUtils.isEmpty(bean.id_45)){
            builder.addParams("c46", bean.id_45);
        }
        if(!TextUtils.isEmpty(bean.id_46)){
            builder.addParams("c47", bean.id_46);
        }
        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                    Log.d("zhouzhuo","修改社区请求成功:"+response);
                    PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                    callBackListener.success(postRequestBean);
                }
            }
        });



    }
}
