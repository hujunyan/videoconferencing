package com.xijiekou.sifasuomax.model;

import android.text.TextUtils;
import android.util.Log;

import com.google.gson.Gson;
import com.xijiekou.sifasuomax.bean.NewOralBean;
import com.xijiekou.sifasuomax.bean.NewWrittenBeans;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.builder.PostFormBuilder;
import com.zhy.http.okhttp.callback.StringCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import okhttp3.Call;

/**
 * Created by zhouzhuo on 2017/8/14.
 * 修改第一步
 */

public class SavePart1 extends BaseModel{
    private NewWrittenBeans beans;
    public SavePart1(NewWrittenBeans beans){
        this.beans = beans;
    }

    public void postWritten(){
        final JSONArray array = new JSONArray();
        ArrayList<NewOralBean> list = beans.list;
        if(list!=null && list.size()>0){
            for (int i =0;i<list.size();i++){
                NewOralBean bean = list.get(i);
                JSONObject object = new JSONObject();
                putString(object,"name",bean.name);
                putString(object,"sex",bean.sex);
                putString(object,"type","1");
                putString(object,"nation",bean.nation);
                putString(object,"age",bean.age);
                putString(object,"job",bean.job);
                putString(object,"tel",bean.tel);
                putString(object,"address",bean.address);
                putString(object,"idcard",bean.idcard);
                array.put(object);
            }

        }
        String url = UrlDomainUtil.urlHeader+"/Staff/saveCasePart1";
        // String postJson = "{\"arr\":"+array.toString()+"}";
        JSONObject object3 = null;
        try {
            object3 = new JSONObject();
            object3.putOpt("arr",array);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        PostFormBuilder builder = OkHttpUtils
                .post()
                .url(url);
        if(beans!=null){
            if(!TextUtils.isEmpty(beans.case_id)){
                builder.addParams("case_id", beans.case_id);
            }
            if(!TextUtils.isEmpty(beans.filing)){
                builder.addParams("filing", beans.filing);
            }
            if(!TextUtils.isEmpty(beans.simple)){
                builder.addParams("simple",beans.simple);
            }
            if(!TextUtils.isEmpty(beans.content)){
                builder.addParams("content",beans.content);
            }
            if(!TextUtils.isEmpty(beans.source)){
                builder.addParams("source",beans.source);
            }

            if(!TextUtils.isEmpty(beans.issuetype)){
                builder.addParams("issuetype",beans.issuetype);
            }
            if(!TextUtils.isEmpty(beans.signurl)){
                builder.addParams("signUrl",beans.signurl);
                Log.d("zhouzhuo","==signurl=="+beans.signurl);
            }

            if(!TextUtils.isEmpty(beans.date)){
                builder.addParams("date",beans.date);
            }

        }

        if(array.length()>0){
            builder.addParams("persons",object3.toString());
        }
        builder.build().execute(new StringCallback() {
            @Override
            public void onError(Call call, Exception e, int id) {
                callBackListener.failed();
            }

            @Override
            public void onResponse(String response, int id) {
                if(response !=null){
                   // callBackListener.success(response);
                    Log.d("zhouzhuo","save part 1 ==请求成功:"+response);
                    try {
                        PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);

                        callBackListener.success(postRequestBean);
                    }catch (Exception e){
                        Log.d("zhouzhuo","请求成功:"+response);
                    }

                }
            }
        });

       /* OkHttpUtils
                .post()
                .url(url)
                .addParams("fileName", beans.fileName)
                .addParams("argue", beans.argue)
                .addParams("performance",beans.performance)
                .addParams("signature",beans.signature)
                .addParams("date",beans.date)
               // .addParams("persons",postJson)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Log.d("zhouzhuo","请求失败:"+e.getMessage());
                        Log.d("zhouzhuo","请求失败:"+e.toString());
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        if(response !=null){
                            Log.d("zhouzhuo","请求成功:"+response);
                            PostRequestBean postRequestBean = new Gson().fromJson(response,PostRequestBean.class);
                            callBackListener.success(postRequestBean);
                        }

                    }*/
        //  });
    }

    public void putString(JSONObject object,String key,String value){
        if(!TextUtils.isEmpty(value)){
            try {
                object.put(key,value);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
