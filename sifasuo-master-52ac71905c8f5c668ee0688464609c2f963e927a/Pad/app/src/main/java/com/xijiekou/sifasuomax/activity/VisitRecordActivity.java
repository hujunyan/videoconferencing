package com.xijiekou.sifasuomax.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.PostRequestBean;
import com.xijiekou.sifasuomax.bean.VisitRecordBean;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.PrintModel2;
import com.xijiekou.sifasuomax.model.SavePart7;
import com.xijiekou.sifasuomax.model.SearchPart7;
import com.xijiekou.sifasuomax.model.VisitRecordModel;
import com.xijiekou.sifasuomax.utils.BeanToJsonUtils;
import com.xijiekou.sifasuomax.utils.IntentUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.view.BottomConfirmView;
import com.xijiekou.sifasuomax.view.SignNameView;
import com.xijiekou.sifasuomax.view.YearMonthDayMorningView;
import com.xijiekou.sifasuomax.view.YearMonthDayView;

/**
 * 回访记录
 * 3-9
 */

public class VisitRecordActivity extends VoiceBaseActivity {
    private String caseId;
    private SignNameView tv_person;//回访人签名
    private EditText et_litigant;//当事人名字
    private EditText et_protocol;//协议编号
    private EditText et_reason;//回访事由
    private YearMonthDayMorningView rl_date;//回访时间
    private EditText et_wyh;//委员会
    private YearMonthDayView rl_date2;//当前日期
    private RelativeLayout rl_voice;
    private String lastResult="";
    private EditText et_detail;
    private TextView tv_evidence_entering;
    private MyHandler handler;
    private String search;
    private String fileName;
    private String filenumber;
    private BottomConfirmView bottom_confirm_view;
    private TextView tv_file_number;
    private String audit;

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 0:
                    VisitRecordBean bean = (VisitRecordBean) msg.obj;
                    setTextNotNull(bean.person,et_litigant);
                    setTextNotNull(bean.number,et_protocol);
                    setTextNotNull(bean.visit_reason,et_reason);
                    rl_date.setDate(bean.date);
                    rl_date2.setData(bean.formdate);
                    et_detail.setText(bean.visit_state);
                    lastResult = bean.visit_state;
                    setTextNotNull(bean.committee,et_wyh);
                    tv_person.setImage(bean.visit_url);
                    break;
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_record);
        initView();
        initData();
    }

    private void initData() {
        SearchPart7 part = new SearchPart7();
        part.setCallBackListener(new BaseModel.CallBackListener<VisitRecordBean>() {
            @Override
            public void success(VisitRecordBean message) {
                if(message!=null){
                    Message message1 = Message.obtain();
                    message1.what = 0;
                    message1.obj = message;
                    handler.sendMessage(message1);
                    search = "search";
                }else {
                    bottom_confirm_view.hidePrint();
                }

            }

            @Override
            public void failed() {

            }
        });
        part.searchPart7(caseId);
    }


    private void initView() {
        handler = new MyHandler();
        caseId = getIntent().getStringExtra("caseId");
        search = getIntent().getStringExtra("search");
        audit = getIntent().getStringExtra("audit");
        tv_person = (SignNameView) findViewById(R.id.tv_person);
        tv_person.setSignTitle("回访人(签名)");
        et_litigant = (EditText) findViewById(R.id.et_litigant);
        et_protocol = (EditText) findViewById(R.id.et_protocol);
        et_reason = (EditText) findViewById(R.id.et_reason);
        rl_date = (YearMonthDayMorningView) findViewById(R.id.rl_date);
        rl_date2 = (YearMonthDayView) findViewById(R.id.rl_date2);
        rl_voice = (RelativeLayout) findViewById(R.id.rl_voice);
        rl_voice.setOnClickListener(this);
        et_detail = (EditText) findViewById(R.id.et_detail);
        et_detail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                lastResult = s.toString();

            }
        });
        et_wyh = (EditText) findViewById(R.id.et_wyh);
        tv_evidence_entering = (TextView) findViewById(R.id.tv_evidence_entering);
        tv_evidence_entering.setOnClickListener(this);

        tv_file_number = (TextView) findViewById(R.id.tv_file_number);
        search = getIntent().getStringExtra("search");

        fileName = getIntent().getStringExtra("filename");
        filenumber = getIntent().getStringExtra("filenumber");

        if(!TextUtils.isEmpty(filenumber)){
            tv_file_number.setText(filenumber);
        }
        bottom_confirm_view = (BottomConfirmView) findViewById(R.id.bottom_confirm_view);
        bottom_confirm_view.setSaveListener(new BottomConfirmView.SaveListener() {
            @Override
            public void save() {
                /*Log.d("zhouzhuo","点击另存为===");
                VisitRecordBean bean = getBean();
                bean.isAudit = "1";
                saveAndCreate(bean);*/
            }
        });
        bottom_confirm_view.setConfirmListener(new BottomConfirmView.ConfirmListener() {
            @Override
            public void confirm() {
                next();
            }
        });
        bottom_confirm_view.setPrintListener(new BottomConfirmView.PrintListener() {
            @Override
            public void print() {
                PrintModel2 model = new PrintModel2();
                VisitRecordBean bean = getBean();
                String content = BeanToJsonUtils.part9(bean);
                Log.d("zhouzhuo","content:"+content);
                model.setCallBackListener(new BaseModel.CallBackListener<String>() {
                    @Override
                    public void success(String message) {
                        if(!TextUtils.isEmpty(message)&&message.equals("success")){
                            ToastUtils.showShort(VisitRecordActivity.this,"打印成功,请勿重复点击");
                        }else {
                            ToastUtils.showShort(VisitRecordActivity.this,"打印失败");
                        }
                    }

                    @Override
                    public void failed() {
                        ToastUtils.showShort(VisitRecordActivity.this,"打印失败,请检查打印服务是否开启");
                    }
                });
                String ip = (String) SharePreferenceUtils.get(VisitRecordActivity.this,"printIp","");
                if(TextUtils.isEmpty(ip)||ip.equals("1")){
                    ToastUtils.showShort(VisitRecordActivity.this,"暂不支持打印");
                }else {
                    model.print(content,"printB6",ip);
                }
            }
        });


    }

    private  VisitRecordBean getBean(){
        VisitRecordBean bean = new VisitRecordBean();
        bean.case_id = caseId;
        bean.person = et_litigant.getText().toString().trim();
        bean.number = et_protocol.getText().toString();
        bean.visit_reason = et_reason.getText().toString();
        bean.date = rl_date.getDate();
        bean.formdate = rl_date2.getDate();
        bean.visit_state = lastResult;
        bean.committee = et_wyh.getText().toString().trim();
        bean.visit_url = tv_person.getFilePath();
        bean.datetime = rl_date2.getDate();
        return bean;
    }

    private void  saveAndCreate(VisitRecordBean bean){
        VisitRecordModel model = new VisitRecordModel();
        model.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
            @Override
            public void success(PostRequestBean result) {
                ToastUtils.showShort(VisitRecordActivity.this,result.data);
                if(result.code ==1 ){
                    Intent intent = getIntent();
                    setResult(0,intent);
                    finish();
                }
                //Log.d("zhouzhuo","回访记录的message=="+message);
            }

            @Override
            public void failed() {

            }
        });
        model.registration(bean);
    }

    private void next() {
        if(!TextUtils.isEmpty(audit)&&audit.equals("1")){
            ToastUtils.showShort(VisitRecordActivity.this,"审核已经通过，不能修改");
            return;
        }
        VisitRecordBean bean = getBean();
        if(TextUtils.isEmpty(search)){
            saveAndCreate(bean);
        }else {
                SavePart7 savePart7 = new SavePart7();
                savePart7.setCallBackListener(new BaseModel.CallBackListener<PostRequestBean>() {
                    @Override
                    public void success(PostRequestBean bean) {
                        ToastUtils.showLong(VisitRecordActivity.this,bean.data);
                        if(bean.code==1){
                            Intent intent = getIntent();
                            setResult(1,intent);
                            finish();
                        }
                    }

                    @Override
                    public void failed() {

                    }
                });
                savePart7.save(bean);
            }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()){
            case R.id.rl_voice:
                requestVoicePermission();
                setListener(new VoiceBaseActivity.VoiceResultListener() {
                    @Override
                    public void result(String result) {
                        if(!TextUtils.isEmpty(result)){
                            Log.d("zhouzhuo","返回结果:"+result);
                            et_detail.getText().insert(et_detail.getSelectionStart(),result);
                        }

                    }
                });
                break;
            case R.id.tv_evidence_entering:
                Intent intent = new Intent(this,EvidenceEnteringActivity.class);
                intent.putExtra("id",caseId);
                IntentUtils.startActivity(VisitRecordActivity.this,intent);
                break;
        }
    }
}
