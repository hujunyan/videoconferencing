package com.xijiekou.sifasuomax.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/7/7.
 */
public class MainFragmentItemBeans extends BaseBean{
        public ArrayList<MainFragmentItemBean> list;
        public String isLastpage;
        public int currentPage;

        public class MainFragmentItemBean implements Serializable{
                public String id;
                public String filename;
                public String filenum;
                public String persons;
                public String argue;
                public String performance;
                public String signature;
                public String date;
                public String casetype;
                public String status;
                public String isdelete;
                public String audit;
                public String company;

        }

}
