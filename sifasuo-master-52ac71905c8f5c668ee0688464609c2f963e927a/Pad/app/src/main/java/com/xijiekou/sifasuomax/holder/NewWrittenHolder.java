package com.xijiekou.sifasuomax.holder;

import android.app.Activity;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.bean.NewWrittenBean;
import com.xijiekou.sifasuomax.utils.UIUtils;

/**
 * Created by zhouzhuo on 2017/7/26.
 */

public class NewWrittenHolder extends BaseHolder<NewWrittenBean>{
    TextView tv_person;
    TextView tv_person_select;
    TextView tv_one;
    TextView et_one;
    TextView tv_two;
    TextView et_two;
    TextView tv_three;
    TextView et_three;
    TextView tv_four;
    TextView et_four;
    TextView tv_five;
    TextView et_five;
    TextView tv_six;
    TextView et_six;
    TextView tv_seven;
    TextView et_seven;
    // 0  自然人 Natural   1 法人 Corporation  2非法人组织  Litigant
    private final String[] titleNatural = {"申请人姓名","性别","民族","出生年月日","职业或职务","联系方式","单位或住址"};
    private TextView[] tvNatural= {tv_one,tv_two,tv_three,tv_four,tv_five,tv_six,tv_seven};

    public NewWrittenHolder(Activity activity) {
        super(activity);
    }

    @Override
    protected View initView() {
        View view = UIUtils.inflate(R.layout.activity_new_written_item);
        tv_person = (TextView) view.findViewById(R.id.tv_person);
        tv_person_select = (TextView) view.findViewById(R.id.tv_person_select);
        tv_one = (TextView) view.findViewById(R.id.tv_one);
        et_one = (TextView) view.findViewById(R.id.et_one);
        tv_two = (TextView) view.findViewById(R.id.tv_two);
        et_two = (TextView) view.findViewById(R.id.et_two);
        tv_three = (TextView) view.findViewById(R.id.tv_three);
        et_three = (TextView) view.findViewById(R.id.et_three);
        tv_four = (TextView) view.findViewById(R.id.tv_four);
        et_four = (TextView) view.findViewById(R.id.et_four);
        tv_five = (TextView) view.findViewById(R.id.tv_five);
        et_five= (TextView) view.findViewById(R.id.et_five);
        tv_six= (TextView) view.findViewById(R.id.tv_six);
        et_six = (TextView) view.findViewById(R.id.et_six);
        tv_seven = (TextView) view.findViewById(R.id.tv_seven);
        et_seven = (TextView) view.findViewById(R.id.et_seven);
        return view;
    }

    @Override
    protected void refreshView() {
        NewWrittenBean natural =  info;
        for (int i=0;i<titleNatural.length;i++){
            tvNatural[i].setText(titleNatural[i]);
        }
        et_one.setText(natural.name);
        if(!TextUtils.isEmpty(natural.sex)||natural.sex.equals("1")){
            et_two.setText("男");
        }else {
            et_two.setText("女");
        }
        et_two.setText(natural.sex);
        et_three.setText(natural.nation);
        et_four.setText(natural.age);
        et_five.setText(natural.job);
        et_six.setText(natural.mobile);
        et_seven.setText(natural.address);
    }


}
