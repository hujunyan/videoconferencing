package com.xijiekou.sifasuomax.bean;

import java.util.ArrayList;

/**
 * Created by zhouzhuo on 2017/8/21.
 */

public class MessageBean {
    public ArrayList<MessageChildBean> list;
    public String Msg;
    public int code;
    public static class MessageChildBean{
        public String id;
        public String message;
        public String litigant;
        public String state;
        public String subject;
    }
}
