package com.xijiekou.sifasuomax.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.RelativeLayout;

import com.xijiekou.sifasuomax.R;
import com.xijiekou.sifasuomax.adapter.MainFragmentAdapterTwo;
import com.xijiekou.sifasuomax.bean.MainFragmentItemBeans;
import com.xijiekou.sifasuomax.model.BaseModel;
import com.xijiekou.sifasuomax.model.MainFragmentCaseListModel;
import com.xijiekou.sifasuomax.utils.DensityUtils;
import com.xijiekou.sifasuomax.utils.SharePreferenceUtils;
import com.xijiekou.sifasuomax.utils.ToastUtils;
import com.xijiekou.sifasuomax.utils.UrlDomainUtil;
import com.xijiekou.sifasuomax.utils.smoothListView.SmoothListView;
import com.xijiekou.sifasuomax.view.MainFragment.FiterView;
import com.xijiekou.sifasuomax.view.MainFragment.FiterViewTwo;
import com.xijiekou.sifasuomax.view.MainFragment.MainFragmentViewOne;
import com.xijiekou.sifasuomax.view.MainFragment.UserInfoView;

import java.util.ArrayList;

import static com.xijiekou.sifasuomax.R.id.listView;

/**
 * Created by zhouzhuo on 2017/9/11.
 */

public class MainFragmentTwo extends BaseFragment implements SmoothListView.ISmoothListViewListener{
    private SmoothListView smoothListView;
    private UserInfoView userInfo;
    private MainFragmentViewOne fragmentViewOne;
    private FiterView fiterView;
    private View fiterView2;
    private FiterViewTwo fiterViewTwo;
    private RelativeLayout real_filterView;
    private View itemHeaderBannerView2; // 从ListView获取的广告子View
    private int titleViewHeight = 0; // 标题栏的高度
    private String url = UrlDomainUtil.urlHeader+"/index/news.html";



    private MainFragmentAdapterTwo adapter;
    //为0=有下一页，为1=最后一页
    private String isLastpage;
    private int currentPage = 1;
    private String content;
    private ArrayList<MainFragmentItemBeans.MainFragmentItemBean> totalList;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = View.inflate(getActivity(), R.layout.fragment_main2, null);
        initView(view);
        intLIstener();
        return view;
    }
    private boolean isScrollIdle = true; // ListView是否在滑动
    private int bannerViewTopMargin; // 广告视图距离顶部的距离
    private View itemHeaderBannerView; // 从ListView获取的广告子View
    private int bannerViewHeight = 180; // 广告视图的高度

    private boolean isStickyTop = false; // 是否吸附在顶部
    private int filterViewTopMargin;

    private void intLIstener() {
        smoothListView.setRefreshEnable(true);
        smoothListView.setLoadMoreEnable(true);
        smoothListView.setSmoothListViewListener(this);
        smoothListView.setOnScrollListener(new SmoothListView.OnSmoothScrollListener() {
            @Override
            public void onSmoothScrolling(View view) {
            }

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                isScrollIdle = (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE);
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (isScrollIdle && bannerViewTopMargin < 0) return;

                // 获取广告头部View、自身的高度、距离顶部的高度
                if (itemHeaderBannerView2 == null) {
                    itemHeaderBannerView2 = smoothListView.getChildAt(1);
                }
                if (itemHeaderBannerView2 != null) {
                    bannerViewTopMargin = (int) DensityUtils.px2dp(getActivity(), itemHeaderBannerView2.getTop());
                    bannerViewHeight = (int) DensityUtils.px2dp(getActivity(), itemHeaderBannerView2.getHeight());
                }

                // 获取筛选View、距离顶部的高度
                if (fiterView2 == null) {
                    fiterView2 = smoothListView.getChildAt(filterViewPosition - firstVisibleItem);
                }
                if (fiterView2 != null) {
                    filterViewTopMargin = (int) DensityUtils.px2dp(getActivity(), fiterView2.getTop());
                }
                Log.d("zhouzhuo","filterViewTopMargin=="+filterViewTopMargin);
                // 处理筛选是否吸附在顶部
                if (filterViewTopMargin <= titleViewHeight || firstVisibleItem > filterViewPosition) {
                    isStickyTop = true; // 吸附在顶部
                    real_filterView.setVisibility(View.VISIBLE);
                } else {
                    isStickyTop = false; // 没有吸附在顶部
                    real_filterView.setVisibility(View.GONE);
                }



            }
        });
    }

    private void initView(View view) {
        smoothListView = (SmoothListView) view.findViewById(listView);
        userInfo = new UserInfoView(getActivity());
        userInfo.fillView("userInfo",smoothListView);
        real_filterView = (RelativeLayout) view.findViewById(R.id.real_filterView);
        fragmentViewOne = new MainFragmentViewOne(getActivity());
        fragmentViewOne.fillView(url,smoothListView);
        fiterView = new FiterView(getActivity());
        fiterView.fillView("aa",smoothListView);
        /*fiterViewTwo = new FiterViewTwo(getActivity());
        fiterViewTwo.fillView("dd",smoothListView);*/

        requestData(null);
        filterViewPosition = smoothListView.getHeaderViewsCount() - 1;

    }

    private void requestData(Object o) {
        MainFragmentCaseListModel model = MainFragmentCaseListModel.getInstance();
        String id = (String) SharePreferenceUtils.get(getActivity(),"id","");
        String company = (String) SharePreferenceUtils.get(getActivity(),"company","");
        model.getList(id,company,content,currentPage);
        model.setCallBackListener(new BaseModel.CallBackListener<MainFragmentItemBeans>() {

            @Override
            public void success(MainFragmentItemBeans beans ) {
                if (beans!=null) {
                    if(currentPage == 1){
                        totalList = beans.list;
                    }else {
                        totalList.addAll(beans.list);
                    }
                    currentPage = beans.currentPage;
                    isLastpage = beans.isLastpage;
                    setAdapter();
                }else {
                    ToastUtils.showShort(getActivity(),"没有查找到相关数据");
                    if(totalList!=null){
                        totalList.clear();
                        setAdapter();
                    }
                }
            }

            @Override
            public void failed() {

            }
        });
    }

    private void setAdapter(){
        if(adapter == null){
            totalList.addAll(totalList);
            totalList.addAll(totalList);
            adapter = new MainFragmentAdapterTwo(getActivity(),totalList);
            smoothListView.setAdapter(adapter);
        }else {
            adapter.notifyDataSetChanged();
        }

    }

    private int filterViewPosition;

    @Override
    public void onClick(View view) {

    }

    public void updatePhoto() {
    }

    @Override
    public void onRefresh() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                smoothListView.stopRefresh();
                smoothListView.setRefreshTime("刚刚");
            }
        }, 2000);

    }

    @Override
    public void onLoadMore() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //loadMore();
                smoothListView.stopLoadMore();
            }
        }, 2000);
    }
}
